package com.ruoyi.web.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.openapi.controller.OpenDataController;

/**
 * OpenApi入口控制器
 *
 * @author yepanpan
 * @date 2023-07-10
 */
@RestController
@RequestMapping("/openapi")
public class ApiDataController extends OpenDataController {
}
