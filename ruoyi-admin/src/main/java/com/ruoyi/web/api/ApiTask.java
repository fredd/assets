package com.ruoyi.web.api;

import org.openapi.consts.ApiConst;
import org.openapi.service.IOpenApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * API相关的定时任务
 */
@Component("apiTask")
public class ApiTask {

    @Autowired
    private IOpenApiService openApiService;

    /**
     * 定时清理未下载的Excel文件
     */
    public void cleanExcel(){
        openApiService.cleanExcel(ApiConst.EXCEL_FOLDER);
    }
}
