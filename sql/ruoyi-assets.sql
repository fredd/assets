/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : ruoyi-assets

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 28/09/2023 16:20:48
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for api_access
-- ----------------------------
DROP TABLE IF EXISTS `api_access`;
CREATE TABLE `api_access`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色',
  `model_id` int(11) NOT NULL COMMENT '模型ID',
  `read` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '可读',
  `write` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '可写',
  `remove` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '可删',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限角色' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of api_access
-- ----------------------------
INSERT INTO `api_access` VALUES (3, '1', 1, 'Y', 'Y', NULL);
INSERT INTO `api_access` VALUES (4, '2', 1, 'Y', 'N', NULL);
INSERT INTO `api_access` VALUES (5, '1', 12, 'Y', 'Y', NULL);
INSERT INTO `api_access` VALUES (6, '2', 12, 'Y', 'N', NULL);
INSERT INTO `api_access` VALUES (11, '1', 15, 'Y', 'Y', NULL);
INSERT INTO `api_access` VALUES (12, '2', 15, 'Y', 'N', NULL);
INSERT INTO `api_access` VALUES (13, '1', 16, 'Y', 'Y', NULL);
INSERT INTO `api_access` VALUES (14, '2', 16, 'Y', 'N', NULL);
INSERT INTO `api_access` VALUES (15, '1', 22, 'Y', 'Y', NULL);
INSERT INTO `api_access` VALUES (16, '2', 22, 'Y', 'Y', NULL);
INSERT INTO `api_access` VALUES (17, '1', 23, 'Y', 'Y', NULL);
INSERT INTO `api_access` VALUES (18, '2', 23, 'Y', 'Y', NULL);
INSERT INTO `api_access` VALUES (19, '1', 24, 'Y', 'Y', NULL);
INSERT INTO `api_access` VALUES (20, '2', 24, 'Y', 'Y', NULL);
INSERT INTO `api_access` VALUES (33, '2', 27, 'Y', 'Y', NULL);
INSERT INTO `api_access` VALUES (34, '3', 27, 'Y', 'Y', NULL);
INSERT INTO `api_access` VALUES (35, '4', 27, 'Y', 'Y', NULL);
INSERT INTO `api_access` VALUES (36, '5', 27, 'Y', 'Y', NULL);
INSERT INTO `api_access` VALUES (37, '6', 27, 'Y', 'Y', NULL);
INSERT INTO `api_access` VALUES (38, '1', 28, 'Y', 'Y', NULL);
INSERT INTO `api_access` VALUES (39, '2', 28, 'Y', 'N', NULL);
INSERT INTO `api_access` VALUES (40, '3', 28, 'Y', 'N', NULL);
INSERT INTO `api_access` VALUES (41, '4', 28, 'Y', 'N', NULL);
INSERT INTO `api_access` VALUES (42, '5', 28, 'Y', 'N', NULL);
INSERT INTO `api_access` VALUES (43, '6', 28, 'Y', 'N', NULL);

-- ----------------------------
-- Table structure for api_event
-- ----------------------------
DROP TABLE IF EXISTS `api_event`;
CREATE TABLE `api_event`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL COMMENT '所属模型',
  `action` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '事件类型',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '模拟脚本',
  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否默认',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '状态',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '触发事件' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of api_event
-- ----------------------------
INSERT INTO `api_event` VALUES (1, 3, 'BU', 'function exec(data){\n  return [\"DELETE FROM api_access WHERE model_id=\"+data.id];\n}', 'Y', '1', NULL);
INSERT INTO `api_event` VALUES (2, 3, 'AD', 'function exec(ids){\n  return [\n   \"DELETE FROM api_access where model_id IN (\"+ids+\")\",\n   \"DELETE FROM api_event where model_id IN (\"+ids+\")\",\n  ];\n}', 'Y', '1', NULL);
INSERT INTO `api_event` VALUES (3, 16, 'AU', 'function exec(data){\n  if(data.status != \'2\' && data.user_id){\n    return \"UPDATE asset_info SET user_id = null WHERE id=\"+data.id; \n  }\n  return \"\";\n}', 'N', '1', '非在用状态的设备，user_id为空');
INSERT INTO `api_event` VALUES (4, 17, 'BU', 'function exec(data){\n  return [\"DELETE FROM asset_purchase_detail WHERE purchase_id=\" + data.id];\n}', 'N', '1', '更新前，先删除采购明细，然后全量保存，前端使用patch方法');
INSERT INTO `api_event` VALUES (5, 26, 'AU', 'function exec(data){\n  if(data.check_status != \'1\'){\n    return \"UPDATE flow_step SET ignore_flag=\'1\' WHERE check_status=\'1\' AND task_id=\"+data.task_id +\" AND pid=\"+data.pid;\n  }\n  return null;\n}', 'N', '1', '审批操作之后，更新同一批次的其它审核人信息为忽略');
INSERT INTO `api_event` VALUES (6, 23, 'AI', 'function exec(data){\n  return [\"insert into asset_stock_detail(id,stock_id, asset_id, num, status, price1, price, house_id, user_id, cate_name, model_name, stock_time) select null, \"+data.id+\", t.id, t.num,  null, t.price, null, t.house_id, t.user_id, c.name, m.name, null from asset_info t join asset_category c on c.id = t.cate_id join asset_model m on m.id = t.model_id where t.status in (\'1\',\'2\',\'3\',\'4\')\"];\n}', 'N', '1', '新增盘点任务后，将资产信息加入进来，方便后期有针对性的操作');
INSERT INTO `api_event` VALUES (7, 23, 'AU', 'function exec(data){\n  if(data.status != \'4\'){\n    return null;\n  }\n  return [\n    \"update asset_info t join asset_stock_detail d on d.asset_id = t.id and d.stock_id=\" +  \n       data.id + \" set t.status=d.status,t.user_id = d.user_id, t.price = d.price\", \n    \"update asset_stock s join (select count(id) c, sum(price) w from asset_stock_detail  where stock_id=\"+data.id+\" and status in (\'1\',\'2\',\'3\', \'4\')) t set s.asset_count = t.c, s.asset_worth=t.w where s.id=\"+data.id,\n    \"update asset_stock s join (select count(id) c, sum(price) w from asset_stock_detail  where stock_id=\"+data.id+\" and status = \'5\') t set s.scrap_count = t.c, s.scrap_worth=t.w where s.id=\"+data.id,\n    \"update asset_stock s join (select count(id) c, sum(price) w from asset_stock_detail  where stock_id=\"+data.id+\" and status = \'6\') t set s.sale_worth = t.c, s.scrap_worth=t.w where s.id=\"+data.id\n];\n}', 'N', '1', '在确认盘点之后，更新资产的状态为盘点状态，并计算盘点概况');
INSERT INTO `api_event` VALUES (8, 19, 'AI', 'function exec(data){\n  if(data.type == \'1\'){\n  return [];\n  }else{\n  return \"UPDATE asset_use SET revert_time = now() WHERE revert_time is null and num=\'\"+data.num+\"\'\";\n  }\n}', 'N', '2', '采购入库后，需要更新采购明细；归还入库的需要将原使用申请的归还时间设置为当前');
INSERT INTO `api_event` VALUES (9, 14, 'DC', 'function exec(data){\n  return \"SELECT count(id) from asset_info where house_id IN (\"+data+\")\";\n}', 'N', '1', '有资产的仓库信息不能删除');
INSERT INTO `api_event` VALUES (10, 1, 'DC', 'function exec(data){\n  return \"SELECT count(id) total FROM asset_info WHERE model_id in (\"+data+\")\";\n}', 'N', '1', '有设备的型号不能删除');
INSERT INTO `api_event` VALUES (11, 13, 'DC', 'function exec(data){\n  return \"select (SELECT count(id) total FROM asset_info WHERE cate_id IN (\"+data+\") ) + (SELECT count(id) total FROM asset_model WHERE cate_id IN (\"+data+\") )\";\n}', 'N', '1', '有型号和资产的资产分类不能删除');
INSERT INTO `api_event` VALUES (12, 22, 'DC', 'function exec(data){\n  return \"SELECT count(id) FROM asset_use where check_status=\'2\' AND id IN(\"+data+\")\";\n}', 'N', '1', '已审核通过的使用申请不能删除');
INSERT INTO `api_event` VALUES (13, 21, 'DC', 'function exec(data){\n  return \"SELECT count(id) FROM asset_maintain where check_status=\'2\' AND id IN(\"+data+\")\";\n}', 'N', '2', '已审核通过的不能删除');
INSERT INTO `api_event` VALUES (14, 27, 'DC', 'function exec(data){\n  return \"SELECT count(id) FROM asset_sale where is_out=\'Y\' AND id IN(\"+data+\")\";\n}', 'N', '1', '已出库的不能删除');

-- ----------------------------
-- Table structure for api_exchange
-- ----------------------------
DROP TABLE IF EXISTS `api_exchange`;
CREATE TABLE `api_exchange`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uri` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接口地址',
  `mode` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'R' COMMENT '模式：读|写',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `read` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '读操作',
  `write` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '写操作',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '说明',
  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否默认',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '数据交换' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of api_exchange
-- ----------------------------

-- ----------------------------
-- Table structure for api_field
-- ----------------------------
DROP TABLE IF EXISTS `api_field`;
CREATE TABLE `api_field`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL COMMENT '模型',
  `table_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '表名',
  `type` int(11) NOT NULL COMMENT '类型',
  `code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '代码',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `max_length` int(11) NOT NULL DEFAULT 0 COMMENT '最小长度',
  `min_length` int(11) NOT NULL DEFAULT 0 COMMENT '最大长度',
  `scale` tinyint(4) NOT NULL DEFAULT 0 COMMENT '小数点位',
  `nullable` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N' COMMENT '为空',
  `rules` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '验证规则',
  `defaults` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '默认值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 562 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '模型字段' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of api_field
-- ----------------------------
INSERT INTO `api_field` VALUES (294, 1, 'api_model', 4, 'id', '', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (295, 1, 'api_model', 12, 'group', '分组', 50, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (296, 1, 'api_model', 12, 'code', '代码', 100, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (297, 1, 'api_model', 12, 'name', '名称', 50, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (298, 1, 'api_model', 12, 'pk_field', '主键代码', 50, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (299, 1, 'api_model', 12, 'unique_field', '唯一字段', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (300, 1, 'api_model', 12, 'org_scope', '组织权限', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (301, 1, 'api_model', 12, 'user_scope', '用户权限', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (302, 1, 'api_model', -1, 'excels', '导入导出配置', 65535, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (303, 1, 'api_model', 1, 'status', '状态', 1, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (304, 1, 'api_model', 1, 'is_default', '是否内置', 1, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (305, 1, 'api_model', 12, 'remark', '备注详细', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (306, 2, 'api_event', 4, 'id', '', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (307, 2, 'api_event', 4, 'model_id', '所属模型', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (308, 2, 'api_event', 1, 'action', '事件类型', 2, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (309, 2, 'api_event', -1, 'content', '模拟脚本', 65535, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (310, 2, 'api_event', 1, 'is_default', '是否默认', 1, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (311, 2, 'api_event', 1, 'status', '状态', 1, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (312, 2, 'api_event', 12, 'remark', '备注', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (313, 3, 'api_access', 4, 'id', '', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (314, 3, 'api_access', 12, 'role_id', '角色', 100, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (315, 3, 'api_access', 4, 'model_id', '模型ID', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (316, 3, 'api_access', 1, 'read', '可读', 1, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (317, 3, 'api_access', 1, 'write', '可写', 1, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (318, 3, 'api_access', 1, 'remove', '可删', 1, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (319, 5, 'api_log', 4, 'id', '', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (320, 5, 'api_log', 4, 'model_id', '数据模型', 10, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (321, 5, 'api_log', 1, 'log_type', '日志类型', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (322, 5, 'api_log', 12, 'log_key', '数据主键', 50, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (323, 5, 'api_log', 12, 'log_data', '详细数据', 500, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (324, 5, 'api_log', 93, 'log_time', '记录时间', 19, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (325, 5, 'api_log', 12, 'user_id', '操作人ID', 50, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (326, 5, 'api_log', 12, 'user_name', '操作人姓名', 50, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (327, 6, 'api_sql', 4, 'id', '', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (328, 6, 'api_sql', 12, 'group', '分组', 50, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (329, 6, 'api_sql', 1, 'type', '类型:table列表，count计算，chart报表，inner子系统，batch数据批处理', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (330, 6, 'api_sql', 12, 'code', '代码', 50, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (331, 6, 'api_sql', 12, 'name', '名称', 20, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (332, 6, 'api_sql', -1, 'content', 'SQL脚本', 65535, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (333, 6, 'api_sql', 1, 'is_default', '是否内置', 1, 0, 0, 'Y', '', 'N');
INSERT INTO `api_field` VALUES (334, 6, 'api_sql', 12, 'remark', '说明', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (335, 7, 'sys_role', -5, 'role_id', '角色ID', 19, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (336, 7, 'sys_role', 12, 'role_name', '角色名称', 30, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (337, 7, 'sys_role', 12, 'role_key', '角色权限字符串', 100, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (338, 7, 'sys_role', 4, 'role_sort', '显示顺序', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (339, 7, 'sys_role', 1, 'data_scope', '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）', 1, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (340, 7, 'sys_role', -7, 'menu_check_strictly', '菜单树选择项是否关联显示', 1, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (341, 7, 'sys_role', -7, 'dept_check_strictly', '部门树选择项是否关联显示', 1, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (342, 7, 'sys_role', 1, 'status', '角色状态（0正常 1停用）', 1, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (343, 7, 'sys_role', 1, 'del_flag', '删除标志（0代表存在 2代表删除）', 1, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (344, 7, 'sys_role', 12, 'create_by', '创建者', 64, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (345, 7, 'sys_role', 93, 'create_time', '创建时间', 19, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (346, 7, 'sys_role', 12, 'update_by', '更新者', 64, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (347, 7, 'sys_role', 93, 'update_time', '更新时间', 19, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (348, 7, 'sys_role', 12, 'remark', '备注', 500, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (349, 13, 'asset_category', 4, 'id', '自增长主键ID', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (350, 13, 'asset_category', 4, 'pid', '上级', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (351, 13, 'asset_category', 12, 'ancestors', '树形路径', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (352, 13, 'asset_category', 12, 'name', '分类名称', 50, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (353, 13, 'asset_category', 12, 'nums', '编码规则', 50, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (354, 13, 'asset_category', 4, 'list_sort', '排序', 10, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (355, 13, 'asset_category', 1, 'status', '状态', 1, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (356, 14, 'asset_warehouse', 4, 'id', '自增长主键ID', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (357, 14, 'asset_warehouse', 12, 'name', '仓库名称', 50, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (358, 14, 'asset_warehouse', 12, 'address', '仓库地址', 200, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (359, 14, 'asset_warehouse', 4, 'user_id', '负责人', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (360, 14, 'asset_warehouse', 12, 'links', '联系方式', 50, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (361, 14, 'asset_warehouse', 1, 'status', '状态', 1, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (362, 14, 'asset_warehouse', 12, 'remark', '仓库说明', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (363, 15, 'asset_model', 4, 'id', '自增长主键ID', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (364, 15, 'asset_model', 4, 'cate_id', '资产分类', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (365, 15, 'asset_model', 12, 'name', '型号名称', 50, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (366, 15, 'asset_model', 12, 'factory', '厂商', 50, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (367, 15, 'asset_model', 12, 'param1', '预留字段1', 200, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (368, 15, 'asset_model', 12, 'param2', '预留字段2', 200, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (369, 15, 'asset_model', 12, 'param3', '预留字段3', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (370, 15, 'asset_model', 12, 'param4', '预留字段4', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (371, 15, 'asset_model', 12, 'param5', '预留字段5', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (372, 15, 'asset_model', 4, 'life_time', '使用寿命', 10, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (373, 15, 'asset_model', 3, 'price', '采购价', 10, 0, 2, 'Y', '', '');
INSERT INTO `api_field` VALUES (374, 15, 'asset_model', 12, 'params', '参数', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (375, 15, 'asset_model', 12, 'unit', '计量单位', 10, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (376, 15, 'asset_model', 3, 'price1', '市场价', 10, 0, 2, 'Y', '', '');
INSERT INTO `api_field` VALUES (377, 15, 'asset_model', 12, 'imgs', '图例', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (378, 15, 'asset_model', 1, 'status', '状态', 1, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (379, 16, 'asset_info', 4, 'id', '自增长主键ID', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (380, 16, 'asset_info', 4, 'cate_id', '资产分类', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (381, 16, 'asset_info', 4, 'model_id', '型号', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (382, 16, 'asset_info', 12, 'num', '资产编号', 200, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (383, 16, 'asset_info', 12, 'param1', '预留字段1', 200, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (384, 16, 'asset_info', 12, 'param2', '预留字段2', 200, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (385, 16, 'asset_info', 12, 'param3', '预留字段3', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (386, 16, 'asset_info', 12, 'param4', '预留字段4', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (387, 16, 'asset_info', 12, 'param5', '预留字段5', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (388, 16, 'asset_info', 3, 'price', '当前价值', 10, 0, 2, 'Y', '', '');
INSERT INTO `api_field` VALUES (389, 16, 'asset_info', 4, 'house_id', '仓库', 10, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (390, 16, 'asset_info', 12, 'imgs', '图例', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (391, 16, 'asset_info', 1, 'is_it', '是否IT设备', 1, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (392, 16, 'asset_info', 4, 'pid', '上级设备', 10, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (393, 16, 'asset_info', 12, 'comment', '资产说明', 200, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (394, 16, 'asset_info', 1, 'status', '资产状态', 1, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (395, 16, 'asset_info', 4, 'dept_id', '所属部门', 10, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (396, 16, 'asset_info', 4, 'user_id', '使用人', 10, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (397, 16, 'asset_info', 91, 'buy_time', '购买时间', 10, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (398, 16, 'asset_info', 91, 'scrap_time', '报废时间', 10, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (399, 17, 'asset_purchase', 4, 'id', '自增长主键ID', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (400, 17, 'asset_purchase', 12, 'num', '采购编号', 50, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (401, 17, 'asset_purchase', 12, 'reason', '采购原由', 255, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (402, 17, 'asset_purchase', 12, 'comment', '采购说明', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (403, 17, 'asset_purchase', 3, 'price', '总价值', 10, 0, 2, 'Y', '', '0');
INSERT INTO `api_field` VALUES (404, 17, 'asset_purchase', 1, 'check_status', '审核结果', 1, 0, 0, 'Y', '', '1');
INSERT INTO `api_field` VALUES (405, 17, 'asset_purchase', 4, 'dept_id', '申请部门', 10, 0, 0, 'N', '', '@orgId()');
INSERT INTO `api_field` VALUES (406, 17, 'asset_purchase', 4, 'user_id', '申请人', 10, 0, 0, 'N', '', '@userId()');
INSERT INTO `api_field` VALUES (407, 17, 'asset_purchase', 93, 'finish_time', '完成时间', 19, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (408, 17, 'asset_purchase', 93, 'apply_time', '申请时间', 19, 0, 0, 'Y', '', '@now()');
INSERT INTO `api_field` VALUES (409, 17, 'asset_purchase', 1, 'apply_status', '申请状态', 1, 0, 0, 'Y', '', '1');
INSERT INTO `api_field` VALUES (410, 18, 'asset_purchase_detail', 4, 'id', '自增长主键ID', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (411, 18, 'asset_purchase_detail', 4, 'purchase_id', '采购单', 10, 0, 0, 'Y', '', '@asset_purchase.id');
INSERT INTO `api_field` VALUES (412, 18, 'asset_purchase_detail', 4, 'cate_id', '资产分类', 10, 0, 0, 'Y', '', '0');
INSERT INTO `api_field` VALUES (413, 18, 'asset_purchase_detail', 4, 'model_id', '型号', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (414, 18, 'asset_purchase_detail', 3, 'amount', '购买数量', 10, 0, 3, 'N', '', '');
INSERT INTO `api_field` VALUES (415, 18, 'asset_purchase_detail', 3, 'ins', '入库数量', 10, 0, 3, 'Y', '', '');
INSERT INTO `api_field` VALUES (416, 18, 'asset_purchase_detail', 12, 'nums', '入库编码', 500, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (417, 19, 'asset_in', 4, 'id', '自增长主键ID', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (418, 19, 'asset_in', 1, 'type', '入库类别', 1, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (419, 19, 'asset_in', 4, 'cate_id', '资产分类', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (420, 19, 'asset_in', 12, 'num', '资产编号', 500, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (421, 19, 'asset_in', 4, 'house_id', '仓库', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (422, 19, 'asset_in', 12, 'remark', '入库说明', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (423, 19, 'asset_in', 4, 'user_id', '接收人', 10, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (424, 19, 'asset_in', 93, 'recv_time', '接收时间', 19, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (425, 19, 'asset_in', 12, 'imgs', '收货单', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (426, 20, 'asset_out', 4, 'id', '自增长主键ID', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (427, 20, 'asset_out', 1, 'type', '出库类别', 1, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (428, 20, 'asset_out', 4, 'house_id', '仓库', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (429, 20, 'asset_out', 4, 'cate_id', '资产分类', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (430, 20, 'asset_out', 4, 'asset_id', '资产', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (431, 20, 'asset_out', 12, 'num', '资产编号', 50, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (432, 20, 'asset_out', 4, 'user_id', '接收人', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (433, 20, 'asset_out', 12, 'remark', '出库说明', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (434, 20, 'asset_out', 12, 'imgs', '确认单', 500, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (435, 20, 'asset_out', 93, 'out_time', '出库时间', 19, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (436, 21, 'asset_maintain', 4, 'id', '自增长主键ID', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (438, 21, 'asset_maintain', 4, 'asset_id', '资产', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (439, 21, 'asset_maintain', 1, 'type', '维保类型', 1, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (440, 21, 'asset_maintain', 12, 'reason', '维保原由', 255, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (441, 21, 'asset_maintain', 1, 'status', '维保状态', 1, 0, 0, 'Y', '', '1');
INSERT INTO `api_field` VALUES (442, 21, 'asset_maintain', 3, 'money', '费用', 10, 0, 2, 'Y', '', '');
INSERT INTO `api_field` VALUES (443, 21, 'asset_maintain', 12, 'comment', '说明', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (444, 21, 'asset_maintain', 12, 'imgs', '图片', 500, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (445, 21, 'asset_maintain', 4, 'user_id', '发起人', 10, 0, 0, 'N', '', '@userId()');
INSERT INTO `api_field` VALUES (446, 21, 'asset_maintain', 93, 'apply_time', '发起时间', 19, 0, 0, 'Y', '', '@now()');
INSERT INTO `api_field` VALUES (447, 21, 'asset_maintain', 93, 'finish_time', '完成时间', 19, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (448, 22, 'asset_use', 4, 'id', '自增长主键ID', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (449, 22, 'asset_use', 4, 'model_id', '型号', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (450, 22, 'asset_use', -6, 'amount', '数量', 3, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (451, 22, 'asset_use', 4, 'dept_id', '申请部门', 10, 0, 0, 'N', '', '@orgId()');
INSERT INTO `api_field` VALUES (452, 22, 'asset_use', 4, 'user_id', '申请人', 10, 0, 0, 'N', '', '@userId()');
INSERT INTO `api_field` VALUES (453, 22, 'asset_use', 12, 'reason', '申请原由', 200, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (454, 22, 'asset_use', 12, 'remark', '申请说明', 200, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (455, 22, 'asset_use', 1, 'check_status', '审核结果', 1, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (456, 22, 'asset_use', 12, 'nums', '资产编号', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (457, 22, 'asset_use', 93, 'recv_time', '接收时间', 19, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (458, 22, 'asset_use', 93, 'apply_time', '申请时间', 19, 0, 0, 'N', '', '@now()');
INSERT INTO `api_field` VALUES (459, 22, 'asset_use', 1, 'apply_status', '申请状态', 1, 0, 0, 'N', '', '1');
INSERT INTO `api_field` VALUES (460, 23, 'asset_stock', 4, 'id', '自增长主键ID', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (461, 23, 'asset_stock', 12, 'name', '代号', 20, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (462, 23, 'asset_stock', 4, 'asset_count', '资产总数', 10, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (463, 23, 'asset_stock', 3, 'asset_worth', '资产总值', 10, 0, 2, 'Y', '', '');
INSERT INTO `api_field` VALUES (464, 23, 'asset_stock', 4, 'scrap_count', '报废总数', 10, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (465, 23, 'asset_stock', 4, 'scrap_worth', '报废总值', 10, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (466, 23, 'asset_stock', 3, 'sale_worth', '出售总值', 10, 0, 2, 'Y', '', '');
INSERT INTO `api_field` VALUES (467, 23, 'asset_stock', 4, 'sale_count', '出售数量', 10, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (468, 23, 'asset_stock', -1, 'remark', '盘点说明', 65535, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (469, 23, 'asset_stock', 93, 'start_time', '开始时间', 19, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (470, 23, 'asset_stock', 93, 'end_time', '结束时间', 19, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (471, 23, 'asset_stock', 4, 'user_id', '负责人', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (472, 23, 'asset_stock', 1, 'status', '状态', 1, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (473, 24, 'asset_stock_detail', 4, 'id', '自增长主键ID', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (474, 24, 'asset_stock_detail', 12, 'stock_id', '盘点主键', 20, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (475, 24, 'asset_stock_detail', 4, 'asset_id', '资产ID', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (476, 24, 'asset_stock_detail', 1, 'status', '当前状态', 1, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (477, 24, 'asset_stock_detail', 3, 'price', '当前价值', 10, 0, 2, 'Y', '', '');
INSERT INTO `api_field` VALUES (478, 24, 'asset_stock_detail', 12, 'cate_name', '资产分类', 50, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (479, 24, 'asset_stock_detail', 12, 'model_name', '资产型号', 50, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (480, 24, 'asset_stock_detail', 12, 'num', '资产编码', 50, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (481, 24, 'asset_stock_detail', 3, 'price1', '原价值', 10, 0, 2, 'Y', '', '');
INSERT INTO `api_field` VALUES (482, 24, 'asset_stock_detail', 4, 'house_id', '所在仓库', 10, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (483, 24, 'asset_stock_detail', 4, 'user_id', '使用人', 10, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (484, 24, 'asset_stock_detail', 93, 'stock_time', '盘点时间', 19, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (485, 25, 'flow_task', 4, 'id', '自增长主键ID', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (486, 25, 'flow_task', 12, 'type', '类型', 20, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (487, 25, 'flow_task', 4, 'target_id', '业务主键', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (488, 25, 'flow_task', 12, 'num', '业务编号', 100, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (489, 25, 'flow_task', 12, 'title', '业务名称', 200, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (490, 25, 'flow_task', 4, 'user_id', '申请人', 10, 0, 0, 'Y', '', '@userId()');
INSERT INTO `api_field` VALUES (491, 25, 'flow_task', 12, 'user_name', '申请人姓名', 50, 0, 0, 'Y', '', '@userName()');
INSERT INTO `api_field` VALUES (492, 25, 'flow_task', 1, 'check_status', '审核结果', 1, 0, 0, 'Y', '', '1');
INSERT INTO `api_field` VALUES (493, 25, 'flow_task', 93, 'apply_time', '申请时间', 19, 0, 0, 'Y', '', '@now()');
INSERT INTO `api_field` VALUES (494, 25, 'flow_task', 12, 'apply_reason', '申请原因', 200, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (495, 26, 'flow_step', 4, 'id', '自增长主键ID', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (496, 26, 'flow_step', 4, 'task_id', '业务主键', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (497, 26, 'flow_step', 4, 'check_user_id', '审核人', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (498, 26, 'flow_step', 12, 'check_user_name', '审核人姓名', 50, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (499, 26, 'flow_step', 1, 'check_status', '审核结果', 1, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (500, 26, 'flow_step', 93, 'check_time', '审核时间', 19, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (501, 26, 'flow_step', 12, 'check_reason', '审核不通过原因', 200, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (502, 26, 'flow_step', 4, 'pid', '上一步骤', 10, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (503, 26, 'flow_step', 1, 'ignore_flag', '忽略标识', 1, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (504, 27, 'asset_sale', 4, 'id', '自增长主键ID', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (505, 27, 'asset_sale', 12, 'cate_id', '分类', 50, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (506, 27, 'asset_sale', 4, 'asset_id', '资产', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (507, 27, 'asset_sale', 12, 'asset_num', '资产编码', 50, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (508, 27, 'asset_sale', 4, 'dept_id', '归属部门', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (509, 27, 'asset_sale', 12, 'dept_name', '部门名称', 500, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (510, 27, 'asset_sale', 4, 'user_id', '操作人', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (511, 27, 'asset_sale', 12, 'user_name', '操作人姓名', 50, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (512, 27, 'asset_sale', 3, 'price', '价格', 10, 0, 2, 'N', '', '');
INSERT INTO `api_field` VALUES (513, 27, 'asset_sale', 12, 'reason', '出售原由', 200, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (514, 27, 'asset_sale', 93, 'sale_time', '出售时间', 19, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (515, 27, 'asset_sale', 1, 'is_out', '已出库', 1, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (516, 28, 'sys_chart', 4, 'id', '参数主键', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (517, 28, 'sys_chart', 12, 'type', '类型', 50, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (518, 28, 'sys_chart', 12, 'name', '名称', 100, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (519, 28, 'sys_chart', 12, 'ds_code', '数据源', 50, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (520, 28, 'sys_chart', -1, 'data_option', '数据解析', 65535, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (521, 28, 'sys_chart', 12, 'search_title', '搜索标题', 20, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (522, 28, 'sys_chart', 12, 'search_field', '搜索字段', 50, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (523, 28, 'sys_chart', 1, 'search_type', '搜索类型', 10, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (524, 28, 'sys_chart', 12, 'search_ds', '搜索来源', 50, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (525, 28, 'sys_chart', 1, 'status', '状态', 1, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (526, 29, 'dev_group', 4, 'id', '主键', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (527, 29, 'dev_group', 12, 'code', '分组代码', 50, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (528, 29, 'dev_group', 12, 'name', '分组名称', 255, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (529, 29, 'dev_group', 12, 'icon', '分组图标', 50, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (530, 29, 'dev_group', 4, 'sorts', '排序', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (531, 30, 'dev_app', 4, 'id', '主键', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (532, 30, 'dev_app', 4, 'group_id', '分组', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (533, 30, 'dev_app', 12, 'type', '类型', 50, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (534, 30, 'dev_app', 12, 'name', '标题', 255, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (535, 30, 'dev_app', 12, 'icon', '图标', 50, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (536, 30, 'dev_app', 4, 'sorts', '排序', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (537, 30, 'dev_app', 12, 'permission', '权限', 100, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (538, 30, 'dev_app', 12, 'api', '接口', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (539, 31, 'dev_form', 4, 'id', '主键', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (540, 31, 'dev_form', 4, 'app_id', '应用', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (541, 31, 'dev_form', 1, 'type', '类型', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (542, 31, 'dev_form', 12, 'field', '字段', 50, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (543, 31, 'dev_form', 12, 'label', '标签', 255, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (544, 31, 'dev_form', 1, 'required', '必填', 1, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (545, 31, 'dev_form', 12, 'ds', '数据源', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (546, 31, 'dev_form', 1, 'mode', '模式', 1, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (547, 31, 'dev_form', 12, 'defaults', '默认值', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (548, 31, 'dev_form', 4, 'sorts', '排序', 10, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (555, 30, 'dev_app', 12, 'code', '代码', 50, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (556, 30, 'dev_app', 12, 'links', '目标连接', 255, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (557, 30, 'dev_app', 1, 'show_top', '桌面展示', 1, 0, 0, 'N', '', '');
INSERT INTO `api_field` VALUES (558, 30, 'dev_app', 12, 'title_field', '分组', 50, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (559, 30, 'dev_app', 12, 'right_field', '类型', 50, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (560, 30, 'dev_app', 12, 'note_field', '标题', 50, 0, 0, 'Y', '', '');
INSERT INTO `api_field` VALUES (561, 30, 'dev_app', 12, 'img_field', '图标', 50, 0, 0, 'Y', '', '');

-- ----------------------------
-- Table structure for api_log
-- ----------------------------
DROP TABLE IF EXISTS `api_log`;
CREATE TABLE `api_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NULL DEFAULT NULL COMMENT '数据模型',
  `log_type` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '日志类型',
  `log_key` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据主键',
  `log_data` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详细数据',
  `log_time` datetime NOT NULL COMMENT '记录时间',
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '操作人ID',
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '操作人姓名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1043 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '接口日志' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of api_log
-- ----------------------------
INSERT INTO `api_log` VALUES (1, 3, 'update', '1', '{\"code\":\"data_student\",\"name\":\"学生信息\",\"id\":\"1\",\"group\":\"基础\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"status\":\"1\",\"excels\":\"{\\n\\\"data_student\\\":{\\n    \\\"@alias\\\":\\\"s\\\",\\n    \\\"num\\\":\\\"学号\\\",\\n    \\\"name\\\":\\\"姓名\\\",\\n    \\\"sex\\\":\\\"性别\\\",\\n    \\\"class_name\\\":\\\"班级\\\"\\n},\\n\\\"data_student_parent\\\":{\\n    \\\"@column\\\":\\\"name:parent_name,type,phone\\\",\\n    \\\"@alias\\\":\\\"p\\\",\\n    \\\"tye\\\":\\\"关系\\\",\\n    \\\"parent_name\\\":\\\"家长姓名\\\",\\n    \\\"phone\\\":\\\"家长联系方式\\\",\\n    \\\"@join\\\":\\\"LEFT ON s.id = p.student_id\\\"\\n}\\n}\"}', '2023-08-19 14:16:47', '1', '若依');
INSERT INTO `api_log` VALUES (2, 5, 'insert', '1', '{\"read\":\"Y\",\"role_id\":\"1\",\"model_id\":\"1\",\"id\":1,\"write\":\"Y\"}', '2023-08-19 14:16:52', '1', '若依');
INSERT INTO `api_log` VALUES (3, 5, 'insert', '2', '{\"read\":\"Y\",\"role_id\":\"2\",\"model_id\":\"1\",\"id\":2,\"write\":\"Y\"}', '2023-08-19 14:16:53', '1', '若依');
INSERT INTO `api_log` VALUES (4, 3, 'update', '1', '{\"code\":\"data_student\",\"name\":\"学生信息\",\"id\":\"1\",\"group\":\"基础\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"status\":\"1\",\"excels\":\"{\\n\\\"data_student\\\":{\\n    \\\"@alias\\\":\\\"s\\\",\\n    \\\"num\\\":\\\"学号\\\",\\n    \\\"name\\\":\\\"姓名\\\",\\n    \\\"sex\\\":\\\"性别\\\",\\n    \\\"class_name\\\":\\\"班级\\\"\\n},\\n\\\"data_student_parent\\\":{\\n    \\\"@column\\\":\\\"name:parent_name,type,phone\\\",\\n    \\\"@alias\\\":\\\"p\\\",\\n    \\\"tye\\\":\\\"关系\\\",\\n    \\\"parent_name\\\":\\\"家长姓名\\\",\\n    \\\"phone\\\":\\\"家长联系方式\\\",\\n    \\\"@join\\\":\\\"LEFT ON s.id = p.student_id\\\"\\n}\\n}\"}', '2023-08-19 14:18:14', '1', '若依');
INSERT INTO `api_log` VALUES (5, 3, 'update', '1', '{\"code\":\"data_student\",\"name\":\"学生信息\",\"id\":\"1\",\"group\":\"基础\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"status\":\"1\",\"excels\":\"{\\\"data_student\\\":{    \\\"@alias\\\":\\\"s\\\",    \\\"num\\\":\\\"学号\\\",    \\\"name\\\":\\\"姓名\\\",    \\\"sex\\\":\\\"性别\\\",    \\\"class_name\\\":\\\"班级\\\"},\\\"data_student_parent\\\":{    \\\"@column\\\":\\\"name:parent_name,type,phone\\\",    \\\"@alias\\\":\\\"p\\\",    \\\"tye\\\":\\\"关系\\\",    \\\"parent_name\\\":\\\"家长姓名\\\",    \\\"phone\\\":\\\"家长联系方式\\\",    \\\"@join\\\":\\\"LEFT ON s.id = p.student_id\\\"}}\"}', '2023-08-19 14:25:54', '1', '若依');
INSERT INTO `api_log` VALUES (6, 3, 'update', '1', '{\"code\":\"data_student\",\"name\":\"学生信息\",\"id\":\"1\",\"group\":\"基础\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"status\":\"1\",\"excels\":\"{\\\"data_student\\\":{    \\\"@alias\\\":\\\"s\\\",    \\\"num\\\":\\\"学号\\\",    \\\"name\\\":\\\"姓名\\\",    \\\"sex\\\":\\\"性别\\\",    \\\"class_name\\\":\\\"班级\\\"},\\\"data_student_parent\\\":{    \\\"@column\\\":\\\"name:parent_name,type,phone\\\",    \\\"@alias\\\":\\\"p\\\",    \\\"tye\\\":\\\"关系\\\",    \\\"parent_name\\\":\\\"家长姓名\\\",    \\\"phone\\\":\\\"家长联系方式\\\",    \\\"@join\\\":\\\"LEFT ON s.id = p.student_id\\\"}}\"}', '2023-08-19 14:26:01', '1', '若依');
INSERT INTO `api_log` VALUES (7, 3, 'update', '1', '{\"code\":\"data_student\",\"name\":\"学生信息\",\"id\":\"1\",\"group\":\"基础\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"status\":\"1\",\"excels\":\"{\\\"data_student\\\":{    \\\"@alias\\\":\\\"s\\\",    \\\"num\\\":\\\"学号\\\",    \\\"name\\\":\\\"姓名\\\",    \\\"sex\\\":\\\"性别\\\",    \\\"class_name\\\":\\\"班级\\\"},\\\"data_student_parent\\\":{    \\\"@column\\\":\\\"name:parent_name,type,phone\\\",    \\\"@alias\\\":\\\"p\\\",    \\\"tye\\\":\\\"关系\\\",    \\\"parent_name\\\":\\\"家长姓名\\\",    \\\"phone\\\":\\\"家长联系方式\\\",    \\\"@join\\\":\\\"LEFT ON s.id = p.student_id\\\"}}\"}', '2023-08-19 14:27:41', '1', '若依');
INSERT INTO `api_log` VALUES (8, 3, 'update', '1', '{\"code\":\"data_student\",\"name\":\"学生信息\",\"id\":\"1\",\"group\":\"基础\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"status\":\"1\",\"excels\":\"{\\\"data_student\\\":{    \\\"@alias\\\":\\\"s\\\",    \\\"num\\\":\\\"学号\\\",    \\\"name\\\":\\\"姓名\\\",    \\\"sex\\\":\\\"性别\\\",    \\\"class_name\\\":\\\"班级\\\"},\\\"data_student_parent\\\":{    \\\"@column\\\":\\\"name:parent_name,type,phone\\\",    \\\"@alias\\\":\\\"p\\\",    \\\"tye\\\":\\\"关系\\\",    \\\"parent_name\\\":\\\"家长姓名\\\",    \\\"phone\\\":\\\"家长联系方式\\\",    \\\"@join\\\":\\\"LEFT ON s.id = p.student_id\\\"}}\"}', '2023-08-19 14:29:08', '1', '若依');
INSERT INTO `api_log` VALUES (9, 3, 'event', '1', '{\"code\":\"data_student\",\"name\":\"学生信息\",\"id\":\"1\",\"group\":\"基础\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"status\":\"1\",\"excels\":\"{\\\"data_student\\\":{    \\\"@alias\\\":\\\"s\\\",    \\\"num\\\":\\\"学号\\\",    \\\"name\\\":\\\"姓名\\\",    \\\"sex\\\":\\\"性别\\\",    \\\"class_name\\\":\\\"班级\\\"},\\\"data_student_parent\\\":{    \\\"@column\\\":\\\"name:parent_name,type,phone\\\",    \\\"@alias\\\":\\\"p\\\",    \\\"tye\\\":\\\"关系\\\",    \\\"parent_name\\\":\\\"家长姓名\\\",    \\\"phone\\\":\\\"家长联系方式\\\",    \\\"@join\\\":\\\"LEFT ON s.id = p.student_id\\\"}}\"}', '2023-08-19 14:30:33', '1', '若依');
INSERT INTO `api_log` VALUES (10, 3, 'update', '1', '{\"code\":\"data_student\",\"name\":\"学生信息\",\"id\":\"1\",\"group\":\"基础\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"status\":\"1\",\"excels\":\"{\\\"data_student\\\":{    \\\"@alias\\\":\\\"s\\\",    \\\"num\\\":\\\"学号\\\",    \\\"name\\\":\\\"姓名\\\",    \\\"sex\\\":\\\"性别\\\",    \\\"class_name\\\":\\\"班级\\\"},\\\"data_student_parent\\\":{    \\\"@column\\\":\\\"name:parent_name,type,phone\\\",    \\\"@alias\\\":\\\"p\\\",    \\\"tye\\\":\\\"关系\\\",    \\\"parent_name\\\":\\\"家长姓名\\\",    \\\"phone\\\":\\\"家长联系方式\\\",    \\\"@join\\\":\\\"LEFT ON s.id = p.student_id\\\"}}\"}', '2023-08-19 14:30:33', '1', '若依');
INSERT INTO `api_log` VALUES (11, 3, 'event', '1', '{\"code\":\"data_student\",\"name\":\"学生信息\",\"id\":\"1\",\"group\":\"基础\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"status\":\"1\",\"excels\":\"{\\\"data_student\\\":{    \\\"@alias\\\":\\\"s\\\",    \\\"num\\\":\\\"学号\\\",    \\\"name\\\":\\\"姓名\\\",    \\\"sex\\\":\\\"性别\\\",    \\\"class_name\\\":\\\"班级\\\"},\\\"data_student_parent\\\":{    \\\"@column\\\":\\\"name:parent_name,type,phone\\\",    \\\"@alias\\\":\\\"p\\\",    \\\"tye\\\":\\\"关系\\\",    \\\"parent_name\\\":\\\"家长姓名\\\",    \\\"phone\\\":\\\"家长联系方式\\\",    \\\"@join\\\":\\\"LEFT ON s.id = p.student_id\\\"}}\"}', '2023-08-19 14:30:53', '1', '若依');
INSERT INTO `api_log` VALUES (12, 3, 'update', '1', '{\"code\":\"data_student\",\"name\":\"学生信息\",\"id\":\"1\",\"group\":\"基础\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"status\":\"1\",\"excels\":\"{\\\"data_student\\\":{    \\\"@alias\\\":\\\"s\\\",    \\\"num\\\":\\\"学号\\\",    \\\"name\\\":\\\"姓名\\\",    \\\"sex\\\":\\\"性别\\\",    \\\"class_name\\\":\\\"班级\\\"},\\\"data_student_parent\\\":{    \\\"@column\\\":\\\"name:parent_name,type,phone\\\",    \\\"@alias\\\":\\\"p\\\",    \\\"tye\\\":\\\"关系\\\",    \\\"parent_name\\\":\\\"家长姓名\\\",    \\\"phone\\\":\\\"家长联系方式\\\",    \\\"@join\\\":\\\"LEFT ON s.id = p.student_id\\\"}}\"}', '2023-08-19 14:30:53', '1', '若依');
INSERT INTO `api_log` VALUES (13, 3, 'event', '1', '{\"code\":\"data_student\",\"name\":\"学生信息\",\"id\":\"1\",\"group\":\"基础\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"status\":\"1\",\"excels\":\"{\\\"data_student\\\":{    \\\"@alias\\\":\\\"s\\\",    \\\"num\\\":\\\"学号\\\",    \\\"name\\\":\\\"姓名\\\",    \\\"sex\\\":\\\"性别\\\",    \\\"class_name\\\":\\\"班级\\\"},\\\"data_student_parent\\\":{    \\\"@column\\\":\\\"name:parent_name,type,phone\\\",    \\\"@alias\\\":\\\"p\\\",    \\\"tye\\\":\\\"关系\\\",    \\\"parent_name\\\":\\\"家长姓名\\\",    \\\"phone\\\":\\\"家长联系方式\\\",    \\\"@join\\\":\\\"LEFT ON s.id = p.student_id\\\"}}\"}', '2023-08-19 14:31:09', '1', '若依');
INSERT INTO `api_log` VALUES (14, 3, 'update', '1', '{\"code\":\"data_student\",\"name\":\"学生信息\",\"id\":\"1\",\"group\":\"基础\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"status\":\"1\",\"excels\":\"{\\\"data_student\\\":{    \\\"@alias\\\":\\\"s\\\",    \\\"num\\\":\\\"学号\\\",    \\\"name\\\":\\\"姓名\\\",    \\\"sex\\\":\\\"性别\\\",    \\\"class_name\\\":\\\"班级\\\"},\\\"data_student_parent\\\":{    \\\"@column\\\":\\\"name:parent_name,type,phone\\\",    \\\"@alias\\\":\\\"p\\\",    \\\"tye\\\":\\\"关系\\\",    \\\"parent_name\\\":\\\"家长姓名\\\",    \\\"phone\\\":\\\"家长联系方式\\\",    \\\"@join\\\":\\\"LEFT ON s.id = p.student_id\\\"}}\"}', '2023-08-19 14:31:09', '1', '若依');
INSERT INTO `api_log` VALUES (15, 5, 'insert', '3', '{\"read\":\"Y\",\"role_id\":\"1\",\"model_id\":\"1\",\"id\":3,\"write\":\"Y\"}', '2023-08-19 14:31:09', '1', '若依');
INSERT INTO `api_log` VALUES (16, 5, 'insert', '4', '{\"read\":\"Y\",\"role_id\":\"2\",\"model_id\":\"1\",\"id\":4,\"write\":\"N\"}', '2023-08-19 14:31:09', '1', '若依');
INSERT INTO `api_log` VALUES (17, 3, 'event', '1', '{\"code\":\"data_student\",\"name\":\"学生信息\",\"id\":\"1\",\"group\":\"基础\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"status\":\"1\",\"excels\":\"{\\\"data_student\\\":{    \\\"@alias\\\":\\\"s\\\",    \\\"num\\\":\\\"学号\\\",    \\\"name\\\":\\\"姓名\\\",    \\\"sex\\\":\\\"性别\\\",    \\\"class_name\\\":\\\"班级\\\"},\\\"data_student_parent\\\":{    \\\"@column\\\":\\\"name:parent_name,type,phone\\\",    \\\"@alias\\\":\\\"p\\\",    \\\"tye\\\":\\\"关系\\\",    \\\"parent_name\\\":\\\"家长姓名\\\",    \\\"phone\\\":\\\"家长联系方式\\\",    \\\"@join\\\":\\\"LEFT ON s.id = p.student_id\\\"}}\"}', '2023-08-19 14:33:27', '1', '若依');
INSERT INTO `api_log` VALUES (18, 3, 'update', '1', '{\"code\":\"data_student\",\"name\":\"学生信息\",\"id\":\"1\",\"group\":\"基础\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"status\":\"1\",\"excels\":\"{\\\"data_student\\\":{    \\\"@alias\\\":\\\"s\\\",    \\\"num\\\":\\\"学号\\\",    \\\"name\\\":\\\"姓名\\\",    \\\"sex\\\":\\\"性别\\\",    \\\"class_name\\\":\\\"班级\\\"},\\\"data_student_parent\\\":{    \\\"@column\\\":\\\"name:parent_name,type,phone\\\",    \\\"@alias\\\":\\\"p\\\",    \\\"tye\\\":\\\"关系\\\",    \\\"parent_name\\\":\\\"家长姓名\\\",    \\\"phone\\\":\\\"家长联系方式\\\",    \\\"@join\\\":\\\"LEFT ON s.id = p.student_id\\\"}}\"}', '2023-08-19 14:33:27', '1', '若依');
INSERT INTO `api_log` VALUES (19, 5, 'insert', '5', '{\"read\":\"Y\",\"role_id\":\"1\",\"model_id\":\"1\",\"id\":5,\"write\":\"Y\"}', '2023-08-19 14:33:27', '1', '若依');
INSERT INTO `api_log` VALUES (20, 5, 'insert', '6', '{\"read\":\"Y\",\"role_id\":\"2\",\"model_id\":\"1\",\"id\":6,\"write\":\"N\"}', '2023-08-19 14:33:27', '1', '若依');
INSERT INTO `api_log` VALUES (21, NULL, 'error', NULL, '模型未定义 @data_student', '2023-08-19 22:54:19', '1', '若依');
INSERT INTO `api_log` VALUES (22, NULL, 'error', NULL, '模型未定义 @data_student', '2023-08-19 22:54:47', '1', '若依');
INSERT INTO `api_log` VALUES (23, NULL, 'error', NULL, '模型未定义 @data_student', '2023-08-19 22:55:06', '1', '若依');
INSERT INTO `api_log` VALUES (24, NULL, 'error', NULL, '模型未定义 @data_student', '2023-08-19 22:55:40', '1', '若依');
INSERT INTO `api_log` VALUES (25, NULL, 'error', NULL, '模型未定义 @data_student', '2023-08-19 23:03:10', '1', '若依');
INSERT INTO `api_log` VALUES (26, 3, 'event', '1', '{\"code\":\"data_student\",\"name\":\"学生信息\",\"id\":\"1\",\"group\":\"基础\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"{\\\"data_student\\\":{    \\n\\\"num\\\":\\\"学号\\\",    \\\"name\\\":\\\"姓名\\\",    \\\"sex\\\":\\\"性别\\\",    \\\"class_name\\\":\\\"班级\\\"},\\\"data_student_parent\\\":{  \\n  \\\"type\\\":\\\"关系\\\",    \\\"parent_name\\\":\\\"家长姓名\\\",    \\\"parent_phone\\\":\\\"家长联系方式\\\",    }}\",\"status\":\"1\"}', '2023-08-19 23:08:16', '1', '若依');
INSERT INTO `api_log` VALUES (27, 3, 'update', '1', '{\"code\":\"data_student\",\"name\":\"学生信息\",\"id\":\"1\",\"group\":\"基础\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"{\\\"data_student\\\":{    \\n\\\"num\\\":\\\"学号\\\",    \\\"name\\\":\\\"姓名\\\",    \\\"sex\\\":\\\"性别\\\",    \\\"class_name\\\":\\\"班级\\\"},\\\"data_student_parent\\\":{  \\n  \\\"type\\\":\\\"关系\\\",    \\\"parent_name\\\":\\\"家长姓名\\\",    \\\"parent_phone\\\":\\\"家长联系方式\\\",    }}\",\"status\":\"1\"}', '2023-08-19 23:08:16', '1', '若依');
INSERT INTO `api_log` VALUES (28, NULL, 'error', NULL, '模型未定义 @data_student', '2023-08-19 23:08:47', '1', '若依');
INSERT INTO `api_log` VALUES (29, NULL, 'error', NULL, '模型未定义 @data_student', '2023-08-20 21:53:36', '1', '若依');
INSERT INTO `api_log` VALUES (30, NULL, 'error', NULL, '模型未定义 @data_student', '2023-08-20 21:59:28', '1', '若依');
INSERT INTO `api_log` VALUES (31, NULL, 'error', NULL, '模型未定义 @api_model', '2023-08-21 14:41:43', '1', '若依');
INSERT INTO `api_log` VALUES (32, NULL, 'error', NULL, '模型未定义 @sys_role', '2023-08-21 14:41:43', '1', '若依');
INSERT INTO `api_log` VALUES (33, NULL, 'error', NULL, '模型未定义 @api_model', '2023-08-21 14:41:58', '1', '若依');
INSERT INTO `api_log` VALUES (34, NULL, 'error', NULL, '模型未定义 @api_model', '2023-08-21 14:42:07', '1', '若依');
INSERT INTO `api_log` VALUES (35, NULL, 'error', NULL, '模型未定义 @sys_role', '2023-08-21 14:42:07', '1', '若依');
INSERT INTO `api_log` VALUES (36, 3, 'insert', '7', '{\"code\":\"sys_role\",\"name\":\"角色\",\"id\":7,\"pk_field\":\"id\",\"unique_field\":\"role_key\",\"status\":\"1\"}', '2023-08-21 14:43:26', '1', '若依');
INSERT INTO `api_log` VALUES (37, NULL, 'error', NULL, '模型未定义 @api_model', '2023-08-21 14:43:26', '1', '若依');
INSERT INTO `api_log` VALUES (38, NULL, 'error', NULL, '模型未定义 @api_model', '2023-08-21 14:43:33', '1', '若依');
INSERT INTO `api_log` VALUES (39, NULL, 'error', NULL, '模型未定义 @sys_role', '2023-08-21 14:43:33', '1', '若依');
INSERT INTO `api_log` VALUES (40, NULL, 'error', NULL, '模型未定义 @sys_role', '2023-08-21 14:43:47', '1', '若依');
INSERT INTO `api_log` VALUES (41, NULL, 'error', NULL, '模型未定义 @api_model', '2023-08-21 14:43:47', '1', '若依');
INSERT INTO `api_log` VALUES (42, NULL, 'error', NULL, '模型未定义 @api_model', '2023-08-21 14:43:51', '1', '若依');
INSERT INTO `api_log` VALUES (43, NULL, 'error', NULL, '模型未定义 @api_access', '2023-08-21 14:43:51', '1', '若依');
INSERT INTO `api_log` VALUES (44, 3, 'event', '1', '{\"code\":\"data_student\",\"name\":\"学生信息\",\"id\":\"1\",\"group\":\"基础\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"{\\\"data_student\\\":{    \\n\\\"num\\\":\\\"学号\\\",    \\\"name\\\":\\\"姓名\\\",    \\\"sex\\\":\\\"性别\\\",    \\\"class_name\\\":\\\"班级\\\"},\\\"data_student_parent\\\":{  \\n  \\\"type\\\":\\\"关系\\\",    \\\"parent_name\\\":\\\"家长姓名\\\",    \\\"parent_phone\\\":\\\"家长联系方式\\\",    }}\",\"status\":\"1\"}', '2023-08-21 14:44:42', '1', '若依');
INSERT INTO `api_log` VALUES (45, 3, 'update', '1', '{\"code\":\"data_student\",\"name\":\"学生信息\",\"id\":\"1\",\"group\":\"基础\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"{\\\"data_student\\\":{    \\n\\\"num\\\":\\\"学号\\\",    \\\"name\\\":\\\"姓名\\\",    \\\"sex\\\":\\\"性别\\\",    \\\"class_name\\\":\\\"班级\\\"},\\\"data_student_parent\\\":{  \\n  \\\"type\\\":\\\"关系\\\",    \\\"parent_name\\\":\\\"家长姓名\\\",    \\\"parent_phone\\\":\\\"家长联系方式\\\",    }}\",\"status\":\"1\"}', '2023-08-21 14:44:42', '1', '若依');
INSERT INTO `api_log` VALUES (46, 5, 'insert', '1', '{\"read\":\"Y\",\"role_id\":\"1\",\"model_id\":\"1\",\"id\":1,\"write\":\"Y\"}', '2023-08-21 14:44:42', '1', '若依');
INSERT INTO `api_log` VALUES (47, 5, 'insert', '2', '{\"read\":\"Y\",\"role_id\":\"2\",\"model_id\":\"1\",\"id\":2,\"write\":\"N\"}', '2023-08-21 14:44:42', '1', '若依');
INSERT INTO `api_log` VALUES (48, NULL, 'error', NULL, '模型未定义 @api_model', '2023-08-21 14:44:43', '1', '若依');
INSERT INTO `api_log` VALUES (49, NULL, 'error', NULL, '模型未定义 @api_model', '2023-08-21 14:45:13', '1', '若依');
INSERT INTO `api_log` VALUES (50, NULL, 'error', NULL, '模型未定义 @api_access', '2023-08-21 14:45:13', '1', '若依');
INSERT INTO `api_log` VALUES (51, NULL, 'error', NULL, '模型未定义 @sys_role', '2023-08-21 14:47:41', '1', '若依');
INSERT INTO `api_log` VALUES (52, NULL, 'error', NULL, '模型未定义 @api_model', '2023-08-21 14:47:41', '1', '若依');
INSERT INTO `api_log` VALUES (53, NULL, 'error', NULL, '模型未定义 @api_model', '2023-08-21 14:47:43', '1', '若依');
INSERT INTO `api_log` VALUES (54, NULL, 'error', NULL, '模型未定义 @api_access', '2023-08-21 14:47:43', '1', '若依');
INSERT INTO `api_log` VALUES (55, NULL, 'error', NULL, '模型未定义 @api_model', '2023-08-21 14:47:48', '1', '若依');
INSERT INTO `api_log` VALUES (56, NULL, 'error', NULL, '模型未定义 @sys_role', '2023-08-21 14:47:48', '1', '若依');
INSERT INTO `api_log` VALUES (57, NULL, 'error', NULL, '模型未定义 @api_model', '2023-08-21 14:47:50', '1', '若依');
INSERT INTO `api_log` VALUES (58, NULL, 'error', NULL, '模型未定义 @api_access', '2023-08-21 14:47:50', '1', '若依');
INSERT INTO `api_log` VALUES (59, 3, 'event', '1', '{\"code\":\"data_student\",\"name\":\"学生信息\",\"id\":\"1\",\"group\":\"基础\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"{\\\"data_student\\\":{    \\n\\\"num\\\":\\\"学号\\\",    \\\"name\\\":\\\"姓名\\\",    \\\"sex\\\":\\\"性别\\\",    \\\"class_name\\\":\\\"班级\\\"},\\\"data_student_parent\\\":{  \\n  \\\"type\\\":\\\"关系\\\",    \\\"parent_name\\\":\\\"家长姓名\\\",    \\\"parent_phone\\\":\\\"家长联系方式\\\",    }}\",\"status\":\"1\"}', '2023-08-21 14:47:54', '1', '若依');
INSERT INTO `api_log` VALUES (60, 3, 'update', '1', '{\"code\":\"data_student\",\"name\":\"学生信息\",\"id\":\"1\",\"group\":\"基础\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"{\\\"data_student\\\":{    \\n\\\"num\\\":\\\"学号\\\",    \\\"name\\\":\\\"姓名\\\",    \\\"sex\\\":\\\"性别\\\",    \\\"class_name\\\":\\\"班级\\\"},\\\"data_student_parent\\\":{  \\n  \\\"type\\\":\\\"关系\\\",    \\\"parent_name\\\":\\\"家长姓名\\\",    \\\"parent_phone\\\":\\\"家长联系方式\\\",    }}\",\"status\":\"1\"}', '2023-08-21 14:47:54', '1', '若依');
INSERT INTO `api_log` VALUES (61, 5, 'insert', '3', '{\"read\":\"Y\",\"role_id\":\"1\",\"model_id\":\"1\",\"id\":3,\"write\":\"Y\"}', '2023-08-21 14:47:54', '1', '若依');
INSERT INTO `api_log` VALUES (62, 5, 'insert', '4', '{\"read\":\"Y\",\"role_id\":\"2\",\"model_id\":\"1\",\"id\":4,\"write\":\"N\"}', '2023-08-21 14:47:54', '1', '若依');
INSERT INTO `api_log` VALUES (63, NULL, 'error', NULL, '模型未定义 @api_model', '2023-08-21 14:47:54', '1', '若依');
INSERT INTO `api_log` VALUES (64, NULL, 'error', NULL, '模型未定义 @api_model', '2023-08-21 14:48:03', '1', '若依');
INSERT INTO `api_log` VALUES (65, NULL, 'error', NULL, '模型未定义 @api_access', '2023-08-21 14:48:03', '1', '若依');
INSERT INTO `api_log` VALUES (66, NULL, 'error', NULL, '模型未定义 @api_model', '2023-08-21 14:48:19', '1', '若依');
INSERT INTO `api_log` VALUES (67, NULL, 'error', NULL, '模型未定义 @api_access', '2023-08-21 14:48:19', '1', '若依');
INSERT INTO `api_log` VALUES (70, 3, 'delete', NULL, '2', '2023-08-22 15:29:40', '1', '若依');
INSERT INTO `api_log` VALUES (71, 3, 'delete', NULL, '2', '2023-08-22 15:34:04', '1', '若依');
INSERT INTO `api_log` VALUES (72, 4, 'insert', '2', '{\"action\":\"AD\",\"model_id\":\"3\",\"id\":2,\"content\":\"function exec(ids){\\n  return [\\n   \\\"DELETE FROM api_access where model_id IN (\\\"+ids+\\\")\\\",\\n   \\\"DELETE FROM api_event where model_id IN (\\\"+ids+\\\")\\\",\\n  ];\\n}\",\"status\":\"1\"}', '2023-08-22 15:55:40', '1', '若依');
INSERT INTO `api_log` VALUES (73, 4, 'update', '2', '{\"action\":\"AD\",\"id\":\"2\",\"model_id\":\"3\",\"is_default\":\"N\",\"content\":\"function exec(ids){\\n  return [\\n   \\\"DELETE FROM api_access where model_id IN (\\\"+ids+\\\")\\\",\\n   \\\"DELETE FROM api_event where model_id IN (\\\"+ids+\\\")\\\",\\n  ];\\n}\",\"status\":\"1\"}', '2023-08-22 15:56:24', '1', '若依');
INSERT INTO `api_log` VALUES (74, 3, 'delete', NULL, '2', '2023-08-22 16:09:20', '1', '若依');
INSERT INTO `api_log` VALUES (75, 3, 'event', '2', '2', '2023-08-22 16:09:20', '1', '若依');
INSERT INTO `api_log` VALUES (76, NULL, 'error', NULL, '模型未定义 @api_log', '2023-08-22 17:37:51', '1', '管理员');
INSERT INTO `api_log` VALUES (77, NULL, 'error', NULL, '模型未定义 @api_log', '2023-08-22 17:38:14', '1', '管理员');
INSERT INTO `api_log` VALUES (78, NULL, 'error', NULL, '模型未定义 @api_log', '2023-08-22 17:41:02', '1', '管理员');
INSERT INTO `api_log` VALUES (79, NULL, 'error', NULL, '模型未定义 @api_log', '2023-08-22 17:41:05', '1', '管理员');
INSERT INTO `api_log` VALUES (84, 3, 'insert', '12', '{\"code\":\"api_log\",\"name\":\"接口日志\",\"id\":12,\"pk_field\":\"id\",\"excels\":\"{\\n\\\"api_log\\\":\\n{    \\n\\\"user_name\\\":\\\"操作人\\\",   \\\"log_type\\\":\\\"操作类型\\\",    \\\"log_time\\\":\\\"记录时间\\\",    \\\"log_key\\\":\\\"数据主键\\\",    \\\"log_data\\\":\\\"数据\\\"},\\n\\\"api_model\\\":\\n{  \\n  \\\"model_code\\\":\\\"模型代码\\\",    \\\"model_name\\\":\\\"模型名称\\\"}\\n}\",\"status\":\"1\"}', '2023-08-22 21:38:22', '1', '管理员');
INSERT INTO `api_log` VALUES (85, 5, 'insert', '5', '{\"read\":\"Y\",\"role_id\":\"1\",\"model_id\":12,\"id\":5,\"write\":\"Y\"}', '2023-08-22 21:38:27', '1', '管理员');
INSERT INTO `api_log` VALUES (86, 5, 'insert', '6', '{\"read\":\"Y\",\"role_id\":\"2\",\"model_id\":12,\"id\":6,\"write\":\"N\"}', '2023-08-22 21:38:31', '1', '管理员');
INSERT INTO `api_log` VALUES (87, 3, 'delete', NULL, '10', '2023-08-23 13:51:29', '1', '管理员');
INSERT INTO `api_log` VALUES (88, 3, 'event', '2', '10', '2023-08-23 13:51:29', '1', '管理员');
INSERT INTO `api_log` VALUES (89, 1, 'insert', '13', '{\"code\":\"asset_category\",\"name\":\"资产分类\",\"id\":13,\"group\":\"asset\",\"pk_field\":\"id\",\"status\":\"1\"}', '2023-08-23 13:52:02', '1', '管理员');
INSERT INTO `api_log` VALUES (90, 3, 'insert', '7', '{\"read\":\"Y\",\"role_id\":\"1\",\"model_id\":13,\"id\":7,\"write\":\"Y\"}', '2023-08-23 13:52:02', '1', '管理员');
INSERT INTO `api_log` VALUES (91, 3, 'insert', '8', '{\"read\":\"Y\",\"role_id\":\"2\",\"model_id\":13,\"id\":8,\"write\":\"N\"}', '2023-08-23 13:52:02', '1', '管理员');
INSERT INTO `api_log` VALUES (92, 6, 'insert', '1', '{\"code\":\"userSelect\",\"name\":\"用户调用接口\",\"remark\":\"查询用户信息，生成下拉列表\",\"id\":1,\"type\":\"table\",\"content\":\"SELECT user_id val, nick_name label FROM sys_user WHERE del_flag=0\",\"group\":\"sys\"}', '2023-08-23 14:12:05', '1', '管理员');
INSERT INTO `api_log` VALUES (93, 6, 'update', '1', '{\"code\":\"user_select\",\"name\":\"用户调用接口\",\"remark\":\"查询用户信息，生成下拉列表\",\"id\":\"1\",\"type\":\"table\",\"content\":\"SELECT user_id val, nick_name label FROM sys_user WHERE del_flag=0\",\"group\":\"sys\"}', '2023-08-23 14:12:36', '1', '管理员');
INSERT INTO `api_log` VALUES (94, 6, 'update', '1', '{\"code\":\"user_select\",\"name\":\"用户调用接口\",\"remark\":\"查询用户信息，生成下拉列表\",\"id\":\"1\",\"type\":\"table\",\"content\":\"SELECT user_id value, nick_name label FROM sys_user WHERE del_flag=0\",\"group\":\"sys\"}', '2023-08-23 15:19:06', '1', '管理员');
INSERT INTO `api_log` VALUES (95, 1, 'insert', '14', '{\"code\":\"asset_warehouse\",\"name\":\"仓库信息\",\"id\":14,\"group\":\"asset\",\"pk_field\":\"id\",\"excels\":\"\\n{\\n\\\"asset_warehouse\\\":\\n{    \\n\\\"name\\\":\\\"仓库名称\\\",   \\\"address\\\":\\\"仓库地址\\\",    \\\"user_id\\\":\\\"负责人\\\",    \\\"nick_name\\\":\\\"负责人\\\",    \\\"remark\\\":\\\"备注\\\"},\\n\\\"@excels\\\":\\n{  \\n  \\\"user_id\\\":\\\"import\\\",    \\\"nick_name\\\":\\\"export\\\"}\\n}\",\"status\":\"1\"}', '2023-08-23 15:24:15', '1', '管理员');
INSERT INTO `api_log` VALUES (96, 3, 'insert', '9', '{\"read\":\"Y\",\"role_id\":\"1\",\"model_id\":14,\"id\":9,\"write\":\"Y\"}', '2023-08-23 15:24:15', '1', '管理员');
INSERT INTO `api_log` VALUES (97, 3, 'insert', '10', '{\"read\":\"Y\",\"role_id\":\"2\",\"model_id\":14,\"id\":10,\"write\":\"N\"}', '2023-08-23 15:24:15', '1', '管理员');
INSERT INTO `api_log` VALUES (98, 1, 'update', '14', '{\"code\":\"asset_warehouse\",\"name\":\"仓库信息\",\"id\":\"14\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"excels\":\"{\\n\\t\\\"asset_warehouse\\\": {\\n\\t\\t\\\"name\\\": \\\"仓库名称\\\",\\n\\t\\t\\\"address\\\": \\\"仓库地址\\\",\\n\\t\\t\\\"user_id\\\": \\\"负责人\\\",\\n\\t\\t\\\"nick_name\\\": \\\"负责人\\\",\\n\\t\\t\\\"remark\\\": \\\"备注\\\"\\n\\t},\\n\\t\\\"@excel\\\": {\\n\\t\\t\\\"user_id\\\": \\\"import\\\",\\n\\t\\t\\\"nick_name\\\": \\\"export\\\"\\n\\t}\\n}\",\"status\":\"1\"}', '2023-08-23 15:26:27', '1', '管理员');
INSERT INTO `api_log` VALUES (99, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":9,\"model_id\":\"14\",\"write\":\"Y\"}', '2023-08-23 15:26:27', '1', '管理员');
INSERT INTO `api_log` VALUES (100, 3, 'update', '9', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":9,\"model_id\":\"14\",\"write\":\"Y\"}', '2023-08-23 15:26:27', '1', '管理员');
INSERT INTO `api_log` VALUES (101, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":10,\"model_id\":\"14\",\"write\":\"N\"}', '2023-08-23 15:26:27', '1', '管理员');
INSERT INTO `api_log` VALUES (102, 3, 'update', '10', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":10,\"model_id\":\"14\",\"write\":\"N\"}', '2023-08-23 15:26:27', '1', '管理员');
INSERT INTO `api_log` VALUES (103, 1, 'update', '12', '{\"code\":\"api_log\",\"name\":\"接口日志\",\"id\":\"12\",\"is_default\":\"N\",\"pk_field\":\"id\",\"excels\":\"{\\n\\t\\\"api_log\\\": {\\n\\t\\t\\\"user_name\\\": \\\"操作人\\\",\\n\\t\\t\\\"log_type\\\": \\\"操作类型\\\",\\n\\t\\t\\\"log_time\\\": \\\"记录时间\\\",\\n\\t\\t\\\"log_key\\\": \\\"数据主键\\\",\\n\\t\\t\\\"log_data\\\": \\\"数据\\\"\\n\\t},\\n\\t\\\"api_model\\\": {\\n\\t\\t\\\"model_code\\\": \\\"模型代码\\\",\\n\\t\\t\\\"model_name\\\": \\\"模型名称\\\"\\n\\t}\\n}\",\"status\":\"1\"}', '2023-08-23 15:26:44', '1', '管理员');
INSERT INTO `api_log` VALUES (104, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":5,\"model_id\":\"12\",\"write\":\"Y\"}', '2023-08-23 15:26:44', '1', '管理员');
INSERT INTO `api_log` VALUES (105, 3, 'update', '5', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":5,\"model_id\":\"12\",\"write\":\"Y\"}', '2023-08-23 15:26:44', '1', '管理员');
INSERT INTO `api_log` VALUES (106, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":6,\"model_id\":\"12\",\"write\":\"N\"}', '2023-08-23 15:26:44', '1', '管理员');
INSERT INTO `api_log` VALUES (107, 3, 'update', '6', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":6,\"model_id\":\"12\",\"write\":\"N\"}', '2023-08-23 15:26:44', '1', '管理员');
INSERT INTO `api_log` VALUES (108, 1, 'update', '14', '{\"code\":\"asset_warehouse\",\"name\":\"仓库信息\",\"id\":\"14\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"excels\":\"{\\n\\t\\\"asset_warehouse\\\": {\\n\\t\\t\\\"name\\\": \\\"仓库名称\\\",\\n\\t\\t\\\"address\\\": \\\"仓库地址\\\",\\n\\t\\t\\\"user_id\\\": \\\"负责人\\\",\\n\\t\\t\\\"nick_name\\\": \\\"负责人\\\",\\n\\t\\t\\\"remark\\\": \\\"备注\\\"\\n\\t},\\n\\t\\\"@ds\\\": {\\n\\t\\t\\\"user_id\\\": \\\"SELECT user_id value, nick_name label FROM sys_user WHERE del_flag=0\\\"\\n\\t}\\n}\",\"status\":\"1\"}', '2023-08-23 15:39:24', '1', '管理员');
INSERT INTO `api_log` VALUES (109, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":9,\"model_id\":\"14\",\"write\":\"Y\"}', '2023-08-23 15:39:24', '1', '管理员');
INSERT INTO `api_log` VALUES (110, 3, 'update', '9', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":9,\"model_id\":\"14\",\"write\":\"Y\"}', '2023-08-23 15:39:24', '1', '管理员');
INSERT INTO `api_log` VALUES (111, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":10,\"model_id\":\"14\",\"write\":\"N\"}', '2023-08-23 15:39:24', '1', '管理员');
INSERT INTO `api_log` VALUES (112, 3, 'update', '10', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":10,\"model_id\":\"14\",\"write\":\"N\"}', '2023-08-23 15:39:24', '1', '管理员');
INSERT INTO `api_log` VALUES (113, 1, 'update', '14', '{\"code\":\"asset_warehouse\",\"name\":\"仓库信息\",\"id\":\"14\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"excels\":\"{\\n\\t\\\"asset_warehouse\\\": {\\n\\t\\t\\\"name\\\": \\\"仓库名称\\\",\\n\\t\\t\\\"address\\\": \\\"仓库地址\\\",\\n\\t\\t\\\"user_id\\\": \\\"负责人\\\",\\n\\t\\t\\\"links\\\": \\\"联系方式\\\",\\n\\t\\t\\\"remark\\\": \\\"备注\\\"\\n\\t},\\n\\t\\\"@ds\\\": {\\n\\t\\t\\\"user_id\\\": \\\"SELECT user_id value, nick_name label FROM sys_user WHERE del_flag=0\\\"\\n\\t}\\n}\",\"status\":\"1\"}', '2023-08-23 15:40:49', '1', '管理员');
INSERT INTO `api_log` VALUES (114, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":9,\"model_id\":\"14\",\"write\":\"Y\"}', '2023-08-23 15:40:49', '1', '管理员');
INSERT INTO `api_log` VALUES (115, 3, 'update', '9', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":9,\"model_id\":\"14\",\"write\":\"Y\"}', '2023-08-23 15:40:49', '1', '管理员');
INSERT INTO `api_log` VALUES (116, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":10,\"model_id\":\"14\",\"write\":\"N\"}', '2023-08-23 15:40:49', '1', '管理员');
INSERT INTO `api_log` VALUES (117, 3, 'update', '10', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":10,\"model_id\":\"14\",\"write\":\"N\"}', '2023-08-23 15:40:49', '1', '管理员');
INSERT INTO `api_log` VALUES (118, 1, 'update', '14', '{\"code\":\"asset_warehouse\",\"name\":\"仓库信息\",\"id\":\"14\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"excels\":\"{\\n\\t\\\"asset_warehouse\\\": {\\n\\t\\t\\\"name\\\": \\\"仓库名称\\\",\\n\\t\\t\\\"address\\\": \\\"仓库地址\\\",\\n\\t\\t\\\"user_id\\\": \\\"负责人\\\",\\n\\t\\t\\\"links\\\": \\\"联系方式\\\",\\n\\t\\t\\\"status\\\": \\\"状态\\\",\\n\\t\\t\\\"remark\\\": \\\"备注\\\"\\n\\t},\\n\\t\\\"@ds\\\": {\\n\\t\\t\\\"user_id\\\": \\\"SELECT user_id value, nick_name label FROM sys_user WHERE del_flag=0\\\"\\n\\t},\\n\\t\\\"@dict\\\": {\\n\\t\\t\\\"status\\\": {\\\"1\\\":\\\"启用\\\"，\\\"2\\\":\\\"禁用\\\"}\\n\\t}\\n}\",\"status\":\"1\"}', '2023-08-23 15:45:27', '1', '管理员');
INSERT INTO `api_log` VALUES (119, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":9,\"model_id\":\"14\",\"write\":\"Y\"}', '2023-08-23 15:45:27', '1', '管理员');
INSERT INTO `api_log` VALUES (120, 3, 'update', '9', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":9,\"model_id\":\"14\",\"write\":\"Y\"}', '2023-08-23 15:45:27', '1', '管理员');
INSERT INTO `api_log` VALUES (121, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":10,\"model_id\":\"14\",\"write\":\"N\"}', '2023-08-23 15:45:28', '1', '管理员');
INSERT INTO `api_log` VALUES (122, 3, 'update', '10', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":10,\"model_id\":\"14\",\"write\":\"N\"}', '2023-08-23 15:45:28', '1', '管理员');
INSERT INTO `api_log` VALUES (123, 14, 'update', '2', '{\"address\":\"三楼\",\"user_id\":\"1\",\"name\":\"杂物间\",\"links\":\"234123\",\"remark\":\"在公司\",\"id\":\"2\",\"status\":\"1\"}', '2023-08-23 15:46:10', '1', '管理员');
INSERT INTO `api_log` VALUES (124, 14, 'update', '1', '{\"address\":\"一楼\",\"user_id\":\"1\",\"name\":\"大仓库\",\"links\":\"222\",\"remark\":\"易危\",\"id\":\"1\",\"status\":\"1\"}', '2023-08-23 15:46:23', '1', '管理员');
INSERT INTO `api_log` VALUES (125, 1, 'update', '14', '{\"code\":\"asset_warehouse\",\"name\":\"仓库信息\",\"id\":\"14\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"name\",\"excels\":\"{\\n\\t\\\"asset_warehouse\\\": {\\n\\t\\t\\\"name\\\": \\\"仓库名称\\\",\\n\\t\\t\\\"address\\\": \\\"仓库地址\\\",\\n\\t\\t\\\"user_id\\\": \\\"负责人\\\",\\n\\t\\t\\\"links\\\": \\\"联系方式\\\",\\n\\t\\t\\\"status\\\": \\\"状态\\\",\\n\\t\\t\\\"remark\\\": \\\"备注\\\"\\n\\t},\\n\\t\\\"@ds\\\": {\\n\\t\\t\\\"user_id\\\": \\\"SELECT user_id value, nick_name label FROM sys_user WHERE del_flag=0\\\"\\n\\t},\\n\\t\\\"@dict\\\": {\\n\\t\\t\\\"status\\\": {\\\"1\\\":\\\"启用\\\"，\\\"2\\\":\\\"禁用\\\"}\\n\\t}\\n}', '2023-08-23 16:48:11', '1', '管理员');
INSERT INTO `api_log` VALUES (126, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":9,\"model_id\":\"14\",\"write\":\"Y\"}', '2023-08-23 16:48:12', '1', '管理员');
INSERT INTO `api_log` VALUES (127, 3, 'update', '9', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":9,\"model_id\":\"14\",\"write\":\"Y\"}', '2023-08-23 16:48:12', '1', '管理员');
INSERT INTO `api_log` VALUES (128, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":10,\"model_id\":\"14\",\"write\":\"N\"}', '2023-08-23 16:48:12', '1', '管理员');
INSERT INTO `api_log` VALUES (129, 3, 'update', '10', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":10,\"model_id\":\"14\",\"write\":\"N\"}', '2023-08-23 16:48:12', '1', '管理员');
INSERT INTO `api_log` VALUES (130, 14, 'update', '1', '{\"address\":\"一楼\",\"user_id\":\"1\",\"name\":\"大仓库\",\"links\":\"222\",\"remark\":\"易危\",\"id\":1,\"status\":\"1\"}', '2023-08-23 17:08:50', '1', '管理员');
INSERT INTO `api_log` VALUES (131, 14, 'update', '2', '{\"address\":\"三楼\",\"user_id\":\"1\",\"name\":\"杂物间\",\"links\":\"234123\",\"remark\":\"在公司\",\"id\":2,\"status\":\"1\"}', '2023-08-23 17:08:50', '1', '管理员');
INSERT INTO `api_log` VALUES (132, 14, 'update', '1', '{\"address\":\"一楼\",\"user_id\":\"1\",\"name\":\"大仓库\",\"links\":\"222\",\"remark\":\"易危\",\"id\":1,\"status\":\"1\"}', '2023-08-23 17:11:34', '1', '管理员');
INSERT INTO `api_log` VALUES (133, 14, 'update', '2', '{\"address\":\"三楼\",\"user_id\":\"1\",\"name\":\"杂物间\",\"links\":\"234123\",\"remark\":\"在公司\",\"id\":2,\"status\":\"1\"}', '2023-08-23 17:11:34', '1', '管理员');
INSERT INTO `api_log` VALUES (134, 14, 'insert', '3', '{\"address\":\"一楼\",\"user_id\":\"1\",\"name\":\"设备仓库\",\"links\":\"222\",\"remark\":\"易危\",\"id\":3,\"status\":\"1\"}', '2023-08-23 17:13:17', '1', '管理员');
INSERT INTO `api_log` VALUES (135, 14, 'update', '2', '{\"address\":\"三楼\",\"user_id\":\"1\",\"name\":\"杂物间\",\"links\":\"234123\",\"remark\":\"在公司\",\"id\":2,\"status\":\"1\"}', '2023-08-23 17:13:17', '1', '管理员');
INSERT INTO `api_log` VALUES (136, 14, 'insert', '4', '{\"address\":\"高新三路产业园\",\"user_id\":\"2\",\"name\":\"临时仓库\",\"links\":\"34535432\",\"remark\":\"\",\"id\":4,\"status\":\"1\"}', '2023-08-23 17:13:17', '1', '管理员');
INSERT INTO `api_log` VALUES (137, 1, 'insert', '15', '{\"code\":\"asset_model\",\"name\":\"资产型号\",\"id\":15,\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"name\",\"status\":\"1\"}', '2023-08-23 17:19:23', '1', '管理员');
INSERT INTO `api_log` VALUES (138, 3, 'insert', '11', '{\"read\":\"Y\",\"role_id\":\"1\",\"model_id\":15,\"id\":11,\"write\":\"Y\"}', '2023-08-23 17:19:23', '1', '管理员');
INSERT INTO `api_log` VALUES (139, 3, 'insert', '12', '{\"read\":\"Y\",\"role_id\":\"2\",\"model_id\":15,\"id\":12,\"write\":\"N\"}', '2023-08-23 17:19:23', '1', '管理员');
INSERT INTO `api_log` VALUES (140, 1, 'insert', '16', '{\"code\":\"asset_info\",\"name\":\"资产信息\",\"id\":16,\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"status\":\"1\"}', '2023-08-28 11:06:37', '1', '管理员');
INSERT INTO `api_log` VALUES (141, 3, 'insert', '13', '{\"read\":\"Y\",\"role_id\":\"1\",\"model_id\":16,\"id\":13,\"write\":\"Y\"}', '2023-08-28 11:06:37', '1', '管理员');
INSERT INTO `api_log` VALUES (142, 3, 'insert', '14', '{\"read\":\"Y\",\"role_id\":\"2\",\"model_id\":16,\"id\":14,\"write\":\"N\"}', '2023-08-28 11:06:37', '1', '管理员');
INSERT INTO `api_log` VALUES (143, 15, 'update', '1913', '{\"factory\":\"\",\"imgs\":\"/profile/upload/2023/08/28/微信截图_20220809145343_20230828112125A001.png\",\"name\":\"DYJ-2022-03-0002\",\"cate_id\":\"30\",\"id\":\"1913\",\"status\":\"1\"}', '2023-08-28 11:21:28', '1', '管理员');
INSERT INTO `api_log` VALUES (144, 16, 'update', '1907', '{\"imgs\":\"/profile/upload/2023/08/28/test_20230828112143A002.png\",\"house_id\":\"1\",\"num\":\"FWQ-2022-03-0002\",\"cate_id\":\"27\",\"model_id\":\"1907\",\"user_id\":\"1\",\"price\":\"0\",\"comment\":\"\",\"id\":\"1907\",\"dept_id\":\"102\",\"is_it\":\"Y\",\"status\":\"2\"}', '2023-08-28 11:21:47', '1', '管理员');
INSERT INTO `api_log` VALUES (145, 6, 'insert', '2', '{\"code\":\"dept_select\",\"name\":\"部门列表查询\",\"id\":2,\"type\":\"table\",\"content\":\"SELECT dept_id id, dept_name name, parent_id FROM sys_dept WHERE del_flag=0\",\"group\":\"sys\"}', '2023-08-28 11:33:08', '1', '管理员');
INSERT INTO `api_log` VALUES (146, 6, 'update', '2', '{\"code\":\"dept_select\",\"name\":\"部门列表查询\",\"remark\":\"查询部门信息，可生成下拉列表\",\"id\":\"2\",\"type\":\"table\",\"content\":\"SELECT dept_id id, dept_name name, parent_id FROM sys_dept WHERE del_flag=0\",\"group\":\"sys\"}', '2023-08-28 11:33:33', '1', '管理员');
INSERT INTO `api_log` VALUES (147, 6, 'update', '2', '{\"code\":\"dept_select\",\"name\":\"部门列表查询\",\"remark\":\"查询部门信息，可生成下拉列表\",\"id\":\"2\",\"type\":\"table\",\"content\":\"SELECT dept_id value, dept_name label, parent_id FROM sys_dept WHERE del_flag=0\",\"group\":\"sys\"}', '2023-08-28 11:34:17', '1', '管理员');
INSERT INTO `api_log` VALUES (148, 6, 'insert', '3', '{\"code\":\"warehouse_select\",\"name\":\"查询仓库信息列表\",\"remark\":\"查询仓库信息,生成下拉列表\",\"id\":3,\"type\":\"table\",\"content\":\"select id value, name label from asset_warehouse\",\"group\":\"asset\"}', '2023-08-28 15:34:43', '1', '管理员');
INSERT INTO `api_log` VALUES (149, 16, 'update', '1907', '{\"imgs\":\"/profile/upload/2023/08/28/test_20230828112143A002.png\",\"house_id\":\"1\",\"num\":\"FWQ-2022-03-0002\",\"cate_id\":\"27\",\"model_id\":\"1907\",\"user_id\":\"1\",\"price\":\"360\",\"comment\":\"\",\"id\":\"1907\",\"dept_id\":\"102\",\"is_it\":\"Y\",\"status\":\"1\"}', '2023-08-28 15:36:55', '1', '管理员');
INSERT INTO `api_log` VALUES (150, 2, 'insert', '3', '{\"action\":\"AU\",\"remark\":\"非在用状态的设备，user_id为空\",\"model_id\":\"16\",\"id\":3,\"content\":\"function exec(data){\\n  if(data.status != \'2\' && data.userId){\\n    return \\\"UPDATE asset_info SET user_id = null WHERE id=\\\"+data.id; \\n  }\\n  return \\\"\\\";\\n}\",\"status\":\"1\"}', '2023-08-28 16:24:39', '1', '管理员');
INSERT INTO `api_log` VALUES (151, 16, 'update', '1907', '{\"imgs\":\"/profile/upload/2023/08/28/test_20230828112143A002.png\",\"house_id\":\"1\",\"num\":\"FWQ-2022-03-0002\",\"cate_id\":\"27\",\"model_id\":\"1907\",\"user_id\":\"1\",\"price\":\"360\",\"comment\":\"\",\"id\":\"1907\",\"dept_id\":\"102\",\"is_it\":\"Y\",\"status\":\"1\"}', '2023-08-28 16:25:33', '1', '管理员');
INSERT INTO `api_log` VALUES (152, 16, 'update', '1907', '{\"imgs\":\"/profile/upload/2023/08/28/test_20230828112143A002.png\",\"house_id\":\"1\",\"num\":\"FWQ-2022-03-0002\",\"cate_id\":\"27\",\"model_id\":\"1907\",\"user_id\":\"1\",\"price\":\"360\",\"comment\":\"\",\"id\":\"1907\",\"dept_id\":\"102\",\"is_it\":\"Y\",\"status\":\"1\"}', '2023-08-28 16:27:08', '1', '管理员');
INSERT INTO `api_log` VALUES (153, 16, 'update', '1907', '{\"imgs\":\"/profile/upload/2023/08/28/test_20230828112143A002.png\",\"house_id\":\"1\",\"num\":\"FWQ-2022-03-0002\",\"cate_id\":\"27\",\"model_id\":\"1907\",\"user_id\":\"1\",\"price\":\"360\",\"comment\":\"\",\"id\":\"1907\",\"dept_id\":\"102\",\"is_it\":\"Y\",\"status\":\"1\"}', '2023-08-28 16:30:50', '1', '管理员');
INSERT INTO `api_log` VALUES (154, 16, 'update', '1907', '{\"imgs\":\"/profile/upload/2023/08/28/test_20230828112143A002.png\",\"house_id\":\"1\",\"num\":\"FWQ-2022-03-0002\",\"cate_id\":\"27\",\"model_id\":\"1907\",\"user_id\":\"1\",\"price\":\"360\",\"comment\":\"\",\"id\":\"1907\",\"dept_id\":\"102\",\"is_it\":\"Y\",\"status\":\"1\"}', '2023-08-28 16:39:45', '1', '管理员');
INSERT INTO `api_log` VALUES (155, 2, 'update', '3', '{\"action\":\"AU\",\"remark\":\"非在用状态的设备，user_id为空\",\"id\":\"3\",\"model_id\":\"16\",\"is_default\":\"N\",\"content\":\"function exec(data){\\n  if(data.status != \'2\' && data.user_id){\\n    return \\\"UPDATE asset_info SET user_id = null WHERE id=\\\"+data.id; \\n  }\\n  return \\\"\\\";\\n}\",\"status\":\"1\"}', '2023-08-28 16:40:24', '1', '管理员');
INSERT INTO `api_log` VALUES (156, 16, 'update', '1907', '{\"imgs\":\"/profile/upload/2023/08/28/test_20230828112143A002.png\",\"house_id\":\"1\",\"num\":\"FWQ-2022-03-0002\",\"cate_id\":\"27\",\"model_id\":\"1907\",\"user_id\":\"1\",\"price\":\"360\",\"comment\":\"\",\"id\":\"1907\",\"dept_id\":\"102\",\"is_it\":\"Y\",\"status\":\"1\"}', '2023-08-28 16:40:30', '1', '管理员');
INSERT INTO `api_log` VALUES (157, 16, 'update', '1907', '{\"imgs\":\"/profile/upload/2023/08/28/test_20230828112143A002.png\",\"house_id\":\"1\",\"num\":\"FWQ-2022-03-0002\",\"cate_id\":\"27\",\"model_id\":\"1907\",\"user_id\":\"1\",\"price\":\"360\",\"comment\":\"\",\"id\":\"1907\",\"dept_id\":\"102\",\"is_it\":\"Y\",\"status\":\"1\"}', '2023-08-28 16:45:33', '1', '管理员');
INSERT INTO `api_log` VALUES (158, 16, 'update', '1907', '{\"imgs\":\"/profile/upload/2023/08/28/test_20230828112143A002.png\",\"house_id\":\"1\",\"num\":\"FWQ-2022-03-0002\",\"cate_id\":\"27\",\"model_id\":\"1907\",\"user_id\":\"1\",\"price\":\"360\",\"comment\":\"\",\"id\":\"1907\",\"dept_id\":\"102\",\"is_it\":\"Y\",\"status\":\"1\"}', '2023-08-28 16:46:17', '1', '管理员');
INSERT INTO `api_log` VALUES (159, 16, 'event', '3', '{\"imgs\":\"/profile/upload/2023/08/28/test_20230828112143A002.png\",\"house_id\":\"1\",\"num\":\"FWQ-2022-03-0002\",\"cate_id\":\"27\",\"model_id\":\"1907\",\"user_id\":\"1\",\"price\":\"360\",\"comment\":\"\",\"id\":\"1907\",\"dept_id\":\"102\",\"is_it\":\"Y\",\"status\":\"1\"}', '2023-08-28 16:46:24', '1', '管理员');
INSERT INTO `api_log` VALUES (160, 16, 'update', '1907', '{\"imgs\":\"/profile/upload/2023/08/28/test_20230828112143A002.png\",\"house_id\":\"1\",\"num\":\"FWQ-2022-03-0002\",\"cate_id\":\"27\",\"model_id\":\"1907\",\"user_id\":\"2\",\"price\":\"360\",\"comment\":\"\",\"id\":\"1907\",\"dept_id\":\"102\",\"is_it\":\"Y\",\"status\":\"2\"}', '2023-08-28 16:47:44', '1', '管理员');
INSERT INTO `api_log` VALUES (161, 16, 'update', '1906', '{\"house_id\":\"1\",\"user_id\":\"0\",\"num\":\"FWQ-2022-03-0001\",\"cate_id\":\"27\",\"comment\":\"\",\"id\":\"1906\",\"model_id\":\"1906\",\"dept_id\":\"102\",\"is_it\":\"N\",\"status\":\"1\"}', '2023-08-28 16:47:51', '1', '管理员');
INSERT INTO `api_log` VALUES (162, 16, 'event', '3', '{\"house_id\":\"1\",\"user_id\":\"0\",\"num\":\"FWQ-2022-03-0001\",\"cate_id\":\"27\",\"comment\":\"\",\"id\":\"1906\",\"model_id\":\"1906\",\"dept_id\":\"102\",\"is_it\":\"N\",\"status\":\"1\"}', '2023-08-28 16:47:58', '1', '管理员');
INSERT INTO `api_log` VALUES (163, 1, 'update', '16', '{\"code\":\"asset_info\",\"name\":\"资产信息\",\"id\":\"16\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"{\\n\\t\\\"asset_info\\\": {\\n\\t\\t\\\"cate_id\\\": \\\"资产分类\\\",\\n\\t\\t\\\"model_id\\\": \\\"资产型号\\\",\\n\\t\\t\\\"num\\\": \\\"编号\\\",\\n\\t\\t\\\"house_id\\\": \\\"所在仓库\\\",\\n\\t\\t\\\"dept_id\\\": \\\"所属部门\\\",\\n\\t\\t\\\"user_id\\\": \\\"负责人\\\",\\n\\t\\t\\\"status\\\": \\\"状态\\\",\\n\\t\\t\\\"price\\\": \\\"当前价值\\\",\\n\\t\\t\\\"buy_time\\\": \\\"购买时间\\\",\\n\\t\\t\\\"comment\\\": \\\"说明\\\"\\n\\t},\\n\\t\\\"@ds\\\": {\\n\\t\\t\\\"user_id\\\": \\\"SELECT user_id value, nick_name label FROM ', '2023-08-28 16:53:32', '1', '管理员');
INSERT INTO `api_log` VALUES (164, 1, 'update', '16', '{\"code\":\"asset_info\",\"name\":\"资产信息\",\"id\":\"16\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"{\\n\\t\\\"asset_info\\\": {\\n\\t\\t\\\"cate_id\\\": \\\"资产分类\\\",\\n\\t\\t\\\"model_id\\\": \\\"资产型号\\\",\\n\\t\\t\\\"num\\\": \\\"编号\\\",\\n\\t\\t\\\"house_id\\\": \\\"所在仓库\\\",\\n\\t\\t\\\"dept_id\\\": \\\"所属部门\\\",\\n\\t\\t\\\"user_id\\\": \\\"负责人\\\",\\n\\t\\t\\\"status\\\": \\\"状态\\\",\\n\\t\\t\\\"price\\\": \\\"当前价值\\\",\\n\\t\\t\\\"buy_time\\\": \\\"购买时间\\\",\\n\\t\\t\\\"comment\\\": \\\"说明\\\"\\n\\t},\\n\\t\\\"@ds\\\": {\\n\\t\\t\\\"user_id\\\": \\\"SELECT user_id value, nick_name label FROM ', '2023-08-28 16:53:55', '1', '管理员');
INSERT INTO `api_log` VALUES (165, 1, 'update', '16', '{\"code\":\"asset_info\",\"name\":\"资产信息\",\"id\":\"16\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"{\\n\\t\\\"asset_info\\\": {\\n\\t\\t\\\"cate_id\\\": \\\"资产分类\\\",\\n\\t\\t\\\"model_id\\\": \\\"资产型号\\\",\\n\\t\\t\\\"num\\\": \\\"编号\\\",\\n\\t\\t\\\"house_id\\\": \\\"所在仓库\\\",\\n\\t\\t\\\"dept_id\\\": \\\"所属部门\\\",\\n\\t\\t\\\"user_id\\\": \\\"负责人\\\",\\n\\t\\t\\\"status\\\": \\\"状态\\\",\\n\\t\\t\\\"price\\\": \\\"当前价值\\\",\\n\\t\\t\\\"buy_time\\\": \\\"购买时间\\\",\\n\\t\\t\\\"comment\\\": \\\"说明\\\"\\n\\t},\\n\\t\\\"@ds\\\": {\\n\\t\\t\\\"user_id\\\": \\\"SELECT user_id value, nick_name label FROM ', '2023-08-28 16:56:52', '1', '管理员');
INSERT INTO `api_log` VALUES (166, 1, 'update', '16', '{\"code\":\"asset_info\",\"name\":\"资产信息\",\"id\":\"16\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"{\\n\\t\\\"asset_info\\\": {\\n\\t\\t\\\"cate_id\\\": \\\"资产分类\\\",\\n\\t\\t\\\"model_id\\\": \\\"资产型号\\\",\\n\\t\\t\\\"num\\\": \\\"编号\\\",\\n\\t\\t\\\"house_id\\\": \\\"所在仓库\\\",\\n\\t\\t\\\"dept_id\\\": \\\"所属部门\\\",\\n\\t\\t\\\"user_id\\\": \\\"负责人\\\",\\n\\t\\t\\\"status\\\": \\\"状态\\\",\\n\\t\\t\\\"price\\\": \\\"当前价值\\\",\\n\\t\\t\\\"buy_time\\\": \\\"购买时间\\\",\\n\\t\\t\\\"comment\\\": \\\"说明\\\"\\n\\t},\\n\\t\\\"@ds\\\": {\\n\\t\\t\\\"user_id\\\": \\\"SELECT user_id value, nick_name label FROM ', '2023-08-28 16:57:20', '1', '管理员');
INSERT INTO `api_log` VALUES (167, 1, 'update', '16', '{\"code\":\"asset_info\",\"name\":\"资产信息\",\"id\":\"16\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"{\\n\\t\\\"asset_info\\\": {\\n\\t\\t\\\"cate_id\\\": \\\"资产分类\\\",\\n\\t\\t\\\"model_id\\\": \\\"资产型号\\\",\\n\\t\\t\\\"num\\\": \\\"编号\\\",\\n\\t\\t\\\"house_id\\\": \\\"所在仓库\\\",\\n\\t\\t\\\"dept_id\\\": \\\"所属部门\\\",\\n\\t\\t\\\"user_id\\\": \\\"负责人\\\",\\n\\t\\t\\\"status\\\": \\\"状态\\\",\\n\\t\\t\\\"price\\\": \\\"当前价值\\\",\\n\\t\\t\\\"buy_time\\\": \\\"购买时间\\\",\\n\\t\\t\\\"comment\\\": \\\"说明\\\"\\n\\t},\\n\\t\\\"@ds\\\": {\\n\\t\\t\\\"user_id\\\": \\\"SELECT user_id value, nick_name label FROM ', '2023-08-28 16:57:33', '1', '管理员');
INSERT INTO `api_log` VALUES (168, 1, 'update', '16', '{\"code\":\"asset_info\",\"name\":\"资产信息\",\"id\":\"16\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"{\\n\\t\\\"asset_info\\\": {\\n\\t\\t\\\"cate_id\\\": \\\"资产分类\\\",\\n\\t\\t\\\"model_id\\\": \\\"资产型号\\\",\\n\\t\\t\\\"num\\\": \\\"编号\\\",\\n\\t\\t\\\"house_id\\\": \\\"所在仓库\\\",\\n\\t\\t\\\"dept_id\\\": \\\"所属部门\\\",\\n\\t\\t\\\"user_id\\\": \\\"负责人\\\",\\n\\t\\t\\\"status\\\": \\\"状态\\\",\\n\\t\\t\\\"price\\\": \\\"当前价值\\\",\\n\\t\\t\\\"buy_time\\\": \\\"购买时间\\\",\\n\\t\\t\\\"comment\\\": \\\"说明\\\"\\n\\t},\\n\\t\\\"@ds\\\": {\\n\\t\\t\\\"user_id\\\": \\\"SELECT user_id value, nick_name label FROM ', '2023-08-28 16:59:30', '1', '管理员');
INSERT INTO `api_log` VALUES (169, 1, 'update', '16', '{\"code\":\"asset_info\",\"name\":\"资产信息\",\"id\":\"16\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"{\\n\\t\\\"asset_info\\\": {\\n\\t\\t\\\"cate_id\\\": \\\"资产分类\\\",\\n\\t\\t\\\"model_id\\\": \\\"资产型号\\\",\\n\\t\\t\\\"num\\\": \\\"编号\\\",\\n\\t\\t\\\"house_id\\\": \\\"所在仓库\\\",\\n\\t\\t\\\"dept_id\\\": \\\"所属部门\\\",\\n\\t\\t\\\"user_id\\\": \\\"负责人\\\",\\n\\t\\t\\\"status\\\": \\\"状态\\\",\\n\\t\\t\\\"price\\\": \\\"当前价值\\\",\\n\\t\\t\\\"buy_time\\\": \\\"购买时间\\\",\\n\\t\\t\\\"comment\\\": \\\"说明\\\"\\n\\t},\\n\\t\\\"@ds\\\": {\\n\\t\\t\\\"user_id\\\": \\\"SELECT user_id value, nick_name label FROM ', '2023-08-28 16:59:31', '1', '管理员');
INSERT INTO `api_log` VALUES (170, 1, 'update', '16', '{\"code\":\"asset_info\",\"name\":\"资产信息\",\"id\":\"16\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"{\\n\\t\\\"asset_info\\\": {\\n\\t\\t\\\"cate_id\\\": \\\"资产分类\\\",\\n\\t\\t\\\"model_id\\\": \\\"资产型号\\\",\\n\\t\\t\\\"num\\\": \\\"编号\\\",\\n\\t\\t\\\"house_id\\\": \\\"所在仓库\\\",\\n\\t\\t\\\"dept_id\\\": \\\"所属部门\\\",\\n\\t\\t\\\"user_id\\\": \\\"负责人\\\",\\n\\t\\t\\\"status\\\": \\\"状态\\\",\\n\\t\\t\\\"price\\\": \\\"当前价值\\\",\\n\\t\\t\\\"buy_time\\\": \\\"购买时间\\\",\\n\\t\\t\\\"comment\\\": \\\"说明\\\"\\n\\t},\\n\\t\\\"@ds\\\": {\\n\\t\\t\\\"user_id\\\": \\\"SELECT user_id value, nick_name label FROM ', '2023-08-28 17:04:27', '1', '管理员');
INSERT INTO `api_log` VALUES (171, 1, 'update', '16', '{\"code\":\"asset_info\",\"name\":\"资产信息\",\"id\":\"16\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"{\\n\\t\\\"asset_info\\\": {\\n\\t\\t\\\"cate_id\\\": \\\"资产分类\\\",\\n\\t\\t\\\"model_id\\\": \\\"资产型号\\\",\\n\\t\\t\\\"num\\\": \\\"编号\\\",\\n\\t\\t\\\"house_id\\\": \\\"所在仓库\\\",\\n\\t\\t\\\"dept_id\\\": \\\"所属部门\\\",\\n\\t\\t\\\"user_id\\\": \\\"负责人\\\",\\n\\t\\t\\\"status\\\": \\\"状态\\\",\\n\\t\\t\\\"price\\\": \\\"当前价值\\\",\\n\\t\\t\\\"buy_time\\\": \\\"购买时间\\\",\\n\\t\\t\\\"comment\\\": \\\"说明\\\"\\n\\t},\\n\\t\\\"@ds\\\": {\\n\\t\\t\\\"user_id\\\": \\\"SELECT user_id value, nick_name label FROM ', '2023-08-28 17:05:28', '1', '管理员');
INSERT INTO `api_log` VALUES (172, 1, 'update', '16', '{\"code\":\"asset_info\",\"name\":\"资产信息\",\"id\":\"16\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"{\\n\\t\\\"asset_info\\\": {\\n\\t\\t\\\"cate_id\\\": \\\"资产分类\\\",\\n\\t\\t\\\"model_id\\\": \\\"资产型号\\\",\\n\\t\\t\\\"num\\\": \\\"编号\\\",\\n\\t\\t\\\"house_id\\\": \\\"所在仓库\\\",\\n\\t\\t\\\"dept_id\\\": \\\"所属部门\\\",\\n\\t\\t\\\"user_id\\\": \\\"负责人\\\",\\n\\t\\t\\\"status\\\": \\\"状态\\\",\\n\\t\\t\\\"price\\\": \\\"当前价值\\\",\\n\\t\\t\\\"buy_time\\\": \\\"购买时间\\\",\\n\\t\\t\\\"comment\\\": \\\"说明\\\"\\n\\t},\\n\\t\\\"@ds\\\": {\\n\\t\\t\\\"user_id\\\": \\\"SELECT user_id value, nick_name label FROM ', '2023-08-28 17:06:12', '1', '管理员');
INSERT INTO `api_log` VALUES (173, 1, 'update', '16', '{\"code\":\"asset_info\",\"name\":\"资产信息\",\"id\":\"16\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"{\\n\\t\\\"asset_info\\\": {\\n\\t\\t\\\"cate_id\\\": \\\"资产分类\\\",\\n\\t\\t\\\"model_id\\\": \\\"资产型号\\\",\\n\\t\\t\\\"num\\\": \\\"编号\\\",\\n\\t\\t\\\"house_id\\\": \\\"所在仓库\\\",\\n\\t\\t\\\"dept_id\\\": \\\"所属部门\\\",\\n\\t\\t\\\"user_id\\\": \\\"负责人\\\",\\n\\t\\t\\\"status\\\": \\\"状态\\\",\\n\\t\\t\\\"price\\\": \\\"当前价值\\\",\\n\\t\\t\\\"buy_time\\\": \\\"购买时间\\\",\\n\\t\\t\\\"comment\\\": \\\"说明\\\"\\n\\t},\\n\\t\\\"@ds\\\": {\\n\\t\\t\\\"user_id\\\": \\\"SELECT user_id value, nick_name label FROM ', '2023-08-28 17:08:58', '1', '管理员');
INSERT INTO `api_log` VALUES (174, 1, 'update', '16', '{\"code\":\"asset_info\",\"name\":\"资产信息\",\"id\":\"16\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"{\\n\\t\\\"asset_info\\\": {\\n\\t\\t\\\"cate_id\\\": \\\"资产分类\\\",\\n\\t\\t\\\"model_id\\\": \\\"资产型号\\\",\\n\\t\\t\\\"num\\\": \\\"编号\\\",\\n\\t\\t\\\"house_id\\\": \\\"所在仓库\\\",\\n\\t\\t\\\"dept_id\\\": \\\"所属部门\\\",\\n\\t\\t\\\"user_id\\\": \\\"负责人\\\",\\n\\t\\t\\\"status\\\": \\\"状态\\\",\\n\\t\\t\\\"price\\\": \\\"当前价值\\\",\\n\\t\\t\\\"buy_time\\\": \\\"购买时间\\\",\\n\\t\\t\\\"comment\\\": \\\"说明\\\"\\n\\t},\\n\\t\\\"@ds\\\": {\\n\\t\\t\\\"user_id\\\": \\\"SELECT user_id value, nick_name label FROM ', '2023-08-28 17:10:20', '1', '管理员');
INSERT INTO `api_log` VALUES (175, 1, 'update', '16', '{\"code\":\"asset_info\",\"name\":\"资产信息\",\"id\":\"16\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"{\\n\\t\\\"asset_info\\\": {\\n\\t\\t\\\"cate_id\\\": \\\"资产分类\\\",\\n\\t\\t\\\"model_id\\\": \\\"资产型号\\\",\\n\\t\\t\\\"num\\\": \\\"编号\\\",\\n\\t\\t\\\"house_id\\\": \\\"所在仓库\\\",\\n\\t\\t\\\"dept_id\\\": \\\"所属部门\\\",\\n\\t\\t\\\"user_id\\\": \\\"负责人\\\",\\n\\t\\t\\\"status\\\": \\\"状态\\\",\\n\\t\\t\\\"price\\\": \\\"当前价值\\\",\\n\\t\\t\\\"buy_time\\\": \\\"购买时间\\\",\\n\\t\\t\\\"comment\\\": \\\"说明\\\"\\n\\t},\\n\\t\\\"@ds\\\": {\\n\\t\\t\\\"user_id\\\": \\\"SELECT user_id value, nick_name label FROM ', '2023-08-28 17:12:21', '1', '管理员');
INSERT INTO `api_log` VALUES (176, 1, 'update', '16', '{\"code\":\"asset_info\",\"name\":\"资产信息\",\"id\":\"16\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"{\\n\\t\\\"asset_info\\\": {\\n\\t\\t\\\"cate_id\\\": \\\"资产分类\\\",\\n\\t\\t\\\"model_id\\\": \\\"资产型号\\\",\\n\\t\\t\\\"num\\\": \\\"编号\\\",\\n\\t\\t\\\"house_id\\\": \\\"所在仓库\\\",\\n\\t\\t\\\"dept_id\\\": \\\"所属部门\\\",\\n\\t\\t\\\"user_id\\\": \\\"负责人\\\",\\n\\t\\t\\\"status\\\": \\\"状态\\\",\\n\\t\\t\\\"price\\\": \\\"当前价值\\\",\\n\\t\\t\\\"buy_time\\\": \\\"购买时间\\\",\\n\\t\\t\\\"comment\\\": \\\"说明\\\"\\n\\t},\\n\\t\\\"@ds\\\": {\\n\\t\\t\\\"user_id\\\": \\\"SELECT user_id value, nick_name label FROM ', '2023-08-28 17:14:13', '1', '管理员');
INSERT INTO `api_log` VALUES (177, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":13,\"model_id\":\"16\",\"write\":\"Y\"}', '2023-08-28 17:14:29', '1', '管理员');
INSERT INTO `api_log` VALUES (178, 3, 'update', '13', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":13,\"model_id\":\"16\",\"write\":\"Y\"}', '2023-08-28 17:14:29', '1', '管理员');
INSERT INTO `api_log` VALUES (179, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":14,\"model_id\":\"16\",\"write\":\"N\"}', '2023-08-28 17:14:32', '1', '管理员');
INSERT INTO `api_log` VALUES (180, 3, 'update', '14', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":14,\"model_id\":\"16\",\"write\":\"N\"}', '2023-08-28 17:14:32', '1', '管理员');
INSERT INTO `api_log` VALUES (181, 1, 'update', '16', '{\"code\":\"asset_info\",\"name\":\"资产信息\",\"id\":\"16\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"{\\n\\t\\\"asset_info\\\": {\\n\\t\\t\\\"cate_id\\\": \\\"资产分类\\\",\\n\\t\\t\\\"model_id\\\": \\\"资产型号\\\",\\n\\t\\t\\\"num\\\": \\\"编号\\\",\\n\\t\\t\\\"house_id\\\": \\\"所在仓库\\\",\\n\\t\\t\\\"dept_id\\\": \\\"所属部门\\\",\\n\\t\\t\\\"user_id\\\": \\\"负责人\\\",\\n\\t\\t\\\"status\\\": \\\"状态\\\",\\n\\t\\t\\\"price\\\": \\\"当前价值\\\",\\n\\t\\t\\\"buy_time\\\": \\\"购买时间\\\",\\n\\t\\t\\\"comment\\\": \\\"说明\\\"\\n\\t},\\n\\t\\\"@ds\\\": {\\n\\t\\t\\\"user_id\\\": \\\"SELECT user_id value, nick_name label FROM ', '2023-08-28 17:14:39', '1', '管理员');
INSERT INTO `api_log` VALUES (182, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":13,\"model_id\":\"16\",\"write\":\"Y\"}', '2023-08-28 17:14:42', '1', '管理员');
INSERT INTO `api_log` VALUES (183, 3, 'update', '13', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":13,\"model_id\":\"16\",\"write\":\"Y\"}', '2023-08-28 17:14:42', '1', '管理员');
INSERT INTO `api_log` VALUES (184, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":14,\"model_id\":\"16\",\"write\":\"N\"}', '2023-08-28 17:14:45', '1', '管理员');
INSERT INTO `api_log` VALUES (185, 3, 'update', '14', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":14,\"model_id\":\"16\",\"write\":\"N\"}', '2023-08-28 17:14:45', '1', '管理员');
INSERT INTO `api_log` VALUES (186, 16, 'insert', '1914', '{\"buy_time\":\"2023-09-06 00:00:00\",\"house_id\":\"3\",\"price\":\"9600\",\"num\":\"FWQ-2019-002-1\",\"cate_id\":\"27\",\"comment\":\"55\",\"model_id\":\"1904\",\"id\":1914,\"dept_id\":\"108\",\"status\":\"1\"}', '2023-08-29 16:00:47', '1', '管理员');
INSERT INTO `api_log` VALUES (187, 16, 'insert', '1915', '{\"buy_time\":\"2023-09-07 00:00:00\",\"house_id\":\"3\",\"price\":\"850\",\"num\":\"DYJ-2020-001-1\",\"cate_id\":\"30\",\"comment\":\"22\",\"model_id\":\"1905\",\"id\":1915,\"dept_id\":\"102\"}', '2023-08-29 16:00:47', '1', '管理员');
INSERT INTO `api_log` VALUES (188, 16, 'insert', '1916', '{\"buy_time\":\"2023-09-08 00:00:00\",\"house_id\":\"3\",\"price\":\"25633\",\"num\":\"JC-2022-03-0002-2\",\"cate_id\":\"29\",\"comment\":\"22\",\"model_id\":\"1911\",\"id\":1916,\"dept_id\":\"103\",\"status\":\"1\"}', '2023-08-29 16:00:47', '1', '管理员');
INSERT INTO `api_log` VALUES (189, 16, 'update', '1914', '{\"buy_time\":\"2023-09-06 00:00:00\",\"house_id\":\"3\",\"price\":\"9600\",\"num\":\"FWQ-2019-002-1\",\"cate_id\":\"27\",\"comment\":\"55\",\"id\":1914,\"model_id\":\"1904\",\"dept_id\":\"108\",\"status\":\"1\"}', '2023-08-29 16:01:47', '1', '管理员');
INSERT INTO `api_log` VALUES (190, 16, 'update', '1915', '{\"buy_time\":\"2023-09-07 00:00:00\",\"house_id\":\"3\",\"price\":\"850\",\"num\":\"DYJ-2020-001-1\",\"cate_id\":\"30\",\"comment\":\"22\",\"id\":1915,\"model_id\":\"1905\",\"dept_id\":\"102\"}', '2023-08-29 16:01:47', '1', '管理员');
INSERT INTO `api_log` VALUES (191, 16, 'update', '1916', '{\"buy_time\":\"2023-09-08 00:00:00\",\"house_id\":\"3\",\"price\":\"25633\",\"num\":\"JC-2022-03-0002-2\",\"cate_id\":\"29\",\"comment\":\"22\",\"id\":1916,\"model_id\":\"1911\",\"dept_id\":\"103\",\"status\":\"1\"}', '2023-08-29 16:01:47', '1', '管理员');
INSERT INTO `api_log` VALUES (192, 1, 'update', '16', '{\"code\":\"asset_info\",\"name\":\"资产信息\",\"id\":\"16\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"{\\n\\t\\\"asset_info\\\": {\\n\\t\\t\\\"cate_id\\\": \\\"资产分类\\\",\\n\\t\\t\\\"model_id\\\": \\\"资产型号\\\",\\n\\t\\t\\\"num\\\": \\\"编号\\\",\\n\\t\\t\\\"house_id\\\": \\\"所在仓库\\\",\\n\\t\\t\\\"dept_id\\\": \\\"所属部门\\\",\\n\\t\\t\\\"user_id\\\": \\\"负责人\\\",\\n\\t\\t\\\"status\\\": \\\"状态\\\",\\n\\t\\t\\\"price\\\": \\\"当前价值\\\",\\n\\t\\t\\\"buy_time\\\": \\\"购买时间\\\",\\n\\t\\t\\\"comment\\\": \\\"说明\\\"\\n\\t},\\n\\t\\\"@ds\\\": {\\n\\t\\t\\\"user_id\\\": \\\"SELECT user_id value, nick_name label FROM ', '2023-08-29 16:03:32', '1', '管理员');
INSERT INTO `api_log` VALUES (193, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":13,\"model_id\":\"16\",\"write\":\"Y\"}', '2023-08-29 16:03:32', '1', '管理员');
INSERT INTO `api_log` VALUES (194, 3, 'update', '13', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":13,\"model_id\":\"16\",\"write\":\"Y\"}', '2023-08-29 16:03:32', '1', '管理员');
INSERT INTO `api_log` VALUES (195, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":14,\"model_id\":\"16\",\"write\":\"N\"}', '2023-08-29 16:03:32', '1', '管理员');
INSERT INTO `api_log` VALUES (196, 3, 'update', '14', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":14,\"model_id\":\"16\",\"write\":\"N\"}', '2023-08-29 16:03:32', '1', '管理员');
INSERT INTO `api_log` VALUES (197, 16, 'update', '1914', '{\"buy_time\":\"2023-09-06 00:00:00\",\"house_id\":\"3\",\"price\":\"9600\",\"num\":\"FWQ-2019-002-1\",\"cate_id\":\"27\",\"comment\":\"55\",\"id\":1914,\"model_id\":\"1904\",\"dept_id\":\"108\",\"status\":\"1\"}', '2023-08-29 16:03:41', '1', '管理员');
INSERT INTO `api_log` VALUES (198, 16, 'update', '1915', '{\"buy_time\":\"2023-09-07 00:00:00\",\"house_id\":\"3\",\"price\":\"850\",\"num\":\"DYJ-2020-001-1\",\"cate_id\":\"30\",\"comment\":\"22\",\"id\":1915,\"model_id\":\"1905\",\"dept_id\":\"102\",\"status\":\"3\"}', '2023-08-29 16:03:41', '1', '管理员');
INSERT INTO `api_log` VALUES (199, 16, 'update', '1916', '{\"buy_time\":\"2023-09-08 00:00:00\",\"house_id\":\"3\",\"price\":\"25633\",\"num\":\"JC-2022-03-0002-2\",\"cate_id\":\"29\",\"comment\":\"22\",\"id\":1916,\"model_id\":\"1911\",\"dept_id\":\"103\",\"status\":\"1\"}', '2023-08-29 16:03:41', '1', '管理员');
INSERT INTO `api_log` VALUES (200, 6, 'insert', '4', '{\"code\":\"asset_model_select\",\"name\":\"查询资产模型信息列表\",\"remark\":\"查询资产型号信息,生成下拉列表\",\"id\":4,\"type\":\"table\",\"is_default\":\"N\",\"content\":\"select id value, name label from asset_model\",\"group\":\"asset\"}', '2023-08-29 16:53:11', '1', '管理员');
INSERT INTO `api_log` VALUES (201, 6, 'insert', '5', '{\"code\":\"asset_category_select\",\"name\":\"查询资产分类信息列表\",\"remark\":\"查询资产分类信息,生成下拉列表\",\"id\":5,\"type\":\"table\",\"is_default\":\"N\",\"content\":\"select id value, name label, pid from asset_category\",\"group\":\"asset\"}', '2023-08-29 16:53:41', '1', '管理员');
INSERT INTO `api_log` VALUES (202, 6, 'update', '4', '{\"code\":\"asset_model_select\",\"name\":\"查询资产模型信息列表\",\"remark\":\"查询资产型号信息,生成下拉列表\",\"id\":\"4\",\"type\":\"table\",\"is_default\":\"N\",\"content\":\"select id value, name label, cate_id from asset_model\",\"group\":\"asset\"}', '2023-08-29 17:30:49', '1', '管理员');
INSERT INTO `api_log` VALUES (203, 6, 'insert', '6', '{\"code\":\"asset_category_model_select\",\"name\":\"查询资产分类和模型信息列表\",\"remark\":\"查询资产分类和型号信息,生成下拉列表\",\"id\":6,\"type\":\"table\",\"is_default\":\"N\",\"content\":\"select m.id value, concat(c.name, \' \', m.name) label\\nfrom asset_model m\\njoin asset_category c on m.cate_id=c.id\",\"group\":\"asset\"}', '2023-08-29 17:44:13', '1', '管理员');
INSERT INTO `api_log` VALUES (204, 6, 'update', '6', '{\"code\":\"asset_category_model_select\",\"name\":\"查询资产分类和模型信息列表\",\"remark\":\"查询资产分类和型号信息,生成下拉列表\",\"id\":\"6\",\"type\":\"table\",\"is_default\":\"N\",\"content\":\"select m.id value, concat(c.name, \' \', m.name) label, m.price2 price\\nfrom asset_model m\\njoin asset_category c on m.cate_id=c.id\",\"group\":\"asset\"}', '2023-08-29 17:49:59', '1', '管理员');
INSERT INTO `api_log` VALUES (205, 1, 'insert', '17', '{\"code\":\"asset_purchase\",\"name\":\"资产采购订单\",\"id\":17,\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"auto\",\"status\":\"1\"}', '2023-08-30 10:10:58', '1', '管理员');
INSERT INTO `api_log` VALUES (206, 1, 'update', '17', '{\"code\":\"asset_purchase\",\"name\":\"资产采购订单\",\"id\":\"17\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"AUTO\",\"status\":\"1\"}', '2023-08-30 10:11:09', '1', '管理员');
INSERT INTO `api_log` VALUES (207, 1, 'insert', '18', '{\"code\":\"asset_purchase_detail\",\"name\":\"资产采购明细\",\"id\":18,\"group\":\"asset\",\"pk_field\":\"id\",\"excels\":\"AUTO\",\"status\":\"1\"}', '2023-08-30 10:12:00', '1', '管理员');
INSERT INTO `api_log` VALUES (208, 1, 'update', '18', '{\"code\":\"asset_purchase_detail\",\"name\":\"资产采购明细\",\"id\":\"18\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"excels\":\"AUTO\",\"status\":\"1\"}', '2023-08-30 10:23:19', '1', '管理员');
INSERT INTO `api_log` VALUES (209, 1, 'update', '18', '{\"code\":\"asset_purchase_detail\",\"name\":\"资产采购明细\",\"id\":\"18\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"excels\":\"AUTO\",\"status\":\"1\"}', '2023-08-30 10:25:40', '1', '管理员');
INSERT INTO `api_log` VALUES (210, 1, 'update', '18', '{\"code\":\"asset_purchase_detail\",\"name\":\"资产采购明细\",\"id\":\"18\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"excels\":\"AUTO\",\"status\":\"1\"}', '2023-08-30 10:27:21', '1', '管理员');
INSERT INTO `api_log` VALUES (211, 1, 'update', '17', '{\"code\":\"asset_purchase\",\"name\":\"资产采购订单\",\"id\":\"17\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"AUTO\",\"status\":\"1\"}', '2023-08-30 10:27:36', '1', '管理员');
INSERT INTO `api_log` VALUES (212, 2, 'insert', '4', '{\"action\":\"BU\",\"remark\":\"更新前，先删除采购明细，然后全量保存，前端使用patch方法\",\"model_id\":\"17\",\"id\":4,\"content\":\"function exec(data){\\n  return [\\\"DELETE FROM asset_purchase_detail WHERE purchase_id=\\\" + data.id];\\n}\",\"status\":\"1\"}', '2023-08-30 10:29:42', '1', '管理员');
INSERT INTO `api_log` VALUES (216, 17, 'insert', '31', '{\"reason\":\"34\",\"user_id\":\"1\",\"price\":\"1\",\"num\":\"123\",\"id\":31,\"dept_id\":\"103\",\"check_status\":\"0\",\"status\":\"1\"}', '2023-08-30 11:01:51', '1', '管理员');
INSERT INTO `api_log` VALUES (217, 18, 'insert', '34', '{\"amount\":\"1\",\"purchase_id\":31,\"cate_id\":\"30\",\"model_id\":\"1905\",\"id\":34}', '2023-08-30 11:01:52', '1', '管理员');
INSERT INTO `api_log` VALUES (218, 18, 'insert', '35', '{\"amount\":\"2\",\"purchase_id\":31,\"cate_id\":\"27\",\"model_id\":\"1907\",\"id\":35}', '2023-08-30 11:01:53', '1', '管理员');
INSERT INTO `api_log` VALUES (219, 17, 'event', '4', '{\"reason\":\"34\",\"user_id\":\"1\",\"price\":\"1\",\"num\":\"123\",\"comment\":\"ooooo\",\"id\":\"31\",\"dept_id\":\"103\",\"check_status\":\"0\",\"status\":\"1\"}', '2023-08-30 11:14:22', '1', '管理员');
INSERT INTO `api_log` VALUES (220, 17, 'update', '31', '{\"reason\":\"34\",\"user_id\":\"1\",\"price\":\"1\",\"num\":\"123\",\"comment\":\"ooooo\",\"id\":\"31\",\"dept_id\":\"103\",\"check_status\":\"0\",\"status\":\"1\"}', '2023-08-30 11:14:22', '1', '管理员');
INSERT INTO `api_log` VALUES (221, 18, 'update', '34', '{\"amount\":\"1\",\"purchase_id\":\"31\",\"cate_id\":\"30\",\"id\":\"34\",\"model_id\":\"1905\"}', '2023-08-30 11:14:22', '1', '管理员');
INSERT INTO `api_log` VALUES (222, 18, 'update', '35', '{\"amount\":\"2\",\"purchase_id\":\"31\",\"cate_id\":\"27\",\"id\":\"35\",\"model_id\":\"1907\"}', '2023-08-30 11:14:22', '1', '管理员');
INSERT INTO `api_log` VALUES (223, 17, 'event', '4', '{\"reason\":\"34\",\"user_id\":\"1\",\"price\":\"1\",\"num\":\"123\",\"comment\":\"ooooo\",\"id\":\"31\",\"dept_id\":\"103\",\"check_status\":\"0\",\"status\":\"1\"}', '2023-08-30 11:19:51', '1', '管理员');
INSERT INTO `api_log` VALUES (224, 17, 'update', '31', '{\"reason\":\"34\",\"user_id\":\"1\",\"price\":\"1\",\"num\":\"123\",\"comment\":\"ooooo\",\"id\":\"31\",\"dept_id\":\"103\",\"check_status\":\"0\",\"status\":\"1\"}', '2023-08-30 11:19:51', '1', '管理员');
INSERT INTO `api_log` VALUES (225, 18, 'insert', '36', '{\"amount\":\"1\",\"purchase_id\":\"31\",\"cate_id\":\"30\",\"model_id\":\"1905\",\"id\":36}', '2023-08-30 11:19:51', '1', '管理员');
INSERT INTO `api_log` VALUES (226, 17, 'event', '4', '{\"reason\":\"34\",\"user_id\":\"1\",\"price\":\"1\",\"num\":\"123\",\"comment\":\"ooooo\",\"id\":\"31\",\"dept_id\":\"103\",\"check_status\":\"0\",\"status\":\"1\"}', '2023-08-30 11:19:55', '1', '管理员');
INSERT INTO `api_log` VALUES (227, 17, 'update', '31', '{\"reason\":\"34\",\"user_id\":\"1\",\"price\":\"1\",\"num\":\"123\",\"comment\":\"ooooo\",\"id\":\"31\",\"dept_id\":\"103\",\"check_status\":\"0\",\"status\":\"1\"}', '2023-08-30 11:19:55', '1', '管理员');
INSERT INTO `api_log` VALUES (228, 18, 'insert', '36', '{\"amount\":\"1\",\"purchase_id\":\"31\",\"cate_id\":\"30\",\"id\":\"36\",\"model_id\":\"1905\"}', '2023-08-30 11:19:55', '1', '管理员');
INSERT INTO `api_log` VALUES (229, 17, 'event', '4', '{\"reason\":\"34\",\"user_id\":\"1\",\"price\":\"2\",\"num\":\"123\",\"comment\":\"ooooo\",\"id\":\"31\",\"dept_id\":\"103\",\"check_status\":\"0\",\"status\":\"1\"}', '2023-08-30 11:20:00', '1', '管理员');
INSERT INTO `api_log` VALUES (230, 17, 'update', '31', '{\"reason\":\"34\",\"user_id\":\"1\",\"price\":\"2\",\"num\":\"123\",\"comment\":\"ooooo\",\"id\":\"31\",\"dept_id\":\"103\",\"check_status\":\"0\",\"status\":\"1\"}', '2023-08-30 11:20:00', '1', '管理员');
INSERT INTO `api_log` VALUES (231, 18, 'insert', '36', '{\"amount\":\"2\",\"purchase_id\":\"31\",\"cate_id\":\"30\",\"id\":\"36\",\"model_id\":\"1905\"}', '2023-08-30 11:20:00', '1', '管理员');
INSERT INTO `api_log` VALUES (232, 1, 'update', '17', '{\"code\":\"asset_purchase\",\"name\":\"资产采购订单\",\"id\":\"17\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"{\\n  \\\"asset_purchase\\\":{\\n\\t\\\"num\\\":\\\"采购编号\\\",\\n\\t\\\"reason\\\":\\\"采购原由\\\",\\n\\t\\\"comment\\\":\\\"采购说明\\\",\\n\\t\\\"price\\\":\\\"总价值\\\",\\n\\t\\\"check_status\\\":\\\"审核结果\\\",\\n\\t\\\"dept_id\\\":\\\"申请部门\\\",\\n\\t\\\"user_id\\\":\\\"申请人\\\",\\n\\t\\\"finish_time\\\":\\\"完成时间\\\",\\n\\t\\\"status\\\":\\\"申请状态\\\"\\n  },\\n  \\\"@dict\\\":{\\n    \\\"status\\\":{\\\"0\\\":\\\"未提交\\\",\\\"1\\\":\\\"已提交\\\",\\\"2\\\":\\\"已完成\\\",\\\"3\\\":\\\"已撤回\\\"}\\n    \\\"check_status\\\":{\\', '2023-08-30 14:07:31', '1', '管理员');
INSERT INTO `api_log` VALUES (233, 6, 'update', '6', '{\"code\":\"purchase_num\",\"name\":\"生成采购单编号\",\"remark\":\"生成采购单编号，年4位月2位+三位序号\",\"id\":\"6\",\"type\":\"count\",\"is_default\":\"N\",\"content\":\"select max(num) from(\\nselect max(num)+1 num from asset_purchase where num like concat(DATE_FORMAT(now(),\'%Y%m\'), \'%\')\\nunion all\\nselect concat(DATE_FORMAT(now(),\'%Y%m\'), \'001\') as num\\n) t\",\"group\":\"asset\"}', '2023-08-30 14:31:37', '1', '管理员');
INSERT INTO `api_log` VALUES (234, 2, 'insert', '5', '{\"action\":\"AI\",\"remark\":\"自动生成采购订单号\",\"model_id\":\"17\",\"id\":5,\"content\":\"function exec(data){\\n  var sql = \\\"UDPATE asset_purchase set num = \\\";\\n  sql += \\\"(select max(num) from( select max(num)+1 num from asset_purchase where num like concat(DATE_FORMAT(now(),\'%Y%m%d\'), \'%\') union all select concat(DATE_FORMAT(now(),\'%Y%m%d\'), \'001\') as num ) t)\\\";\\n  sql += \\\" WHERE id=\\\"+data.id;\\n  return sql;\\n}\",\"status\":\"1\"}', '2023-08-30 14:46:35', '1', '管理员');
INSERT INTO `api_log` VALUES (236, 2, 'update', '5', '{\"action\":\"AI\",\"remark\":\"自动生成采购订单号\",\"id\":\"5\",\"model_id\":\"17\",\"is_default\":\"N\",\"content\":\"function exec(data){\\n  var sql = \\\"UPDATE asset_purchase set num = \\\";\\n  sql += \\\"(select max(num) from( select max(num)+1 num from asset_purchase where num like concat(DATE_FORMAT(now(),\'%Y%m%d\'), \'%\') union all select concat(DATE_FORMAT(now(),\'%Y%m%d\'), \'001\') as num ) t)\\\";\\n  sql += \\\" WHERE id=\\\"+data.id;\\n  return sql;\\n}\",\"status\":\"1\"}', '2023-08-30 14:52:46', '1', '管理员');
INSERT INTO `api_log` VALUES (237, 1, 'update', '17', '{\"code\":\"asset_purchase\",\"name\":\"资产采购订单\",\"id\":\"17\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"{\\n  \\\"asset_purchase\\\":{\\n\\t\\\"num\\\":\\\"采购编号\\\",\\n\\t\\\"reason\\\":\\\"采购原由\\\",\\n\\t\\\"comment\\\":\\\"采购说明\\\",\\n\\t\\\"price\\\":\\\"总价值\\\",\\n\\t\\\"check_status\\\":\\\"审核结果\\\",\\n\\t\\\"dept_id\\\":\\\"申请部门\\\",\\n\\t\\\"user_id\\\":\\\"申请人\\\",\\n\\t\\\"finish_time\\\":\\\"完成时间\\\",\\n\\t\\\"status\\\":\\\"申请状态\\\"\\n  },\\n  \\\"@dict\\\":{\\n    \\\"status\\\":{\\\"1\\\":\\\"未提交\\\",\\\"2\\\":\\\"已提交\\\",\\\"3\\\":\\\"已完成\\\",\\\"4\\\":\\\"已撤回\\\"}\\n    \\\"check_status\\\":{\\', '2023-08-30 15:05:07', '1', '管理员');
INSERT INTO `api_log` VALUES (238, 16, 'insert', '1917', '{\"buy_time\":\"2023-09-06 00:00:00\",\"house_id\":\"3\",\"price\":\"9600\",\"num\":\"FWQ-2019-002-1\",\"cate_id\":\"27\",\"comment\":\"55\",\"model_id\":\"1904\",\"id\":1917,\"dept_id\":\"108\",\"status\":\"1\"}', '2023-08-31 11:19:47', '1', '管理员');
INSERT INTO `api_log` VALUES (239, 16, 'insert', '1918', '{\"buy_time\":\"2023-09-07 00:00:00\",\"house_id\":\"3\",\"price\":\"850\",\"num\":\"DYJ-2020-001-1\",\"cate_id\":\"30\",\"comment\":\"22\",\"model_id\":\"1905\",\"id\":1918,\"dept_id\":\"102\",\"status\":\"3\"}', '2023-08-31 11:19:47', '1', '管理员');
INSERT INTO `api_log` VALUES (240, 16, 'insert', '1919', '{\"buy_time\":\"2023-09-08 00:00:00\",\"house_id\":\"3\",\"price\":\"25633\",\"num\":\"JC-2022-03-0002-2\",\"cate_id\":\"29\",\"comment\":\"22\",\"model_id\":\"1911\",\"id\":1919,\"dept_id\":\"103\",\"status\":\"1\"}', '2023-08-31 11:19:47', '1', '管理员');
INSERT INTO `api_log` VALUES (241, 1, 'insert', '19', '{\"code\":\"asset_in\",\"name\":\"入库记录\",\"id\":19,\"pk_field\":\"id\",\"excels\":\"AUTO\",\"status\":\"1\"}', '2023-08-31 14:17:06', '1', '管理员');
INSERT INTO `api_log` VALUES (242, 1, 'insert', '20', '{\"code\":\"asset_out\",\"name\":\"出库记录\",\"id\":20,\"group\":\"asset\",\"pk_field\":\"id\",\"excels\":\"AUTO\",\"status\":\"1\"}', '2023-08-31 14:17:26', '1', '管理员');
INSERT INTO `api_log` VALUES (243, 1, 'update', '19', '{\"code\":\"asset_in\",\"name\":\"入库记录\",\"id\":\"19\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"excels\":\"{\\n  \\\"asset_in\\\":{\\n\\t\\\"id\\\":\\\"自增长主键ID\\\",\\n\\t\\\"type\\\":\\\"原由：采购、归还\\\",\\n\\t\\\"cate_id\\\":\\\"资产分类\\\",\\n\\t\\\"num\\\":\\\"资产编号\\\",\\n\\t\\\"house_id\\\":\\\"仓库\\\",\\n\\t\\\"remark\\\":\\\"入库说明\\\",\\n\\t\\\"user_id\\\":\\\"接收人\\\",\\n\\t\\\"recv_time\\\":\\\"接收时间\\\",\\n\\t\\\"imgs\\\":\\\"收货单\\\"\\n  },\\n\\t\\\"@dict\\\":{},\\n\\t\\\"@ds\\\":{},\\n\\t\\\"@excel\\\":{}\\n}\",\"status\":\"1\"}', '2023-08-31 14:17:32', '1', '管理员');
INSERT INTO `api_log` VALUES (244, 1, 'update', '13', '{\"code\":\"asset_category\",\"name\":\"资产分类\",\"id\":\"13\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"name\",\"excels\":\"{\\r\\n\\t\\\"asset_category\\\": {\\r\\n\\t\\t\\\"pid\\\": \\\"上级分类\\\",\\r\\n\\t\\t\\\"name\\\": \\\"分类名称\\\",\\r\\n\\t\\t\\\"nums\\\": \\\"编码规则\\\",\\r\\n\\t\\t\\\"list_sort\\\": \\\"排序\\\",\\r\\n\\t\\t\\\"status\\\": \\\"状态\\\"\\r\\n\\t},\\r\\n\\t\\\"@ds\\\": {\\r\\n\\t\\t\\\"pid\\\": \\\"select 0 value, \'顶级\' label union SELECT id value, name label FROM asset_category\\\"\\r\\n\\t},\\r\\n\\t\\\"@dict\\\": {\\r\\n\\t\\t\\\"status\\\": {\\\"1\\\":\\\"启用\\\",\\\"2\\\":\\\"禁用\\\"}\\r\\n\\t}', '2023-08-31 14:17:48', '1', '管理员');
INSERT INTO `api_log` VALUES (245, 1, 'update', '19', '{\"code\":\"asset_in\",\"name\":\"入库记录\",\"id\":\"19\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"excels\":\"{\\n  \\\"asset_in\\\":{\\n\\t\\\"id\\\":\\\"自增长主键ID\\\",\\n\\t\\\"type\\\":\\\"入库类别\\\",\\n\\t\\\"cate_id\\\":\\\"资产分类\\\",\\n\\t\\\"num\\\":\\\"资产编号\\\",\\n\\t\\\"house_id\\\":\\\"仓库\\\",\\n\\t\\\"remark\\\":\\\"入库说明\\\",\\n\\t\\\"user_id\\\":\\\"接收人\\\",\\n\\t\\\"recv_time\\\":\\\"接收时间\\\"\\n  },\\n\\t\\\"@dict\\\":{\\n\\\"type\\\":{\\\"1\\\":\\\"采购\\\",\\\"2\\\":\\\"归还\\\"}\\n},\\n\\t\\\"@ds\\\":{\\n\\\"cate_id\\\":\\\"@asset_category_select\\\",\\n\\\"house_id\\\":\\\"@asset_warehouse_select\\\",\\n\\\"user_id\\\":\\\"@user_select\\', '2023-08-31 14:26:19', '1', '管理员');
INSERT INTO `api_log` VALUES (246, 1, 'update', '19', '{\"code\":\"asset_in\",\"name\":\"入库记录\",\"id\":\"19\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"excels\":\"{\\n  \\\"asset_in\\\":{\\n\\t\\\"type\\\":\\\"入库类别\\\",\\n\\t\\\"cate_id\\\":\\\"资产分类\\\",\\n\\t\\\"num\\\":\\\"资产编号\\\",\\n\\t\\\"house_id\\\":\\\"仓库\\\",\\n\\t\\\"remark\\\":\\\"入库说明\\\",\\n\\t\\\"user_id\\\":\\\"接收人\\\",\\n\\t\\\"recv_time\\\":\\\"接收时间\\\"\\n  },\\n\\t\\\"@dict\\\":{\\n\\\"type\\\":{\\\"1\\\":\\\"采购\\\",\\\"2\\\":\\\"归还\\\"}\\n},\\n\\t\\\"@ds\\\":{\\n\\\"cate_id\\\":\\\"@asset_category_select\\\",\\n\\\"house_id\\\":\\\"@warehouse_select\\\",\\n\\\"user_id\\\":\\\"@user_select\\\"\\n},\\n\\t\\\"@excel\\\":{}\\n}\",\"s', '2023-08-31 14:26:51', '1', '管理员');
INSERT INTO `api_log` VALUES (247, 19, 'insert', '59', '{\"imgs\":\"/profile/upload/2023/08/31/微信截图_20220217162549_20230831142844A001.png\",\"house_id\":\"1\",\"recv_time\":\"2023-08-31 14:28:45.947\",\"user_id\":\"1\",\"num\":\"435432545\",\"cate_id\":\"30\",\"id\":59,\"type\":\"1\"}', '2023-08-31 14:28:46', '1', '管理员');
INSERT INTO `api_log` VALUES (248, 16, 'insert', '1920', '{\"buy_time\":\"2023-08-31 14:28:45.955\",\"house_id\":\"1\",\"num\":\"435432545\",\"cate_id\":\"30\",\"model_id\":\"1905\",\"id\":1920,\"status\":\"1\"}', '2023-08-31 14:28:46', '1', '管理员');
INSERT INTO `api_log` VALUES (250, 1, 'insert', '21', '{\"code\":\"asset_maintain\",\"name\":\"资产维保\",\"id\":21,\"pk_field\":\"id\",\"excels\":\"AUTO\",\"status\":\"1\"}', '2023-08-31 18:02:54', '1', '管理员');
INSERT INTO `api_log` VALUES (251, 21, 'insert', '1', '{\"reason\":\"999\",\"user_id\":\"1\",\"cate_id\":\"27\",\"asset_id\":\"1917\",\"id\":1,\"type\":\"1\",\"dept_id\":\"103\",\"status\":\"1\"}', '2023-08-31 18:03:07', '1', '管理员');
INSERT INTO `api_log` VALUES (252, 1, 'update', '21', '{\"code\":\"asset_maintain\",\"name\":\"资产维保\",\"id\":\"21\",\"is_default\":\"N\",\"pk_field\":\"id\",\"excels\":\"{\\n  \\\"asset_maintain\\\":{\\n\\t\\\"cate_id\\\":\\\"资产分类\\\",\\n\\t\\\"asset_id\\\":\\\"资产编号\\\",\\n\\t\\\"type\\\":\\\"维保类型\\\",\\n\\t\\\"reason\\\":\\\"维保原由\\\",\\n\\t\\\"status\\\":\\\"维保状态\\\",\\n\\t\\\"money\\\":\\\"费用\\\",\\n\\t\\\"comment\\\":\\\"说明\\\",\\n\\t\\\"user_id\\\":\\\"申请人\\\",\\n\\t\\\"dept_id\\\":\\\"申请部门\\\"\\n  },\\n\\\"@dict\\\":{\\n\\\"type\\\":{\\\"1\\\":\\\"维修\\\",\\\"2\\\":\\\"维保\\\"}\\n},\\n\\t\\\"@ds\\\":{\\n\\\"cate_id\\\":\\\"@asset_category_select\\\",\\n\\\"asset_id\\\":\\\"SELECT id value, num label FROM asset', '2023-09-01 08:53:33', '1', '管理员');
INSERT INTO `api_log` VALUES (253, 1, 'update', '21', '{\"code\":\"asset_maintain\",\"name\":\"资产维保\",\"id\":\"21\",\"is_default\":\"N\",\"pk_field\":\"id\",\"excels\":\"{\\n  \\\"asset_maintain\\\":{\\n\\t\\\"cate_id\\\":\\\"资产分类\\\",\\n\\t\\\"asset_id\\\":\\\"资产编号\\\",\\n\\t\\\"type\\\":\\\"维保类型\\\",\\n\\t\\\"reason\\\":\\\"维保原由\\\",\\n\\t\\\"status\\\":\\\"维保状态\\\",\\n\\t\\\"money\\\":\\\"费用\\\",\\n\\t\\\"comment\\\":\\\"说明\\\",\\n\\t\\\"user_id\\\":\\\"申请人\\\",\\n\\t\\\"dept_id\\\":\\\"申请部门\\\"\\n  },\\n\\\"@dict\\\":{\\n\\\"type\\\":{\\\"1\\\":\\\"维修\\\",\\\"2\\\":\\\"维保\\\"},\\n\\\"status\\\":{\\\"1\\\":\\\"未开始\\\",\\\"2\\\":\\\"进行中\\\",\\\"3\\\":\\\"已完成\\\",\\\"4\\\":\\\"已报废\\\"}\\n},\\n\\t\\\"@ds\\\":{\\n\\\"cate_id\\\":\\\"@asset_c', '2023-09-01 08:54:56', '1', '管理员');
INSERT INTO `api_log` VALUES (254, 1, 'update', '21', '{\"code\":\"asset_maintain\",\"name\":\"资产维保\",\"id\":\"21\",\"is_default\":\"N\",\"pk_field\":\"id\",\"excels\":\"{\\n  \\\"asset_maintain\\\":{\\n\\t\\\"cate_id\\\":\\\"资产分类\\\",\\n\\t\\\"asset_id\\\":\\\"资产编号\\\",\\n\\t\\\"type\\\":\\\"维保类型\\\",\\n\\t\\\"reason\\\":\\\"维保原由\\\",\\n\\t\\\"status\\\":\\\"维保状态\\\",\\n\\t\\\"money\\\":\\\"费用\\\",\\n\\t\\\"comment\\\":\\\"说明\\\",\\n\\t\\\"user_id\\\":\\\"申请人\\\",\\n\\t\\\"dept_id\\\":\\\"申请部门\\\"\\n  },\\n\\\"@dict\\\":{\\n\\\"type\\\":{\\\"1\\\":\\\"维修\\\",\\\"2\\\":\\\"维保\\\"},\\n\\\"status\\\":{\\\"1\\\":\\\"未开始\\\",\\\"2\\\":\\\"进行中\\\",\\\"3\\\":\\\"已完成\\\",\\\"4\\\":\\\"无法修复\\\"}\\n},\\n\\t\\\"@ds\\\":{\\n\\\"cate_id\\\":\\\"@asset_', '2023-09-01 08:55:29', '1', '管理员');
INSERT INTO `api_log` VALUES (255, 1, 'update', '20', '{\"code\":\"asset_out\",\"name\":\"出库记录\",\"id\":\"20\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"excels\":\"AUTO\",\"status\":\"1\"}', '2023-09-01 08:55:37', '1', '管理员');
INSERT INTO `api_log` VALUES (256, 1, 'update', '20', '{\"code\":\"asset_out\",\"name\":\"出库记录\",\"id\":\"20\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"excels\":\"{\\n  \\\"asset_out\\\":{\\n\\t\\\"id\\\":\\\"自增长主键ID\\\",\\n\\t\\\"type\\\":\\\"出库类别\\\",\\n\\t\\\"cate_id\\\":\\\"资产分类\\\",\\n\\t\\\"asset_id\\\":\\\"资产编号\\\",\\n\\t\\\"user_id\\\":\\\"接收人\\\",\\n\\t\\\"remark\\\":\\\"出库说明\\\",\\n\\t\\\"out_time\\\":\\\"出库时间\\\"\\n  },\\n\\t\\\"@dict\\\":{},\\n\\t\\\"@ds\\\":{},\\n\\t\\\"@excel\\\":{}\\n}\",\"status\":\"1\"}', '2023-09-01 08:56:40', '1', '管理员');
INSERT INTO `api_log` VALUES (257, 1, 'update', '20', '{\"code\":\"asset_out\",\"name\":\"出库记录\",\"id\":\"20\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"excels\":\"{\\n  \\\"asset_out\\\":{\\n\\t\\\"id\\\":\\\"自增长主键ID\\\",\\n\\t\\\"type\\\":\\\"出库类别\\\",\\n\\t\\\"cate_id\\\":\\\"资产分类\\\",\\n\\t\\\"asset_id\\\":\\\"资产编号\\\",\\n\\t\\\"user_id\\\":\\\"接收人\\\",\\n\\t\\\"remark\\\":\\\"出库说明\\\",\\n\\t\\\"out_time\\\":\\\"出库时间\\\"\\n  },\\n\\\"@dict\\\":{\\n\\\"type\\\":{\\\"1\\\":\\\"借用\\\",\\\"2\\\":\\\"报废\\\",\\\"3\\\":\\\"出售\\\"}\\n},\\n\\t\\\"@ds\\\":{\\n\\\"cate_id\\\":\\\"@asset_category_select\\\",\\n\\\"asset_id\\\":\\\"SELECT id value, num label FROM asset_info\\\",\\n\\\"user_id\\\":\\\"@', '2023-09-01 08:57:43', '1', '管理员');
INSERT INTO `api_log` VALUES (258, 1, 'update', '21', '{\"code\":\"asset_maintain\",\"name\":\"资产维保\",\"id\":\"21\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"excels\":\"{\\n  \\\"asset_maintain\\\":{\\n\\t\\\"cate_id\\\":\\\"资产分类\\\",\\n\\t\\\"asset_id\\\":\\\"资产编号\\\",\\n\\t\\\"type\\\":\\\"维保类型\\\",\\n\\t\\\"reason\\\":\\\"维保原由\\\",\\n\\t\\\"status\\\":\\\"维保状态\\\",\\n\\t\\\"money\\\":\\\"费用\\\",\\n\\t\\\"comment\\\":\\\"说明\\\",\\n\\t\\\"user_id\\\":\\\"申请人\\\",\\n\\t\\\"dept_id\\\":\\\"申请部门\\\"\\n  },\\n\\\"@dict\\\":{\\n\\\"type\\\":{\\\"1\\\":\\\"维修\\\",\\\"2\\\":\\\"维保\\\"},\\n\\\"status\\\":{\\\"1\\\":\\\"未开始\\\",\\\"2\\\":\\\"进行中\\\",\\\"3\\\":\\\"已完成\\\",\\\"4\\\":\\\"无法修复\\\"}\\n},\\n\\t\\\"@ds\\\":{\\n\\\"cat', '2023-09-01 08:57:53', '1', '管理员');
INSERT INTO `api_log` VALUES (260, 1, 'insert', '22', '{\"code\":\"asset_use\",\"name\":\"资产使用申请\",\"id\":22,\"group\":\"asset\",\"pk_field\":\"id\",\"excels\":\"AUTO\",\"status\":\"1\"}', '2023-09-01 11:35:31', '1', '管理员');
INSERT INTO `api_log` VALUES (261, 3, 'insert', '15', '{\"read\":\"Y\",\"role_id\":\"1\",\"model_id\":22,\"id\":15,\"write\":\"Y\"}', '2023-09-01 11:35:31', '1', '管理员');
INSERT INTO `api_log` VALUES (262, 3, 'insert', '16', '{\"read\":\"Y\",\"role_id\":\"2\",\"model_id\":22,\"id\":16,\"write\":\"Y\"}', '2023-09-01 11:35:31', '1', '管理员');
INSERT INTO `api_log` VALUES (263, 22, 'insert', '27', '{\"reason\":\"14324234\",\"amount\":\"2\",\"user_id\":\"1\",\"apply_status\":\"1\",\"cate_id\":\"27\",\"apply_time\":\"2023-09-01 11:35:42.053\",\"model_id\":\"1904\",\"id\":27,\"dept_id\":\"103\"}', '2023-09-01 11:35:42', '1', '管理员');
INSERT INTO `api_log` VALUES (265, 1, 'insert', '23', '{\"code\":\"asset_stock\",\"name\":\"资产盘点\",\"id\":23,\"group\":\"asset\",\"pk_field\":\"id\",\"excels\":\"AUTO\",\"status\":\"1\"}', '2023-09-01 11:48:34', '1', '管理员');
INSERT INTO `api_log` VALUES (266, 3, 'insert', '17', '{\"read\":\"Y\",\"role_id\":\"1\",\"model_id\":23,\"id\":17,\"write\":\"Y\"}', '2023-09-01 11:48:34', '1', '管理员');
INSERT INTO `api_log` VALUES (267, 3, 'insert', '18', '{\"read\":\"Y\",\"role_id\":\"2\",\"model_id\":23,\"id\":18,\"write\":\"Y\"}', '2023-09-01 11:48:34', '1', '管理员');
INSERT INTO `api_log` VALUES (268, 1, 'insert', '24', '{\"code\":\"asset_stock_detail\",\"name\":\"资产盘点明细\",\"id\":24,\"group\":\"asset\",\"pk_field\":\"id\",\"status\":\"1\"}', '2023-09-01 11:48:56', '1', '管理员');
INSERT INTO `api_log` VALUES (269, 3, 'insert', '19', '{\"read\":\"Y\",\"role_id\":\"1\",\"model_id\":24,\"id\":19,\"write\":\"Y\"}', '2023-09-01 11:48:56', '1', '管理员');
INSERT INTO `api_log` VALUES (270, 3, 'insert', '20', '{\"read\":\"Y\",\"role_id\":\"2\",\"model_id\":24,\"id\":20,\"write\":\"Y\"}', '2023-09-01 11:48:56', '1', '管理员');
INSERT INTO `api_log` VALUES (271, 1, 'update', '24', '{\"code\":\"asset_stock_detail\",\"name\":\"资产盘点明细\",\"id\":\"24\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"stock_id,asset_id\",\"status\":\"1\"}', '2023-09-01 11:49:18', '1', '管理员');
INSERT INTO `api_log` VALUES (272, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":19,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 11:49:18', '1', '管理员');
INSERT INTO `api_log` VALUES (273, 3, 'update', '19', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":19,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 11:49:18', '1', '管理员');
INSERT INTO `api_log` VALUES (274, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":20,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 11:49:18', '1', '管理员');
INSERT INTO `api_log` VALUES (275, 3, 'update', '20', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":20,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 11:49:18', '1', '管理员');
INSERT INTO `api_log` VALUES (276, 23, 'insert', '9', '{\"start_time\":\"2023-09-02\",\"user_id\":\"35245\",\"name\":\"4545\",\"end_time\":\"2023-09-02\",\"remark\":\"4235\",\"id\":9}', '2023-09-01 13:41:50', '1', '管理员');
INSERT INTO `api_log` VALUES (277, 23, 'update', '9', '{\"start_time\":\"2023-09-02T00:00:00\",\"sale_count\":\"1\",\"asset_worth\":\"0\",\"user_id\":\"1\",\"name\":\"4545\",\"end_time\":\"2023-09-02T00:00:00\",\"purchase_count\":\"1\",\"remark\":\"4235\",\"id\":\"9\"}', '2023-09-01 13:44:41', '1', '管理员');
INSERT INTO `api_log` VALUES (278, 2, 'update', '5', '{\"action\":\"AI\",\"remark\":\"自动生成采购订单号\",\"id\":\"5\",\"model_id\":\"17\",\"is_default\":\"N\",\"content\":\"function exec(data){\\n  var sql = \\\"UPDATE asset_purchase set num = \\\";\\n  sql += \\\"(select max(num) from( select max(num)+1 num from asset_purchase where num like concat(DATE_FORMAT(now(),\'%Y%m%d\'), \'%\') union all select concat(DATE_FORMAT(now(),\'%Y%m%d\'), \'001\') as num ) t)\\\";\\n  sql += \\\" WHERE id=\\\"+data.id;\\n  return sql;\\n}\",\"status\":\"2\"}', '2023-09-01 14:00:57', '1', '管理员');
INSERT INTO `api_log` VALUES (279, 23, 'update', '9', '{\"start_time\":\"2023-09-01 12:00:00\",\"sale_count\":\"1\",\"asset_worth\":\"0\",\"user_id\":\"1\",\"name\":\"4545\",\"end_time\":\"2023-09-02 10:00:00\",\"purchase_count\":\"1\",\"remark\":\"4235\",\"id\":\"9\"}', '2023-09-01 14:05:46', '1', '管理员');
INSERT INTO `api_log` VALUES (280, 1, 'update', '24', '{\"code\":\"asset_stock_detail\",\"name\":\"资产盘点明细\",\"id\":\"24\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"stock_id,asset_id\",\"excels\":\"AUTO\",\"status\":\"1\"}', '2023-09-01 14:16:45', '1', '管理员');
INSERT INTO `api_log` VALUES (281, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":19,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 14:16:45', '1', '管理员');
INSERT INTO `api_log` VALUES (282, 3, 'update', '19', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":19,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 14:16:45', '1', '管理员');
INSERT INTO `api_log` VALUES (283, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":20,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 14:16:45', '1', '管理员');
INSERT INTO `api_log` VALUES (284, 3, 'update', '20', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":20,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 14:16:45', '1', '管理员');
INSERT INTO `api_log` VALUES (285, 1, 'update', '24', '{\"code\":\"asset_stock_detail\",\"name\":\"资产盘点明细\",\"id\":\"24\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"stock_id,asset_id\",\"excels\":\"{\\n  \\\"asset_stock_detail\\\":{\\n\\t\\\"id\\\":\\\"自增长主键ID\\\",\\n\\t\\\"stock_id\\\":\\\"盘点主键\\\",\\n\\t\\\"asset_id\\\":\\\"资产编号\\\",\\n\\t\\\"status\\\":\\\"当前状态\\\",\\n\\t\\\"price\\\":\\\"当前价值\\\"\\n  },\\n\\t\\\"@dict\\\":{},\\n\\t\\\"@ds\\\":{},\\n\\t\\\"@excel\\\":{}\\n}\",\"status\":\"1\"}', '2023-09-01 14:20:37', '1', '管理员');
INSERT INTO `api_log` VALUES (286, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":19,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 14:20:37', '1', '管理员');
INSERT INTO `api_log` VALUES (287, 3, 'update', '19', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":19,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 14:20:37', '1', '管理员');
INSERT INTO `api_log` VALUES (288, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":20,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 14:20:37', '1', '管理员');
INSERT INTO `api_log` VALUES (289, 3, 'update', '20', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":20,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 14:20:37', '1', '管理员');
INSERT INTO `api_log` VALUES (290, 1, 'update', '24', '{\"code\":\"asset_stock_detail\",\"name\":\"资产盘点明细\",\"id\":\"24\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"stock_id,asset_id\",\"excels\":\"{\\n  \\\"asset_stock_detail\\\":{\\n\\t\\\"id\\\":\\\"自增长主键ID\\\",\\n\\t\\\"stock_id\\\":\\\"盘点主键\\\",\\n\\t\\\"asset_id\\\":\\\"资产编号\\\",\\n\\t\\\"status\\\":\\\"当前状态\\\",\\n\\t\\\"price\\\":\\\"当前价值\\\"\\n  },,\\n\\t\\\"@dict\\\": {\\n\\t\\t\\\"status\\\": {\\\"1\\\":\\\"闲置\\\", \\\"2\\\":\\\"在用\\\", \\\"3\\\":\\\"损坏\\\",\\\"4\\\":\\\"维修\\\",\\\"5\\\":\\\"报废\\\"}\\n\\t},\\n\\t\\\"@ds\\\":{\\n},\\n\\t\\\"@excel\\\":{}\\n}\",\"status\":\"1\"}', '2023-09-01 14:21:32', '1', '管理员');
INSERT INTO `api_log` VALUES (291, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":19,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 14:21:32', '1', '管理员');
INSERT INTO `api_log` VALUES (292, 3, 'update', '19', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":19,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 14:21:32', '1', '管理员');
INSERT INTO `api_log` VALUES (293, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":20,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 14:21:32', '1', '管理员');
INSERT INTO `api_log` VALUES (294, 3, 'update', '20', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":20,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 14:21:32', '1', '管理员');
INSERT INTO `api_log` VALUES (295, 1, 'update', '24', '{\"code\":\"asset_stock_detail\",\"name\":\"资产盘点明细\",\"id\":\"24\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"stock_id,asset_id\",\"excels\":\"AUTO\",\"status\":\"1\"}', '2023-09-01 14:27:44', '1', '管理员');
INSERT INTO `api_log` VALUES (296, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":19,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 14:27:44', '1', '管理员');
INSERT INTO `api_log` VALUES (297, 3, 'update', '19', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":19,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 14:27:44', '1', '管理员');
INSERT INTO `api_log` VALUES (298, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":20,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 14:27:44', '1', '管理员');
INSERT INTO `api_log` VALUES (299, 3, 'update', '20', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":20,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 14:27:44', '1', '管理员');
INSERT INTO `api_log` VALUES (300, 1, 'update', '24', '{\"code\":\"asset_stock_detail\",\"name\":\"资产盘点明细\",\"id\":\"24\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"stock_id,asset_id\",\"excels\":\"{\\n  \\\"asset_stock_detail\\\":{\\n\\t\\\"id\\\":\\\"自增长主键ID\\\",\\n\\t\\\"stock_id\\\":\\\"盘点主键\\\",\\n\\t\\\"asset_id\\\":\\\"资产ID\\\",\\n\\t\\\"status\\\":\\\"当前状态\\\",\\n\\t\\\"price\\\":\\\"当前价值\\\",\\n\\t\\\"cate_name\\\":\\\"资产分类\\\",\\n\\t\\\"model_name\\\":\\\"资产型号\\\",\\n\\t\\\"num\\\":\\\"资产编码\\\"\\n  },\\n\\t\\\"@dict\\\": {\\n\\t\\t\\\"status\\\": {\\\"1\\\":\\\"闲置\\\", \\\"2\\\":\\\"在用\\\", \\\"3\\\":\\\"损坏\\\",\\\"4\\\":\\\"维修\\\",\\\"5\\\":\\\"报废\\\"}\\n\\t},\\n\\t\\\"@ds\\', '2023-09-01 14:28:38', '1', '管理员');
INSERT INTO `api_log` VALUES (301, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":19,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 14:28:38', '1', '管理员');
INSERT INTO `api_log` VALUES (302, 3, 'update', '19', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":19,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 14:28:38', '1', '管理员');
INSERT INTO `api_log` VALUES (303, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":20,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 14:28:38', '1', '管理员');
INSERT INTO `api_log` VALUES (304, 3, 'update', '20', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":20,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 14:28:38', '1', '管理员');
INSERT INTO `api_log` VALUES (305, 2, 'insert', '6', '{\"action\":\"AI\",\"remark\":\"新增盘点任务后，将资产信息加入进来，方便后期有针对性的操作\",\"model_id\":\"23\",\"id\":6,\"content\":\"function exec(data){\\n  return [\\\"insert into asset_stock_detail(id,stock_id, asset_id, num, status, price, cate_name, model_name) select null, \\\"+data.id+\\\", t.id, t.num,  null, null, c.name, m.name from asset_info t join asset_category c on c.id = t.cate_id join asset_model m on m.id = t.model_id where t.status in (\'1\',\'2\',\'3\',\'4\')\\\"];\\n}\",\"status\":\"1\"}', '2023-09-01 14:34:54', '1', '管理员');
INSERT INTO `api_log` VALUES (306, 23, 'delete', NULL, '9', '2023-09-01 14:36:25', '1', '管理员');
INSERT INTO `api_log` VALUES (309, 23, 'insert', '12', '{\"start_time\":\"2023-09-01 14:37:38\",\"user_id\":\"1\",\"name\":\"2023-09-01\",\"end_time\":\"2023-09-02 00:00:00\",\"id\":12}', '2023-09-01 14:38:04', '1', '管理员');
INSERT INTO `api_log` VALUES (310, 23, 'event', '6', '{\"start_time\":\"2023-09-01 14:37:38\",\"user_id\":\"1\",\"name\":\"2023-09-01\",\"end_time\":\"2023-09-02 00:00:00\",\"id\":12}', '2023-09-01 14:38:04', '1', '管理员');
INSERT INTO `api_log` VALUES (311, 24, 'update', '15', '{\"model_name\":\"DYJ-2020-001\",\"num\":\"435432545\",\"cate_name\":\"打印机\",\"id\":\"15\",\"asset_id\":\"1920\",\"stock_id\":\"12\",\"status\":\"2\"}', '2023-09-01 14:52:09', '1', '管理员');
INSERT INTO `api_log` VALUES (312, 24, 'update', '15', '{\"model_name\":\"DYJ-2020-001\",\"price\":\"56\",\"num\":\"435432545\",\"cate_name\":\"打印机\",\"id\":\"15\",\"asset_id\":\"1920\",\"stock_id\":\"12\",\"status\":\"2\"}', '2023-09-01 14:52:12', '1', '管理员');
INSERT INTO `api_log` VALUES (313, 2, 'update', '6', '{\"action\":\"AI\",\"remark\":\"新增盘点任务后，将资产信息加入进来，方便后期有针对性的操作\",\"id\":\"6\",\"model_id\":\"23\",\"is_default\":\"N\",\"content\":\"function exec(data){\\n  return [\\\"insert into asset_stock_detail(id,stock_id, asset_id, num, status, price1, price, cate_name, model_name) select null, \\\"+data.id+\\\", t.id, t.num,  null, t.price, null, c.name, m.name from asset_info t join asset_category c on c.id = t.cate_id join asset_model m on m.id = t.model_id where t.status in (\'1\',\'2\',\'3\',\'4\')\\\"];\\n}\",\"status\":\"1\"}', '2023-09-01 14:54:00', '1', '管理员');
INSERT INTO `api_log` VALUES (314, 1, 'update', '24', '{\"code\":\"asset_stock_detail\",\"name\":\"资产盘点明细\",\"id\":\"24\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"stock_id,asset_id\",\"excels\":\"{\\n  \\\"asset_stock_detail\\\":{\\n\\t\\\"id\\\":\\\"自增长主键ID\\\",\\n\\t\\\"stock_id\\\":\\\"盘点主键\\\",\\n\\t\\\"asset_id\\\":\\\"资产ID\\\",\\n\\t\\\"status\\\":\\\"当前状态\\\",\\n\\t\\\"price\\\":\\\"当前价值\\\",\\n\\t\\\"cate_name\\\":\\\"资产分类\\\",\\n\\t\\\"model_name\\\":\\\"资产型号\\\",\\n\\t\\\"num\\\":\\\"资产编码\\\"\\n  },\\n\\t\\\"@dict\\\": {\\n\\t\\t\\\"status\\\": {\\\"1\\\":\\\"闲置\\\", \\\"2\\\":\\\"在用\\\", \\\"3\\\":\\\"损坏\\\",\\\"4\\\":\\\"维修\\\",\\\"5\\\":\\\"报废\\\"}\\n\\t},\\n\\t\\\"@ds\\', '2023-09-01 14:54:19', '1', '管理员');
INSERT INTO `api_log` VALUES (315, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":19,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 14:54:19', '1', '管理员');
INSERT INTO `api_log` VALUES (316, 3, 'update', '19', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":19,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 14:54:19', '1', '管理员');
INSERT INTO `api_log` VALUES (317, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":20,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 14:54:19', '1', '管理员');
INSERT INTO `api_log` VALUES (318, 3, 'update', '20', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":20,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-01 14:54:19', '1', '管理员');
INSERT INTO `api_log` VALUES (319, 24, 'update', '14', '{\"model_name\":\"JC-2022-03-0002\",\"price\":\"54\",\"num\":\"JC-2022-03-0002-2\",\"cate_name\":\"机床\",\"id\":\"14\",\"asset_id\":\"1919\",\"stock_id\":\"12\"}', '2023-09-01 14:58:26', '1', '管理员');
INSERT INTO `api_log` VALUES (320, 24, 'update', '14', '{\"model_name\":\"JC-2022-03-0002\",\"price\":\"54\",\"num\":\"JC-2022-03-0002-2\",\"cate_name\":\"机床\",\"id\":\"14\",\"asset_id\":\"1919\",\"stock_id\":\"12\",\"status\":\"3\"}', '2023-09-01 14:58:29', '1', '管理员');
INSERT INTO `api_log` VALUES (321, 24, 'update', '13', '{\"model_name\":\"DYJ-2020-001\",\"num\":\"DYJ-2020-001-1\",\"cate_name\":\"打印机\",\"id\":\"13\",\"asset_id\":\"1918\",\"stock_id\":\"12\",\"status\":\"2\"}', '2023-09-01 14:58:30', '1', '管理员');
INSERT INTO `api_log` VALUES (322, 24, 'update', '13', '{\"model_name\":\"DYJ-2020-001\",\"price\":\"23\",\"num\":\"DYJ-2020-001-1\",\"cate_name\":\"打印机\",\"id\":\"13\",\"asset_id\":\"1918\",\"stock_id\":\"12\",\"status\":\"2\"}', '2023-09-01 14:58:32', '1', '管理员');
INSERT INTO `api_log` VALUES (323, 24, 'update', '12', '{\"model_name\":\"FWQ-2019-002\",\"num\":\"FWQ-2019-002-1\",\"cate_name\":\"服务器\",\"id\":\"12\",\"asset_id\":\"1917\",\"stock_id\":\"12\",\"status\":\"3\"}', '2023-09-01 14:58:34', '1', '管理员');
INSERT INTO `api_log` VALUES (324, 24, 'update', '12', '{\"model_name\":\"FWQ-2019-002\",\"price\":\"54\",\"num\":\"FWQ-2019-002-1\",\"cate_name\":\"服务器\",\"id\":\"12\",\"asset_id\":\"1917\",\"stock_id\":\"12\",\"status\":\"3\"}', '2023-09-01 14:58:36', '1', '管理员');
INSERT INTO `api_log` VALUES (325, 2, 'update', '6', '{\"action\":\"AI\",\"remark\":\"新增盘点任务后，将资产信息加入进来，方便后期有针对性的操作\",\"id\":\"6\",\"model_id\":\"23\",\"is_default\":\"N\",\"content\":\"function exec(data){\\n  return [\\\"insert into asset_stock_detail(id,stock_id, asset_id, num, status, price1, price, house_id, user_id, cate_name, model_name) select null, \\\"+data.id+\\\", t.id, t.num,  null, t.price, null, t.house_id, t.user_id, c.name, m.name from asset_info t join asset_category c on c.id = t.cate_id join asset_model m on m.id = t.model_id where t.status in (\'1\',\'2\',\'3\',', '2023-09-01 15:36:17', '1', '管理员');
INSERT INTO `api_log` VALUES (326, 23, 'delete', NULL, '12', '2023-09-01 15:37:12', '1', '管理员');
INSERT INTO `api_log` VALUES (327, 24, 'delete', NULL, '12,13,14,15', '2023-09-01 15:37:12', '1', '管理员');
INSERT INTO `api_log` VALUES (328, 23, 'insert', '13', '{\"start_time\":\"2023-08-31 00:00:00\",\"user_id\":\"1\",\"name\":\"2023-09-01\",\"end_time\":\"2023-09-01 00:00:00\",\"id\":13}', '2023-09-01 15:37:31', '1', '管理员');
INSERT INTO `api_log` VALUES (329, 23, 'event', '6', '{\"start_time\":\"2023-08-31 00:00:00\",\"user_id\":\"1\",\"name\":\"2023-09-01\",\"end_time\":\"2023-09-01 00:00:00\",\"id\":13}', '2023-09-01 15:37:31', '1', '管理员');
INSERT INTO `api_log` VALUES (330, 2, 'update', '6', '{\"action\":\"AI\",\"remark\":\"新增盘点任务后，将资产信息加入进来，方便后期有针对性的操作\",\"id\":\"6\",\"model_id\":\"23\",\"is_default\":\"N\",\"content\":\"function exec(data){\\n  return [\\\"insert into asset_stock_detail(id,stock_id, asset_id, num, status, price1, price, house_id, user_id, cate_name, model_name, stock_time) select null, \\\"+data.id+\\\", t.id, t.num,  null, t.price, null, t.house_id, t.user_id, c.name, m.name, null from asset_info t join asset_category c on c.id = t.cate_id join asset_model m on m.id = t.model_id where t.statu', '2023-09-01 15:40:02', '1', '管理员');
INSERT INTO `api_log` VALUES (331, 24, 'update', '22', '{\"model_name\":\"DYJ-2020-001\",\"price\":\"56\",\"num\":\"435432545\",\"cate_name\":\"打印机\",\"id\":\"22\",\"asset_id\":\"1920\",\"stock_id\":\"13\"}', '2023-09-01 15:40:08', '1', '管理员');
INSERT INTO `api_log` VALUES (332, 23, 'update', '13', '{\"start_time\":\"2023-08-31T00:00:00\",\"asset_worth\":\"0\",\"user_id\":\"1\",\"name\":\"2023-09-01\",\"end_time\":\"2023-09-01T00:00:00\",\"id\":\"13\"}', '2023-09-01 16:04:02', '1', '管理员');
INSERT INTO `api_log` VALUES (333, 23, 'update', '13', '{\"start_time\":\"2023-08-31T00:00:00\",\"asset_worth\":\"0\",\"user_id\":\"1\",\"name\":\"2023-09-01\",\"end_time\":\"2023-09-01T00:00:00\",\"id\":\"13\"}', '2023-09-01 16:04:07', '1', '管理员');
INSERT INTO `api_log` VALUES (334, 1, 'update', '23', '{\"code\":\"asset_stock\",\"name\":\"资产盘点\",\"id\":\"23\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"excels\":\"{\\n  \\\"asset_stock\\\":{\\n\\t\\\"name\\\":\\\"代号\\\",\\n\\t\\\"asset_count\\\":\\\"资产总数\\\",\\n\\t\\\"asset_worth\\\":\\\"资产总值\\\",\\n\\t\\\"scrap_count\\\":\\\"报废总数\\\",\\n\\t\\\"scrap_worth\\\":\\\"报废总值\\\",\\n\\t\\\"maintain_count\\\":\\\"维保次数\\\",\\n\\t\\\"maintain_price\\\":\\\"维保费用\\\",\\n\\t\\\"purchase_count\\\":\\\"采购次数\\\",\\n\\t\\\"purchase_worth\\\":\\\"采购总值\\\",\\n\\t\\\"sale_worth\\\":\\\"出售总值\\\",\\n\\t\\\"sale_count\\\":\\\"出售数量\\\",\\n\\t\\\"remark\\\":\\\"盘点说明\\\",\\n\\t\\\"start_time\\\":\\\"开始时间\\\",', '2023-09-01 16:04:59', '1', '管理员');
INSERT INTO `api_log` VALUES (335, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":17,\"model_id\":\"23\",\"write\":\"Y\"}', '2023-09-01 16:04:59', '1', '管理员');
INSERT INTO `api_log` VALUES (336, 3, 'update', '17', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":17,\"model_id\":\"23\",\"write\":\"Y\"}', '2023-09-01 16:04:59', '1', '管理员');
INSERT INTO `api_log` VALUES (337, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":18,\"model_id\":\"23\",\"write\":\"Y\"}', '2023-09-01 16:04:59', '1', '管理员');
INSERT INTO `api_log` VALUES (338, 3, 'update', '18', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":18,\"model_id\":\"23\",\"write\":\"Y\"}', '2023-09-01 16:04:59', '1', '管理员');
INSERT INTO `api_log` VALUES (339, 23, 'update', '13', '{\"start_time\":\"2023-08-31T00:00:00\",\"asset_worth\":\"0\",\"user_id\":\"1\",\"name\":\"2023-09-01\",\"end_time\":\"2023-09-01T00:00:00\",\"id\":\"13\",\"status\":\"2\"}', '2023-09-01 16:05:17', '1', '管理员');
INSERT INTO `api_log` VALUES (340, 23, 'update', '13', '{\"start_time\":\"2023-08-31T00:00:00\",\"asset_worth\":\"0\",\"user_id\":\"1\",\"name\":\"2023-09-01\",\"end_time\":\"2023-09-01T00:00:00\",\"id\":\"13\",\"status\":\"2\"}', '2023-09-01 16:08:46', '1', '管理员');
INSERT INTO `api_log` VALUES (341, 23, 'update', '13', '{\"start_time\":\"2023-08-31T00:00:00\",\"asset_worth\":\"0\",\"user_id\":\"1\",\"name\":\"2023-09-01\",\"end_time\":\"2023-09-01T00:00:00\",\"id\":\"13\",\"status\":\"3\"}', '2023-09-01 16:08:52', '1', '管理员');
INSERT INTO `api_log` VALUES (342, 23, 'update', '13', '{\"start_time\":\"2023-08-31T00:00:00\",\"asset_worth\":\"0\",\"user_id\":\"1\",\"name\":\"2023-09-01\",\"end_time\":\"2023-09-01T00:00:00\",\"id\":\"13\",\"status\":\"4\"}', '2023-09-01 16:08:56', '1', '管理员');
INSERT INTO `api_log` VALUES (343, 6, 'insert', '7', '{\"code\":\"asset_stock_undone\",\"name\":\"统计未盘点资产\",\"remark\":\"统计未盘点资产,状态或价值为空\",\"id\":7,\"type\":\"count\",\"content\":\"function exec(data){\\n  return \\\"select count(id) total from asset_stock_detail where stock_id= \\\" + data.stockId + \\\" and ( status is null or price is null )\\\";\\n}\",\"group\":\"asset\"}', '2023-09-01 16:14:50', '1', '管理员');
INSERT INTO `api_log` VALUES (344, 23, 'update', '13', '{\"start_time\":\"2023-08-31T00:00:00\",\"asset_worth\":\"0\",\"user_id\":\"1\",\"name\":\"2023-09-01\",\"end_time\":\"2023-09-01T00:00:00\",\"id\":\"13\",\"status\":\"3\"}', '2023-09-01 16:19:06', '1', '管理员');
INSERT INTO `api_log` VALUES (345, 24, 'update', '22', '{\"house_id\":\"1\",\"model_name\":\"DYJ-2020-001\",\"stock_time\":\"2023-09-01T15:40:08\",\"price\":\"56\",\"num\":\"435432545\",\"cate_name\":\"打印机\",\"id\":\"22\",\"asset_id\":\"1920\",\"stock_id\":\"13\",\"status\":\"2\"}', '2023-09-01 16:22:48', '1', '管理员');
INSERT INTO `api_log` VALUES (346, 24, 'update', '21', '{\"house_id\":\"3\",\"model_name\":\"JC-2022-03-0002\",\"num\":\"JC-2022-03-0002-2\",\"cate_name\":\"机床\",\"id\":\"21\",\"asset_id\":\"1919\",\"stock_id\":\"13\",\"price1\":\"25633\",\"status\":\"2\"}', '2023-09-01 16:22:52', '1', '管理员');
INSERT INTO `api_log` VALUES (347, 24, 'update', '21', '{\"house_id\":\"3\",\"model_name\":\"JC-2022-03-0002\",\"price\":\"63\",\"num\":\"JC-2022-03-0002-2\",\"cate_name\":\"机床\",\"id\":\"21\",\"asset_id\":\"1919\",\"stock_id\":\"13\",\"price1\":\"25633\",\"status\":\"2\"}', '2023-09-01 16:22:56', '1', '管理员');
INSERT INTO `api_log` VALUES (348, 24, 'update', '20', '{\"house_id\":\"3\",\"model_name\":\"DYJ-2020-001\",\"num\":\"DYJ-2020-001-1\",\"cate_name\":\"打印机\",\"id\":\"20\",\"asset_id\":\"1918\",\"stock_id\":\"13\",\"price1\":\"850\",\"status\":\"3\"}', '2023-09-01 16:22:57', '1', '管理员');
INSERT INTO `api_log` VALUES (349, 24, 'update', '20', '{\"house_id\":\"3\",\"model_name\":\"DYJ-2020-001\",\"price\":\"63\",\"num\":\"DYJ-2020-001-1\",\"cate_name\":\"打印机\",\"id\":\"20\",\"asset_id\":\"1918\",\"stock_id\":\"13\",\"price1\":\"850\",\"status\":\"3\"}', '2023-09-01 16:23:00', '1', '管理员');
INSERT INTO `api_log` VALUES (350, 24, 'update', '19', '{\"house_id\":\"3\",\"model_name\":\"FWQ-2019-002\",\"num\":\"FWQ-2019-002-1\",\"cate_name\":\"服务器\",\"id\":\"19\",\"asset_id\":\"1917\",\"stock_id\":\"13\",\"price1\":\"9600\",\"status\":\"1\"}', '2023-09-01 16:23:02', '1', '管理员');
INSERT INTO `api_log` VALUES (351, 24, 'update', '19', '{\"house_id\":\"3\",\"model_name\":\"FWQ-2019-002\",\"price\":\"98\",\"num\":\"FWQ-2019-002-1\",\"cate_name\":\"服务器\",\"id\":\"19\",\"asset_id\":\"1917\",\"stock_id\":\"13\",\"price1\":\"9600\",\"status\":\"1\"}', '2023-09-01 16:23:04', '1', '管理员');
INSERT INTO `api_log` VALUES (352, 24, 'update', '21', '{\"house_id\":\"3\",\"stock_time\":\"2023-09-01T16:22:55\",\"num\":\"JC-2022-03-0002-2\",\"asset_id\":\"1919\",\"stock_id\":\"13\",\"model_name\":\"JC-2022-03-0002\",\"user_id\":\"1\",\"price\":\"63\",\"cate_name\":\"机床\",\"id\":\"21\",\"price1\":\"25633\",\"status\":\"2\"}', '2023-09-01 16:28:52', '1', '管理员');
INSERT INTO `api_log` VALUES (353, 24, 'update', '21', '{\"house_id\":\"3\",\"stock_time\":\"2023-09-01T16:22:55\",\"num\":\"JC-2022-03-0002-2\",\"asset_id\":\"1919\",\"stock_id\":\"13\",\"model_name\":\"JC-2022-03-0002\",\"price\":\"63\",\"cate_name\":\"机床\",\"id\":\"21\",\"price1\":\"25633\",\"status\":\"2\"}', '2023-09-01 17:03:10', '1', '管理员');
INSERT INTO `api_log` VALUES (354, 24, 'update', '21', '{\"house_id\":\"3\",\"stock_time\":\"2023-09-01T16:22:55\",\"num\":\"JC-2022-03-0002-2\",\"asset_id\":\"1919\",\"stock_id\":\"13\",\"model_name\":\"JC-2022-03-0002\",\"price\":\"63\",\"cate_name\":\"机床\",\"id\":\"21\",\"price1\":\"25633\",\"status\":\"2\"}', '2023-09-01 17:04:12', '1', '管理员');
INSERT INTO `api_log` VALUES (355, 24, 'update', '21', '{\"house_id\":\"3\",\"stock_time\":\"2023-09-01T16:22:55\",\"num\":\"JC-2022-03-0002-2\",\"asset_id\":\"1919\",\"stock_id\":\"13\",\"model_name\":\"JC-2022-03-0002\",\"price\":\"63\",\"cate_name\":\"机床\",\"id\":\"21\",\"price1\":\"25633\",\"status\":\"2\"}', '2023-09-01 17:11:49', '1', '管理员');
INSERT INTO `api_log` VALUES (356, 24, 'update', '21', '{\"house_id\":\"3\",\"stock_time\":\"2023-09-01T16:22:55\",\"num\":\"JC-2022-03-0002-2\",\"asset_id\":\"1919\",\"stock_id\":\"13\",\"model_name\":\"JC-2022-03-0002\",\"price\":\"63\",\"cate_name\":\"机床\",\"id\":\"21\",\"price1\":\"25633\",\"status\":\"2\"}', '2023-09-01 17:14:07', '1', '管理员');
INSERT INTO `api_log` VALUES (357, 2, 'insert', '7', '{\"action\":\"AU\",\"id\":7,\"model_id\":\"23\",\"content\":\"function exec(data){\\n  if(data.status != \'4\'){\\n    return null;\\n  }\\n  return [\\\"update asset_info t join asset_stock_detail d on d.asset_id = t.id and d.stock_id=\\\" + data.id + \\\" set t.status=d.status,t.user_id = d.user_id, t.price = d.price\\\", \\\"\\\"];\\n}\",\"status\":\"1\"}', '2023-09-01 17:29:50', '1', '管理员');
INSERT INTO `api_log` VALUES (358, 2, 'update', '7', '{\"action\":\"AU\",\"id\":\"7\",\"model_id\":\"23\",\"is_default\":\"N\",\"content\":\"function exec(data){\\n  if(data.status != \'4\'){\\n    return null;\\n  }\\n  return [\\n    \\\"update asset_info t join asset_stock_detail d on d.asset_id = t.id and d.stock_id=\\\" +  \\n       data.id + \\\" set t.status=d.status,t.user_id = d.user_id, t.price = d.price\\\", \\n    \\\"update asset_stock s join (select count(id) c, sum(price) w from asset_stock_detail  w \\n       here stock_id=13 and status in (\'1\',\'2\',\'3\', \'4\')) t set s.as', '2023-09-04 09:18:25', '1', '管理员');
INSERT INTO `api_log` VALUES (359, 2, 'update', '7', '{\"action\":\"AU\",\"remark\":\"在确认盘点之后，更新资产的状态为盘点状态，并计算盘点概况\",\"id\":\"7\",\"model_id\":\"23\",\"is_default\":\"N\",\"content\":\"function exec(data){\\n  if(data.status != \'4\'){\\n    return null;\\n  }\\n  return [\\n    \\\"update asset_info t join asset_stock_detail d on d.asset_id = t.id and d.stock_id=\\\" +  \\n       data.id + \\\" set t.status=d.status,t.user_id = d.user_id, t.price = d.price\\\", \\n    \\\"update asset_stock s join (select count(id) c, sum(price) w from asset_stock_detail  where stock_id=\\\"+data.id+\\\" and', '2023-09-04 09:21:32', '1', '管理员');
INSERT INTO `api_log` VALUES (360, 23, 'update', '13', '{\"id\":\"13\",\"status\":\"4\"}', '2023-09-04 09:47:52', '1', '管理员');
INSERT INTO `api_log` VALUES (361, 23, 'event', '7', '{\"id\":\"13\",\"status\":\"4\"}', '2023-09-04 09:47:52', '1', '管理员');
INSERT INTO `api_log` VALUES (362, 23, 'update', '13', '{\"id\":\"13\",\"status\":\"4\"}', '2023-09-04 09:48:07', '1', '管理员');
INSERT INTO `api_log` VALUES (363, 23, 'event', '7', '{\"id\":\"13\",\"status\":\"4\"}', '2023-09-04 09:48:07', '1', '管理员');
INSERT INTO `api_log` VALUES (364, 23, 'insert', '14', '{\"start_time\":\"2023-02-01 00:00:00\",\"user_id\":\"1\",\"name\":\"2023-09-04\",\"end_time\":\"2023-09-04 09:49:21\",\"id\":14,\"status\":\"1\"}', '2023-09-04 09:49:27', '1', '管理员');
INSERT INTO `api_log` VALUES (365, 23, 'event', '6', '{\"start_time\":\"2023-02-01 00:00:00\",\"user_id\":\"1\",\"name\":\"2023-09-04\",\"end_time\":\"2023-09-04 09:49:21\",\"id\":14,\"status\":\"1\"}', '2023-09-04 09:49:27', '1', '管理员');
INSERT INTO `api_log` VALUES (366, 24, 'update', '29', '{\"house_id\":\"1\",\"model_name\":\"DYJ-2020-001\",\"num\":\"435432545\",\"cate_name\":\"打印机\",\"id\":\"29\",\"asset_id\":\"1920\",\"stock_id\":\"14\",\"price1\":\"56\",\"status\":\"2\"}', '2023-09-04 10:01:12', '1', '管理员');
INSERT INTO `api_log` VALUES (367, 24, 'update', '29', '{\"house_id\":\"1\",\"model_name\":\"DYJ-2020-001\",\"user_id\":\"1\",\"num\":\"435432545\",\"cate_name\":\"打印机\",\"id\":\"29\",\"asset_id\":\"1920\",\"stock_id\":\"14\",\"price1\":\"56\",\"status\":\"2\"}', '2023-09-04 10:01:14', '1', '管理员');
INSERT INTO `api_log` VALUES (368, 24, 'update', '29', '{\"house_id\":\"1\",\"num\":\"435432545\",\"asset_id\":\"1920\",\"stock_id\":\"14\",\"model_name\":\"DYJ-2020-001\",\"user_id\":\"1\",\"price\":\"45\",\"cate_name\":\"打印机\",\"id\":\"29\",\"price1\":\"56\",\"status\":\"2\"}', '2023-09-04 10:01:17', '1', '管理员');
INSERT INTO `api_log` VALUES (369, 24, 'update', '28', '{\"house_id\":\"3\",\"model_name\":\"JC-2022-03-0002\",\"num\":\"JC-2022-03-0002-2\",\"cate_name\":\"机床\",\"id\":\"28\",\"asset_id\":\"1919\",\"stock_id\":\"14\",\"price1\":\"63\",\"status\":\"1\"}', '2023-09-04 10:01:20', '1', '管理员');
INSERT INTO `api_log` VALUES (370, 24, 'update', '27', '{\"house_id\":\"3\",\"model_name\":\"DYJ-2020-001\",\"num\":\"DYJ-2020-001-1\",\"cate_name\":\"打印机\",\"id\":\"27\",\"asset_id\":\"1918\",\"stock_id\":\"14\",\"price1\":\"63\",\"status\":\"3\"}', '2023-09-04 10:01:21', '1', '管理员');
INSERT INTO `api_log` VALUES (371, 24, 'update', '26', '{\"house_id\":\"3\",\"model_name\":\"FWQ-2019-002\",\"num\":\"FWQ-2019-002-1\",\"cate_name\":\"服务器\",\"id\":\"26\",\"asset_id\":\"1917\",\"stock_id\":\"14\",\"price1\":\"98\",\"status\":\"3\"}', '2023-09-04 10:01:23', '1', '管理员');
INSERT INTO `api_log` VALUES (372, 24, 'update', '28', '{\"house_id\":\"3\",\"num\":\"JC-2022-03-0002-2\",\"asset_id\":\"1919\",\"stock_id\":\"14\",\"model_name\":\"JC-2022-03-0002\",\"price\":\"23\",\"cate_name\":\"机床\",\"id\":\"28\",\"price1\":\"63\",\"status\":\"1\"}', '2023-09-04 10:01:29', '1', '管理员');
INSERT INTO `api_log` VALUES (373, 24, 'update', '27', '{\"house_id\":\"3\",\"num\":\"DYJ-2020-001-1\",\"asset_id\":\"1918\",\"stock_id\":\"14\",\"model_name\":\"DYJ-2020-001\",\"price\":\"34\",\"cate_name\":\"打印机\",\"id\":\"27\",\"price1\":\"63\",\"status\":\"3\"}', '2023-09-04 10:01:30', '1', '管理员');
INSERT INTO `api_log` VALUES (374, 24, 'update', '26', '{\"house_id\":\"3\",\"num\":\"FWQ-2019-002-1\",\"asset_id\":\"1917\",\"stock_id\":\"14\",\"model_name\":\"FWQ-2019-002\",\"price\":\"54\",\"cate_name\":\"服务器\",\"id\":\"26\",\"price1\":\"98\",\"status\":\"3\"}', '2023-09-04 10:01:32', '1', '管理员');
INSERT INTO `api_log` VALUES (375, 23, 'update', '14', '{\"start_time\":\"2023-02-01T00:00:00\",\"asset_worth\":\"0\",\"user_id\":\"1\",\"name\":\"2023-09-04\",\"end_time\":\"2023-09-04T09:49:21\",\"id\":\"14\",\"status\":\"2\"}', '2023-09-04 10:01:48', '1', '管理员');
INSERT INTO `api_log` VALUES (376, 23, 'update', '14', '{\"start_time\":\"2023-02-01T00:00:00\",\"asset_worth\":\"0\",\"user_id\":\"1\",\"name\":\"2023-09-04\",\"end_time\":\"2023-09-04T09:49:21\",\"id\":\"14\",\"status\":\"3\"}', '2023-09-04 10:01:54', '1', '管理员');
INSERT INTO `api_log` VALUES (377, 23, 'update', '14', '{\"id\":\"14\",\"status\":\"4\"}', '2023-09-04 10:01:59', '1', '管理员');
INSERT INTO `api_log` VALUES (378, 23, 'event', '7', '{\"id\":\"14\",\"status\":\"4\"}', '2023-09-04 10:01:59', '1', '管理员');
INSERT INTO `api_log` VALUES (379, 23, 'update', '14', '{\"id\":\"14\",\"status\":\"4\"}', '2023-09-04 10:02:56', '1', '管理员');
INSERT INTO `api_log` VALUES (380, 23, 'event', '7', '{\"id\":\"14\",\"status\":\"4\"}', '2023-09-04 10:02:56', '1', '管理员');
INSERT INTO `api_log` VALUES (381, 23, 'update', '13', '{\"id\":\"13\",\"status\":\"4\"}', '2023-09-04 10:03:34', '1', '管理员');
INSERT INTO `api_log` VALUES (382, 23, 'event', '7', '{\"id\":\"13\",\"status\":\"4\"}', '2023-09-04 10:03:34', '1', '管理员');
INSERT INTO `api_log` VALUES (383, 23, 'update', '14', '{\"id\":\"14\",\"status\":\"4\"}', '2023-09-04 10:03:38', '1', '管理员');
INSERT INTO `api_log` VALUES (384, 23, 'event', '7', '{\"id\":\"14\",\"status\":\"4\"}', '2023-09-04 10:03:38', '1', '管理员');
INSERT INTO `api_log` VALUES (385, 17, 'event', '4', '{\"reason\":\"34\",\"user_id\":\"1\",\"price\":\"2\",\"num\":\"20230702\",\"apply_status\":\"2\",\"comment\":\"ooooo\",\"id\":\"31\",\"dept_id\":\"103\",\"check_status\":\"0\"}', '2023-09-04 10:18:47', '1', '管理员');
INSERT INTO `api_log` VALUES (386, 17, 'update', '31', '{\"reason\":\"34\",\"user_id\":\"1\",\"price\":\"2\",\"num\":\"20230702\",\"apply_status\":\"2\",\"comment\":\"ooooo\",\"id\":\"31\",\"dept_id\":\"103\",\"check_status\":\"0\"}', '2023-09-04 10:18:47', '1', '管理员');
INSERT INTO `api_log` VALUES (387, 18, 'insert', '36', '{\"amount\":\"2\",\"purchase_id\":\"31\",\"cate_id\":\"30\",\"id\":\"36\",\"model_id\":\"1905\"}', '2023-09-04 10:18:47', '1', '管理员');
INSERT INTO `api_log` VALUES (388, 17, 'event', '4', '{\"reason\":\"33241\",\"user_id\":\"1\",\"price\":\"0\",\"num\":\"12421341324\",\"apply_status\":\"1\",\"comment\":\"123423\",\"id\":\"25\",\"dept_id\":\"103\",\"check_status\":\"1\",\"finish_time\":\"2022-03-09T00:00:00\"}', '2023-09-04 10:20:11', '1', '管理员');
INSERT INTO `api_log` VALUES (389, 17, 'update', '25', '{\"reason\":\"33241\",\"user_id\":\"1\",\"price\":\"0\",\"num\":\"12421341324\",\"apply_status\":\"1\",\"comment\":\"123423\",\"id\":\"25\",\"dept_id\":\"103\",\"check_status\":\"1\",\"finish_time\":\"2022-03-09T00:00:00\"}', '2023-09-04 10:20:11', '1', '管理员');
INSERT INTO `api_log` VALUES (390, 18, 'insert', '30', '{\"amount\":\"2\",\"purchase_id\":\"25\",\"cate_id\":\"27\",\"id\":\"30\",\"model_id\":\"1909\",\"nums\":\"2\"}', '2023-09-04 10:20:11', '1', '管理员');
INSERT INTO `api_log` VALUES (391, 18, 'insert', '31', '{\"amount\":\"10\",\"purchase_id\":\"25\",\"cate_id\":\"29\",\"id\":\"31\",\"model_id\":\"1910\",\"nums\":\"10\"}', '2023-09-04 10:20:11', '1', '管理员');
INSERT INTO `api_log` VALUES (392, 17, 'event', '4', '{\"reason\":\"34\",\"user_id\":\"1\",\"price\":\"2\",\"num\":\"20230702\",\"apply_status\":\"2\",\"comment\":\"ooooo\",\"id\":\"31\",\"dept_id\":\"103\",\"check_status\":\"0\"}', '2023-09-04 10:21:07', '1', '管理员');
INSERT INTO `api_log` VALUES (393, 17, 'update', '31', '{\"reason\":\"34\",\"user_id\":\"1\",\"price\":\"2\",\"num\":\"20230702\",\"apply_status\":\"2\",\"comment\":\"ooooo\",\"id\":\"31\",\"dept_id\":\"103\",\"check_status\":\"0\"}', '2023-09-04 10:21:07', '1', '管理员');
INSERT INTO `api_log` VALUES (394, 18, 'insert', '36', '{\"amount\":\"2\",\"purchase_id\":\"31\",\"cate_id\":\"30\",\"id\":\"36\",\"model_id\":\"1905\"}', '2023-09-04 10:21:07', '1', '管理员');
INSERT INTO `api_log` VALUES (395, 17, 'event', '4', '{\"reason\":\"33241\",\"user_id\":\"1\",\"price\":\"0\",\"num\":\"12421341324\",\"apply_status\":\"1\",\"comment\":\"123423\",\"id\":\"25\",\"dept_id\":\"103\",\"check_status\":\"1\",\"finish_time\":\"2022-03-09T00:00:00\"}', '2023-09-04 10:24:58', '1', '管理员');
INSERT INTO `api_log` VALUES (396, 17, 'update', '25', '{\"reason\":\"33241\",\"user_id\":\"1\",\"price\":\"0\",\"num\":\"12421341324\",\"apply_status\":\"1\",\"comment\":\"123423\",\"id\":\"25\",\"dept_id\":\"103\",\"check_status\":\"1\",\"finish_time\":\"2022-03-09T00:00:00\"}', '2023-09-04 10:24:58', '1', '管理员');
INSERT INTO `api_log` VALUES (397, 18, 'insert', '30', '{\"amount\":\"2\",\"purchase_id\":\"25\",\"cate_id\":\"27\",\"id\":\"30\",\"model_id\":\"1909\",\"nums\":\"2\"}', '2023-09-04 10:24:58', '1', '管理员');
INSERT INTO `api_log` VALUES (398, 18, 'insert', '31', '{\"amount\":\"10\",\"purchase_id\":\"25\",\"cate_id\":\"29\",\"id\":\"31\",\"model_id\":\"1910\",\"nums\":\"10\"}', '2023-09-04 10:24:58', '1', '管理员');
INSERT INTO `api_log` VALUES (399, 2, 'insert', '8', '{\"action\":\"AI\",\"remark\":\"采购入库后，需要更新采购明细；归还入库的需要将原使用申请的归还时间设置为当前\",\"id\":8,\"model_id\":\"19\",\"content\":\"function exec(data){\\n  if(data.type == \'1\'){\\n  return [];\\n  }else{\\n  return \\\"UPDATE asset_use SET revert_time = now() WHERE revert_time is null and num=\'\\\"+data.num+\\\"\'\\\";\\n  }\\n}\",\"status\":\"1\"}', '2023-09-04 14:19:05', '1', '管理员');
INSERT INTO `api_log` VALUES (400, 2, 'update', '8', '{\"action\":\"AI\",\"remark\":\"采购入库后，需要更新采购明细；归还入库的需要将原使用申请的归还时间设置为当前\",\"id\":\"8\",\"model_id\":\"19\",\"is_default\":\"N\",\"content\":\"function exec(data){\\n  if(data.type == \'1\'){\\n  return [];\\n  }else{\\n  return \\\"UPDATE asset_use SET revert_time = now() WHERE revert_time is null and num=\'\\\"+data.num+\\\"\'\\\";\\n  }\\n}\",\"status\":\"2\"}', '2023-09-04 16:34:09', '1', '管理员');
INSERT INTO `api_log` VALUES (404, 19, 'insert', '61', '{\"house_id\":\"1\",\"recv_time\":\"2023-09-04 16:54:17.898\",\"user_id\":\"1\",\"num\":\"41242314\",\"cate_id\":\"30\",\"id\":61,\"type\":\"1\"}', '2023-09-04 16:54:18', '1', '管理员');
INSERT INTO `api_log` VALUES (405, 19, 'event', '8', '{\"house_id\":\"1\",\"recv_time\":\"2023-09-04 16:54:17.898\",\"user_id\":\"1\",\"num\":\"41242314\",\"cate_id\":\"30\",\"id\":61,\"type\":\"1\"}', '2023-09-04 16:54:18', '1', '管理员');
INSERT INTO `api_log` VALUES (406, 16, 'insert', '1922', '{\"buy_time\":\"2023-09-04 16:54:17.921\",\"house_id\":\"1\",\"price\":\"1\",\"num\":\"41242314\",\"cate_id\":\"30\",\"id\":1922,\"model_id\":\"1905\",\"status\":\"1\"}', '2023-09-04 16:54:18', '1', '管理员');
INSERT INTO `api_log` VALUES (407, 18, 'update', '36', '{\"amount\":\"2\",\"purchase_id\":\"31\",\"cate_id\":\"30\",\"id\":\"36\",\"model_id\":\"1905\",\"nums\":\"[\\\"41242314\\\"]\",\"ins\":\"1\"}', '2023-09-04 16:54:18', '1', '管理员');
INSERT INTO `api_log` VALUES (408, 19, 'insert', '62', '{\"house_id\":\"1\",\"recv_time\":\"2023-09-04 16:56:00.78\",\"user_id\":\"1\",\"num\":\"12341234\",\"cate_id\":\"30\",\"remark\":\"2134324234\",\"id\":62,\"type\":\"1\"}', '2023-09-04 16:56:01', '1', '管理员');
INSERT INTO `api_log` VALUES (409, 19, 'event', '8', '{\"house_id\":\"1\",\"recv_time\":\"2023-09-04 16:56:00.78\",\"user_id\":\"1\",\"num\":\"12341234\",\"cate_id\":\"30\",\"remark\":\"2134324234\",\"id\":62,\"type\":\"1\"}', '2023-09-04 16:56:01', '1', '管理员');
INSERT INTO `api_log` VALUES (410, 16, 'insert', '1923', '{\"buy_time\":\"2023-09-04 16:56:00.794\",\"house_id\":\"1\",\"price\":\"1\",\"num\":\"12341234\",\"cate_id\":\"30\",\"id\":1923,\"model_id\":\"1905\",\"status\":\"1\"}', '2023-09-04 16:56:01', '1', '管理员');
INSERT INTO `api_log` VALUES (411, 18, 'update', '36', '{\"amount\":\"2\",\"purchase_id\":\"31\",\"cate_id\":\"30\",\"id\":\"36\",\"model_id\":\"1905\",\"nums\":\"[\\\"41242314\\\"],12341234\",\"ins\":\"2\"}', '2023-09-04 16:56:01', '1', '管理员');
INSERT INTO `api_log` VALUES (413, 1, 'insert', '25', '{\"code\":\"flow_task\",\"name\":\"审批任务\",\"id\":25,\"group\":\"flow\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"AUTO\",\"status\":\"1\"}', '2023-09-04 16:59:15', '1', '管理员');
INSERT INTO `api_log` VALUES (414, 1, 'insert', '26', '{\"code\":\"flow_step\",\"name\":\"审批过程\",\"id\":26,\"group\":\"flow\",\"pk_field\":\"id\",\"excels\":\"AUTO\",\"status\":\"1\"}', '2023-09-04 16:59:43', '1', '管理员');
INSERT INTO `api_log` VALUES (415, 25, 'insert', '28', '{\"user_id\":\"1\",\"user_name\":\"管理员\",\"num\":\"20230702\",\"target_id\":\"31\",\"apply_time\":\"2023-09-04 16:59:52.096\",\"id\":28,\"type\":\"asset_purchase\",\"title\":\"采购订单\",\"check_status\":\"1\"}', '2023-09-04 16:59:52', '1', '管理员');
INSERT INTO `api_log` VALUES (416, 26, 'insert', '28', '{\"check_user_id\":\"1\",\"task_id\":28,\"pid\":\"0\",\"id\":28,\"check_status\":\"1\"}', '2023-09-04 16:59:52', '1', '管理员');
INSERT INTO `api_log` VALUES (417, 25, 'update', '28', '{\"user_id\":\"1\",\"user_name\":\"管理员\",\"num\":\"20230702\",\"target_id\":\"31\",\"apply_time\":\"2023-09-04 17:05:17.943\",\"id\":28,\"type\":\"asset_purchase\",\"title\":\"采购订单\",\"check_status\":\"1\"}', '2023-09-04 17:05:18', '1', '管理员');
INSERT INTO `api_log` VALUES (418, 26, 'insert', '29', '{\"check_user_id\":\"1\",\"task_id\":28,\"pid\":\"0\",\"id\":29,\"check_status\":\"1\"}', '2023-09-04 17:05:18', '1', '管理员');
INSERT INTO `api_log` VALUES (419, 17, 'event', '4', '{\"apply_status\":\"2\",\"apply_time\":\"2023-09-04 17:05:18.184\",\"id\":\"31\",\"check_status\":\"1\"}', '2023-09-04 17:05:18', '1', '管理员');
INSERT INTO `api_log` VALUES (420, 17, 'update', '31', '{\"apply_status\":\"2\",\"apply_time\":\"2023-09-04 17:05:18.184\",\"id\":\"31\",\"check_status\":\"1\"}', '2023-09-04 17:05:18', '1', '管理员');
INSERT INTO `api_log` VALUES (421, 2, 'update', '5', '{\"action\":\"AU\",\"remark\":\"审批操作之后，更新同一批次的其它审核人信息为忽略\",\"id\":\"5\",\"model_id\":\"26\",\"is_default\":\"N\",\"content\":\"function exec(data){\\n  if(data.check_status != \'1\'){\\n    return \\\"UPDATE flow_step SET ignore_flag=\'1\' WHERE check_status=\'1\' AND task_id=\\\"+data.task_id +\\\" AND pid=\\\"+data.pid;\\n  }\\n  return null;\\n}\",\"status\":\"1\"}', '2023-09-04 17:50:22', '1', '管理员');
INSERT INTO `api_log` VALUES (422, 26, 'update', '29', '{\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"28\",\"pid\":\"0\",\"id\":\"29\",\"check_status\":\"2\",\"check_time\":\"2023-09-04 17:51:26.658\"}', '2023-09-04 17:51:27', '1', '管理员');
INSERT INTO `api_log` VALUES (423, 26, 'event', '5', '{\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"28\",\"pid\":\"0\",\"id\":\"29\",\"check_status\":\"2\",\"check_time\":\"2023-09-04 17:51:26.658\"}', '2023-09-04 17:51:27', '1', '管理员');
INSERT INTO `api_log` VALUES (424, 25, 'update', '28', '{\"id\":\"28\",\"check_status\":\"2\"}', '2023-09-04 17:51:27', '1', '管理员');
INSERT INTO `api_log` VALUES (425, 25, 'insert', '29', '{\"apply_reason\":\"1234324342\",\"user_id\":\"1\",\"user_name\":\"管理员\",\"num\":\"202308011\",\"target_id\":\"27\",\"apply_time\":\"2023-09-05 15:22:48.958\",\"id\":29,\"type\":\"asset_purchase\",\"title\":\"采购订单\",\"check_status\":\"1\"}', '2023-09-05 15:22:49', '1', '管理员');
INSERT INTO `api_log` VALUES (426, 26, 'insert', '30', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":29,\"pid\":\"0\",\"id\":30,\"check_status\":\"1\"}', '2023-09-05 15:22:49', '1', '管理员');
INSERT INTO `api_log` VALUES (427, 26, 'insert', '31', '{\"check_user_name\":\"若依\",\"check_user_id\":\"2\",\"task_id\":29,\"pid\":\"0\",\"id\":31,\"check_status\":\"1\"}', '2023-09-05 15:22:49', '1', '管理员');
INSERT INTO `api_log` VALUES (428, 17, 'event', '4', '{\"apply_status\":\"2\",\"apply_time\":\"2023-09-05 15:22:49.227\",\"id\":\"27\",\"check_status\":\"1\"}', '2023-09-05 15:22:49', '1', '管理员');
INSERT INTO `api_log` VALUES (429, 17, 'update', '27', '{\"apply_status\":\"2\",\"apply_time\":\"2023-09-05 15:22:49.227\",\"id\":\"27\",\"check_status\":\"1\"}', '2023-09-05 15:22:49', '1', '管理员');
INSERT INTO `api_log` VALUES (430, 26, 'update', '30', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"29\",\"pid\":\"0\",\"id\":\"30\",\"check_status\":\"3\",\"check_time\":\"2023-09-05 15:23:11.664\"}', '2023-09-05 15:23:12', '1', '管理员');
INSERT INTO `api_log` VALUES (431, 26, 'event', '5', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"29\",\"pid\":\"0\",\"id\":\"30\",\"check_status\":\"3\",\"check_time\":\"2023-09-05 15:23:11.664\"}', '2023-09-05 15:23:12', '1', '管理员');
INSERT INTO `api_log` VALUES (432, 25, 'update', '25', '{\"apply_reason\":\"423141234\",\"user_id\":\"1\",\"user_name\":\"管理员\",\"num\":\"12421341324\",\"target_id\":\"25\",\"apply_time\":\"2023-09-05 16:31:57.943\",\"id\":25,\"type\":\"asset_purchase\",\"title\":\"采购订单\",\"check_status\":\"1\"}', '2023-09-05 16:31:58', '1', '管理员');
INSERT INTO `api_log` VALUES (433, 26, 'insert', '32', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":25,\"pid\":\"0\",\"id\":32,\"check_status\":\"1\"}', '2023-09-05 16:31:58', '1', '管理员');
INSERT INTO `api_log` VALUES (434, 26, 'insert', '33', '{\"check_user_name\":\"若依\",\"check_user_id\":\"2\",\"task_id\":25,\"pid\":\"0\",\"id\":33,\"check_status\":\"1\"}', '2023-09-05 16:31:58', '1', '管理员');
INSERT INTO `api_log` VALUES (435, 17, 'event', '4', '{\"apply_status\":\"2\",\"apply_time\":\"2023-09-05 16:31:58.196\",\"id\":\"25\",\"check_status\":\"1\"}', '2023-09-05 16:31:58', '1', '管理员');
INSERT INTO `api_log` VALUES (436, 17, 'update', '25', '{\"apply_status\":\"2\",\"apply_time\":\"2023-09-05 16:31:58.196\",\"id\":\"25\",\"check_status\":\"1\"}', '2023-09-05 16:31:58', '1', '管理员');
INSERT INTO `api_log` VALUES (437, 26, 'update', '32', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"25\",\"pid\":\"0\",\"id\":\"32\",\"check_status\":\"2\",\"check_time\":\"2023-09-05 16:34:53.018\"}', '2023-09-05 16:34:53', '1', '管理员');
INSERT INTO `api_log` VALUES (438, 26, 'event', '5', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"25\",\"pid\":\"0\",\"id\":\"32\",\"check_status\":\"2\",\"check_time\":\"2023-09-05 16:34:53.018\"}', '2023-09-05 16:34:53', '1', '管理员');
INSERT INTO `api_log` VALUES (439, 26, 'insert', '34', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":\"25\",\"pid\":\"32\",\"id\":34,\"check_status\":\"1\"}', '2023-09-05 16:34:53', '1', '管理员');
INSERT INTO `api_log` VALUES (440, 26, 'insert', '35', '{\"check_user_name\":\"若依\",\"check_user_id\":\"2\",\"task_id\":\"25\",\"pid\":\"32\",\"id\":35,\"check_status\":\"1\"}', '2023-09-05 16:34:53', '1', '管理员');
INSERT INTO `api_log` VALUES (441, 26, 'update', '34', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"check_reason\":\"fdafdasf\",\"ignore_flag\":\"0\",\"task_id\":\"25\",\"pid\":\"32\",\"id\":\"34\",\"check_status\":\"3\",\"check_time\":\"2023-09-05 16:35:00.18\"}', '2023-09-05 16:35:00', '1', '管理员');
INSERT INTO `api_log` VALUES (442, 26, 'event', '5', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"check_reason\":\"fdafdasf\",\"ignore_flag\":\"0\",\"task_id\":\"25\",\"pid\":\"32\",\"id\":\"34\",\"check_status\":\"3\",\"check_time\":\"2023-09-05 16:35:00.18\"}', '2023-09-05 16:35:00', '1', '管理员');
INSERT INTO `api_log` VALUES (443, 25, 'update', '25', '{\"id\":\"25\",\"check_status\":\"3\"}', '2023-09-05 16:35:00', '1', '管理员');
INSERT INTO `api_log` VALUES (444, 25, 'update', '24', '{\"apply_reason\":\"53245\",\"user_id\":\"1\",\"user_name\":\"管理员\",\"num\":\"34124321\",\"target_id\":\"24\",\"apply_time\":\"2023-09-05 16:56:03.788\",\"id\":24,\"type\":\"asset_purchase\",\"title\":\"采购订单\",\"check_status\":\"1\"}', '2023-09-05 16:56:04', '1', '管理员');
INSERT INTO `api_log` VALUES (445, 26, 'insert', '36', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":24,\"pid\":\"0\",\"id\":36,\"check_status\":\"1\"}', '2023-09-05 16:56:04', '1', '管理员');
INSERT INTO `api_log` VALUES (446, 26, 'insert', '37', '{\"check_user_name\":\"若依\",\"check_user_id\":\"2\",\"task_id\":24,\"pid\":\"0\",\"id\":37,\"check_status\":\"1\"}', '2023-09-05 16:56:04', '1', '管理员');
INSERT INTO `api_log` VALUES (447, 17, 'event', '4', '{\"apply_status\":\"2\",\"apply_time\":\"2023-09-05 16:56:04.187\",\"id\":\"24\",\"check_status\":\"1\"}', '2023-09-05 16:56:04', '1', '管理员');
INSERT INTO `api_log` VALUES (448, 17, 'update', '24', '{\"apply_status\":\"2\",\"apply_time\":\"2023-09-05 16:56:04.187\",\"id\":\"24\",\"check_status\":\"1\"}', '2023-09-05 16:56:04', '1', '管理员');
INSERT INTO `api_log` VALUES (449, 26, 'update', '36', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"check_reason\":\"5435435\",\"ignore_flag\":\"0\",\"task_id\":\"24\",\"pid\":\"0\",\"id\":\"36\",\"check_status\":\"3\",\"check_time\":\"2023-09-05 16:56:11.66\"}', '2023-09-05 16:56:12', '1', '管理员');
INSERT INTO `api_log` VALUES (450, 26, 'event', '5', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"check_reason\":\"5435435\",\"ignore_flag\":\"0\",\"task_id\":\"24\",\"pid\":\"0\",\"id\":\"36\",\"check_status\":\"3\",\"check_time\":\"2023-09-05 16:56:11.66\"}', '2023-09-05 16:56:12', '1', '管理员');
INSERT INTO `api_log` VALUES (451, 25, 'update', '24', '{\"id\":\"24\",\"check_status\":\"3\"}', '2023-09-05 16:56:12', '1', '管理员');
INSERT INTO `api_log` VALUES (452, 17, 'event', '4', '{\"id\":24,\"check_status\":\"3\"}', '2023-09-05 16:56:12', '1', '管理员');
INSERT INTO `api_log` VALUES (453, 17, 'update', '24', '{\"id\":24,\"check_status\":\"3\"}', '2023-09-05 16:56:12', '1', '管理员');
INSERT INTO `api_log` VALUES (454, 17, 'event', '4', '{\"reason\":\"33241\",\"user_id\":\"1\",\"price\":\"0\",\"num\":\"12421341324\",\"apply_status\":\"2\",\"comment\":\"123423\",\"apply_time\":\"2023-09-05T16:31:58\",\"id\":\"25\",\"dept_id\":\"103\",\"check_status\":\"1\",\"finish_time\":\"2022-03-09T00:00:00\"}', '2023-09-05 17:44:32', '1', '管理员');
INSERT INTO `api_log` VALUES (455, 17, 'update', '25', '{\"reason\":\"33241\",\"num\":\"12421341324\",\"apply_status\":\"2\",\"apply_time\":\"2023-09-05T16:31:58\",\"check_status\":\"1\",\"finish_time\":\"2022-03-09T00:00:00\",\"user_id\":\"1\",\"price\":\"0\",\"comment\":\"123423\",\"id\":\"25\",\"dept_id\":\"103\"}', '2023-09-05 17:44:32', '1', '管理员');
INSERT INTO `api_log` VALUES (456, 18, 'insert', '37', '{\"amount\":\"1\",\"purchase_id\":\"25\",\"cate_id\":\"27\",\"id\":37,\"model_id\":\"1908\"}', '2023-09-05 17:44:32', '1', '管理员');
INSERT INTO `api_log` VALUES (457, 17, 'event', '4', '{\"reason\":\"34\",\"user_id\":\"1\",\"price\":\"0\",\"num\":\"20230702\",\"apply_status\":\"2\",\"comment\":\"ooooo\",\"apply_time\":\"2023-09-04T17:05:18\",\"id\":\"31\",\"dept_id\":\"103\",\"check_status\":\"1\"}', '2023-09-05 17:45:34', '1', '管理员');
INSERT INTO `api_log` VALUES (458, 17, 'update', '31', '{\"reason\":\"34\",\"user_id\":\"1\",\"price\":\"0\",\"num\":\"20230702\",\"apply_status\":\"2\",\"comment\":\"ooooo\",\"apply_time\":\"2023-09-04T17:05:18\",\"id\":\"31\",\"dept_id\":\"103\",\"check_status\":\"1\"}', '2023-09-05 17:45:34', '1', '管理员');
INSERT INTO `api_log` VALUES (459, 18, 'insert', '38', '{\"amount\":\"1\",\"purchase_id\":\"31\",\"cate_id\":\"27\",\"id\":38,\"model_id\":\"1909\"}', '2023-09-05 17:45:34', '1', '管理员');
INSERT INTO `api_log` VALUES (460, 14, 'insert', '5', '{\"address\":\"28号\",\"user_id\":\"1\",\"name\":\"1号仓库\",\"links\":\"14543565\",\"id\":5,\"status\":\"1\"}', '2023-09-06 15:34:24', '1', '管理员');
INSERT INTO `api_log` VALUES (461, 14, 'insert', '6', '{\"address\":\"中心二楼\",\"user_id\":\"1\",\"name\":\"杂物间\",\"links\":\"1\",\"id\":6,\"status\":\"1\"}', '2023-09-06 15:34:39', '1', '管理员');
INSERT INTO `api_log` VALUES (462, 13, 'insert', '35', '{\"list_sort\":\"1\",\"name\":\"IT类\",\"pid\":\"0\",\"id\":35,\"status\":\"1\"}', '2023-09-06 15:43:31', '1', '管理员');
INSERT INTO `api_log` VALUES (463, 13, 'insert', '36', '{\"list_sort\":\"1\",\"name\":\"服务器\",\"pid\":\"35\",\"id\":36,\"status\":\"1\"}', '2023-09-06 15:44:54', '1', '管理员');
INSERT INTO `api_log` VALUES (464, 13, 'insert', '37', '{\"list_sort\":\"2\",\"name\":\"工具类\",\"pid\":\"0\",\"id\":37,\"status\":\"1\"}', '2023-09-06 15:45:04', '1', '管理员');
INSERT INTO `api_log` VALUES (465, 13, 'insert', '38', '{\"list_sort\":\"3\",\"name\":\"器械类\",\"pid\":\"0\",\"id\":38,\"status\":\"1\"}', '2023-09-06 15:45:18', '1', '管理员');
INSERT INTO `api_log` VALUES (466, 13, 'insert', '39', '{\"list_sort\":\"2\",\"name\":\"PC\",\"pid\":\"35\",\"id\":39,\"status\":\"1\"}', '2023-09-06 15:47:44', '1', '管理员');
INSERT INTO `api_log` VALUES (467, 13, 'insert', '40', '{\"list_sort\":\"3\",\"name\":\"路由器\",\"pid\":\"35\",\"id\":40,\"status\":\"1\"}', '2023-09-06 15:47:57', '1', '管理员');
INSERT INTO `api_log` VALUES (468, 13, 'insert', '41', '{\"list_sort\":\"4\",\"name\":\"打印机\",\"pid\":\"35\",\"id\":41,\"status\":\"1\"}', '2023-09-06 15:48:04', '1', '管理员');
INSERT INTO `api_log` VALUES (469, 13, 'insert', '42', '{\"list_sort\":\"1\",\"name\":\"饮水机\",\"pid\":\"37\",\"id\":42,\"status\":\"1\"}', '2023-09-06 15:48:23', '1', '管理员');
INSERT INTO `api_log` VALUES (470, 13, 'insert', '43', '{\"list_sort\":\"2\",\"name\":\"办公桌\",\"pid\":\"37\",\"id\":43,\"status\":\"1\"}', '2023-09-06 15:48:36', '1', '管理员');
INSERT INTO `api_log` VALUES (471, 13, 'insert', '44', '{\"list_sort\":\"1\",\"name\":\"电表\",\"pid\":\"38\",\"id\":44,\"status\":\"1\"}', '2023-09-06 15:48:46', '1', '管理员');
INSERT INTO `api_log` VALUES (472, 15, 'insert', '1914', '{\"life_time\":\"120\",\"factory\":\"华为\",\"unit\":\"台\",\"price\":\"56400\",\"name\":\"2U-X86\",\"cate_id\":\"36\",\"id\":1914,\"params\":\"双核 32G内存\",\"price1\":\"56400\"}', '2023-09-06 15:50:43', '1', '管理员');
INSERT INTO `api_log` VALUES (473, 15, 'insert', '1915', '{\"life_time\":\"120\",\"factory\":\"华为\",\"unit\":\"台\",\"price\":\"34000\",\"name\":\"1U ARM\",\"cate_id\":\"36\",\"id\":1915,\"params\":\"32G 1T\",\"price1\":\"36400\"}', '2023-09-06 15:51:33', '1', '管理员');
INSERT INTO `api_log` VALUES (474, 15, 'insert', '1916', '{\"life_time\":\"60\",\"factory\":\"HP\",\"unit\":\"台\",\"price\":\"3500\",\"name\":\"HP 1000\",\"cate_id\":\"41\",\"id\":1916,\"params\":\"黑白 双向\",\"price1\":\"3500\"}', '2023-09-06 15:52:12', '1', '管理员');
INSERT INTO `api_log` VALUES (475, 15, 'insert', '1917', '{\"life_time\":\"24\",\"factory\":\"无\",\"unit\":\"台\",\"price\":\"50\",\"name\":\"立式\",\"cate_id\":\"42\",\"id\":1917,\"params\":\"1米\",\"price1\":\"50\"}', '2023-09-06 15:52:38', '1', '管理员');
INSERT INTO `api_log` VALUES (476, 16, 'insert', '1924', '{\"house_id\":\"5\",\"num\":\"34324\",\"cate_id\":\"42\",\"id\":1924,\"model_id\":\"1917\",\"dept_id\":\"102\",\"status\":\"1\"}', '2023-09-06 17:09:57', '1', '管理员');
INSERT INTO `api_log` VALUES (477, 16, 'insert', '1925', '{\"buy_time\":\"2023-05-06 00:00:00\",\"house_id\":\"6\",\"price\":\"34000\",\"num\":\"2U-2023-9-1\",\"cate_id\":\"36\",\"comment\":\"\",\"id\":1925,\"model_id\":\"1914\",\"dept_id\":\"100\",\"status\":\"1\"}', '2023-09-06 17:12:05', '1', '管理员');
INSERT INTO `api_log` VALUES (478, 17, 'insert', '33', '{\"reason\":\"打印机\",\"user_id\":\"1\",\"price\":\"7000\",\"num\":\"202309061712:3614.715520130055752\",\"apply_status\":\"1\",\"comment\":\"2台\",\"id\":33,\"dept_id\":\"103\"}', '2023-09-06 17:13:01', '1', '管理员');
INSERT INTO `api_log` VALUES (479, 18, 'insert', '39', '{\"amount\":\"2\",\"purchase_id\":33,\"cate_id\":\"41\",\"id\":39,\"model_id\":\"1916\"}', '2023-09-06 17:13:01', '1', '管理员');
INSERT INTO `api_log` VALUES (480, 25, 'insert', '30', '{\"apply_reason\":\"56\",\"user_id\":\"1\",\"user_name\":\"管理员\",\"num\":\"2023090617123615\",\"target_id\":\"33\",\"apply_time\":\"2023-09-06 17:25:14.257\",\"id\":30,\"type\":\"asset_purchase\",\"title\":\"采购订单\",\"check_status\":\"1\"}', '2023-09-06 17:25:14', '1', '管理员');
INSERT INTO `api_log` VALUES (481, 26, 'insert', '38', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":30,\"pid\":\"0\",\"id\":38,\"check_status\":\"1\"}', '2023-09-06 17:25:14', '1', '管理员');
INSERT INTO `api_log` VALUES (482, 26, 'insert', '39', '{\"check_user_name\":\"一号仓管\",\"check_user_id\":\"3\",\"task_id\":30,\"pid\":\"0\",\"id\":39,\"check_status\":\"1\"}', '2023-09-06 17:25:14', '1', '管理员');
INSERT INTO `api_log` VALUES (483, 25, 'update', '30', '{\"apply_reason\":\"999\",\"user_id\":\"1\",\"user_name\":\"管理员\",\"num\":\"2023090617123615\",\"target_id\":\"33\",\"apply_time\":\"2023-09-06 17:27:26.334\",\"id\":30,\"type\":\"asset_purchase\",\"title\":\"采购订单\",\"check_status\":\"1\"}', '2023-09-06 17:27:26', '1', '管理员');
INSERT INTO `api_log` VALUES (484, 26, 'insert', '40', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":30,\"pid\":\"0\",\"id\":40,\"check_status\":\"1\"}', '2023-09-06 17:27:26', '1', '管理员');
INSERT INTO `api_log` VALUES (485, 26, 'insert', '41', '{\"check_user_name\":\"一号仓管\",\"check_user_id\":\"3\",\"task_id\":30,\"pid\":\"0\",\"id\":41,\"check_status\":\"1\"}', '2023-09-06 17:27:26', '1', '管理员');
INSERT INTO `api_log` VALUES (486, 25, 'update', '30', '{\"apply_reason\":\"9988\",\"user_id\":\"1\",\"user_name\":\"管理员\",\"num\":\"2023090617123615\",\"target_id\":\"33\",\"apply_time\":\"2023-09-06 17:27:38.359\",\"id\":30,\"type\":\"asset_purchase\",\"title\":\"采购订单\",\"check_status\":\"1\"}', '2023-09-06 17:27:38', '1', '管理员');
INSERT INTO `api_log` VALUES (487, 26, 'insert', '42', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":30,\"pid\":\"0\",\"id\":42,\"check_status\":\"1\"}', '2023-09-06 17:27:38', '1', '管理员');
INSERT INTO `api_log` VALUES (488, 26, 'insert', '43', '{\"check_user_name\":\"一号仓管\",\"check_user_id\":\"3\",\"task_id\":30,\"pid\":\"0\",\"id\":43,\"check_status\":\"1\"}', '2023-09-06 17:27:38', '1', '管理员');
INSERT INTO `api_log` VALUES (489, 17, 'event', '4', '{\"apply_status\":\"2\",\"apply_time\":\"2023-09-06 17:27:38.627\",\"id\":\"33\",\"check_status\":\"1\"}', '2023-09-06 17:27:39', '1', '管理员');
INSERT INTO `api_log` VALUES (490, 17, 'update', '33', '{\"apply_status\":\"2\",\"apply_time\":\"2023-09-06 17:27:38.627\",\"id\":\"33\",\"check_status\":\"1\"}', '2023-09-06 17:27:39', '1', '管理员');
INSERT INTO `api_log` VALUES (491, 26, 'update', '42', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"check_reason\":\"999\",\"ignore_flag\":\"0\",\"task_id\":\"30\",\"pid\":\"0\",\"id\":\"42\",\"check_status\":\"3\",\"check_time\":\"2023-09-06 17:27:51.199\"}', '2023-09-06 17:27:51', '1', '管理员');
INSERT INTO `api_log` VALUES (492, 26, 'event', '5', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"check_reason\":\"999\",\"ignore_flag\":\"0\",\"task_id\":\"30\",\"pid\":\"0\",\"id\":\"42\",\"check_status\":\"3\",\"check_time\":\"2023-09-06 17:27:51.199\"}', '2023-09-06 17:27:51', '1', '管理员');
INSERT INTO `api_log` VALUES (493, 25, 'update', '30', '{\"id\":\"30\",\"check_status\":\"3\"}', '2023-09-06 17:27:51', '1', '管理员');
INSERT INTO `api_log` VALUES (494, 17, 'event', '4', '{\"id\":33,\"check_status\":\"3\"}', '2023-09-06 17:27:51', '1', '管理员');
INSERT INTO `api_log` VALUES (495, 17, 'update', '33', '{\"id\":33,\"check_status\":\"3\"}', '2023-09-06 17:27:51', '1', '管理员');
INSERT INTO `api_log` VALUES (496, 1, 'update', '14', '{\"code\":\"asset_warehouse\",\"name\":\"仓库信息\",\"id\":\"14\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"name\",\"excels\":\"{\\n\\t\\\"asset_warehouse\\\": {\\n\\t\\t\\\"name\\\": \\\"仓库名称\\\",\\n\\t\\t\\\"address\\\": \\\"仓库地址\\\",\\n\\t\\t\\\"user_id\\\": \\\"负责人\\\",\\n\\t\\t\\\"links\\\": \\\"联系方式\\\",\\n\\t\\t\\\"status\\\": \\\"状态\\\",\\n\\t\\t\\\"remark\\\": \\\"备注\\\"\\n\\t},\\n\\t\\\"@ds\\\": {\\n\\t\\t\\\"user_id\\\": \\\"SELECT user_id value, nick_name label FROM sys_user WHERE del_flag=0\\\"\\n\\t},\\n\\t\\\"@dict\\\": {\\n\\t\\t\\\"status\\\": {\\\"1\\\":\\\"启用\\\"，\\\"2\\\":\\\"禁用\\\"}\\n\\t}\\n}', '2023-09-07 09:56:13', '1', '管理员');
INSERT INTO `api_log` VALUES (497, 3, 'insert', '21', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":21,\"model_id\":\"14\",\"write\":\"Y\"}', '2023-09-07 09:56:13', '1', '管理员');
INSERT INTO `api_log` VALUES (498, 3, 'insert', '22', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":22,\"model_id\":\"14\",\"write\":\"N\"}', '2023-09-07 09:56:13', '1', '管理员');
INSERT INTO `api_log` VALUES (499, 3, 'insert', '23', '{\"read\":\"Y\",\"role_id\":\"3\",\"id\":23,\"model_id\":\"14\",\"write\":\"N\"}', '2023-09-07 09:56:13', '1', '管理员');
INSERT INTO `api_log` VALUES (500, 3, 'insert', '24', '{\"read\":\"Y\",\"role_id\":\"4\",\"id\":24,\"model_id\":\"14\",\"write\":\"N\"}', '2023-09-07 09:56:13', '1', '管理员');
INSERT INTO `api_log` VALUES (501, 3, 'insert', '25', '{\"read\":\"Y\",\"role_id\":\"5\",\"id\":25,\"model_id\":\"14\",\"write\":\"N\"}', '2023-09-07 09:56:13', '1', '管理员');
INSERT INTO `api_log` VALUES (502, 3, 'insert', '26', '{\"read\":\"Y\",\"role_id\":\"6\",\"id\":26,\"model_id\":\"14\",\"write\":\"N\"}', '2023-09-07 09:56:13', '1', '管理员');
INSERT INTO `api_log` VALUES (503, 2, 'insert', '9', '{\"action\":\"DC\",\"remark\":\"有资产的仓库信息不能删除\",\"id\":9,\"model_id\":\"14\",\"content\":\"function exec(data){\\n  return \\\"SELECT count(id) from asset_info where house_id IN (\\\"+data+\\\")\\\";\\n}\",\"status\":\"1\"}', '2023-09-07 09:58:57', '1', '管理员');
INSERT INTO `api_log` VALUES (504, 17, 'event', '4', '{\"reason\":\"打印机\",\"user_id\":\"1\",\"price\":\"7000\",\"num\":\"2023090617123615\",\"apply_status\":\"2\",\"comment\":\"2台\",\"apply_time\":\"2023-09-06T17:27:39\",\"id\":\"33\",\"dept_id\":\"103\",\"check_status\":\"3\"}', '2023-09-07 17:09:04', '1', '管理员');
INSERT INTO `api_log` VALUES (505, 17, 'update', '33', '{\"reason\":\"打印机\",\"user_id\":\"1\",\"price\":\"7000\",\"num\":\"2023090617123615\",\"apply_status\":\"2\",\"comment\":\"2台\",\"apply_time\":\"2023-09-06T17:27:39\",\"id\":\"33\",\"dept_id\":\"103\",\"check_status\":\"3\"}', '2023-09-07 17:09:04', '1', '管理员');
INSERT INTO `api_log` VALUES (506, 18, 'insert', '40', '{\"amount\":\"2\",\"purchase_id\":\"33\",\"cate_id\":\"41\",\"id\":40,\"model_id\":\"1916\"}', '2023-09-07 17:09:04', '1', '管理员');
INSERT INTO `api_log` VALUES (507, 17, 'event', '4', '{\"reason\":\"打印机\",\"user_id\":\"1\",\"price\":\"7000\",\"num\":\"2023090617123615\",\"apply_status\":\"2\",\"comment\":\"2台\",\"apply_time\":\"2023-09-06T17:27:39\",\"id\":\"33\",\"dept_id\":\"103\",\"check_status\":\"3\"}', '2023-09-07 17:10:18', '1', '管理员');
INSERT INTO `api_log` VALUES (508, 17, 'update', '33', '{\"reason\":\"打印机\",\"user_id\":\"1\",\"price\":\"7000\",\"num\":\"2023090617123615\",\"apply_status\":\"2\",\"comment\":\"2台\",\"apply_time\":\"2023-09-06T17:27:39\",\"id\":\"33\",\"dept_id\":\"103\",\"check_status\":\"3\"}', '2023-09-07 17:10:18', '1', '管理员');
INSERT INTO `api_log` VALUES (509, 18, 'insert', '40', '{\"amount\":\"2\",\"purchase_id\":\"33\",\"cate_id\":\"41\",\"id\":\"40\",\"model_id\":\"1916\",\"nums\":\"\",\"ins\":\"0\"}', '2023-09-07 17:10:18', '1', '管理员');
INSERT INTO `api_log` VALUES (510, 17, 'event', '4', '{\"reason\":\"打印机\",\"user_id\":\"1\",\"price\":\"7000\",\"num\":\"2023090617123615\",\"apply_status\":\"2\",\"comment\":\"2台3\",\"apply_time\":\"2023-09-06T17:27:39\",\"id\":\"33\",\"dept_id\":\"103\",\"check_status\":\"3\"}', '2023-09-07 17:10:26', '1', '管理员');
INSERT INTO `api_log` VALUES (511, 17, 'update', '33', '{\"reason\":\"打印机\",\"user_id\":\"1\",\"price\":\"7000\",\"num\":\"2023090617123615\",\"apply_status\":\"2\",\"comment\":\"2台3\",\"apply_time\":\"2023-09-06T17:27:39\",\"id\":\"33\",\"dept_id\":\"103\",\"check_status\":\"3\"}', '2023-09-07 17:10:26', '1', '管理员');
INSERT INTO `api_log` VALUES (512, 18, 'insert', '40', '{\"amount\":\"2\",\"purchase_id\":\"33\",\"cate_id\":\"41\",\"id\":\"40\",\"model_id\":\"1916\",\"nums\":\"\",\"ins\":\"0\"}', '2023-09-07 17:10:26', '1', '管理员');
INSERT INTO `api_log` VALUES (513, 25, 'update', '30', '{\"apply_reason\":\"打印机\",\"user_id\":\"1\",\"user_name\":\"管理员\",\"num\":\"2023090617123615\",\"target_id\":\"33\",\"apply_time\":\"2023-09-07 17:13:41.913\",\"id\":30,\"type\":\"asset_purchase\",\"title\":\"采购订单\",\"check_status\":\"1\"}', '2023-09-07 17:13:42', '1', '管理员');
INSERT INTO `api_log` VALUES (514, 26, 'insert', '44', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":30,\"pid\":\"0\",\"id\":44,\"check_status\":\"1\"}', '2023-09-07 17:13:42', '1', '管理员');
INSERT INTO `api_log` VALUES (515, 26, 'insert', '45', '{\"check_user_name\":\"一号仓管\",\"check_user_id\":\"3\",\"task_id\":30,\"pid\":\"0\",\"id\":45,\"check_status\":\"1\"}', '2023-09-07 17:13:42', '1', '管理员');
INSERT INTO `api_log` VALUES (516, 17, 'event', '4', '{\"apply_status\":\"2\",\"apply_time\":\"2023-09-07 17:13:42.181\",\"id\":\"33\",\"check_status\":\"1\"}', '2023-09-07 17:13:42', '1', '管理员');
INSERT INTO `api_log` VALUES (517, 17, 'update', '33', '{\"apply_status\":\"2\",\"apply_time\":\"2023-09-07 17:13:42.181\",\"id\":\"33\",\"check_status\":\"1\"}', '2023-09-07 17:13:42', '1', '管理员');
INSERT INTO `api_log` VALUES (518, 26, 'update', '44', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"30\",\"pid\":\"0\",\"id\":\"44\",\"check_status\":\"2\",\"check_time\":\"2023-09-07 17:14:11.995\"}', '2023-09-07 17:14:12', '1', '管理员');
INSERT INTO `api_log` VALUES (519, 26, 'event', '5', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"30\",\"pid\":\"0\",\"id\":\"44\",\"check_status\":\"2\",\"check_time\":\"2023-09-07 17:14:11.995\"}', '2023-09-07 17:14:12', '1', '管理员');
INSERT INTO `api_log` VALUES (520, 25, 'update', '30', '{\"id\":\"30\",\"check_status\":\"2\"}', '2023-09-07 17:14:12', '1', '管理员');
INSERT INTO `api_log` VALUES (521, 17, 'update', '33', '{\"id\":33,\"check_status\":\"2\"}', '2023-09-07 17:14:12', '1', '管理员');
INSERT INTO `api_log` VALUES (522, 17, 'event', '4', '{\"reason\":\"打印机\",\"user_id\":\"1\",\"price\":\"7000\",\"num\":\"2023090617123615\",\"apply_status\":\"1\",\"comment\":\"2台3\",\"apply_time\":\"2023-09-07T17:13:42\",\"id\":\"33\",\"dept_id\":\"103\",\"check_status\":\"1\"}', '2023-09-07 17:31:09', '1', '管理员');
INSERT INTO `api_log` VALUES (523, 17, 'update', '33', '{\"reason\":\"打印机\",\"user_id\":\"1\",\"price\":\"7000\",\"num\":\"2023090617123615\",\"apply_status\":\"1\",\"comment\":\"2台3\",\"apply_time\":\"2023-09-07T17:13:42\",\"id\":\"33\",\"dept_id\":\"103\",\"check_status\":\"1\"}', '2023-09-07 17:31:09', '1', '管理员');
INSERT INTO `api_log` VALUES (524, 18, 'insert', '41', '{\"amount\":\"2\",\"purchase_id\":\"33\",\"cate_id\":\"41\",\"id\":41,\"model_id\":\"1916\"}', '2023-09-07 17:31:09', '1', '管理员');
INSERT INTO `api_log` VALUES (525, 25, 'update', '30', '{\"apply_reason\":\"打印机\",\"user_id\":\"1\",\"user_name\":\"管理员\",\"num\":\"2023090617123615\",\"target_id\":\"33\",\"apply_time\":\"2023-09-07 17:33:51.868\",\"id\":30,\"type\":\"asset_purchase\",\"title\":\"采购订单\",\"check_status\":\"1\"}', '2023-09-07 17:33:52', '1', '管理员');
INSERT INTO `api_log` VALUES (526, 26, 'insert', '46', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":30,\"pid\":\"0\",\"id\":46,\"check_status\":\"1\"}', '2023-09-07 17:33:52', '1', '管理员');
INSERT INTO `api_log` VALUES (527, 26, 'insert', '47', '{\"check_user_name\":\"一号仓管\",\"check_user_id\":\"3\",\"task_id\":30,\"pid\":\"0\",\"id\":47,\"check_status\":\"1\"}', '2023-09-07 17:33:52', '1', '管理员');
INSERT INTO `api_log` VALUES (528, 17, 'event', '4', '{\"apply_status\":\"2\",\"apply_time\":\"2023-09-07 17:33:52.245\",\"id\":\"33\",\"check_status\":\"1\"}', '2023-09-07 17:33:52', '1', '管理员');
INSERT INTO `api_log` VALUES (529, 17, 'update', '33', '{\"apply_status\":\"2\",\"apply_time\":\"2023-09-07 17:33:52.245\",\"id\":\"33\",\"check_status\":\"1\"}', '2023-09-07 17:33:52', '1', '管理员');
INSERT INTO `api_log` VALUES (530, 26, 'update', '46', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"30\",\"pid\":\"0\",\"id\":\"46\",\"check_status\":\"3\",\"check_time\":\"2023-09-07 17:34:40.421\"}', '2023-09-07 17:34:40', '1', '管理员');
INSERT INTO `api_log` VALUES (531, 26, 'event', '5', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"30\",\"pid\":\"0\",\"id\":\"46\",\"check_status\":\"3\",\"check_time\":\"2023-09-07 17:34:40.421\"}', '2023-09-07 17:34:40', '1', '管理员');
INSERT INTO `api_log` VALUES (532, 25, 'update', '30', '{\"id\":\"30\",\"check_status\":\"3\"}', '2023-09-07 17:34:40', '1', '管理员');
INSERT INTO `api_log` VALUES (533, 17, 'update', '33', '{\"id\":33,\"check_status\":\"3\"}', '2023-09-07 17:34:40', '1', '管理员');
INSERT INTO `api_log` VALUES (534, 25, 'update', '30', '{\"apply_reason\":\"打印机\",\"user_id\":\"1\",\"user_name\":\"管理员\",\"num\":\"2023090617123615\",\"target_id\":\"33\",\"apply_time\":\"2023-09-07 17:34:58.074\",\"id\":30,\"type\":\"asset_purchase\",\"title\":\"采购订单\",\"check_status\":\"1\"}', '2023-09-07 17:34:58', '1', '管理员');
INSERT INTO `api_log` VALUES (535, 26, 'insert', '48', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":30,\"pid\":\"0\",\"id\":48,\"check_status\":\"1\"}', '2023-09-07 17:34:58', '1', '管理员');
INSERT INTO `api_log` VALUES (536, 26, 'insert', '49', '{\"check_user_name\":\"一号仓管\",\"check_user_id\":\"3\",\"task_id\":30,\"pid\":\"0\",\"id\":49,\"check_status\":\"1\"}', '2023-09-07 17:34:58', '1', '管理员');
INSERT INTO `api_log` VALUES (537, 17, 'event', '4', '{\"apply_status\":\"2\",\"apply_time\":\"2023-09-07 17:34:58.34\",\"id\":\"33\",\"check_status\":\"1\"}', '2023-09-07 17:34:58', '1', '管理员');
INSERT INTO `api_log` VALUES (538, 17, 'update', '33', '{\"apply_status\":\"2\",\"apply_time\":\"2023-09-07 17:34:58.34\",\"id\":\"33\",\"check_status\":\"1\"}', '2023-09-07 17:34:58', '1', '管理员');
INSERT INTO `api_log` VALUES (539, 26, 'update', '48', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"30\",\"pid\":\"0\",\"id\":\"48\",\"check_status\":\"3\",\"check_time\":\"2023-09-07 17:35:17.826\"}', '2023-09-07 17:35:26', '1', '管理员');
INSERT INTO `api_log` VALUES (540, 26, 'event', '5', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"30\",\"pid\":\"0\",\"id\":\"48\",\"check_status\":\"3\",\"check_time\":\"2023-09-07 17:35:17.826\"}', '2023-09-07 17:35:26', '1', '管理员');
INSERT INTO `api_log` VALUES (541, 25, 'update', '30', '{\"id\":\"30\",\"check_status\":\"3\"}', '2023-09-07 17:35:36', '1', '管理员');
INSERT INTO `api_log` VALUES (542, 17, 'update', '33', '{\"id\":33,\"check_status\":\"3\"}', '2023-09-07 17:35:50', '1', '管理员');
INSERT INTO `api_log` VALUES (543, 25, 'update', '30', '{\"apply_reason\":\"打印机\",\"user_id\":\"1\",\"user_name\":\"管理员\",\"num\":\"2023090617123615\",\"target_id\":\"33\",\"apply_time\":\"2023-09-07 17:36:02.945\",\"id\":30,\"type\":\"asset_purchase\",\"title\":\"采购订单\",\"check_status\":\"1\"}', '2023-09-07 17:36:25', '1', '管理员');
INSERT INTO `api_log` VALUES (544, 26, 'insert', '50', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":30,\"pid\":\"0\",\"id\":50,\"check_status\":\"1\"}', '2023-09-07 17:36:25', '1', '管理员');
INSERT INTO `api_log` VALUES (545, 26, 'insert', '51', '{\"check_user_name\":\"一号仓管\",\"check_user_id\":\"3\",\"task_id\":30,\"pid\":\"0\",\"id\":51,\"check_status\":\"1\"}', '2023-09-07 17:36:25', '1', '管理员');
INSERT INTO `api_log` VALUES (546, 17, 'event', '4', '{\"reason\":\"打印机\",\"user_id\":\"1\",\"price\":\"7000\",\"num\":\"2023090617123615\",\"apply_status\":\"2\",\"comment\":\"2台3\",\"apply_time\":\"2023-09-07T17:34:58\",\"id\":\"33\",\"dept_id\":\"103\",\"check_status\":\"3\"}', '2023-09-07 17:37:16', '1', '管理员');
INSERT INTO `api_log` VALUES (547, 17, 'update', '33', '{\"reason\":\"打印机\",\"user_id\":\"1\",\"price\":\"7000\",\"num\":\"2023090617123615\",\"apply_status\":\"2\",\"comment\":\"2台3\",\"apply_time\":\"2023-09-07T17:34:58\",\"id\":\"33\",\"dept_id\":\"103\",\"check_status\":\"3\"}', '2023-09-07 17:37:16', '1', '管理员');
INSERT INTO `api_log` VALUES (548, 18, 'insert', '42', '{\"amount\":\"2\",\"purchase_id\":\"33\",\"cate_id\":\"41\",\"id\":42,\"model_id\":\"1916\"}', '2023-09-07 17:37:16', '1', '管理员');
INSERT INTO `api_log` VALUES (549, 25, 'update', '30', '{\"apply_reason\":\"打印机\",\"user_id\":\"1\",\"user_name\":\"管理员\",\"num\":\"2023090617123615\",\"target_id\":\"33\",\"apply_time\":\"2023-09-07 17:37:26.647\",\"id\":30,\"type\":\"asset_purchase\",\"title\":\"采购订单\",\"check_status\":\"1\"}', '2023-09-07 17:37:33', '1', '管理员');
INSERT INTO `api_log` VALUES (550, 26, 'insert', '52', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":30,\"pid\":\"0\",\"id\":52,\"check_status\":\"1\"}', '2023-09-07 17:37:33', '1', '管理员');
INSERT INTO `api_log` VALUES (551, 26, 'insert', '53', '{\"check_user_name\":\"若依\",\"check_user_id\":\"2\",\"task_id\":30,\"pid\":\"0\",\"id\":53,\"check_status\":\"1\"}', '2023-09-07 17:37:33', '1', '管理员');
INSERT INTO `api_log` VALUES (552, 26, 'insert', '54', '{\"check_user_name\":\"一号仓管\",\"check_user_id\":\"3\",\"task_id\":30,\"pid\":\"0\",\"id\":54,\"check_status\":\"1\"}', '2023-09-07 17:37:33', '1', '管理员');
INSERT INTO `api_log` VALUES (553, 17, 'event', '4', '{\"apply_status\":\"2\",\"apply_time\":\"2023-09-07 17:37:33.457\",\"id\":\"33\",\"check_status\":\"1\"}', '2023-09-07 17:37:54', '1', '管理员');
INSERT INTO `api_log` VALUES (554, 17, 'update', '33', '{\"apply_status\":\"2\",\"apply_time\":\"2023-09-07 17:37:33.457\",\"id\":\"33\",\"check_status\":\"1\"}', '2023-09-07 17:37:54', '1', '管理员');
INSERT INTO `api_log` VALUES (555, 17, 'event', '4', '{\"reason\":\"打印机\",\"user_id\":\"1\",\"price\":\"7000\",\"num\":\"2023090617123615\",\"apply_status\":\"2\",\"comment\":\"2台3\",\"apply_time\":\"2023-09-07T17:37:33\",\"id\":\"33\",\"dept_id\":\"103\",\"check_status\":\"1\"}', '2023-09-07 17:41:27', '1', '管理员');
INSERT INTO `api_log` VALUES (556, 17, 'update', '33', '{\"reason\":\"打印机\",\"user_id\":\"1\",\"price\":\"7000\",\"num\":\"2023090617123615\",\"apply_status\":\"2\",\"comment\":\"2台3\",\"apply_time\":\"2023-09-07T17:37:33\",\"id\":\"33\",\"dept_id\":\"103\",\"check_status\":\"1\"}', '2023-09-07 17:41:27', '1', '管理员');
INSERT INTO `api_log` VALUES (557, 18, 'insert', '43', '{\"amount\":\"2\",\"purchase_id\":\"33\",\"cate_id\":\"41\",\"id\":43,\"model_id\":\"1916\"}', '2023-09-07 17:41:27', '1', '管理员');
INSERT INTO `api_log` VALUES (558, 26, 'update', '52', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"check_reason\":\"333\",\"ignore_flag\":\"0\",\"task_id\":\"30\",\"pid\":\"0\",\"id\":\"52\",\"check_status\":\"2\",\"check_time\":\"2023-09-07 17:41:50.118\"}', '2023-09-07 17:42:00', '1', '管理员');
INSERT INTO `api_log` VALUES (559, 26, 'event', '5', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"check_reason\":\"333\",\"ignore_flag\":\"0\",\"task_id\":\"30\",\"pid\":\"0\",\"id\":\"52\",\"check_status\":\"2\",\"check_time\":\"2023-09-07 17:41:50.118\"}', '2023-09-07 17:42:00', '1', '管理员');
INSERT INTO `api_log` VALUES (560, 25, 'update', '30', '{\"id\":\"30\",\"check_status\":\"2\"}', '2023-09-07 17:42:05', '1', '管理员');
INSERT INTO `api_log` VALUES (561, 17, 'update', '33', '{\"id\":33,\"check_status\":\"2\"}', '2023-09-07 17:42:08', '1', '管理员');
INSERT INTO `api_log` VALUES (562, 2, 'insert', '10', '{\"action\":\"DC\",\"remark\":\"有设备的型号不能删除\",\"id\":10,\"model_id\":\"1\",\"content\":\"function exec(data){\\n  return \\\"SELECT count(id) total FROM asset_info WHERE model_id in (\\\"+data+\\\")\\\";\\n}\",\"status\":\"1\"}', '2023-09-08 09:42:08', '1', '管理员');
INSERT INTO `api_log` VALUES (563, 2, 'insert', '11', '{\"action\":\"DC\",\"remark\":\"有型号和资产的资产分类不能删除\",\"id\":11,\"model_id\":\"13\",\"content\":\"function exec(data){\\n  return \\\"select (SELECT count(id) total FROM asset_info WHERE cate_id IN (\\\"+data+\\\") ) + (SELECT count(id) total FROM asset_model WHERE cate_id IN (\\\"+data+\\\") )\\\";\\n}\",\"status\":\"1\"}', '2023-09-08 09:44:57', '1', '管理员');
INSERT INTO `api_log` VALUES (564, 2, 'insert', '12', '{\"action\":\"DC\",\"remark\":\"已审核通过的使用申请不能删除\",\"id\":12,\"model_id\":\"22\",\"content\":\"function exec(data){\\n  return \\\"SELECT count(id) FROM asset_use where check_status=\'2\' AND id IN(\\\"+data+\\\")\\\";\\n}\",\"status\":\"1\"}', '2023-09-08 09:54:14', '1', '管理员');
INSERT INTO `api_log` VALUES (565, 2, 'insert', '13', '{\"action\":\"DC\",\"remark\":\"已审核通过的不能删除\",\"id\":13,\"model_id\":\"21\",\"content\":\"function exec(data){\\n  return \\\"SELECT count(id) FROM asset_maintain where check_status=\'2\' AND id IN(\\\"+data+\\\")\\\";\\n}\",\"status\":\"1\"}', '2023-09-08 10:09:02', '1', '管理员');
INSERT INTO `api_log` VALUES (566, 2, 'insert', '14', '{\"action\":\"DC\",\"remark\":\"已出库的不能删除\",\"id\":14,\"model_id\":\"20\",\"content\":\"function exec(data){\\n  return \\\"SELECT count(id) FROM asset_use where is_out=\'Y\' AND id IN(\\\"+data+\\\")\\\";\\n}\",\"status\":\"1\"}', '2023-09-08 10:09:42', '1', '管理员');
INSERT INTO `api_log` VALUES (567, 1, 'insert', '27', '{\"code\":\"asset_sale\",\"name\":\"资产出售\",\"id\":27,\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"asset_id\",\"excels\":\"AUTO\",\"status\":\"1\"}', '2023-09-08 10:10:51', '1', '管理员');
INSERT INTO `api_log` VALUES (568, 3, 'insert', '27', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":27,\"model_id\":27,\"write\":\"Y\"}', '2023-09-08 10:10:51', '1', '管理员');
INSERT INTO `api_log` VALUES (569, 3, 'insert', '28', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":28,\"model_id\":27,\"write\":\"Y\"}', '2023-09-08 10:10:51', '1', '管理员');
INSERT INTO `api_log` VALUES (570, 3, 'insert', '29', '{\"read\":\"Y\",\"role_id\":\"3\",\"id\":29,\"model_id\":27,\"write\":\"Y\"}', '2023-09-08 10:10:51', '1', '管理员');
INSERT INTO `api_log` VALUES (571, 3, 'insert', '30', '{\"read\":\"Y\",\"role_id\":\"4\",\"id\":30,\"model_id\":27,\"write\":\"Y\"}', '2023-09-08 10:10:51', '1', '管理员');
INSERT INTO `api_log` VALUES (572, 3, 'insert', '31', '{\"read\":\"Y\",\"role_id\":\"5\",\"id\":31,\"model_id\":27,\"write\":\"Y\"}', '2023-09-08 10:10:51', '1', '管理员');
INSERT INTO `api_log` VALUES (573, 3, 'insert', '32', '{\"read\":\"Y\",\"role_id\":\"6\",\"id\":32,\"model_id\":27,\"write\":\"Y\"}', '2023-09-08 10:10:51', '1', '管理员');
INSERT INTO `api_log` VALUES (574, 2, 'update', '14', '{\"action\":\"DC\",\"remark\":\"已出库的不能删除\",\"id\":\"14\",\"model_id\":\"27\",\"is_default\":\"N\",\"content\":\"function exec(data){\\n  return \\\"SELECT count(id) FROM asset_sale where is_out=\'Y\' AND id IN(\\\"+data+\\\")\\\";\\n}\",\"status\":\"1\"}', '2023-09-08 10:11:20', '1', '管理员');
INSERT INTO `api_log` VALUES (575, 19, 'insert', '63', '{\"house_id\":\"5\",\"recv_time\":\"2023-09-08 10:13:59.52\",\"user_id\":\"1\",\"num\":\"42314324\",\"cate_id\":\"41\",\"remark\":\"4324324\",\"id\":63,\"type\":\"1\"}', '2023-09-08 10:14:00', '1', '管理员');
INSERT INTO `api_log` VALUES (576, 19, 'event', '8', '{\"house_id\":\"5\",\"recv_time\":\"2023-09-08 10:13:59.52\",\"user_id\":\"1\",\"num\":\"42314324\",\"cate_id\":\"41\",\"remark\":\"4324324\",\"id\":63,\"type\":\"1\"}', '2023-09-08 10:14:00', '1', '管理员');
INSERT INTO `api_log` VALUES (577, 16, 'insert', '1926', '{\"buy_time\":\"2023-09-08 10:13:59.542\",\"house_id\":\"5\",\"price\":\"3500\",\"num\":\"42314324\",\"cate_id\":\"41\",\"id\":1926,\"model_id\":\"1916\",\"status\":\"1\"}', '2023-09-08 10:14:00', '1', '管理员');
INSERT INTO `api_log` VALUES (578, 18, 'update', '43', '{\"amount\":\"2\",\"purchase_id\":\"33\",\"cate_id\":\"41\",\"id\":\"43\",\"model_id\":\"1916\",\"nums\":\"42314324\",\"ins\":\"1\"}', '2023-09-08 10:14:00', '1', '管理员');
INSERT INTO `api_log` VALUES (579, 19, 'insert', '64', '{\"house_id\":\"6\",\"recv_time\":\"2023-09-08 10:16:37.374\",\"user_id\":\"1\",\"num\":\"HP453254235\",\"cate_id\":\"41\",\"id\":64,\"type\":\"1\"}', '2023-09-08 10:16:37', '1', '管理员');
INSERT INTO `api_log` VALUES (580, 19, 'event', '8', '{\"house_id\":\"6\",\"recv_time\":\"2023-09-08 10:16:37.374\",\"user_id\":\"1\",\"num\":\"HP453254235\",\"cate_id\":\"41\",\"id\":64,\"type\":\"1\"}', '2023-09-08 10:16:37', '1', '管理员');
INSERT INTO `api_log` VALUES (581, 16, 'insert', '1927', '{\"buy_time\":\"2023-09-08 10:16:37.389\",\"house_id\":\"6\",\"price\":\"3500\",\"num\":\"HP453254235\",\"cate_id\":\"41\",\"id\":1927,\"model_id\":\"1916\",\"status\":\"1\"}', '2023-09-08 10:16:37', '1', '管理员');
INSERT INTO `api_log` VALUES (582, 18, 'update', '43', '{\"amount\":\"2\",\"purchase_id\":\"33\",\"cate_id\":\"41\",\"id\":\"43\",\"model_id\":\"1916\",\"nums\":\"42314324,HP453254235\",\"ins\":\"2\"}', '2023-09-08 10:16:37', '1', '管理员');
INSERT INTO `api_log` VALUES (583, 17, 'update', '33', '{\"apply_status\":\"3\",\"id\":\"33\",\"finish_time\":\"2023-09-08 10:52:12.12\"}', '2023-09-08 10:52:12', '1', '管理员');
INSERT INTO `api_log` VALUES (584, 17, 'update', '33', '{\"apply_status\":\"4\",\"id\":\"33\",\"finish_time\":\"2023-09-08 10:52:27.487\"}', '2023-09-08 10:52:27', '1', '管理员');
INSERT INTO `api_log` VALUES (585, 17, 'insert', '34', '{\"reason\":\"54325\",\"user_id\":\"1\",\"price\":\"112800\",\"num\":\"2023090811194119\",\"apply_status\":\"1\",\"comment\":\"543252345\",\"id\":34,\"dept_id\":\"103\"}', '2023-09-08 11:19:52', '1', '管理员');
INSERT INTO `api_log` VALUES (586, 18, 'insert', '44', '{\"amount\":\"2\",\"purchase_id\":34,\"cate_id\":\"36\",\"id\":44,\"model_id\":\"1914\"}', '2023-09-08 11:19:52', '1', '管理员');
INSERT INTO `api_log` VALUES (587, 25, 'insert', '31', '{\"apply_reason\":\"54325\",\"user_id\":\"1\",\"user_name\":\"管理员\",\"num\":\"2023090811194119\",\"target_id\":\"34\",\"apply_time\":\"2023-09-08 11:19:58.99\",\"id\":31,\"type\":\"asset_purchase\",\"title\":\"采购订单\",\"check_status\":\"1\"}', '2023-09-08 11:19:59', '1', '管理员');
INSERT INTO `api_log` VALUES (588, 26, 'insert', '55', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":31,\"pid\":\"0\",\"id\":55,\"check_status\":\"1\"}', '2023-09-08 11:19:59', '1', '管理员');
INSERT INTO `api_log` VALUES (589, 26, 'insert', '56', '{\"check_user_name\":\"一号仓管\",\"check_user_id\":\"3\",\"task_id\":31,\"pid\":\"0\",\"id\":56,\"check_status\":\"1\"}', '2023-09-08 11:19:59', '1', '管理员');
INSERT INTO `api_log` VALUES (590, 25, 'update', '31', '{\"apply_reason\":\"54325\",\"user_id\":\"1\",\"user_name\":\"管理员\",\"num\":\"2023090811194119\",\"target_id\":\"34\",\"apply_time\":\"2023-09-08 11:20:50.788\",\"id\":31,\"type\":\"asset_purchase\",\"title\":\"采购订单\",\"check_status\":\"1\"}', '2023-09-08 11:20:51', '1', '管理员');
INSERT INTO `api_log` VALUES (591, 26, 'insert', '57', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":31,\"pid\":\"0\",\"id\":57,\"check_status\":\"1\"}', '2023-09-08 11:20:51', '1', '管理员');
INSERT INTO `api_log` VALUES (592, 17, 'update', '34', '{\"apply_status\":\"2\",\"apply_time\":\"2023-09-08 11:20:51.219\",\"id\":\"34\",\"check_status\":\"1\"}', '2023-09-08 11:20:51', '1', '管理员');
INSERT INTO `api_log` VALUES (593, 26, 'update', '57', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"31\",\"pid\":\"0\",\"id\":\"57\",\"check_status\":\"2\",\"check_time\":\"2023-09-08 11:21:13.145\"}', '2023-09-08 11:21:13', '1', '管理员');
INSERT INTO `api_log` VALUES (594, 26, 'event', '5', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"31\",\"pid\":\"0\",\"id\":\"57\",\"check_status\":\"2\",\"check_time\":\"2023-09-08 11:21:13.145\"}', '2023-09-08 11:21:13', '1', '管理员');
INSERT INTO `api_log` VALUES (595, 26, 'insert', '58', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":\"31\",\"pid\":\"57\",\"id\":58,\"check_status\":\"1\"}', '2023-09-08 11:21:13', '1', '管理员');
INSERT INTO `api_log` VALUES (596, 26, 'update', '58', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"31\",\"pid\":\"57\",\"id\":\"58\",\"check_status\":\"2\",\"check_time\":\"2023-09-08 11:21:38.248\"}', '2023-09-08 11:21:38', '1', '管理员');
INSERT INTO `api_log` VALUES (597, 26, 'event', '5', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"31\",\"pid\":\"57\",\"id\":\"58\",\"check_status\":\"2\",\"check_time\":\"2023-09-08 11:21:38.248\"}', '2023-09-08 11:21:38', '1', '管理员');
INSERT INTO `api_log` VALUES (598, 25, 'update', '31', '{\"id\":\"31\",\"check_status\":\"2\"}', '2023-09-08 11:21:38', '1', '管理员');
INSERT INTO `api_log` VALUES (599, 17, 'update', '34', '{\"id\":34,\"check_status\":\"2\"}', '2023-09-08 11:21:38', '1', '管理员');
INSERT INTO `api_log` VALUES (600, 17, 'update', '34', '{\"apply_status\":\"3\",\"id\":\"34\",\"finish_time\":\"2023-09-08 11:24:48.624\"}', '2023-09-08 11:24:49', '1', '管理员');
INSERT INTO `api_log` VALUES (601, 22, 'insert', '28', '{\"reason\":\"rweqrwe\",\"amount\":\"1\",\"user_id\":\"1\",\"apply_status\":\"1\",\"cate_id\":\"36\",\"apply_time\":\"2023-09-08 13:45:13.824\",\"id\":28,\"model_id\":\"1914\",\"dept_id\":\"103\"}', '2023-09-08 13:45:14', '1', '管理员');
INSERT INTO `api_log` VALUES (602, 25, 'insert', '32', '{\"apply_reason\":\"rweqrwe\",\"user_id\":\"1\",\"user_name\":\"管理员\",\"num\":\"2023090813463322\",\"target_id\":\"28\",\"apply_time\":\"2023-09-08 13:46:40.44\",\"id\":32,\"type\":\"asset_use\",\"title\":\"使用申请\",\"check_status\":\"1\"}', '2023-09-08 13:46:40', '1', '管理员');
INSERT INTO `api_log` VALUES (603, 26, 'insert', '59', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":32,\"pid\":\"0\",\"id\":59,\"check_status\":\"1\"}', '2023-09-08 13:46:40', '1', '管理员');
INSERT INTO `api_log` VALUES (604, 26, 'insert', '60', '{\"check_user_name\":\"一号仓管\",\"check_user_id\":\"3\",\"task_id\":32,\"pid\":\"0\",\"id\":60,\"check_status\":\"1\"}', '2023-09-08 13:46:40', '1', '管理员');
INSERT INTO `api_log` VALUES (605, 25, 'insert', '33', '{\"apply_reason\":\"rweqrwe\",\"user_id\":\"1\",\"user_name\":\"管理员\",\"num\":\"2023090813472727\",\"target_id\":\"28\",\"apply_time\":\"2023-09-08 13:47:31.801\",\"id\":33,\"type\":\"asset_use\",\"title\":\"使用申请\",\"check_status\":\"1\"}', '2023-09-08 13:47:32', '1', '管理员');
INSERT INTO `api_log` VALUES (606, 26, 'insert', '61', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":33,\"pid\":\"0\",\"id\":61,\"check_status\":\"1\"}', '2023-09-08 13:47:32', '1', '管理员');
INSERT INTO `api_log` VALUES (607, 22, 'update', '28', '{\"apply_status\":\"2\",\"apply_time\":\"2023-09-08 13:47:32.058\",\"id\":\"28\",\"check_status\":\"1\"}', '2023-09-08 13:47:32', '1', '管理员');
INSERT INTO `api_log` VALUES (608, 26, 'update', '61', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"33\",\"pid\":\"0\",\"id\":\"61\",\"check_status\":\"2\",\"check_time\":\"2023-09-08 13:47:44.348\"}', '2023-09-08 13:47:44', '1', '管理员');
INSERT INTO `api_log` VALUES (609, 26, 'event', '5', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"33\",\"pid\":\"0\",\"id\":\"61\",\"check_status\":\"2\",\"check_time\":\"2023-09-08 13:47:44.348\"}', '2023-09-08 13:47:44', '1', '管理员');
INSERT INTO `api_log` VALUES (610, 25, 'update', '33', '{\"id\":\"33\",\"check_status\":\"2\"}', '2023-09-08 13:47:44', '1', '管理员');
INSERT INTO `api_log` VALUES (611, 22, 'update', '28', '{\"id\":28,\"check_status\":\"2\"}', '2023-09-08 13:47:44', '1', '管理员');
INSERT INTO `api_log` VALUES (612, 26, 'update', '59', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"32\",\"pid\":\"0\",\"id\":\"59\",\"check_status\":\"2\",\"check_time\":\"2023-09-08 13:47:56.655\"}', '2023-09-08 13:47:57', '1', '管理员');
INSERT INTO `api_log` VALUES (613, 26, 'event', '5', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"32\",\"pid\":\"0\",\"id\":\"59\",\"check_status\":\"2\",\"check_time\":\"2023-09-08 13:47:56.655\"}', '2023-09-08 13:47:57', '1', '管理员');
INSERT INTO `api_log` VALUES (614, 25, 'update', '32', '{\"id\":\"32\",\"check_status\":\"2\"}', '2023-09-08 13:47:57', '1', '管理员');
INSERT INTO `api_log` VALUES (615, 22, 'update', '28', '{\"id\":28,\"check_status\":\"2\"}', '2023-09-08 13:47:57', '1', '管理员');
INSERT INTO `api_log` VALUES (616, 20, 'insert', '41', '{\"house_id\":\"6\",\"user_id\":\"1\",\"cate_id\":\"36\",\"remark\":\"42134234\",\"id\":41,\"asset_id\":\"1925\",\"type\":\"1\"}', '2023-09-08 15:15:05', '1', '管理员');
INSERT INTO `api_log` VALUES (617, 16, 'update', '1925', '{\"id\":\"1925\",\"status\":\"2\"}', '2023-09-08 15:15:05', '1', '管理员');
INSERT INTO `api_log` VALUES (618, 22, 'update', '28', '{\"recv_time\":\"2023-09-08 15:15:05.672\",\"apply_status\":\"4\",\"id\":\"28\"}', '2023-09-08 15:15:06', '1', '管理员');
INSERT INTO `api_log` VALUES (619, 20, 'insert', '42', '{\"out_time\":\"2023-09-08 15:22:57.092\",\"house_id\":\"6\",\"user_id\":\"1\",\"cate_id\":\"36\",\"remark\":\"324324\",\"id\":42,\"asset_id\":\"1925\",\"type\":\"1\"}', '2023-09-08 15:22:57', '1', '管理员');
INSERT INTO `api_log` VALUES (620, 16, 'update', '1925', '{\"user_id\":\"1\",\"id\":\"1925\",\"status\":\"2\"}', '2023-09-08 15:22:57', '1', '管理员');
INSERT INTO `api_log` VALUES (621, 22, 'update', '28', '{\"recv_time\":\"2023-09-08 15:22:57.2\",\"apply_status\":\"4\",\"id\":\"28\",\"nums\":\"2U-2023-9-1\"}', '2023-09-08 15:22:57', '1', '管理员');
INSERT INTO `api_log` VALUES (624, 19, 'insert', '67', '{\"house_id\":\"5\",\"recv_time\":\"2023-09-08 16:10:31.762\",\"user_id\":\"1\",\"num\":\"2U-2023-9-1\",\"cate_id\":\"36\",\"remark\":\"43214\",\"id\":67,\"type\":\"2\"}', '2023-09-08 16:10:32', '1', '管理员');
INSERT INTO `api_log` VALUES (625, 16, 'update', '1925', '{\"house_id\":\"5\",\"id\":\"1925\",\"status\":\"1\"}', '2023-09-08 16:10:32', '1', '管理员');
INSERT INTO `api_log` VALUES (626, 22, 'insert', '29', '{\"reason\":\"243432434\",\"amount\":\"2\",\"user_id\":\"1\",\"apply_status\":\"1\",\"cate_id\":\"41\",\"apply_time\":\"2023-09-08 16:19:31.392\",\"id\":29,\"model_id\":\"1916\",\"dept_id\":\"103\"}', '2023-09-08 16:19:31', '1', '管理员');
INSERT INTO `api_log` VALUES (627, 25, 'insert', '34', '{\"apply_reason\":\"243432434\",\"user_id\":\"1\",\"user_name\":\"管理员\",\"num\":\"2023090816194523\",\"target_id\":\"29\",\"apply_time\":\"2023-09-08 16:19:49.673\",\"id\":34,\"type\":\"asset_use\",\"title\":\"使用申请\",\"check_status\":\"1\"}', '2023-09-08 16:19:50', '1', '管理员');
INSERT INTO `api_log` VALUES (628, 26, 'insert', '62', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":34,\"pid\":\"0\",\"id\":62,\"check_status\":\"1\"}', '2023-09-08 16:19:50', '1', '管理员');
INSERT INTO `api_log` VALUES (629, 26, 'insert', '63', '{\"check_user_name\":\"若依\",\"check_user_id\":\"2\",\"task_id\":34,\"pid\":\"0\",\"id\":63,\"check_status\":\"1\"}', '2023-09-08 16:19:50', '1', '管理员');
INSERT INTO `api_log` VALUES (630, 22, 'update', '29', '{\"apply_status\":\"2\",\"apply_time\":\"2023-09-08 16:19:49.932\",\"id\":\"29\",\"check_status\":\"1\"}', '2023-09-08 16:19:50', '1', '管理员');
INSERT INTO `api_log` VALUES (631, 26, 'update', '62', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"check_reason\":\"42134324\",\"ignore_flag\":\"0\",\"task_id\":\"34\",\"pid\":\"0\",\"id\":\"62\",\"check_status\":\"3\",\"check_time\":\"2023-09-08 16:20:34.061\"}', '2023-09-08 16:20:34', '1', '管理员');
INSERT INTO `api_log` VALUES (632, 26, 'event', '5', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"check_reason\":\"42134324\",\"ignore_flag\":\"0\",\"task_id\":\"34\",\"pid\":\"0\",\"id\":\"62\",\"check_status\":\"3\",\"check_time\":\"2023-09-08 16:20:34.061\"}', '2023-09-08 16:20:34', '1', '管理员');
INSERT INTO `api_log` VALUES (633, 25, 'update', '34', '{\"id\":\"34\",\"check_status\":\"3\"}', '2023-09-08 16:20:34', '1', '管理员');
INSERT INTO `api_log` VALUES (634, 22, 'update', '29', '{\"id\":29,\"check_status\":\"3\"}', '2023-09-08 16:20:34', '1', '管理员');
INSERT INTO `api_log` VALUES (635, 22, 'insert', '30', '{\"reason\":\"3454\",\"amount\":\"2\",\"user_id\":\"1\",\"apply_status\":\"1\",\"cate_id\":\"41\",\"apply_time\":\"2023-09-08 16:21:09.877\",\"id\":30,\"model_id\":\"1916\",\"dept_id\":\"103\"}', '2023-09-08 16:21:10', '1', '管理员');
INSERT INTO `api_log` VALUES (636, 25, 'insert', '35', '{\"apply_reason\":\"3454\",\"user_id\":\"1\",\"user_name\":\"管理员\",\"num\":\"20230908162112210\",\"target_id\":\"30\",\"apply_time\":\"2023-09-08 16:21:16.246\",\"id\":35,\"type\":\"asset_use\",\"title\":\"使用申请\",\"check_status\":\"1\"}', '2023-09-08 16:21:16', '1', '管理员');
INSERT INTO `api_log` VALUES (637, 26, 'insert', '64', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":35,\"pid\":\"0\",\"id\":64,\"check_status\":\"1\"}', '2023-09-08 16:21:16', '1', '管理员');
INSERT INTO `api_log` VALUES (638, 22, 'update', '30', '{\"apply_status\":\"2\",\"apply_time\":\"2023-09-08 16:21:16.613\",\"id\":\"30\",\"check_status\":\"1\"}', '2023-09-08 16:21:17', '1', '管理员');
INSERT INTO `api_log` VALUES (639, 26, 'update', '64', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"35\",\"pid\":\"0\",\"id\":\"64\",\"check_status\":\"2\",\"check_time\":\"2023-09-08 16:21:24.314\"}', '2023-09-08 16:21:24', '1', '管理员');
INSERT INTO `api_log` VALUES (640, 26, 'event', '5', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"35\",\"pid\":\"0\",\"id\":\"64\",\"check_status\":\"2\",\"check_time\":\"2023-09-08 16:21:24.314\"}', '2023-09-08 16:21:24', '1', '管理员');
INSERT INTO `api_log` VALUES (641, 25, 'update', '35', '{\"id\":\"35\",\"check_status\":\"2\"}', '2023-09-08 16:21:24', '1', '管理员');
INSERT INTO `api_log` VALUES (642, 22, 'update', '30', '{\"id\":30,\"check_status\":\"2\"}', '2023-09-08 16:21:24', '1', '管理员');
INSERT INTO `api_log` VALUES (643, 20, 'insert', '43', '{\"out_time\":\"2023-09-08 16:21:39.859\",\"house_id\":\"5\",\"user_id\":\"1\",\"cate_id\":\"41\",\"remark\":\"43523454235\",\"id\":43,\"asset_id\":\"1926\",\"type\":\"1\"}', '2023-09-08 16:21:40', '1', '管理员');
INSERT INTO `api_log` VALUES (644, 16, 'update', '1926', '{\"user_id\":\"1\",\"id\":\"1926\",\"status\":\"2\"}', '2023-09-08 16:21:40', '1', '管理员');
INSERT INTO `api_log` VALUES (645, 22, 'update', '30', '{\"recv_time\":\"2023-09-08 16:21:39.877\",\"apply_status\":\"3\",\"id\":\"30\",\"nums\":\"42314324\"}', '2023-09-08 16:21:40', '1', '管理员');
INSERT INTO `api_log` VALUES (646, 15, 'insert', '1918', '{\"life_time\":\"100\",\"factory\":\"TPLINK\",\"unit\":\"台\",\"price\":\"190\",\"name\":\"TPLINK_100\",\"cate_id\":\"40\",\"id\":1918,\"params\":\"100M\",\"price1\":\"210\"}', '2023-09-11 14:45:44', '1', '管理员');
INSERT INTO `api_log` VALUES (647, 16, 'insert', '1928', '{\"buy_time\":\"2021-08-03\",\"house_id\":\"6\",\"price\":\"120\",\"num\":\"TP-100-01\",\"cate_id\":\"40\",\"id\":1928,\"model_id\":\"1918\",\"dept_id\":\"103\",\"status\":\"1\"}', '2023-09-11 14:46:29', '1', '管理员');
INSERT INTO `api_log` VALUES (648, 16, 'update', '1924', '{\"house_id\":\"5\",\"num\":\"34324\",\"cate_id\":\"42\",\"model_id\":\"1917\",\"buy_time\":\"2023-05-06 00:00:00\",\"price\":\"34\",\"comment\":\"\",\"id\":1924,\"dept_id\":\"102\",\"status\":\"1\"}', '2023-09-11 14:54:46', '1', '管理员');
INSERT INTO `api_log` VALUES (649, 16, 'update', '1925', '{\"house_id\":\"5\",\"num\":\"2U-2023-9-1\",\"cate_id\":\"36\",\"model_id\":\"1914\",\"buy_time\":\"2023-05-06 00:00:00\",\"price\":\"34000\",\"comment\":\"\",\"id\":1925,\"dept_id\":\"100\",\"status\":\"1\"}', '2023-09-11 14:54:46', '1', '管理员');
INSERT INTO `api_log` VALUES (650, 16, 'update', '1926', '{\"house_id\":\"5\",\"num\":\"42314324\",\"cate_id\":\"41\",\"model_id\":\"1916\",\"buy_time\":\"2023-09-08 00:00:00\",\"user_id\":\"1\",\"price\":\"3500\",\"comment\":\"\",\"id\":1926,\"status\":\"2\"}', '2023-09-11 14:54:46', '1', '管理员');
INSERT INTO `api_log` VALUES (651, 16, 'update', '1927', '{\"house_id\":\"6\",\"num\":\"HP453254235\",\"cate_id\":\"41\",\"model_id\":\"1916\",\"buy_time\":\"2023-09-08 00:00:00\",\"price\":\"3500\",\"comment\":\"\",\"id\":1927,\"status\":\"1\"}', '2023-09-11 14:54:46', '1', '管理员');
INSERT INTO `api_log` VALUES (652, 16, 'update', '1928', '{\"house_id\":\"6\",\"num\":\"TP-100-01\",\"cate_id\":\"40\",\"model_id\":\"1918\",\"buy_time\":\"2021-08-03 00:00:00\",\"price\":\"120\",\"comment\":\"\",\"id\":1928,\"dept_id\":\"103\",\"status\":\"1\"}', '2023-09-11 14:54:46', '1', '管理员');
INSERT INTO `api_log` VALUES (653, 16, 'insert', '1929', '{\"buy_time\":\"2023-05-06 00:00:00\",\"house_id\":\"5\",\"price\":\"45\",\"num\":\"34324434543\",\"cate_id\":\"42\",\"comment\":\"\",\"id\":1929,\"model_id\":\"1917\",\"dept_id\":\"102\",\"status\":\"1\"}', '2023-09-11 14:54:46', '1', '管理员');
INSERT INTO `api_log` VALUES (654, 16, 'insert', '1930', '{\"buy_time\":\"2023-05-06 00:00:00\",\"house_id\":\"5\",\"price\":\"34000\",\"num\":\"2U-2023-8-1\",\"cate_id\":\"36\",\"comment\":\"\",\"id\":1930,\"model_id\":\"1914\",\"dept_id\":\"100\",\"status\":\"1\"}', '2023-09-11 14:54:46', '1', '管理员');
INSERT INTO `api_log` VALUES (655, 16, 'insert', '1931', '{\"buy_time\":\"2023-08-08 00:00:00\",\"house_id\":\"5\",\"user_id\":\"1\",\"price\":\"3500\",\"num\":\"4231432433\",\"cate_id\":\"41\",\"comment\":\"\",\"id\":1931,\"model_id\":\"1916\",\"status\":\"2\"}', '2023-09-11 14:54:46', '1', '管理员');
INSERT INTO `api_log` VALUES (656, 16, 'insert', '1932', '{\"buy_time\":\"2023-09-08 00:00:00\",\"house_id\":\"6\",\"price\":\"3500\",\"num\":\"HP4538774235\",\"cate_id\":\"41\",\"comment\":\"\",\"id\":1932,\"model_id\":\"1916\",\"status\":\"1\"}', '2023-09-11 14:54:46', '1', '管理员');
INSERT INTO `api_log` VALUES (657, 16, 'insert', '1933', '{\"buy_time\":\"2021-08-03 00:00:00\",\"house_id\":\"6\",\"price\":\"120\",\"num\":\"TP-100-02\",\"cate_id\":\"40\",\"comment\":\"\",\"id\":1933,\"model_id\":\"1918\",\"dept_id\":\"103\",\"status\":\"1\"}', '2023-09-11 14:54:46', '1', '管理员');
INSERT INTO `api_log` VALUES (658, 21, 'insert', '1', '{\"reason\":\"34432\",\"user_id\":\"1\",\"cate_id\":\"36\",\"apply_time\":\"2023-09-11 16:10:07.13\",\"id\":1,\"asset_id\":\"1930\",\"type\":\"1\",\"status\":\"1\"}', '2023-09-11 16:10:07', '1', '管理员');
INSERT INTO `api_log` VALUES (659, 21, 'update', '1', '{\"reason\":\"34432\",\"user_id\":\"1\",\"cate_id\":\"36\",\"comment\":\"234234\",\"apply_time\":\"2023-09-11T16:10:07\",\"id\":\"1\",\"asset_id\":\"1930\",\"type\":\"1\",\"status\":\"2\"}', '2023-09-11 16:12:49', '1', '管理员');
INSERT INTO `api_log` VALUES (660, 21, 'update', '1', '{\"reason\":\"34432\",\"cate_id\":\"36\",\"apply_time\":\"2023-09-11T16:10:07\",\"asset_id\":\"1930\",\"type\":\"1\",\"finish_time\":\"2023-09-11\",\"money\":\"365\",\"user_id\":\"1\",\"comment\":\"234234\",\"id\":\"1\",\"status\":\"3\"}', '2023-09-11 16:14:01', '1', '管理员');
INSERT INTO `api_log` VALUES (661, 21, 'update', '1', '{\"reason\":\"34432\",\"cate_id\":\"36\",\"apply_time\":\"2023-09-11T16:10:07\",\"asset_id\":\"1930\",\"type\":\"1\",\"finish_time\":\"2023-09-11 16:16:18\",\"money\":\"365\",\"user_id\":\"1\",\"comment\":\"234234\",\"id\":\"1\",\"status\":\"3\"}', '2023-09-11 16:16:19', '1', '管理员');
INSERT INTO `api_log` VALUES (662, 1, 'update', '21', '{\"code\":\"asset_maintain\",\"name\":\"资产维保\",\"id\":\"21\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"excels\":\"{\\n  \\\"asset_maintain\\\":{\\n\\t\\\"cate_id\\\":\\\"资产分类\\\",\\n\\t\\\"asset_id\\\":\\\"资产编号\\\",\\n\\t\\\"type\\\":\\\"维保类型\\\",\\n\\t\\\"reason\\\":\\\"维保原由\\\",\\n\\t\\\"status\\\":\\\"维保状态\\\",\\n\\t\\\"money\\\":\\\"费用\\\",\\n\\t\\\"comment\\\":\\\"说明\\\",\\n\\t\\\"user_id\\\":\\\"负责人\\\",\\n\\t\\\"apply_time\\\":\\\"发起时间\\\",\\n\\t\\\"finish_time\\\":\\\"完成时间\\\"\\n  },\\n\\\"@dict\\\":{\\n\\\"type\\\":{\\\"1\\\":\\\"维修\\\",\\\"2\\\":\\\"维保\\\"},\\n\\\"status\\\":{\\\"1\\\":\\\"未开始\\\",\\\"2\\\":\\\"进行中\\\",\\\"3\\\":\\\"已完成\\\",\\\"4\\\":\\', '2023-09-11 16:17:38', '1', '管理员');
INSERT INTO `api_log` VALUES (663, 27, 'insert', '1', '{\"reason\":\"431241324\",\"user_id\":\"1\",\"user_name\":\"管理员\",\"price\":\"45\",\"dept_name\":\"长沙分公司\",\"cate_id\":\"42\",\"id\":1,\"asset_id\":\"1929\",\"dept_id\":\"102\",\"asset_num\":\"34324434543\",\"sale_time\":\"2023-09-04\"}', '2023-09-11 16:42:34', '1', '管理员');
INSERT INTO `api_log` VALUES (664, 20, 'insert', '44', '{\"out_time\":\"2023-09-11 17:07:02.059\",\"house_id\":\"5\",\"user_id\":\"1\",\"cate_id\":\"42\",\"remark\":\"435435\",\"id\":44,\"asset_id\":\"1929\",\"type\":\"3\"}', '2023-09-11 17:07:02', '1', '管理员');
INSERT INTO `api_log` VALUES (665, 27, 'update', '1', '{\"is_out\":\"Y\",\"id\":\"1\"}', '2023-09-11 17:07:02', '1', '管理员');
INSERT INTO `api_log` VALUES (666, 1, 'update', '27', '{\"code\":\"asset_sale\",\"name\":\"资产出售\",\"id\":\"27\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"asset_id\",\"excels\":\"{\\n  \\\"asset_sale\\\":{\\n\\t\\\"cate_id\\\":\\\"资产分类\\\",\\n\\t\\\"asset_num\\\":\\\"资产编号\\\",\\n\\t\\\"dept_name\\\":\\\"归属部门\\\",\\n\\t\\\"user_name\\\":\\\"操作人\\\",\\n\\t\\\"price\\\":\\\"价格\\\",\\n\\t\\\"reason\\\":\\\"出售原由\\\",\\n\\t\\\"sale_time\\\":\\\"出售时间\\\",\\n\\t\\\"is_out\\\":\\\"已出库\\\"\\n  },\\n\\t\\\"@dict\\\":{\\n\\\"is_out:{\\\"Y\\\":\\\"是\\\",\\\"N\\\":\\\"否\\\"}},\\n\\t\\\"@ds\\\":{cate_id:\\\"@category_select\\\"},\\n\\t\\\"@excel\\\":{}\\n}\",\"status\":\"1\"}', '2023-09-11 17:31:13', '1', '管理员');
INSERT INTO `api_log` VALUES (667, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":27,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:31:14', '1', '管理员');
INSERT INTO `api_log` VALUES (668, 3, 'update', '27', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":27,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:31:14', '1', '管理员');
INSERT INTO `api_log` VALUES (669, 3, 'insert', '33', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":33,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:31:14', '1', '管理员');
INSERT INTO `api_log` VALUES (670, 3, 'insert', '34', '{\"read\":\"Y\",\"role_id\":\"3\",\"id\":34,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:31:14', '1', '管理员');
INSERT INTO `api_log` VALUES (671, 3, 'insert', '35', '{\"read\":\"Y\",\"role_id\":\"4\",\"id\":35,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:31:14', '1', '管理员');
INSERT INTO `api_log` VALUES (672, 3, 'insert', '36', '{\"read\":\"Y\",\"role_id\":\"5\",\"id\":36,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:31:14', '1', '管理员');
INSERT INTO `api_log` VALUES (673, 3, 'insert', '37', '{\"read\":\"Y\",\"role_id\":\"6\",\"id\":37,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:31:14', '1', '管理员');
INSERT INTO `api_log` VALUES (674, 1, 'update', '27', '{\"code\":\"asset_sale\",\"name\":\"资产出售\",\"id\":\"27\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"asset_id\",\"excels\":\"{\\n  \\\"asset_sale\\\":{\\n\\t\\\"cate_id\\\":\\\"资产分类\\\",\\n\\t\\\"asset_num\\\":\\\"资产编号\\\",\\n\\t\\\"dept_name\\\":\\\"归属部门\\\",\\n\\t\\\"user_name\\\":\\\"操作人\\\",\\n\\t\\\"price\\\":\\\"价格\\\",\\n\\t\\\"reason\\\":\\\"出售原由\\\",\\n\\t\\\"sale_time\\\":\\\"出售时间\\\",\\n\\t\\\"is_out\\\":\\\"已出库\\\"\\n  },\\n\\t\\\"@dict\\\":{\\n\\\"is_out:{\\\"Y\\\":\\\"是\\\",\\\"N\\\":\\\"否\\\"}},\\n\\t\\\"@ds\\\":{cate_id:\\\"@asset_category_select\\\"},\\n\\t\\\"@excel\\\":{}\\n}\",\"status\":\"1\"}', '2023-09-11 17:31:37', '1', '管理员');
INSERT INTO `api_log` VALUES (675, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":33,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:31:37', '1', '管理员');
INSERT INTO `api_log` VALUES (676, 3, 'update', '33', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":33,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:31:37', '1', '管理员');
INSERT INTO `api_log` VALUES (677, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"3\",\"id\":34,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:31:37', '1', '管理员');
INSERT INTO `api_log` VALUES (678, 3, 'update', '34', '{\"read\":\"Y\",\"role_id\":\"3\",\"id\":34,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:31:37', '1', '管理员');
INSERT INTO `api_log` VALUES (679, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"4\",\"id\":35,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:31:37', '1', '管理员');
INSERT INTO `api_log` VALUES (680, 3, 'update', '35', '{\"read\":\"Y\",\"role_id\":\"4\",\"id\":35,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:31:37', '1', '管理员');
INSERT INTO `api_log` VALUES (681, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"5\",\"id\":36,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:31:37', '1', '管理员');
INSERT INTO `api_log` VALUES (682, 3, 'update', '36', '{\"read\":\"Y\",\"role_id\":\"5\",\"id\":36,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:31:37', '1', '管理员');
INSERT INTO `api_log` VALUES (683, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"6\",\"id\":37,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:31:37', '1', '管理员');
INSERT INTO `api_log` VALUES (684, 3, 'update', '37', '{\"read\":\"Y\",\"role_id\":\"6\",\"id\":37,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:31:37', '1', '管理员');
INSERT INTO `api_log` VALUES (685, 1, 'update', '27', '{\"code\":\"asset_sale\",\"name\":\"资产出售\",\"id\":\"27\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"asset_id\",\"excels\":\"{\\n  \\\"asset_sale\\\":{\\n\\t\\\"cate_id\\\":\\\"资产分类\\\",\\n\\t\\\"asset_num\\\":\\\"资产编号\\\",\\n\\t\\\"dept_name\\\":\\\"归属部门\\\",\\n\\t\\\"user_name\\\":\\\"操作人\\\",\\n\\t\\\"price\\\":\\\"价格\\\",\\n\\t\\\"reason\\\":\\\"出售原由\\\",\\n\\t\\\"sale_time\\\":\\\"出售时间\\\",\\n\\t\\\"is_out\\\":\\\"已出库\\\"\\n  },\\n\\t\\\"@dict\\\":{\\n\\\"is_out\\\":{\\\"Y\\\":\\\"是\\\",\\\"N\\\":\\\"否\\\"}},\\n\\t\\\"@ds\\\":{cate_id:\\\"@asset_category_select\\\"},\\n\\t\\\"@excel\\\":{}\\n}\",\"status\":\"1\"}', '2023-09-11 17:32:25', '1', '管理员');
INSERT INTO `api_log` VALUES (686, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":33,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:32:25', '1', '管理员');
INSERT INTO `api_log` VALUES (687, 3, 'update', '33', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":33,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:32:25', '1', '管理员');
INSERT INTO `api_log` VALUES (688, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"3\",\"id\":34,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:32:25', '1', '管理员');
INSERT INTO `api_log` VALUES (689, 3, 'update', '34', '{\"read\":\"Y\",\"role_id\":\"3\",\"id\":34,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:32:25', '1', '管理员');
INSERT INTO `api_log` VALUES (690, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"4\",\"id\":35,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:32:25', '1', '管理员');
INSERT INTO `api_log` VALUES (691, 3, 'update', '35', '{\"read\":\"Y\",\"role_id\":\"4\",\"id\":35,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:32:25', '1', '管理员');
INSERT INTO `api_log` VALUES (692, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"5\",\"id\":36,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:32:25', '1', '管理员');
INSERT INTO `api_log` VALUES (693, 3, 'update', '36', '{\"read\":\"Y\",\"role_id\":\"5\",\"id\":36,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:32:25', '1', '管理员');
INSERT INTO `api_log` VALUES (694, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"6\",\"id\":37,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:32:25', '1', '管理员');
INSERT INTO `api_log` VALUES (695, 3, 'update', '37', '{\"read\":\"Y\",\"role_id\":\"6\",\"id\":37,\"model_id\":\"27\",\"write\":\"Y\"}', '2023-09-11 17:32:25', '1', '管理员');
INSERT INTO `api_log` VALUES (696, 2, 'update', '13', '{\"action\":\"DC\",\"remark\":\"已审核通过的不能删除\",\"id\":\"13\",\"model_id\":\"21\",\"is_default\":\"N\",\"content\":\"function exec(data){\\n  return \\\"SELECT count(id) FROM asset_maintain where check_status=\'2\' AND id IN(\\\"+data+\\\")\\\";\\n}\",\"status\":\"2\"}', '2023-09-11 17:43:39', '1', '管理员');
INSERT INTO `api_log` VALUES (697, 23, 'insert', '1', '{\"start_time\":\"2023-09-12 09:48:35\",\"user_id\":\"1\",\"name\":\"2023-09-12\",\"end_time\":\"2023-09-13 10:00:00\",\"remark\":\"4324\",\"id\":1,\"status\":\"1\"}', '2023-09-12 09:48:52', '1', '管理员');
INSERT INTO `api_log` VALUES (698, 23, 'event', '6', '{\"start_time\":\"2023-09-12 09:48:35\",\"user_id\":\"1\",\"name\":\"2023-09-12\",\"end_time\":\"2023-09-13 10:00:00\",\"remark\":\"4324\",\"id\":1,\"status\":\"1\"}', '2023-09-12 09:48:52', '1', '管理员');
INSERT INTO `api_log` VALUES (699, 24, 'update', '10', '{\"house_id\":\"6\",\"model_name\":\"TPLINK_100\",\"num\":\"TP-100-02\",\"cate_name\":\"路由器\",\"id\":\"10\",\"asset_id\":\"1933\",\"stock_id\":\"1\",\"price1\":\"120\",\"status\":\"3\"}', '2023-09-12 09:54:14', '1', '管理员');
INSERT INTO `api_log` VALUES (700, 24, 'update', '10', '{\"house_id\":\"6\",\"num\":\"TP-100-02\",\"asset_id\":\"1933\",\"stock_id\":\"1\",\"model_name\":\"TPLINK_100\",\"price\":\"60\",\"cate_name\":\"路由器\",\"id\":\"10\",\"price1\":\"120\",\"status\":\"3\"}', '2023-09-12 09:54:18', '1', '管理员');
INSERT INTO `api_log` VALUES (701, 24, 'update', '9', '{\"house_id\":\"6\",\"model_name\":\"HP 1000\",\"num\":\"HP4538774235\",\"cate_name\":\"打印机\",\"id\":\"9\",\"asset_id\":\"1932\",\"stock_id\":\"1\",\"price1\":\"3500\",\"status\":\"1\"}', '2023-09-12 09:54:20', '1', '管理员');
INSERT INTO `api_log` VALUES (702, 24, 'update', '8', '{\"house_id\":\"5\",\"model_name\":\"HP 1000\",\"user_id\":\"1\",\"num\":\"4231432433\",\"cate_name\":\"打印机\",\"id\":\"8\",\"asset_id\":\"1931\",\"stock_id\":\"1\",\"price1\":\"3500\",\"status\":\"2\"}', '2023-09-12 09:54:23', '1', '管理员');
INSERT INTO `api_log` VALUES (703, 1, 'update', '24', '{\"code\":\"asset_stock_detail\",\"name\":\"资产盘点明细\",\"id\":\"24\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"stock_id,asset_id\",\"excels\":\"AUTO\",\"status\":\"1\"}', '2023-09-12 10:08:53', '1', '管理员');
INSERT INTO `api_log` VALUES (704, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":19,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-12 10:08:53', '1', '管理员');
INSERT INTO `api_log` VALUES (705, 3, 'update', '19', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":19,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-12 10:08:53', '1', '管理员');
INSERT INTO `api_log` VALUES (706, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":20,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-12 10:08:53', '1', '管理员');
INSERT INTO `api_log` VALUES (707, 3, 'update', '20', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":20,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-12 10:08:53', '1', '管理员');
INSERT INTO `api_log` VALUES (708, 1, 'update', '24', '{\"code\":\"asset_stock_detail\",\"name\":\"资产盘点明细\",\"id\":\"24\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"stock_id,asset_id\",\"excels\":\"{\\n  \\\"asset_stock_detail\\\":{\\n\\t\\\"id\\\":\\\"编号\\\",\\n\\t\\\"cate_name\\\":\\\"资产分类\\\",\\n\\t\\\"model_name\\\":\\\"资产型号\\\",\\n\\t\\\"num\\\":\\\"资产编码\\\",\\n\\t\\\"status\\\":\\\"当前状态\\\",\\n\\t\\\"user_id\\\":\\\"使用人\\\",\\n\\t\\\"price1\\\":\\\"原价值\\\",\\n\\t\\\"price\\\":\\\"当前价值\\\",\\n\\t\\\"house_id\\\":\\\"所在仓库\\\",\\n\\t\\\"stock_time\\\":\\\"盘点时间\\\"\\n  },\\n\\t\\\"@dict\\\":{},\\n\\t\\\"@ds\\\":{\\\"user_id\\\":\\\"@user_select\\\", \\\"house_id\\\":\\', '2023-09-12 10:11:53', '1', '管理员');
INSERT INTO `api_log` VALUES (709, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":19,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-12 10:11:53', '1', '管理员');
INSERT INTO `api_log` VALUES (710, 3, 'update', '19', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":19,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-12 10:11:53', '1', '管理员');
INSERT INTO `api_log` VALUES (711, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":20,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-12 10:11:53', '1', '管理员');
INSERT INTO `api_log` VALUES (712, 3, 'update', '20', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":20,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-12 10:11:53', '1', '管理员');
INSERT INTO `api_log` VALUES (713, 1, 'update', '16', '{\"code\":\"asset_info\",\"name\":\"资产信息\",\"id\":\"16\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"num\",\"excels\":\"{\\n\\t\\\"asset_info\\\": {\\n\\t\\t\\\"cate_id\\\": \\\"资产分类\\\",\\n\\t\\t\\\"model_id\\\": \\\"资产型号\\\",\\n\\t\\t\\\"num\\\": \\\"编号\\\",\\n\\t\\t\\\"house_id\\\": \\\"所在仓库\\\",\\n\\t\\t\\\"dept_id\\\": \\\"所属部门\\\",\\n\\t\\t\\\"user_id\\\": \\\"负责人\\\",\\n\\t\\t\\\"status\\\": \\\"状态\\\",\\n\\t\\t\\\"price\\\": \\\"当前价值\\\",\\n\\t\\t\\\"buy_time\\\": \\\"购买时间\\\",\\n\\t\\t\\\"comment\\\": \\\"说明\\\"\\n\\t},\\n\\t\\\"@ds\\\": {\\n\\t\\t\\\"user_id\\\": \\\"SELECT user_id value, nick_name label FROM ', '2023-09-12 10:12:40', '1', '管理员');
INSERT INTO `api_log` VALUES (714, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":13,\"model_id\":\"16\",\"write\":\"Y\"}', '2023-09-12 10:12:40', '1', '管理员');
INSERT INTO `api_log` VALUES (715, 3, 'update', '13', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":13,\"model_id\":\"16\",\"write\":\"Y\"}', '2023-09-12 10:12:40', '1', '管理员');
INSERT INTO `api_log` VALUES (716, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":14,\"model_id\":\"16\",\"write\":\"N\"}', '2023-09-12 10:12:40', '1', '管理员');
INSERT INTO `api_log` VALUES (717, 3, 'update', '14', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":14,\"model_id\":\"16\",\"write\":\"N\"}', '2023-09-12 10:12:40', '1', '管理员');
INSERT INTO `api_log` VALUES (718, 1, 'update', '24', '{\"code\":\"asset_stock_detail\",\"name\":\"资产盘点明细\",\"id\":\"24\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"stock_id,asset_id\",\"excels\":\"{\\n  \\\"asset_stock_detail\\\":{\\n\\t\\\"id\\\":\\\"编号\\\",\\n\\t\\\"cate_name\\\":\\\"资产分类\\\",\\n\\t\\\"model_name\\\":\\\"资产型号\\\",\\n\\t\\\"num\\\":\\\"资产编码\\\",\\n\\t\\\"status\\\":\\\"当前状态\\\",\\n\\t\\\"user_id\\\":\\\"使用人\\\",\\n\\t\\\"price1\\\":\\\"原价值\\\",\\n\\t\\\"price\\\":\\\"当前价值\\\",\\n\\t\\\"house_id\\\":\\\"所在仓库\\\",\\n\\t\\\"stock_time\\\":\\\"盘点时间\\\"\\n  },\\n\\t\\\"@dict\\\":{\\\"status\\\": {\\\"1\\\":\\\"闲置\\\", \\\"2\\\":\\\"在用\\\", \\\"3\\\":\\\"损坏\\\",\\\"4\\\"', '2023-09-12 10:12:51', '1', '管理员');
INSERT INTO `api_log` VALUES (719, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":19,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-12 10:12:51', '1', '管理员');
INSERT INTO `api_log` VALUES (720, 3, 'update', '19', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":19,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-12 10:12:51', '1', '管理员');
INSERT INTO `api_log` VALUES (721, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":20,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-12 10:12:51', '1', '管理员');
INSERT INTO `api_log` VALUES (722, 3, 'update', '20', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":20,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-12 10:12:51', '1', '管理员');
INSERT INTO `api_log` VALUES (723, NULL, 'error', NULL, '脚本未定义 @asset_warehouse_select', '2023-09-12 10:12:56', '1', '管理员');
INSERT INTO `api_log` VALUES (724, NULL, 'error', NULL, '脚本未定义 @asset_warehouse_select', '2023-09-12 10:12:59', '1', '管理员');
INSERT INTO `api_log` VALUES (725, 1, 'update', '24', '{\"code\":\"asset_stock_detail\",\"name\":\"资产盘点明细\",\"id\":\"24\",\"is_default\":\"N\",\"group\":\"asset\",\"pk_field\":\"id\",\"unique_field\":\"stock_id,asset_id\",\"excels\":\"{\\n  \\\"asset_stock_detail\\\":{\\n\\t\\\"id\\\":\\\"编号\\\",\\n\\t\\\"cate_name\\\":\\\"资产分类\\\",\\n\\t\\\"model_name\\\":\\\"资产型号\\\",\\n\\t\\\"num\\\":\\\"资产编码\\\",\\n\\t\\\"status\\\":\\\"当前状态\\\",\\n\\t\\\"user_id\\\":\\\"使用人\\\",\\n\\t\\\"price1\\\":\\\"原价值\\\",\\n\\t\\\"price\\\":\\\"当前价值\\\",\\n\\t\\\"house_id\\\":\\\"所在仓库\\\",\\n\\t\\\"stock_time\\\":\\\"盘点时间\\\"\\n  },\\n\\t\\\"@dict\\\":{\\\"status\\\": {\\\"1\\\":\\\"闲置\\\", \\\"2\\\":\\\"在用\\\", \\\"3\\\":\\\"损坏\\\",\\\"4\\\"', '2023-09-12 10:13:26', '1', '管理员');
INSERT INTO `api_log` VALUES (726, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":19,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-12 10:13:26', '1', '管理员');
INSERT INTO `api_log` VALUES (727, 3, 'update', '19', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":19,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-12 10:13:26', '1', '管理员');
INSERT INTO `api_log` VALUES (728, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":20,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-12 10:13:26', '1', '管理员');
INSERT INTO `api_log` VALUES (729, 3, 'update', '20', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":20,\"model_id\":\"24\",\"write\":\"Y\"}', '2023-09-12 10:13:26', '1', '管理员');
INSERT INTO `api_log` VALUES (730, 24, 'update', '1', '{\"house_id\":\"5\",\"model_name\":\"立式\",\"stock_time\":\"2023-09-12 10:30:00\",\"price\":27.2,\"num\":\"34324\",\"cate_name\":\"饮水机\",\"id\":\"1\",\"price1\":\"34\",\"status\":\"1\"}', '2023-09-12 10:21:31', '1', '管理员');
INSERT INTO `api_log` VALUES (731, 24, 'update', '1', '{\"house_id\":\"5\",\"model_name\":\"立式\",\"stock_time\":\"2023-09-12 10:30:00\",\"price\":27.2,\"num\":\"34324\",\"cate_name\":\"饮水机\",\"id\":\"1\",\"price1\":\"34\",\"status\":\"1\"}', '2023-09-12 10:21:31', '1', '管理员');
INSERT INTO `api_log` VALUES (732, 24, 'update', '2', '{\"house_id\":\"5\",\"model_name\":\"2U-X86\",\"stock_time\":\"2023-09-12 10:30:00\",\"price\":\"27200\",\"num\":\"2U-2023-9-1\",\"cate_name\":\"服务器\",\"id\":\"2\",\"price1\":\"34000\",\"status\":\"1\"}', '2023-09-12 10:21:31', '1', '管理员');
INSERT INTO `api_log` VALUES (733, 24, 'update', '2', '{\"house_id\":\"5\",\"model_name\":\"2U-X86\",\"stock_time\":\"2023-09-12 10:30:00\",\"price\":\"27200\",\"num\":\"2U-2023-9-1\",\"cate_name\":\"服务器\",\"id\":\"2\",\"price1\":\"34000\",\"status\":\"1\"}', '2023-09-12 10:21:31', '1', '管理员');
INSERT INTO `api_log` VALUES (734, 24, 'update', '3', '{\"house_id\":\"5\",\"model_name\":\"HP 1000\",\"user_id\":\"1\",\"stock_time\":\"2023-09-12 10:30:00\",\"price\":\"2800\",\"num\":\"42314324\",\"cate_name\":\"打印机\",\"id\":\"3\",\"price1\":\"3500\",\"status\":\"2\"}', '2023-09-12 10:21:31', '1', '管理员');
INSERT INTO `api_log` VALUES (735, 24, 'update', '3', '{\"house_id\":\"5\",\"model_name\":\"HP 1000\",\"user_id\":\"1\",\"stock_time\":\"2023-09-12 10:30:00\",\"price\":\"2800\",\"num\":\"42314324\",\"cate_name\":\"打印机\",\"id\":\"3\",\"price1\":\"3500\",\"status\":\"2\"}', '2023-09-12 10:21:31', '1', '管理员');
INSERT INTO `api_log` VALUES (736, 24, 'update', '4', '{\"house_id\":\"6\",\"model_name\":\"HP 1000\",\"stock_time\":\"2023-09-12 10:30:00\",\"price\":\"2800\",\"num\":\"HP453254235\",\"cate_name\":\"打印机\",\"id\":\"4\",\"price1\":\"3500\",\"status\":\"1\"}', '2023-09-12 10:21:31', '1', '管理员');
INSERT INTO `api_log` VALUES (737, 24, 'update', '4', '{\"house_id\":\"6\",\"model_name\":\"HP 1000\",\"stock_time\":\"2023-09-12 10:30:00\",\"price\":\"2800\",\"num\":\"HP453254235\",\"cate_name\":\"打印机\",\"id\":\"4\",\"price1\":\"3500\",\"status\":\"1\"}', '2023-09-12 10:21:31', '1', '管理员');
INSERT INTO `api_log` VALUES (738, 24, 'update', '5', '{\"house_id\":\"6\",\"model_name\":\"TPLINK_100\",\"stock_time\":\"2023-09-12 10:30:00\",\"price\":\"96\",\"num\":\"TP-100-01\",\"cate_name\":\"路由器\",\"id\":\"5\",\"price1\":\"120\",\"status\":\"1\"}', '2023-09-12 10:21:31', '1', '管理员');
INSERT INTO `api_log` VALUES (739, 24, 'update', '5', '{\"house_id\":\"6\",\"model_name\":\"TPLINK_100\",\"stock_time\":\"2023-09-12 10:30:00\",\"price\":\"96\",\"num\":\"TP-100-01\",\"cate_name\":\"路由器\",\"id\":\"5\",\"price1\":\"120\",\"status\":\"1\"}', '2023-09-12 10:21:31', '1', '管理员');
INSERT INTO `api_log` VALUES (740, 24, 'update', '6', '{\"house_id\":\"5\",\"model_name\":\"立式\",\"stock_time\":\"2023-09-12 10:30:00\",\"price\":\"36\",\"num\":\"34324434543\",\"cate_name\":\"饮水机\",\"id\":\"6\",\"price1\":\"45\",\"status\":\"1\"}', '2023-09-12 10:21:31', '1', '管理员');
INSERT INTO `api_log` VALUES (741, 24, 'update', '6', '{\"house_id\":\"5\",\"model_name\":\"立式\",\"stock_time\":\"2023-09-12 10:30:00\",\"price\":\"36\",\"num\":\"34324434543\",\"cate_name\":\"饮水机\",\"id\":\"6\",\"price1\":\"45\",\"status\":\"1\"}', '2023-09-12 10:21:31', '1', '管理员');
INSERT INTO `api_log` VALUES (742, 24, 'update', '7', '{\"house_id\":\"5\",\"model_name\":\"2U-X86\",\"stock_time\":\"2023-09-12 10:30:00\",\"price\":\"27200\",\"num\":\"2U-2023-8-1\",\"cate_name\":\"服务器\",\"id\":\"7\",\"price1\":\"34000\",\"status\":\"1\"}', '2023-09-12 10:21:31', '1', '管理员');
INSERT INTO `api_log` VALUES (743, 24, 'update', '7', '{\"house_id\":\"5\",\"model_name\":\"2U-X86\",\"stock_time\":\"2023-09-12 10:30:00\",\"price\":\"27200\",\"num\":\"2U-2023-8-1\",\"cate_name\":\"服务器\",\"id\":\"7\",\"price1\":\"34000\",\"status\":\"1\"}', '2023-09-12 10:21:31', '1', '管理员');
INSERT INTO `api_log` VALUES (744, 24, 'update', '8', '{\"house_id\":\"5\",\"model_name\":\"HP 1000\",\"user_id\":\"1\",\"stock_time\":\"2023-09-12 10:30:00\",\"price\":\"2800\",\"num\":\"4231432433\",\"cate_name\":\"打印机\",\"id\":\"8\",\"price1\":\"3500\",\"status\":\"2\"}', '2023-09-12 10:21:31', '1', '管理员');
INSERT INTO `api_log` VALUES (745, 24, 'update', '8', '{\"house_id\":\"5\",\"model_name\":\"HP 1000\",\"user_id\":\"1\",\"stock_time\":\"2023-09-12 10:30:00\",\"price\":\"2800\",\"num\":\"4231432433\",\"cate_name\":\"打印机\",\"id\":\"8\",\"price1\":\"3500\",\"status\":\"2\"}', '2023-09-12 10:21:31', '1', '管理员');
INSERT INTO `api_log` VALUES (746, 24, 'update', '9', '{\"house_id\":\"6\",\"model_name\":\"HP 1000\",\"stock_time\":\"2023-09-12 10:30:00\",\"price\":\"2800\",\"num\":\"HP4538774235\",\"cate_name\":\"打印机\",\"id\":\"9\",\"price1\":\"3500\",\"status\":\"1\"}', '2023-09-12 10:21:31', '1', '管理员');
INSERT INTO `api_log` VALUES (747, 24, 'update', '9', '{\"house_id\":\"6\",\"model_name\":\"HP 1000\",\"stock_time\":\"2023-09-12 10:30:00\",\"price\":\"2800\",\"num\":\"HP4538774235\",\"cate_name\":\"打印机\",\"id\":\"9\",\"price1\":\"3500\",\"status\":\"1\"}', '2023-09-12 10:21:31', '1', '管理员');
INSERT INTO `api_log` VALUES (748, 24, 'update', '10', '{\"house_id\":\"6\",\"model_name\":\"TPLINK_100\",\"stock_time\":\"2023-09-12 10:30:00\",\"price\":\"96\",\"num\":\"TP-100-02\",\"cate_name\":\"路由器\",\"id\":\"10\",\"price1\":\"120\",\"status\":\"3\"}', '2023-09-12 10:21:31', '1', '管理员');
INSERT INTO `api_log` VALUES (749, 24, 'update', '10', '{\"house_id\":\"6\",\"model_name\":\"TPLINK_100\",\"stock_time\":\"2023-09-12 10:30:00\",\"price\":\"96\",\"num\":\"TP-100-02\",\"cate_name\":\"路由器\",\"id\":\"10\",\"price1\":\"120\",\"status\":\"3\"}', '2023-09-12 10:21:31', '1', '管理员');
INSERT INTO `api_log` VALUES (750, 23, 'update', '1', '{\"start_time\":\"2023-09-12T09:48:35\",\"asset_worth\":\"0\",\"user_id\":\"1\",\"name\":\"2023-09-12\",\"end_time\":\"2023-09-13T10:00:00\",\"remark\":\"4324\",\"id\":\"1\",\"status\":\"3\"}', '2023-09-12 10:23:23', '1', '管理员');
INSERT INTO `api_log` VALUES (751, 23, 'update', '1', '{\"id\":\"1\",\"status\":\"4\"}', '2023-09-12 10:23:38', '1', '管理员');
INSERT INTO `api_log` VALUES (752, 2, 'update', '7', '{\"action\":\"AU\",\"remark\":\"在确认盘点之后，更新资产的状态为盘点状态，并计算盘点概况\",\"id\":\"7\",\"model_id\":\"23\",\"is_default\":\"N\",\"content\":\"function exec(data){\\n  if(data.status != \'4\'){\\n    return null;\\n  }\\n  return [\\n    \\\"update asset_info t join asset_stock_detail d on d.asset_id = t.id and d.stock_id=\\\" +  \\n       data.id + \\\" set t.status=d.status,t.user_id = d.user_id, t.price = d.price\\\", \\n    \\\"update asset_stock s join (select count(id) c, sum(price) w from asset_stock_detail  where stock_id=\\\"+data.id+\\\" and', '2023-09-12 10:27:26', '1', '管理员');
INSERT INTO `api_log` VALUES (753, 23, 'update', '1', '{\"id\":\"1\",\"status\":\"4\"}', '2023-09-12 10:27:32', '1', '管理员');
INSERT INTO `api_log` VALUES (754, 23, 'event', '7', '{\"id\":\"1\",\"status\":\"4\"}', '2023-09-12 10:27:32', '1', '管理员');
INSERT INTO `api_log` VALUES (755, 23, 'insert', '2', '{\"start_time\":\"2023-08-30 10:00:00\",\"user_id\":\"1\",\"name\":\"20230902\",\"end_time\":\"2023-08-31 10:00:00\",\"id\":2,\"status\":\"1\"}', '2023-09-12 10:30:29', '1', '管理员');
INSERT INTO `api_log` VALUES (756, 23, 'event', '6', '{\"start_time\":\"2023-08-30 10:00:00\",\"user_id\":\"1\",\"name\":\"20230902\",\"end_time\":\"2023-08-31 10:00:00\",\"id\":2,\"status\":\"1\"}', '2023-09-12 10:30:29', '1', '管理员');
INSERT INTO `api_log` VALUES (757, 23, 'update', '2', '{\"start_time\":\"2023-08-30T10:00:00\",\"user_id\":\"1\",\"name\":\"20230902\",\"end_time\":\"2023-08-31T10:00:00\",\"id\":\"2\",\"status\":\"2\"}', '2023-09-12 10:30:48', '1', '管理员');
INSERT INTO `api_log` VALUES (758, 6, 'insert', '8', '{\"code\":\"chart_asset_category\",\"name\":\"资产分类统计\",\"remark\":\"按小类统计设备数量，可接收参数为上级分类：cateId\",\"id\":8,\"type\":\"chart\",\"content\":\"function exec(query){\\nvar sql = \\\"SELECT COUNT(t.id) value, c.name label FROM asset_info t JOIN asset_category c ON c.id = t.cate_id\\\";\\nif(query != null){\\n  sql += \\\" WHERE c.pid=\\\"+query.cateId;\\n}\\nreturn sql;\\n}\",\"group\":\"asset\"}', '2023-09-12 11:25:49', '1', '管理员');
INSERT INTO `api_log` VALUES (760, 1, 'insert', '28', '{\"code\":\"sys_chart\",\"name\":\"报表信息\",\"id\":28,\"group\":\"sys\",\"pk_field\":\"id\",\"unique_field\":\"code\",\"status\":\"1\"}', '2023-09-12 11:27:11', '1', '管理员');
INSERT INTO `api_log` VALUES (761, 3, 'insert', '38', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":38,\"model_id\":28,\"write\":\"Y\"}', '2023-09-12 11:27:11', '1', '管理员');
INSERT INTO `api_log` VALUES (762, 3, 'insert', '39', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":39,\"model_id\":28,\"write\":\"N\"}', '2023-09-12 11:27:11', '1', '管理员');
INSERT INTO `api_log` VALUES (763, 3, 'insert', '40', '{\"read\":\"Y\",\"role_id\":\"3\",\"id\":40,\"model_id\":28,\"write\":\"N\"}', '2023-09-12 11:27:11', '1', '管理员');
INSERT INTO `api_log` VALUES (764, 3, 'insert', '41', '{\"read\":\"Y\",\"role_id\":\"4\",\"id\":41,\"model_id\":28,\"write\":\"N\"}', '2023-09-12 11:27:11', '1', '管理员');
INSERT INTO `api_log` VALUES (765, 3, 'insert', '42', '{\"read\":\"Y\",\"role_id\":\"5\",\"id\":42,\"model_id\":28,\"write\":\"N\"}', '2023-09-12 11:27:11', '1', '管理员');
INSERT INTO `api_log` VALUES (766, 3, 'insert', '43', '{\"read\":\"Y\",\"role_id\":\"6\",\"id\":43,\"model_id\":28,\"write\":\"N\"}', '2023-09-12 11:27:11', '1', '管理员');
INSERT INTO `api_log` VALUES (767, 1, 'update', '28', '{\"code\":\"sys_chart\",\"name\":\"报表信息\",\"id\":\"28\",\"is_default\":\"N\",\"group\":\"sys\",\"pk_field\":\"id\",\"unique_field\":\"\",\"status\":\"1\"}', '2023-09-12 11:29:18', '1', '管理员');
INSERT INTO `api_log` VALUES (768, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":38,\"model_id\":\"28\",\"write\":\"Y\"}', '2023-09-12 11:29:18', '1', '管理员');
INSERT INTO `api_log` VALUES (769, 3, 'update', '38', '{\"read\":\"Y\",\"role_id\":\"1\",\"id\":38,\"model_id\":\"28\",\"write\":\"Y\"}', '2023-09-12 11:29:18', '1', '管理员');
INSERT INTO `api_log` VALUES (770, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":39,\"model_id\":\"28\",\"write\":\"N\"}', '2023-09-12 11:29:18', '1', '管理员');
INSERT INTO `api_log` VALUES (771, 3, 'update', '39', '{\"read\":\"Y\",\"role_id\":\"2\",\"id\":39,\"model_id\":\"28\",\"write\":\"N\"}', '2023-09-12 11:29:18', '1', '管理员');
INSERT INTO `api_log` VALUES (772, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"3\",\"id\":40,\"model_id\":\"28\",\"write\":\"N\"}', '2023-09-12 11:29:18', '1', '管理员');
INSERT INTO `api_log` VALUES (773, 3, 'update', '40', '{\"read\":\"Y\",\"role_id\":\"3\",\"id\":40,\"model_id\":\"28\",\"write\":\"N\"}', '2023-09-12 11:29:18', '1', '管理员');
INSERT INTO `api_log` VALUES (774, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"4\",\"id\":41,\"model_id\":\"28\",\"write\":\"N\"}', '2023-09-12 11:29:18', '1', '管理员');
INSERT INTO `api_log` VALUES (775, 3, 'update', '41', '{\"read\":\"Y\",\"role_id\":\"4\",\"id\":41,\"model_id\":\"28\",\"write\":\"N\"}', '2023-09-12 11:29:18', '1', '管理员');
INSERT INTO `api_log` VALUES (776, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"5\",\"id\":42,\"model_id\":\"28\",\"write\":\"N\"}', '2023-09-12 11:29:18', '1', '管理员');
INSERT INTO `api_log` VALUES (777, 3, 'update', '42', '{\"read\":\"Y\",\"role_id\":\"5\",\"id\":42,\"model_id\":\"28\",\"write\":\"N\"}', '2023-09-12 11:29:18', '1', '管理员');
INSERT INTO `api_log` VALUES (778, 3, 'event', '1', '{\"read\":\"Y\",\"role_id\":\"6\",\"id\":43,\"model_id\":\"28\",\"write\":\"N\"}', '2023-09-12 11:29:18', '1', '管理员');
INSERT INTO `api_log` VALUES (779, 3, 'update', '43', '{\"read\":\"Y\",\"role_id\":\"6\",\"id\":43,\"model_id\":\"28\",\"write\":\"N\"}', '2023-09-12 11:29:18', '1', '管理员');
INSERT INTO `api_log` VALUES (780, 28, 'insert', '1', '{\"name\":\"资产分类统计\",\"ds_code\":\"chart_asset_category\",\"serach_title\":\"资产分类\",\"id\":1,\"search_ds\":\"asset_category_select\",\"type\":\"pie\",\"search_type\":\"tree\",\"search_field\":\"cateId\",\"status\":\"1\"}', '2023-09-12 11:29:20', '1', '管理员');
INSERT INTO `api_log` VALUES (781, 6, 'update', '8', '{\"code\":\"chart_asset_category\",\"name\":\"资产分类统计\",\"remark\":\"按小类统计设备数量，可接收参数为上级分类：cateId\",\"id\":\"8\",\"type\":\"chart\",\"is_default\":\"N\",\"content\":\"function exec(query){\\nvar sql = \\\"SELECT COUNT(t.id) value, c.name FROM asset_info t JOIN asset_category c ON c.id = t.cate_id\\\";\\nif(query && query.cateId != null){\\n  sql += \\\" WHERE c.pid=\\\"+query.cateId;\\n}\\nreturn sql;\\n}\",\"group\":\"asset\"}', '2023-09-12 15:02:54', '1', '管理员');
INSERT INTO `api_log` VALUES (782, 6, 'update', '8', '{\"code\":\"chart_asset_category\",\"name\":\"资产分类统计\",\"remark\":\"按小类统计设备数量，可接收参数为上级分类：cateId\",\"id\":\"8\",\"type\":\"chart\",\"is_default\":\"N\",\"content\":\"function exec(query){\\nvar sql = \\\"SELECT COUNT(t.id) value, c.name FROM asset_info t JOIN asset_category c ON c.id = t.cate_id\\\";\\nif(query && query.cateId != null){\\n  sql += \\\" WHERE c.pid=\\\"+query.cateId;\\n}\\nreturn sql + \\\" GROUP BY name ORDER BY value\\\";\\n}\",\"group\":\"asset\"}', '2023-09-12 15:03:47', '1', '管理员');
INSERT INTO `api_log` VALUES (783, 28, 'insert', '2', '{\"data_option\":\"{\\n  \\\"series\\\": [\\n    {\\n      \\\"showBackground\\\": true,\\n      \\\"backgroundStyle\\\": {\\n        \\\"color\\\": \\\"rgba(180, 180, 180, 0.2)\\\"\\n      }\\n    }\\n  ]\\n}\",\"name\":\"资产柱状图\",\"ds_code\":\"chart_asset_category\",\"id\":2,\"type\":\"bar\",\"status\":\"1\"}', '2023-09-12 16:24:03', '1', '管理员');
INSERT INTO `api_log` VALUES (784, 28, 'update', '2', '{\"data_option\":\"{\\n  \\\"series\\\": [\\n    {\\n      \\\"type\\\": \\\"bar\\\",\\n      \\\"showBackground\\\": true,\\n      \\\"backgroundStyle\\\": {\\n        \\\"color\\\": \\\"rgba(180, 180, 180, 0.2)\\\"\\n      }\\n    }\\n  ]\\n}\",\"name\":\"资产柱状图\",\"ds_code\":\"chart_asset_category\",\"id\":\"2\",\"type\":\"bar\",\"search_field\":\"N\",\"status\":\"1\"}', '2023-09-12 16:28:50', '1', '管理员');
INSERT INTO `api_log` VALUES (785, 28, 'insert', '3', '{\"name\":\"资产分类统计线\",\"ds_code\":\"chart_asset_category\",\"id\":3,\"type\":\"line\",\"status\":\"1\"}', '2023-09-12 16:57:37', '1', '管理员');
INSERT INTO `api_log` VALUES (786, 6, 'insert', '9', '{\"code\":\"asset_status_guage\",\"name\":\"资产状态比\",\"remark\":\"资产使用率和完好率\",\"id\":9,\"type\":\"chart\",\"content\":\"select count(if(status = \'2\',1, null)) used, count(if(status = \'3\',1, null)) wrong, count(id) total from asset_info where `status` != \'5\' and `status` !=\'6\'\",\"group\":\"asset\"}', '2023-09-12 17:04:23', '1', '管理员');
INSERT INTO `api_log` VALUES (787, 28, 'insert', '4', '{\"name\":\"资产使用率\",\"ds_code\":\"chart_asset_category\",\"id\":4,\"type\":\"guage\",\"status\":\"1\"}', '2023-09-12 17:04:53', '1', '管理员');
INSERT INTO `api_log` VALUES (788, 28, 'update', '4', '{\"name\":\"资产使用率\",\"ds_code\":\"asset_status_guage\",\"id\":\"4\",\"type\":\"guage\",\"search_field\":\"N\",\"status\":\"1\"}', '2023-09-12 17:05:05', '1', '管理员');
INSERT INTO `api_log` VALUES (789, 28, 'update', '4', '{\"data_option\":\"function exec(dsData){\\n  let data = null;\\n if(dsData == null || dsData.length == 0){\\n   data = {used:0,wrong:0,total:0};\\n}else{\\n  data = dsData[0];\\n}\\n var option = {series: [\\n    {\\n      type: \'gauge\',\\n      min:0,\\n      max: data.total,\\n      anchor: {\\n        show: true,\\n        showAbove: true,\\n        size: 18,\\n        itemStyle: {\\n          color: \'#FAC858\'\\n        }\\n      },\\n      data: [{value:data.used,name:\\\"在用\\\"},{value:data.total-data.wrong,name:\'完', '2023-09-12 17:18:42', '1', '管理员');
INSERT INTO `api_log` VALUES (790, 28, 'update', '4', '{\"data_option\":\"function exec(dsData){\\n  let data = null;\\n if(dsData == null || dsData.length == 0){\\n   data = {used:0,wrong:0,total:0};\\n}else{\\n  data = dsData[0];\\n}\\n var option = {series: [\\n    {\\n      type: \'gauge\',\\n      min:0,\\n      max: data.total,\\n      anchor: {\\n        show: true,\\n        showAbove: true,\\n        size: 18,\\n        itemStyle: {\\n          color: \'#FAC858\'\\n        }\\n      },\\n      data: [{value:data.used,name:\\\"在用\\\"},{value:data.total-data.wrong,name:\'完', '2023-09-12 17:20:50', '1', '管理员');
INSERT INTO `api_log` VALUES (791, 28, 'update', '4', '{\"data_option\":\"function exec(dsData){\\n  let data = null;\\n if(dsData == null || dsData.length == 0){\\n   data = {used:0,wrong:0,total:0};\\n}else{\\n  data = dsData[0];\\n}\\n var option = {series: [\\n    {\\n      type: \'gauge\',\\n      min:0,\\n      max: data.total,\\n      anchor: {\\n        show: true,\\n        showAbove: true,\\n        size: 18,\\n        itemStyle: {\\n          color: \'#FAC858\'\\n        }\\n      },      \\n      axisLine: {\\n        roundCap: true\\n      },\\n      data: [{value:', '2023-09-12 17:22:00', '1', '管理员');
INSERT INTO `api_log` VALUES (792, 28, 'update', '4', '{\"data_option\":\"function exec(dsData){\\n  let data = null;\\n if(dsData == null || dsData.length == 0){\\n   data = {used:0,wrong:0,total:0};\\n}else{\\n  data = dsData[0];\\n}\\n var option = {series: [\\n    {\\n      type: \'gauge\',\\n      min:0,\\n      max: data.total,\\n      anchor: {\\n        show: true,\\n        showAbove: true,\\n        size: 18,\\n        itemStyle: {\\n          color: \'#FAC858\'\\n        }\\n      },      \\n      axisLine: {\\n        roundCap: true\\n      },\\n      data: [{value:', '2023-09-12 17:24:33', '1', '管理员');
INSERT INTO `api_log` VALUES (793, 28, 'update', '4', '{\"data_option\":\"function exec(dsData){\\n  let data = null;\\n if(dsData == null || dsData.length == 0){\\n   data = {used:0,wrong:0,total:0};\\n}else{\\n  data = dsData[0];\\n}\\n var option = {series: [\\n    {\\n      type: \'gauge\',\\n      min:0,\\n      max: data.total,\\n      anchor: {\\n        show: true,\\n        showAbove: true,\\n        size: 18,\\n        itemStyle: {\\n          color: \'#FAC858\'\\n        }\\n      },      \\n      axisLine: {\\n        roundCap: true\\n      },\\n      data: [{value:', '2023-09-12 17:25:09', '1', '管理员');
INSERT INTO `api_log` VALUES (794, 28, 'update', '4', '{\"data_option\":\"function exec(dsData){\\n  let data = null;\\n if(dsData == null || dsData.length == 0){\\n   data = {used:0,wrong:0,total:0};\\n}else{\\n  data = dsData[0];\\n}\\n var option = {series: [\\n    {\\n      type: \'gauge\',\\n      min:0,\\n      max: data.total,\\n      anchor: {\\n        show: true,\\n        showAbove: true,\\n        size: 18,\\n        itemStyle: {\\n          color: \'#FAC858\'\\n        }\\n      },      \\n      axisLine: {\\n        roundCap: true\\n      },\\n      data: [\\n     ', '2023-09-12 17:29:12', '1', '管理员');
INSERT INTO `api_log` VALUES (795, 28, 'update', '4', '{\"data_option\":\"function exec(dsData){\\n  let data = null;\\n if(dsData == null || dsData.length == 0){\\n   data = {used:0,wrong:0,total:0};\\n}else{\\n  data = dsData[0];\\n}\\n var option = {series: [\\n    {\\n      type: \'gauge\',\\n      min:0,\\n      max: data.total,\\n      anchor: {\\n        show: true,\\n        showAbove: true,\\n        size: 18,\\n        itemStyle: {\\n          color: \'#FAC858\'\\n        }\\n      },      \\n      axisLine: {\\n        roundCap: true\\n      },\\n      data: [\\n     ', '2023-09-12 17:30:08', '1', '管理员');
INSERT INTO `api_log` VALUES (796, 28, 'update', '4', '{\"data_option\":\"function exec(dsData){\\n  let data = null;\\n if(dsData == null || dsData.length == 0){\\n   data = {used:0,wrong:0,total:0};\\n}else{\\n  data = dsData[0];\\n}\\n var option = {series: [\\n    {\\n      type: \'gauge\',\\n      min:0,\\n      max: data.total,\\n      anchor: {\\n        show: true,\\n        showAbove: true,\\n        size: 18,\\n        itemStyle: {\\n          color: \'#FAC858\'\\n        }\\n      },      \\n      axisLine: {\\n        roundCap: true\\n      },\\n      data: [\\n     ', '2023-09-12 17:31:02', '1', '管理员');
INSERT INTO `api_log` VALUES (797, 28, 'update', '4', '{\"data_option\":\"function exec(dsData){\\n  let data = null;\\n if(dsData == null || dsData.length == 0){\\n   data = {used:0,wrong:0,total:0};\\n}else{\\n  data = dsData[0];\\n}\\n var option = {series: [\\n    {\\n      type: \'gauge\',\\n      min:0,\\n      max: data.total,\\n      anchor: {\\n        show: true,\\n        showAbove: true,\\n        size: 18,\\n        itemStyle: {\\n          color: \'#FAC858\'\\n        }\\n      },      \\n      axisLine: {\\n        roundCap: true\\n      },\\n      data: [\\n     ', '2023-09-12 17:31:55', '1', '管理员');
INSERT INTO `api_log` VALUES (798, 16, 'update', '1934', '{\"buy_time\":\"2023-05-06 00:00:00\",\"house_id\":\"5\",\"price\":27.2,\"num\":\"34324243\",\"cate_id\":\"42\",\"comment\":\"\",\"id\":1934,\"model_id\":\"1917\",\"dept_id\":\"102\",\"status\":\"1\"}', '2023-09-13 15:36:38', '1', '管理员');
INSERT INTO `api_log` VALUES (799, 16, 'update', '1934', '{\"buy_time\":\"2023-05-06 00:00:00\",\"house_id\":\"5\",\"price\":27.2,\"num\":\"34324243\",\"cate_id\":\"42\",\"comment\":\"\",\"id\":1934,\"model_id\":\"1917\",\"dept_id\":\"102\",\"status\":\"1\"}', '2023-09-13 15:36:38', '1', '管理员');
INSERT INTO `api_log` VALUES (800, 16, 'update', '1925', '{\"buy_time\":\"2023-05-06 00:00:00\",\"house_id\":\"5\",\"price\":\"27200\",\"num\":\"2U-2023-9-1\",\"cate_id\":\"36\",\"comment\":\"\",\"id\":1925,\"model_id\":\"1914\",\"dept_id\":\"100\",\"status\":\"1\"}', '2023-09-13 15:36:38', '1', '管理员');
INSERT INTO `api_log` VALUES (801, 16, 'update', '1925', '{\"buy_time\":\"2023-05-06 00:00:00\",\"house_id\":\"5\",\"price\":\"27200\",\"num\":\"2U-2023-9-1\",\"cate_id\":\"36\",\"comment\":\"\",\"id\":1925,\"model_id\":\"1914\",\"dept_id\":\"100\",\"status\":\"1\"}', '2023-09-13 15:36:38', '1', '管理员');
INSERT INTO `api_log` VALUES (802, 16, 'update', '1935', '{\"buy_time\":\"2023-09-08 00:00:00\",\"house_id\":\"5\",\"user_id\":\"1\",\"price\":\"2800\",\"num\":\"HP-314324\",\"cate_id\":\"41\",\"comment\":\"\",\"id\":1935,\"model_id\":\"1916\",\"status\":\"2\"}', '2023-09-13 15:37:16', '1', '管理员');
INSERT INTO `api_log` VALUES (803, 16, 'update', '1934', '{\"buy_time\":\"2023-05-06 00:00:00\",\"house_id\":\"5\",\"price\":27.2,\"num\":\"34324243\",\"cate_id\":\"42\",\"comment\":\"\",\"id\":1934,\"model_id\":\"1917\",\"dept_id\":\"102\",\"status\":\"1\"}', '2023-09-13 15:40:56', '1', '管理员');
INSERT INTO `api_log` VALUES (804, 16, 'update', '1934', '{\"buy_time\":\"2023-05-06 00:00:00\",\"house_id\":\"5\",\"price\":27.2,\"num\":\"34324243\",\"cate_id\":\"42\",\"comment\":\"\",\"id\":1934,\"model_id\":\"1917\",\"dept_id\":\"102\",\"status\":\"1\"}', '2023-09-13 15:40:56', '1', '管理员');
INSERT INTO `api_log` VALUES (805, 16, 'update', '1925', '{\"buy_time\":\"2023-05-06 00:00:00\",\"house_id\":\"5\",\"price\":\"27200\",\"num\":\"2U-2023-9-1\",\"cate_id\":\"36\",\"comment\":\"\",\"id\":1925,\"model_id\":\"1914\",\"dept_id\":\"100\",\"status\":\"1\"}', '2023-09-13 15:40:56', '1', '管理员');
INSERT INTO `api_log` VALUES (806, 16, 'update', '1925', '{\"buy_time\":\"2023-05-06 00:00:00\",\"house_id\":\"5\",\"price\":\"27200\",\"num\":\"2U-2023-9-1\",\"cate_id\":\"36\",\"comment\":\"\",\"id\":1925,\"model_id\":\"1914\",\"dept_id\":\"100\",\"status\":\"1\"}', '2023-09-13 15:40:56', '1', '管理员');
INSERT INTO `api_log` VALUES (807, 16, 'update', '1935', '{\"buy_time\":\"2023-09-08 00:00:00\",\"house_id\":\"5\",\"user_id\":\"1\",\"price\":\"2800\",\"num\":\"HP-314324\",\"cate_id\":\"41\",\"comment\":\"\",\"id\":1935,\"model_id\":\"1916\",\"status\":\"2\"}', '2023-09-13 15:40:56', '1', '管理员');
INSERT INTO `api_log` VALUES (808, 16, 'update', '1935', '{\"buy_time\":\"2023-09-08 00:00:00\",\"house_id\":\"5\",\"user_id\":\"1\",\"price\":\"2800\",\"num\":\"HP-314324\",\"cate_id\":\"41\",\"comment\":\"\",\"id\":1935,\"model_id\":\"1916\",\"status\":\"2\"}', '2023-09-13 15:40:56', '1', '管理员');
INSERT INTO `api_log` VALUES (809, 16, 'update', '1936', '{\"buy_time\":\"2023-09-08 00:00:00\",\"house_id\":\"6\",\"price\":\"2800\",\"num\":\"HP-254235\",\"cate_id\":\"41\",\"comment\":\"\",\"id\":1936,\"model_id\":\"1916\",\"status\":\"1\"}', '2023-09-13 15:40:56', '1', '管理员');
INSERT INTO `api_log` VALUES (810, 16, 'update', '1936', '{\"buy_time\":\"2023-09-08 00:00:00\",\"house_id\":\"6\",\"price\":\"2800\",\"num\":\"HP-254235\",\"cate_id\":\"41\",\"comment\":\"\",\"id\":1936,\"model_id\":\"1916\",\"status\":\"1\"}', '2023-09-13 15:40:56', '1', '管理员');
INSERT INTO `api_log` VALUES (811, 16, 'insert', '1938', '{\"buy_time\":\"2021-08-03 00:00:00\",\"house_id\":\"6\",\"price\":\"96\",\"num\":\"TP-100-04\",\"cate_id\":\"40\",\"comment\":\"\",\"model_id\":\"1918\",\"dept_id\":\"103\",\"status\":\"1\"}', '2023-09-13 15:40:56', '1', '管理员');
INSERT INTO `api_log` VALUES (812, 16, 'insert', '1937', '{\"buy_time\":\"2021-08-03 00:00:00\",\"house_id\":\"6\",\"price\":\"96\",\"num\":\"TP-100-04\",\"cate_id\":\"40\",\"comment\":\"\",\"model_id\":\"1918\",\"dept_id\":\"103\",\"status\":\"1\"}', '2023-09-13 15:40:56', '1', '管理员');
INSERT INTO `api_log` VALUES (813, 16, 'insert', '1939', '{\"buy_time\":\"2023-05-06 00:00:00\",\"house_id\":\"5\",\"price\":\"36\",\"num\":\"W-434543\",\"cate_id\":\"42\",\"comment\":\"\",\"model_id\":\"1917\",\"dept_id\":\"102\",\"status\":\"1\"}', '2023-09-13 15:40:56', '1', '管理员');
INSERT INTO `api_log` VALUES (814, 16, 'insert', '1940', '{\"buy_time\":\"2023-05-06 00:00:00\",\"house_id\":\"5\",\"price\":\"36\",\"num\":\"W-434543\",\"cate_id\":\"42\",\"comment\":\"\",\"model_id\":\"1917\",\"dept_id\":\"102\",\"status\":\"1\"}', '2023-09-13 15:40:56', '1', '管理员');
INSERT INTO `api_log` VALUES (815, 16, 'update', '1930', '{\"buy_time\":\"2023-05-06 00:00:00\",\"house_id\":\"5\",\"price\":\"27200\",\"num\":\"2U-2023-8-1\",\"cate_id\":\"36\",\"comment\":\"\",\"id\":1930,\"model_id\":\"1914\",\"dept_id\":\"100\",\"status\":\"1\"}', '2023-09-13 15:40:57', '1', '管理员');
INSERT INTO `api_log` VALUES (816, 16, 'update', '1930', '{\"buy_time\":\"2023-05-06 00:00:00\",\"house_id\":\"5\",\"price\":\"27200\",\"num\":\"2U-2023-8-1\",\"cate_id\":\"36\",\"comment\":\"\",\"id\":1930,\"model_id\":\"1914\",\"dept_id\":\"100\",\"status\":\"1\"}', '2023-09-13 15:40:57', '1', '管理员');
INSERT INTO `api_log` VALUES (817, 16, 'insert', '1941', '{\"buy_time\":\"2023-08-08 00:00:00\",\"house_id\":\"5\",\"user_id\":\"1\",\"price\":\"2800\",\"num\":\"HP-3332433\",\"cate_id\":\"41\",\"comment\":\"\",\"model_id\":\"1916\",\"status\":\"2\"}', '2023-09-13 15:40:57', '1', '管理员');
INSERT INTO `api_log` VALUES (818, 16, 'insert', '1942', '{\"buy_time\":\"2023-08-08 00:00:00\",\"house_id\":\"5\",\"user_id\":\"1\",\"price\":\"2800\",\"num\":\"HP-3332433\",\"cate_id\":\"41\",\"comment\":\"\",\"model_id\":\"1916\",\"status\":\"2\"}', '2023-09-13 15:40:57', '1', '管理员');
INSERT INTO `api_log` VALUES (819, 16, 'insert', '1943', '{\"buy_time\":\"2023-09-08 00:00:00\",\"house_id\":\"6\",\"price\":\"2800\",\"num\":\"HP-538774\",\"cate_id\":\"41\",\"comment\":\"\",\"model_id\":\"1916\",\"status\":\"1\"}', '2023-09-13 15:40:57', '1', '管理员');
INSERT INTO `api_log` VALUES (820, 16, 'insert', '1944', '{\"buy_time\":\"2023-09-08 00:00:00\",\"house_id\":\"6\",\"price\":\"2800\",\"num\":\"HP-538774\",\"cate_id\":\"41\",\"comment\":\"\",\"model_id\":\"1916\",\"status\":\"1\"}', '2023-09-13 15:40:57', '1', '管理员');
INSERT INTO `api_log` VALUES (821, 16, 'insert', '1945', '{\"buy_time\":\"2021-08-03 00:00:00\",\"house_id\":\"6\",\"price\":\"96\",\"num\":\"TP-100-03\",\"cate_id\":\"40\",\"comment\":\"\",\"model_id\":\"1918\",\"dept_id\":\"103\",\"status\":\"3\"}', '2023-09-13 15:40:57', '1', '管理员');
INSERT INTO `api_log` VALUES (822, 16, 'insert', '1946', '{\"buy_time\":\"2021-08-03 00:00:00\",\"house_id\":\"6\",\"price\":\"96\",\"num\":\"TP-100-03\",\"cate_id\":\"40\",\"comment\":\"\",\"model_id\":\"1918\",\"dept_id\":\"103\",\"status\":\"3\"}', '2023-09-13 15:40:57', '1', '管理员');
INSERT INTO `api_log` VALUES (823, 16, 'update', '1934', '{\"buy_time\":\"2023-05-06 00:00:00\",\"house_id\":\"5\",\"price\":27.2,\"num\":\"34324243\",\"cate_id\":\"42\",\"comment\":\"\",\"id\":1934,\"model_id\":\"1917\",\"dept_id\":\"102\",\"status\":\"1\"}', '2023-09-13 15:43:33', '1', '管理员');
INSERT INTO `api_log` VALUES (824, 16, 'update', '1925', '{\"buy_time\":\"2023-05-06 00:00:00\",\"house_id\":\"5\",\"price\":\"27200\",\"num\":\"2U-2023-9-1\",\"cate_id\":\"36\",\"comment\":\"\",\"id\":1925,\"model_id\":\"1914\",\"dept_id\":\"100\",\"status\":\"1\"}', '2023-09-13 15:54:54', '1', '管理员');
INSERT INTO `api_log` VALUES (825, 16, 'update', '1935', '{\"buy_time\":\"2023-09-08 00:00:00\",\"house_id\":\"5\",\"user_id\":\"1\",\"price\":\"2800\",\"num\":\"HP-314324\",\"cate_id\":\"41\",\"comment\":\"\",\"id\":1935,\"model_id\":\"1916\",\"status\":\"2\"}', '2023-09-13 15:54:54', '1', '管理员');
INSERT INTO `api_log` VALUES (826, 16, 'update', '1936', '{\"buy_time\":\"2023-09-08 00:00:00\",\"house_id\":\"6\",\"price\":\"2800\",\"num\":\"HP-254235\",\"cate_id\":\"41\",\"comment\":\"\",\"id\":1936,\"model_id\":\"1916\",\"status\":\"1\"}', '2023-09-13 15:54:54', '1', '管理员');
INSERT INTO `api_log` VALUES (827, 16, 'update', '1937', '{\"buy_time\":\"2021-08-03 00:00:00\",\"house_id\":\"6\",\"price\":\"96\",\"num\":\"TP-100-04\",\"cate_id\":\"40\",\"comment\":\"\",\"id\":1937,\"model_id\":\"1918\",\"dept_id\":\"103\",\"status\":\"1\"}', '2023-09-13 15:54:54', '1', '管理员');
INSERT INTO `api_log` VALUES (828, 16, 'update', '1939', '{\"buy_time\":\"2023-05-06 00:00:00\",\"house_id\":\"5\",\"price\":\"36\",\"num\":\"W-434543\",\"cate_id\":\"42\",\"comment\":\"\",\"id\":1939,\"model_id\":\"1917\",\"dept_id\":\"102\",\"status\":\"1\"}', '2023-09-13 15:54:54', '1', '管理员');
INSERT INTO `api_log` VALUES (829, 16, 'update', '1930', '{\"buy_time\":\"2023-05-06 00:00:00\",\"house_id\":\"5\",\"price\":\"27200\",\"num\":\"2U-2023-8-1\",\"cate_id\":\"36\",\"comment\":\"\",\"id\":1930,\"model_id\":\"1914\",\"dept_id\":\"100\",\"status\":\"1\"}', '2023-09-13 15:54:54', '1', '管理员');
INSERT INTO `api_log` VALUES (830, 16, 'update', '1941', '{\"buy_time\":\"2023-08-08 00:00:00\",\"house_id\":\"5\",\"user_id\":\"1\",\"price\":\"2800\",\"num\":\"HP-3332433\",\"cate_id\":\"41\",\"comment\":\"\",\"id\":1941,\"model_id\":\"1916\",\"status\":\"2\"}', '2023-09-13 15:54:54', '1', '管理员');
INSERT INTO `api_log` VALUES (831, 16, 'update', '1943', '{\"buy_time\":\"2023-09-08 00:00:00\",\"house_id\":\"6\",\"price\":\"2800\",\"num\":\"HP-538774\",\"cate_id\":\"41\",\"comment\":\"\",\"id\":1943,\"model_id\":\"1916\",\"status\":\"1\"}', '2023-09-13 15:54:54', '1', '管理员');
INSERT INTO `api_log` VALUES (832, 16, 'update', '1945', '{\"buy_time\":\"2021-08-03 00:00:00\",\"house_id\":\"6\",\"price\":\"96\",\"num\":\"TP-100-03\",\"cate_id\":\"40\",\"comment\":\"\",\"id\":1945,\"model_id\":\"1918\",\"dept_id\":\"103\",\"status\":\"3\"}', '2023-09-13 15:54:54', '1', '管理员');
INSERT INTO `api_log` VALUES (833, 16, 'update', '1934', '{\"buy_time\":\"2023-05-06 00:00:00\",\"house_id\":\"5\",\"price\":27.2,\"num\":\"34324243\",\"cate_id\":\"42\",\"comment\":\"\",\"id\":1934,\"model_id\":\"1917\",\"dept_id\":\"102\",\"status\":\"1\"}', '2023-09-13 15:55:40', '1', '管理员');
INSERT INTO `api_log` VALUES (834, 16, 'update', '1925', '{\"buy_time\":\"2023-05-06 00:00:00\",\"house_id\":\"5\",\"price\":\"27200\",\"num\":\"2U-2023-9-1\",\"cate_id\":\"36\",\"comment\":\"\",\"id\":1925,\"model_id\":\"1914\",\"dept_id\":\"100\",\"status\":\"1\"}', '2023-09-13 15:55:43', '1', '管理员');
INSERT INTO `api_log` VALUES (835, 16, 'update', '1935', '{\"buy_time\":\"2023-09-08 00:00:00\",\"house_id\":\"5\",\"user_id\":\"1\",\"price\":\"2800\",\"num\":\"HP-314324\",\"cate_id\":\"41\",\"comment\":\"\",\"id\":1935,\"model_id\":\"1916\",\"status\":\"2\"}', '2023-09-13 15:55:43', '1', '管理员');
INSERT INTO `api_log` VALUES (836, 16, 'update', '1936', '{\"buy_time\":\"2023-09-08 00:00:00\",\"house_id\":\"6\",\"price\":\"2800\",\"num\":\"HP-254235\",\"cate_id\":\"41\",\"comment\":\"\",\"id\":1936,\"model_id\":\"1916\",\"status\":\"1\"}', '2023-09-13 15:55:43', '1', '管理员');
INSERT INTO `api_log` VALUES (837, 16, 'update', '1937', '{\"buy_time\":\"2021-08-03 00:00:00\",\"house_id\":\"6\",\"price\":\"96\",\"num\":\"TP-100-04\",\"cate_id\":\"40\",\"comment\":\"\",\"id\":1937,\"model_id\":\"1918\",\"dept_id\":\"103\",\"status\":\"1\"}', '2023-09-13 15:55:43', '1', '管理员');
INSERT INTO `api_log` VALUES (838, 16, 'update', '1939', '{\"buy_time\":\"2023-05-06 00:00:00\",\"house_id\":\"5\",\"price\":\"36\",\"num\":\"W-434543\",\"cate_id\":\"42\",\"comment\":\"\",\"id\":1939,\"model_id\":\"1917\",\"dept_id\":\"102\",\"status\":\"1\"}', '2023-09-13 15:55:43', '1', '管理员');
INSERT INTO `api_log` VALUES (839, 16, 'update', '1930', '{\"buy_time\":\"2023-05-06 00:00:00\",\"house_id\":\"5\",\"price\":\"27200\",\"num\":\"2U-2023-8-1\",\"cate_id\":\"36\",\"comment\":\"\",\"id\":1930,\"model_id\":\"1914\",\"dept_id\":\"100\",\"status\":\"1\"}', '2023-09-13 15:55:43', '1', '管理员');
INSERT INTO `api_log` VALUES (840, 16, 'update', '1941', '{\"buy_time\":\"2023-08-08 00:00:00\",\"house_id\":\"5\",\"user_id\":\"1\",\"price\":\"2800\",\"num\":\"HP-3332433\",\"cate_id\":\"41\",\"comment\":\"\",\"id\":1941,\"model_id\":\"1916\",\"status\":\"2\"}', '2023-09-13 15:55:43', '1', '管理员');
INSERT INTO `api_log` VALUES (841, 16, 'update', '1943', '{\"buy_time\":\"2023-09-08 00:00:00\",\"house_id\":\"6\",\"price\":\"2800\",\"num\":\"HP-538774\",\"cate_id\":\"41\",\"comment\":\"\",\"id\":1943,\"model_id\":\"1916\",\"status\":\"1\"}', '2023-09-13 15:55:43', '1', '管理员');
INSERT INTO `api_log` VALUES (842, 16, 'update', '1945', '{\"buy_time\":\"2021-08-03 00:00:00\",\"house_id\":\"6\",\"price\":\"96\",\"num\":\"TP-100-03\",\"cate_id\":\"40\",\"comment\":\"\",\"id\":1945,\"model_id\":\"1918\",\"dept_id\":\"103\",\"status\":\"3\"}', '2023-09-13 15:55:43', '1', '管理员');
INSERT INTO `api_log` VALUES (843, 6, 'insert', '10', '{\"code\":\"asset_over_view\",\"name\":\"资产概况\",\"type\":\"chart\",\"content\":\"select sum(users) users, sum(assets) assets, sum(purchases) purchase, sum(maintains) maintains from (\\nselect count(user_id) users, 0 assets, 0 purchases, 0 maintains from sys_user where del_flag=0\\nUNION ALL\\nselect 0 users, count(id) assets, 0 purchases, 0 maintains from asset_info where `status` in (\'1\',\'2\',\'3\',\'4\')\\nUNION ALL\\nselect 0 users, 0 assets, count(id) purchases, 0 maintains from asset_purchase where check_status=\'2', '2023-09-14 10:56:08', '1', '管理员');
INSERT INTO `api_log` VALUES (844, 6, 'update', '10', '{\"code\":\"asset_over_view\",\"name\":\"资产概况\",\"remark\":\"统计用户数、资产数、维保次数和采购次数\",\"id\":\"10\",\"type\":\"chart\",\"is_default\":\"N\",\"content\":\"select sum(users) users, sum(assets) assets, sum(purchases) purchases, sum(maintains) maintains from (\\nselect count(user_id) users, 0 assets, 0 purchases, 0 maintains from sys_user where del_flag=0\\nUNION ALL\\nselect 0 users, count(id) assets, 0 purchases, 0 maintains from asset_info where `status` in (\'1\',\'2\',\'3\',\'4\')\\nUNION ALL\\nselect 0 users, 0 assets, count(id) purch', '2023-09-14 11:01:14', '1', '管理员');
INSERT INTO `api_log` VALUES (845, 6, 'update', '10', '{\"code\":\"asset_over_view\",\"name\":\"资产概况\",\"remark\":\"统计用户数、资产数、维保次数和采购次数\",\"id\":\"10\",\"type\":\"chart\",\"is_default\":\"N\",\"content\":\"select sum(users) users, sum(assets) assets, sum(purchases) purchases, sum(maintains) maintains from (\\nselect count(user_id) users, 0 assets, 0 purchases, 0 maintains from sys_user where del_flag=0\\nUNION ALL\\nselect 0 users, count(id) assets, 0 purchases, 0 maintains from asset_info where `status` in (\'1\',\'2\',\'3\',\'4\')\\nUNION ALL\\nselect 0 users, 0 assets, count(id) purch', '2023-09-14 11:01:18', '1', '管理员');
INSERT INTO `api_log` VALUES (846, 6, 'insert', '11', '{\"code\":\"asset_dept\",\"name\":\"资产分布\",\"remark\":\"按部门统计资产数量\",\"type\":\"chart\",\"content\":\"\\nselect count(t.id) as `value`, d.dept_name label from asset_info t \\njoin sys_dept d on d.dept_id = t.dept_id\\n where t.`status` in (\'1\',\'2\',\'3\',\'4\')\\n group by d.dept_name\\n \\nUNION ALL\\n\\nselect count(id) as `value`, \'未分配\' label from asset_info \\n where (dept_id = 0 || dept_id is null) AND `status` in (\'1\',\'2\',\'3\',\'4\')\\n \",\"group\":\"asset\"}', '2023-09-14 11:29:17', '1', '管理员');
INSERT INTO `api_log` VALUES (847, 6, 'update', '11', '{\"code\":\"asset_dept\",\"name\":\"资产分布\",\"remark\":\"按部门统计资产数量\",\"id\":\"11\",\"type\":\"chart\",\"is_default\":\"N\",\"content\":\"select count(t.id) as `value`, d.dept_name name from asset_info t \\njoin sys_dept d on d.dept_id = t.dept_id\\n where t.`status` in (\'1\',\'2\',\'3\',\'4\')\\n group by d.dept_name\\n \\nUNION ALL\\n\\nselect count(id) as `value`, \'未分配\' name from asset_info \\n where (dept_id = 0 || dept_id is null) AND `status` in (\'1\',\'2\',\'3\',\'4\')\\n \",\"group\":\"asset\"}', '2023-09-14 11:31:12', '1', '管理员');
INSERT INTO `api_log` VALUES (848, 6, 'insert', '12', '{\"code\":\"asset_stock\",\"name\":\"资产盘点统计\",\"remark\":\"按盘点时间和当前数据统计\",\"type\":\"chart\",\"content\":\"select * from (select *  from (select asset_count value, DATE_FORMAT(start_time,\'%Y%m\') as name from asset_stock where status =\'4\' order by start_time desc limit 10) t order by name asc) p\\nunion all\\nselect count(id) value, \'当前\' as  name from asset_info where status in (\'1\',\'2\',\'3\',\'4\')\",\"group\":\"asset\"}', '2023-09-14 17:34:45', '1', '管理员');
INSERT INTO `api_log` VALUES (849, 24, 'update', '16', '{\"house_id\":\"5\",\"model_name\":\"立式\",\"price\":\"27.2\",\"num\":\"34324\",\"cate_name\":\"饮水机\",\"id\":\"16\",\"asset_id\":\"1924\",\"stock_id\":\"2\",\"price1\":\"27.2\"}', '2023-09-14 17:39:31', '1', '管理员');
INSERT INTO `api_log` VALUES (850, 24, 'update', '16', '{\"house_id\":\"5\",\"model_name\":\"立式\",\"price\":\"27.2\",\"num\":\"34324\",\"cate_name\":\"饮水机\",\"id\":\"16\",\"asset_id\":\"1924\",\"stock_id\":\"2\",\"price1\":\"27.2\",\"status\":\"2\"}', '2023-09-14 17:39:32', '1', '管理员');
INSERT INTO `api_log` VALUES (851, 24, 'update', '16', '{\"house_id\":\"5\",\"model_name\":\"立式\",\"user_id\":\"2\",\"price\":\"27.2\",\"num\":\"34324\",\"cate_name\":\"饮水机\",\"id\":\"16\",\"asset_id\":\"1924\",\"stock_id\":\"2\",\"price1\":\"27.2\",\"status\":\"2\"}', '2023-09-14 17:39:36', '1', '管理员');
INSERT INTO `api_log` VALUES (852, 24, 'update', '17', '{\"house_id\":\"5\",\"model_name\":\"2U-X86\",\"price\":\"0\",\"num\":\"2U-2023-9-1\",\"cate_name\":\"服务器\",\"id\":\"17\",\"asset_id\":\"1925\",\"stock_id\":\"2\",\"price1\":\"27200\",\"status\":\"1\"}', '2023-09-14 17:39:38', '1', '管理员');
INSERT INTO `api_log` VALUES (853, 24, 'update', '18', '{\"house_id\":\"5\",\"model_name\":\"HP 1000\",\"user_id\":\"1\",\"price\":\"0\",\"num\":\"42314324\",\"cate_name\":\"打印机\",\"id\":\"18\",\"asset_id\":\"1926\",\"stock_id\":\"2\",\"price1\":\"2800\",\"status\":\"1\"}', '2023-09-14 17:39:40', '1', '管理员');
INSERT INTO `api_log` VALUES (854, 24, 'update', '19', '{\"house_id\":\"6\",\"model_name\":\"HP 1000\",\"price\":\"0\",\"num\":\"HP453254235\",\"cate_name\":\"打印机\",\"id\":\"19\",\"asset_id\":\"1927\",\"stock_id\":\"2\",\"price1\":\"2800\",\"status\":\"1\"}', '2023-09-14 17:39:41', '1', '管理员');
INSERT INTO `api_log` VALUES (855, 24, 'update', '20', '{\"house_id\":\"6\",\"model_name\":\"TPLINK_100\",\"price\":\"0\",\"num\":\"TP-100-01\",\"cate_name\":\"路由器\",\"id\":\"20\",\"asset_id\":\"1928\",\"stock_id\":\"2\",\"price1\":\"96\",\"status\":\"1\"}', '2023-09-14 17:39:43', '1', '管理员');
INSERT INTO `api_log` VALUES (856, 24, 'update', '18', '{\"house_id\":\"5\",\"model_name\":\"HP 1000\",\"user_id\":\"1\",\"price\":\"0\",\"num\":\"42314324\",\"cate_name\":\"打印机\",\"id\":\"18\",\"asset_id\":\"1926\",\"stock_id\":\"2\",\"price1\":\"2800\",\"status\":\"2\"}', '2023-09-14 17:39:44', '1', '管理员');
INSERT INTO `api_log` VALUES (857, 24, 'update', '21', '{\"house_id\":\"5\",\"model_name\":\"立式\",\"price\":\"0\",\"num\":\"34324434543\",\"cate_name\":\"饮水机\",\"id\":\"21\",\"asset_id\":\"1929\",\"stock_id\":\"2\",\"price1\":\"36\",\"status\":\"1\"}', '2023-09-14 17:39:48', '1', '管理员');
INSERT INTO `api_log` VALUES (858, 24, 'update', '22', '{\"house_id\":\"5\",\"model_name\":\"2U-X86\",\"price\":\"0\",\"num\":\"2U-2023-8-1\",\"cate_name\":\"服务器\",\"id\":\"22\",\"asset_id\":\"1930\",\"stock_id\":\"2\",\"price1\":\"27200\",\"status\":\"1\"}', '2023-09-14 17:39:49', '1', '管理员');
INSERT INTO `api_log` VALUES (859, 24, 'update', '23', '{\"house_id\":\"5\",\"model_name\":\"HP 1000\",\"user_id\":\"1\",\"price\":\"0\",\"num\":\"4231432433\",\"cate_name\":\"打印机\",\"id\":\"23\",\"asset_id\":\"1931\",\"stock_id\":\"2\",\"price1\":\"2800\",\"status\":\"1\"}', '2023-09-14 17:39:52', '1', '管理员');
INSERT INTO `api_log` VALUES (860, 24, 'update', '24', '{\"house_id\":\"6\",\"model_name\":\"HP 1000\",\"price\":\"0\",\"num\":\"HP4538774235\",\"cate_name\":\"打印机\",\"id\":\"24\",\"asset_id\":\"1932\",\"stock_id\":\"2\",\"price1\":\"2800\",\"status\":\"1\"}', '2023-09-14 17:39:53', '1', '管理员');
INSERT INTO `api_log` VALUES (861, 24, 'update', '25', '{\"house_id\":\"6\",\"model_name\":\"TPLINK_100\",\"price\":\"0\",\"num\":\"TP-100-02\",\"cate_name\":\"路由器\",\"id\":\"25\",\"asset_id\":\"1933\",\"stock_id\":\"2\",\"price1\":\"96\",\"status\":\"1\"}', '2023-09-14 17:39:55', '1', '管理员');
INSERT INTO `api_log` VALUES (862, 24, 'update', '23', '{\"house_id\":\"5\",\"model_name\":\"HP 1000\",\"price\":\"0\",\"num\":\"4231432433\",\"cate_name\":\"打印机\",\"id\":\"23\",\"asset_id\":\"1931\",\"stock_id\":\"2\",\"price1\":\"2800\",\"status\":\"1\"}', '2023-09-14 17:40:07', '1', '管理员');
INSERT INTO `api_log` VALUES (863, 24, 'update', '24', '{\"house_id\":\"6\",\"model_name\":\"HP 1000\",\"price\":\"0\",\"num\":\"HP4538774235\",\"cate_name\":\"打印机\",\"id\":\"24\",\"asset_id\":\"1932\",\"stock_id\":\"2\",\"price1\":\"2800\",\"status\":\"2\"}', '2023-09-14 17:40:13', '1', '管理员');
INSERT INTO `api_log` VALUES (864, 24, 'update', '24', '{\"house_id\":\"6\",\"model_name\":\"HP 1000\",\"user_id\":\"1\",\"price\":\"0\",\"num\":\"HP4538774235\",\"cate_name\":\"打印机\",\"id\":\"24\",\"asset_id\":\"1932\",\"stock_id\":\"2\",\"price1\":\"2800\",\"status\":\"2\"}', '2023-09-14 17:40:15', '1', '管理员');
INSERT INTO `api_log` VALUES (865, 24, 'update', '17', '{\"house_id\":\"5\",\"model_name\":\"2U-X86\",\"price\":\"27200\",\"num\":\"2U-2023-9-1\",\"cate_name\":\"服务器\",\"id\":\"17\",\"asset_id\":\"1925\",\"stock_id\":\"2\",\"price1\":\"27200\",\"status\":\"1\"}', '2023-09-14 17:40:21', '1', '管理员');
INSERT INTO `api_log` VALUES (866, 24, 'update', '18', '{\"house_id\":\"5\",\"model_name\":\"HP 1000\",\"user_id\":\"1\",\"price\":\"2600\",\"num\":\"42314324\",\"cate_name\":\"打印机\",\"id\":\"18\",\"asset_id\":\"1926\",\"stock_id\":\"2\",\"price1\":\"2800\",\"status\":\"2\"}', '2023-09-14 17:40:23', '1', '管理员');
INSERT INTO `api_log` VALUES (867, 24, 'update', '19', '{\"house_id\":\"6\",\"model_name\":\"HP 1000\",\"price\":\"2600\",\"num\":\"HP453254235\",\"cate_name\":\"打印机\",\"id\":\"19\",\"asset_id\":\"1927\",\"stock_id\":\"2\",\"price1\":\"2800\",\"status\":\"1\"}', '2023-09-14 17:40:27', '1', '管理员');
INSERT INTO `api_log` VALUES (868, 24, 'update', '20', '{\"house_id\":\"6\",\"model_name\":\"TPLINK_100\",\"price\":\"90\",\"num\":\"TP-100-01\",\"cate_name\":\"路由器\",\"id\":\"20\",\"asset_id\":\"1928\",\"stock_id\":\"2\",\"price1\":\"96\",\"status\":\"1\"}', '2023-09-14 17:40:29', '1', '管理员');
INSERT INTO `api_log` VALUES (869, 24, 'update', '21', '{\"house_id\":\"5\",\"model_name\":\"立式\",\"price\":\"30\",\"num\":\"34324434543\",\"cate_name\":\"饮水机\",\"id\":\"21\",\"asset_id\":\"1929\",\"stock_id\":\"2\",\"price1\":\"36\",\"status\":\"1\"}', '2023-09-14 17:40:33', '1', '管理员');
INSERT INTO `api_log` VALUES (870, 24, 'update', '22', '{\"house_id\":\"5\",\"model_name\":\"2U-X86\",\"price\":\"27200\",\"num\":\"2U-2023-8-1\",\"cate_name\":\"服务器\",\"id\":\"22\",\"asset_id\":\"1930\",\"stock_id\":\"2\",\"price1\":\"27200\",\"status\":\"1\"}', '2023-09-14 17:40:35', '1', '管理员');
INSERT INTO `api_log` VALUES (871, 24, 'update', '23', '{\"house_id\":\"5\",\"model_name\":\"HP 1000\",\"price\":\"2500\",\"num\":\"4231432433\",\"cate_name\":\"打印机\",\"id\":\"23\",\"asset_id\":\"1931\",\"stock_id\":\"2\",\"price1\":\"2800\",\"status\":\"1\"}', '2023-09-14 17:40:38', '1', '管理员');
INSERT INTO `api_log` VALUES (872, 24, 'update', '24', '{\"house_id\":\"6\",\"model_name\":\"HP 1000\",\"user_id\":\"1\",\"price\":\"2500\",\"num\":\"HP4538774235\",\"cate_name\":\"打印机\",\"id\":\"24\",\"asset_id\":\"1932\",\"stock_id\":\"2\",\"price1\":\"2800\",\"status\":\"2\"}', '2023-09-14 17:40:41', '1', '管理员');
INSERT INTO `api_log` VALUES (873, 24, 'update', '25', '{\"house_id\":\"6\",\"model_name\":\"TPLINK_100\",\"price\":\"80\",\"num\":\"TP-100-02\",\"cate_name\":\"路由器\",\"id\":\"25\",\"asset_id\":\"1933\",\"stock_id\":\"2\",\"price1\":\"96\",\"status\":\"1\"}', '2023-09-14 17:40:45', '1', '管理员');
INSERT INTO `api_log` VALUES (874, 23, 'update', '2', '{\"start_time\":\"2023-08-30T10:00:00\",\"user_id\":\"1\",\"name\":\"20230902\",\"end_time\":\"2023-08-31T10:00:00\",\"id\":\"2\",\"status\":\"3\"}', '2023-09-14 17:41:06', '1', '管理员');
INSERT INTO `api_log` VALUES (875, 23, 'update', '2', '{\"id\":\"2\",\"status\":\"4\"}', '2023-09-14 17:41:09', '1', '管理员');
INSERT INTO `api_log` VALUES (876, 23, 'event', '7', '{\"id\":\"2\",\"status\":\"4\"}', '2023-09-14 17:41:09', '1', '管理员');
INSERT INTO `api_log` VALUES (877, 6, 'insert', '13', '{\"code\":\"asset_status\",\"name\":\"资产状态统计\",\"remark\":\"资产状态统计\",\"type\":\"chart\",\"content\":\"select count(t.id) value, d.dict_label name \\nfrom sys_dict_data d\\nleft join asset_info t  on t.status=d.dict_value\\nwhere  d.dict_type=\'assets_status\'\\ngroup by name\",\"group\":\"asset\"}', '2023-09-15 15:53:54', '1', '管理员');
INSERT INTO `api_log` VALUES (878, 1, 'insert', '29', '{\"code\":\"dev_group\",\"name\":\"应用分组\",\"group\":\"dev\",\"pk_field\":\"id\",\"unique_field\":\"code\",\"status\":\"1\"}', '2023-09-21 17:50:21', '1', '管理员');
INSERT INTO `api_log` VALUES (879, 1, 'insert', '30', '{\"code\":\"dev_app\",\"name\":\"应用信息\",\"group\":\"dev\",\"pk_field\":\"id\",\"status\":\"1\"}', '2023-09-21 17:50:40', '1', '管理员');
INSERT INTO `api_log` VALUES (880, 1, 'insert', '31', '{\"code\":\"dev_form\",\"name\":\"表单字段\",\"group\":\"dev\",\"pk_field\":\"id\",\"unique_field\":\"app_id,code\",\"status\":\"1\"}', '2023-09-21 17:51:02', '1', '管理员');
INSERT INTO `api_log` VALUES (881, 1, 'insert', '32', '{\"code\":\"dev_list\",\"name\":\"列表配置\",\"group\":\"dev\",\"pk_field\":\"id\",\"status\":\"1\"}', '2023-09-21 17:51:20', '1', '管理员');
INSERT INTO `api_log` VALUES (882, 6, 'insert', '14', '{\"code\":\"dev_asset_my\",\"name\":\"我的资产\",\"remark\":\"移动端个人资产查询\",\"type\":\"table\",\"content\":\"function(params){\\n  var sql = \\\"SELECT t.* FROM asset_info t JOIN asset_category c ON c.id = t.cate_id JOIN asset_model m ON m.id = t.model_id WHERE  t.status=\'2\' AND t.user_id=${userId}\\\";\\n  if(params.keyword){\\n  sql += \\\" AND (t.num like concat(\'%\', \'\\\"+params.keyword+\\\"\',\'%\') OR m.name like concat(\'%\', \'\\\"+params.keyword+\\\"\',\'%\') )\\\" ;\\n} \\n  var page = (params && params.page) ? params.page-1 : 0; \\n  var ', '2023-09-22 11:45:20', '1', '管理员');
INSERT INTO `api_log` VALUES (883, NULL, 'error', NULL, '脚本未定义 @undefined', '2023-09-22 14:01:55', '1', '管理员');
INSERT INTO `api_log` VALUES (884, NULL, 'error', NULL, '脚本未定义 @undefined', '2023-09-22 14:02:52', '1', '管理员');
INSERT INTO `api_log` VALUES (885, NULL, 'error', NULL, '脚本未定义 @undefined', '2023-09-22 14:04:28', '1', '管理员');
INSERT INTO `api_log` VALUES (886, NULL, 'error', NULL, '脚本未定义 @undefined', '2023-09-22 14:04:29', '1', '管理员');
INSERT INTO `api_log` VALUES (887, NULL, 'error', NULL, '脚本未定义 @undefined', '2023-09-22 14:04:36', '1', '管理员');
INSERT INTO `api_log` VALUES (888, NULL, 'error', NULL, '脚本未定义 @undefined', '2023-09-22 14:06:30', '1', '管理员');
INSERT INTO `api_log` VALUES (889, NULL, 'error', NULL, '脚本未定义 @undefined', '2023-09-22 14:06:33', '1', '管理员');
INSERT INTO `api_log` VALUES (890, NULL, 'error', NULL, '脚本未定义 @undefined', '2023-09-22 14:07:28', '1', '管理员');
INSERT INTO `api_log` VALUES (891, NULL, 'error', NULL, '脚本未定义 @undefined', '2023-09-22 14:07:35', '1', '管理员');
INSERT INTO `api_log` VALUES (892, NULL, 'error', NULL, '脚本未定义 @undefined', '2023-09-22 14:08:13', '1', '管理员');
INSERT INTO `api_log` VALUES (893, NULL, 'error', NULL, '脚本未定义 @undefined', '2023-09-22 14:08:20', '1', '管理员');
INSERT INTO `api_log` VALUES (894, NULL, 'error', NULL, '脚本未定义 @undefined', '2023-09-22 14:08:20', '1', '管理员');
INSERT INTO `api_log` VALUES (895, NULL, 'error', NULL, '脚本未定义 @undefined', '2023-09-22 14:11:27', '1', '管理员');
INSERT INTO `api_log` VALUES (896, NULL, 'error', NULL, '脚本未定义 @undefined', '2023-09-22 14:11:28', '1', '管理员');
INSERT INTO `api_log` VALUES (897, NULL, 'error', NULL, '脚本未定义 @undefined', '2023-09-22 14:11:49', '1', '管理员');
INSERT INTO `api_log` VALUES (898, NULL, 'error', NULL, '脚本未定义 @undefined', '2023-09-22 14:11:51', '1', '管理员');
INSERT INTO `api_log` VALUES (899, 6, 'update', '14', '{\"code\":\"dev_asset_my\",\"name\":\"我的资产\",\"remark\":\"移动端个人资产查询\",\"id\":\"14\",\"type\":\"table\",\"is_default\":\"N\",\"content\":\"function exec(params){\\n  var sql = \\\"SELECT t.* FROM asset_info t JOIN asset_category c ON c.id = t.cate_id JOIN asset_model m ON m.id = t.model_id WHERE  t.status=\'2\' AND t.user_id=${userId}\\\";\\n  if(params.keyword){\\n  sql += \\\" AND (t.num like concat(\'%\', \'\\\"+params.keyword+\\\"\',\'%\') OR m.name like concat(\'%\', \'\\\"+params.keyword+\\\"\',\'%\') )\\\" ;\\n} \\n  var page = (params && params.pag', '2023-09-22 14:13:04', '1', '管理员');
INSERT INTO `api_log` VALUES (900, NULL, 'error', NULL, '脚本未定义 @undefined', '2023-09-22 14:13:09', '1', '管理员');
INSERT INTO `api_log` VALUES (901, NULL, 'error', NULL, '脚本未定义 @undefined', '2023-09-22 14:36:10', '1', '管理员');
INSERT INTO `api_log` VALUES (902, 6, 'update', '14', '{\"code\":\"dev_asset_my\",\"name\":\"我的资产\",\"remark\":\"移动端个人资产查询\",\"id\":\"14\",\"type\":\"table\",\"is_default\":\"N\",\"content\":\"function exec(params){\\n  var sql = \\\"SELECT t.id, concat(m.name, \' \', t.num) name, c.name cate_name, m.params FROM asset_info t JOIN asset_category c ON c.id = t.cate_id JOIN asset_model m ON m.id = t.model_id WHERE  t.status=\'2\' AND t.user_id=${userId}\\\";\\n  if(params.keyword){\\n  sql += \\\" AND (t.num like concat(\'%\', \'\\\"+params.keyword+\\\"\',\'%\') OR m.name like concat(\'%\', \'\\\"+params.', '2023-09-22 15:08:24', '1', '管理员');
INSERT INTO `api_log` VALUES (903, NULL, 'error', NULL, '脚本未定义 @asset_model_tree', '2023-09-22 16:11:19', '1', '管理员');
INSERT INTO `api_log` VALUES (904, 6, 'insert', '15', '{\"code\":\"dev_model_tree\",\"name\":\"移动端模型选择树\",\"remark\":\"移动端模型选择树，包括分类\",\"type\":\"table\",\"content\":\"select id + 1000000 value, pid, name text from asset_category \\nunion all\\nselect id value, cate_id + 1000000 as pid, name text from asset_model\",\"group\":\"dev\"}', '2023-09-22 16:17:25', '1', '管理员');
INSERT INTO `api_log` VALUES (905, 6, 'update', '15', '{\"code\":\"dev_model_tree\",\"name\":\"移动端模型选择树\",\"remark\":\"移动端模型选择树，包括分类\",\"id\":\"15\",\"type\":\"table\",\"is_default\":\"N\",\"content\":\"select id + 1000000 value, pid  + 1000000 pid, name text from asset_category \\nunion all\\nselect id value, cate_id + 1000000 as pid, name text from asset_model\",\"group\":\"dev\"}', '2023-09-22 16:18:25', '1', '管理员');
INSERT INTO `api_log` VALUES (906, 6, 'update', '15', '{\"code\":\"dev_model_tree\",\"name\":\"移动端模型选择树\",\"remark\":\"移动端模型选择树，包括分类\",\"id\":\"15\",\"type\":\"table\",\"is_default\":\"N\",\"content\":\"select id + 1000000 value, if(pid=0,0, pid+ 1000000) pid, name text from asset_category \\nunion all\\nselect id value, cate_id + 1000000 as pid, name text from asset_model\",\"group\":\"dev\"}', '2023-09-22 16:19:44', '1', '管理员');
INSERT INTO `api_log` VALUES (907, 22, 'insert', '31', '{\"reason\":\"24324324\",\"amount\":\"4\",\"user_id\":\"1\",\"apply_status\":\"1\",\"apply_time\":\"2023-09-22 17:45:00.99\",\"model_id\":\"1914\",\"dept_id\":\"103\"}', '2023-09-22 17:45:01', '1', '管理员');
INSERT INTO `api_log` VALUES (908, 22, 'insert', '32', '{\"reason\":\"234234\",\"amount\":\"43\",\"user_id\":\"1\",\"apply_status\":\"1\",\"apply_time\":\"2023-09-22 17:52:41.561\",\"model_id\":\"1917\",\"dept_id\":\"103\"}', '2023-09-22 17:52:42', '1', '管理员');
INSERT INTO `api_log` VALUES (909, 22, 'insert', '33', '{\"reason\":\"2341324134\",\"amount\":\"34\",\"user_id\":\"1\",\"apply_status\":\"1\",\"apply_time\":\"2023-09-22 17:54:38.489\",\"model_id\":\"1914\",\"dept_id\":\"103\"}', '2023-09-22 17:54:38', '1', '管理员');
INSERT INTO `api_log` VALUES (910, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 21:15:19', '1', '管理员');
INSERT INTO `api_log` VALUES (911, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 21:15:19', '1', '管理员');
INSERT INTO `api_log` VALUES (912, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 21:15:19', '1', '管理员');
INSERT INTO `api_log` VALUES (913, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 21:30:51', '1', '管理员');
INSERT INTO `api_log` VALUES (914, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 21:30:52', '1', '管理员');
INSERT INTO `api_log` VALUES (915, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 21:30:52', '1', '管理员');
INSERT INTO `api_log` VALUES (916, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 21:32:45', '1', '管理员');
INSERT INTO `api_log` VALUES (917, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 21:32:45', '1', '管理员');
INSERT INTO `api_log` VALUES (918, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 21:32:45', '1', '管理员');
INSERT INTO `api_log` VALUES (919, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 21:32:55', '1', '管理员');
INSERT INTO `api_log` VALUES (920, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 21:32:55', '1', '管理员');
INSERT INTO `api_log` VALUES (921, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 21:32:55', '1', '管理员');
INSERT INTO `api_log` VALUES (922, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 21:33:51', '1', '管理员');
INSERT INTO `api_log` VALUES (923, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 21:33:51', '1', '管理员');
INSERT INTO `api_log` VALUES (924, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 21:33:51', '1', '管理员');
INSERT INTO `api_log` VALUES (925, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 21:34:01', '1', '管理员');
INSERT INTO `api_log` VALUES (926, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 21:34:01', '1', '管理员');
INSERT INTO `api_log` VALUES (927, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 21:34:01', '1', '管理员');
INSERT INTO `api_log` VALUES (928, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 21:34:16', '1', '管理员');
INSERT INTO `api_log` VALUES (929, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 21:34:16', '1', '管理员');
INSERT INTO `api_log` VALUES (930, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 21:34:16', '1', '管理员');
INSERT INTO `api_log` VALUES (931, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 22:39:22', '1', '管理员');
INSERT INTO `api_log` VALUES (932, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 22:39:22', '1', '管理员');
INSERT INTO `api_log` VALUES (933, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 22:39:22', '1', '管理员');
INSERT INTO `api_log` VALUES (934, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 22:51:51', '1', '管理员');
INSERT INTO `api_log` VALUES (935, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 22:51:51', '1', '管理员');
INSERT INTO `api_log` VALUES (936, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 22:51:51', '1', '管理员');
INSERT INTO `api_log` VALUES (937, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 22:53:46', '1', '管理员');
INSERT INTO `api_log` VALUES (938, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 22:53:46', '1', '管理员');
INSERT INTO `api_log` VALUES (939, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 22:53:46', '1', '管理员');
INSERT INTO `api_log` VALUES (940, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 22:57:01', '1', '管理员');
INSERT INTO `api_log` VALUES (941, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 22:57:01', '1', '管理员');
INSERT INTO `api_log` VALUES (942, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 22:57:01', '1', '管理员');
INSERT INTO `api_log` VALUES (943, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 22:57:41', '1', '管理员');
INSERT INTO `api_log` VALUES (944, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 22:57:41', '1', '管理员');
INSERT INTO `api_log` VALUES (945, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 22:57:41', '1', '管理员');
INSERT INTO `api_log` VALUES (946, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 23:00:59', '1', '管理员');
INSERT INTO `api_log` VALUES (947, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 23:00:59', '1', '管理员');
INSERT INTO `api_log` VALUES (948, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 23:00:59', '1', '管理员');
INSERT INTO `api_log` VALUES (949, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 23:01:31', '1', '管理员');
INSERT INTO `api_log` VALUES (950, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 23:01:31', '1', '管理员');
INSERT INTO `api_log` VALUES (951, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 23:01:31', '1', '管理员');
INSERT INTO `api_log` VALUES (952, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 23:01:36', '1', '管理员');
INSERT INTO `api_log` VALUES (953, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 23:01:36', '1', '管理员');
INSERT INTO `api_log` VALUES (954, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 23:01:36', '1', '管理员');
INSERT INTO `api_log` VALUES (955, 6, 'insert', '16', '{\"code\":\"dev_dept_tree\",\"name\":\"移动端部门树\",\"remark\":\"移动端部门树\",\"type\":\"table\",\"content\":\"select dept_id value, parent_id pid, dept_name text\",\"group\":\"dev\"}', '2023-09-22 23:04:18', '1', '管理员');
INSERT INTO `api_log` VALUES (956, 6, 'insert', '17', '{\"code\":\"dev_user_select\",\"name\":\"移动端用户选择\",\"type\":\"table\",\"content\":\"select user_id value, nick_name text from sys_user where del_flag=0\",\"group\":\"dev\"}', '2023-09-22 23:05:06', '1', '管理员');
INSERT INTO `api_log` VALUES (957, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 23:05:49', '1', '管理员');
INSERT INTO `api_log` VALUES (958, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-22 23:05:54', '1', '管理员');
INSERT INTO `api_log` VALUES (959, 6, 'update', '16', '{\"code\":\"dev_dept_tree\",\"name\":\"移动端部门树\",\"remark\":\"移动端部门树\",\"id\":\"16\",\"type\":\"table\",\"is_default\":\"N\",\"content\":\"select dept_id value, parent_id pid, dept_name text from sys_dept\",\"group\":\"dev\"}', '2023-09-22 23:06:20', '1', '管理员');
INSERT INTO `api_log` VALUES (960, 6, 'insert', '18', '{\"code\":\"dev_warehouse_select\",\"name\":\"移动端仓库选择器\",\"type\":\"table\",\"content\":\"select id value, name text from asset_warehouse\",\"group\":\"dev\"}', '2023-09-22 23:07:24', '1', '管理员');
INSERT INTO `api_log` VALUES (961, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-25 16:33:57', '1', '管理员');
INSERT INTO `api_log` VALUES (962, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-25 16:33:57', '1', '管理员');
INSERT INTO `api_log` VALUES (963, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-25 16:33:57', '1', '管理员');
INSERT INTO `api_log` VALUES (964, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-25 16:34:27', '1', '管理员');
INSERT INTO `api_log` VALUES (965, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-25 16:34:27', '1', '管理员');
INSERT INTO `api_log` VALUES (966, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-25 16:34:27', '1', '管理员');
INSERT INTO `api_log` VALUES (967, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-25 16:35:01', '1', '管理员');
INSERT INTO `api_log` VALUES (968, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-25 16:35:01', '1', '管理员');
INSERT INTO `api_log` VALUES (969, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-25 16:35:01', '1', '管理员');
INSERT INTO `api_log` VALUES (970, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-25 16:35:46', '1', '管理员');
INSERT INTO `api_log` VALUES (971, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-25 16:35:46', '1', '管理员');
INSERT INTO `api_log` VALUES (972, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-25 16:35:46', '1', '管理员');
INSERT INTO `api_log` VALUES (973, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-25 16:36:23', '1', '管理员');
INSERT INTO `api_log` VALUES (974, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-25 16:36:23', '1', '管理员');
INSERT INTO `api_log` VALUES (975, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-25 16:36:23', '1', '管理员');
INSERT INTO `api_log` VALUES (976, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-25 16:36:36', '1', '管理员');
INSERT INTO `api_log` VALUES (977, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-25 16:36:36', '1', '管理员');
INSERT INTO `api_log` VALUES (978, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-25 16:36:36', '1', '管理员');
INSERT INTO `api_log` VALUES (979, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-25 16:37:06', '1', '管理员');
INSERT INTO `api_log` VALUES (980, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-25 16:37:06', '1', '管理员');
INSERT INTO `api_log` VALUES (981, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-25 16:37:06', '1', '管理员');
INSERT INTO `api_log` VALUES (982, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-25 16:37:24', '1', '管理员');
INSERT INTO `api_log` VALUES (983, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-25 16:37:24', '1', '管理员');
INSERT INTO `api_log` VALUES (984, NULL, 'error', NULL, '脚本未定义 @null', '2023-09-25 16:37:24', '1', '管理员');
INSERT INTO `api_log` VALUES (985, 22, 'insert', '34', '{\"reason\":\"234234324\",\"amount\":\"3\",\"user_id\":\"1\",\"apply_status\":\"1\",\"apply_time\":\"2023-09-26 10:28:50.865\",\"model_id\":\"1000039\",\"dept_id\":\"103\"}', '2023-09-26 10:28:51', '1', '管理员');
INSERT INTO `api_log` VALUES (986, 22, 'insert', '35', '{\"reason\":\"324324\",\"amount\":\"3\",\"user_id\":\"1\",\"apply_status\":\"1\",\"apply_time\":\"2023-09-26 10:29:43.427\",\"model_id\":\"1914\",\"dept_id\":\"103\"}', '2023-09-26 10:29:43', '1', '管理员');
INSERT INTO `api_log` VALUES (988, 17, 'insert', '36', '{\"reason\":\"dad\",\"user_id\":\"1\",\"price\":\"0\",\"num\":\"2023092615272711\",\"apply_status\":\"1\",\"comment\":\"asdasd\",\"apply_time\":\"2023-09-26 15:27:32.918\",\"dept_id\":\"103\",\"check_status\":\"1\"}', '2023-09-26 15:27:33', '1', '管理员');
INSERT INTO `api_log` VALUES (989, 18, 'insert', '45', '{\"amount\":\"1\",\"purchase_id\":36,\"cate_id\":\"0\",\"model_id\":\"1000043\"}', '2023-09-26 15:27:33', '1', '管理员');
INSERT INTO `api_log` VALUES (990, 6, 'insert', '19', '{\"code\":\"dev_purchase_my\",\"name\":\"我的采购申请\",\"type\":\"table\",\"content\":\"function exec(params){\\n  var sql = \\\"SELECT t.id, t.num, t.price price, t.reason, t.comment FROM asset_purchase WHERE t.user_id=${userId}\\\";\\n  if(params.keyword){\\n  sql += \\\" AND t.num like concat(\'%\', \'\\\"+params.keyword+\\\"\',\'%\')\\\" ;\\n} \\nsql += \\\" ORDER BY t.id DESC \\\"\\n  var page = (params && params.page) ? params.page-1 : 0; \\n  var limit = (params && params.limit ) ? params.limit : 10; \\n if(page == 0){\\nsql += \\\" LIMIT ', '2023-09-26 15:49:23', '1', '管理员');
INSERT INTO `api_log` VALUES (991, 6, 'update', '19', '{\"code\":\"dev_purchase_my\",\"name\":\"我的采购申请\",\"id\":\"19\",\"type\":\"table\",\"is_default\":\"N\",\"content\":\"function exec(params){\\n  var sql = \\\"SELECT t.id, t.num, t.price price, t.reason, t.comment FROM asset_purchase t WHERE t.user_id=${userId}\\\";\\n  if(params.keyword){\\n  sql += \\\" AND t.num like concat(\'%\', \'\\\"+params.keyword+\\\"\',\'%\')\\\" ;\\n} \\nsql += \\\" ORDER BY t.id DESC \\\"\\n  var page = (params && params.page) ? params.page-1 : 0; \\n  var limit = (params && params.limit ) ? params.limit : 10; \\n if(', '2023-09-26 15:50:59', '1', '管理员');
INSERT INTO `api_log` VALUES (992, 6, 'update', '15', '{\"code\":\"dev_model_tree\",\"name\":\"移动端模型选择树\",\"remark\":\"移动端模型选择树，包括分类\",\"id\":\"15\",\"type\":\"table\",\"is_default\":\"N\",\"content\":\"select c.id + 1000000 value, if(c.pid=0,0, c.pid+ 1000000) pid, c.name text from asset_category c join asset_model m on m.cate_id = c.id\\nunion all\\nselect id value, cate_id + 1000000 as pid, name text from asset_model\",\"group\":\"dev\"}', '2023-09-26 18:09:22', '1', '管理员');
INSERT INTO `api_log` VALUES (993, 6, 'update', '15', '{\"code\":\"dev_model_tree\",\"name\":\"移动端模型选择树\",\"remark\":\"移动端模型选择树，包括分类\",\"id\":\"15\",\"type\":\"table\",\"is_default\":\"N\",\"content\":\"select distinct c.id + 1000000 value, if(c.pid=0,0, c.pid+ 1000000) pid, c.name text from asset_category c join asset_model m on m.cate_id = c.id\\nunion all\\nselect id value, cate_id + 1000000 as pid, name text from asset_model\",\"group\":\"dev\"}', '2023-09-26 18:10:29', '1', '管理员');
INSERT INTO `api_log` VALUES (994, 25, 'insert', '36', '{\"apply_reason\":\"dad\",\"num\":\"2023092615272711\",\"target_id\":\"36\",\"type\":\"asset_purchase\",\"title\":\"采购订单\"}', '2023-09-27 11:17:52', '1', '管理员');
INSERT INTO `api_log` VALUES (995, 26, 'insert', '65', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":36,\"pid\":\"0\",\"check_status\":\"1\"}', '2023-09-27 11:17:52', '1', '管理员');
INSERT INTO `api_log` VALUES (997, 25, 'insert', '38', '{\"apply_reason\":\"dad\",\"user_id\":\"1\",\"user_name\":\"管理员\",\"num\":\"2023092615272711\",\"target_id\":\"36\",\"apply_time\":\"2023-09-27 11:26:56.783\",\"type\":\"asset_purchase\",\"title\":\"采购订单\",\"check_status\":\"1\"}', '2023-09-27 11:26:57', '1', '管理员');
INSERT INTO `api_log` VALUES (998, 26, 'insert', '66', '{\"check_user_name\":\"若依\",\"check_user_id\":\"2\",\"task_id\":38,\"pid\":\"0\",\"check_status\":\"1\"}', '2023-09-27 11:26:57', '1', '管理员');
INSERT INTO `api_log` VALUES (999, 25, 'insert', '39', '{\"apply_reason\":\"dad\",\"user_id\":\"1\",\"user_name\":\"管理员\",\"num\":\"2023092615272711\",\"target_id\":\"36\",\"apply_time\":\"2023-09-27 11:40:11.614\",\"type\":\"asset_purchase\",\"title\":\"采购订单\",\"check_status\":\"1\"}', '2023-09-27 11:40:12', '1', '管理员');
INSERT INTO `api_log` VALUES (1000, 26, 'insert', '67', '{\"check_user_name\":\"若依\",\"check_user_id\":\"2\",\"task_id\":39,\"pid\":\"0\",\"check_status\":\"1\"}', '2023-09-27 11:40:12', '1', '管理员');
INSERT INTO `api_log` VALUES (1001, 17, 'update', '36', '{\"apply_status\":\"2\",\"apply_time\":\"2023-09-27 11:40:11.699\",\"id\":\"36\",\"check_status\":\"1\"}', '2023-09-27 11:40:12', '1', '管理员');
INSERT INTO `api_log` VALUES (1002, 25, 'insert', '40', '{\"apply_reason\":\"dad\",\"user_id\":\"1\",\"user_name\":\"管理员\",\"num\":\"2023092615272711\",\"target_id\":\"36\",\"apply_time\":\"2023-09-27 11:41:18.021\",\"type\":\"asset_purchase\",\"title\":\"采购订单\",\"check_status\":\"1\"}', '2023-09-27 11:41:18', '1', '管理员');
INSERT INTO `api_log` VALUES (1003, 26, 'insert', '68', '{\"check_user_name\":\"若依\",\"check_user_id\":\"2\",\"task_id\":40,\"pid\":\"0\",\"check_status\":\"1\"}', '2023-09-27 11:41:18', '1', '管理员');
INSERT INTO `api_log` VALUES (1004, 17, 'update', '36', '{\"apply_status\":\"2\",\"apply_time\":\"2023-09-27 11:41:18.039\",\"id\":\"36\",\"check_status\":\"1\"}', '2023-09-27 11:41:18', '1', '管理员');
INSERT INTO `api_log` VALUES (1005, 22, 'insert', '36', '{\"reason\":\"2313\",\"amount\":\"1\",\"user_id\":\"1\",\"apply_status\":\"1\",\"apply_time\":\"2023-09-27 11:43:01.75\",\"model_id\":\"1916\",\"dept_id\":\"103\"}', '2023-09-27 11:43:02', '1', '管理员');
INSERT INTO `api_log` VALUES (1006, 6, 'insert', '20', '{\"code\":\"asset_use_my\",\"name\":\"我的使用申请\",\"type\":\"table\",\"content\":\"function exec(params){\\n  var sql = \\\"SELECT t.id, m.name model_name, concat(t.amount, \'\') amount, t.reason FROM asset_use t JOIN asset_model m ON m.id = t.model_id WHERE t.user_id=${userId}\\\";\\n  if(params.keyword){\\n  sql += \\\" AND t.nums like concat(\'%\', \'\\\"+params.keyword+\\\"\',\'%\')\\\" ;\\n} \\nsql += \\\" ORDER BY t.id DESC \\\"\\n  var page = (params && params.page) ? params.page-1 : 0; \\n  var limit = (params && params.limit ) ? para', '2023-09-27 14:11:32', '1', '管理员');
INSERT INTO `api_log` VALUES (1007, 6, 'insert', '21', '{\"code\":\"dev_asset_num\",\"name\":\"根据编码查资产\",\"type\":\"table\",\"is_default\":\"N\",\"content\":\"function exec(params){\\n  var sql = \\\"SELECT id from asset_info \\\";\\nif(params.keyword){\\nsql += \\\" WHERE num = \'\\\" + params.keyword + \\\"\'\\\";\\n}else{\\nsql += \\\" WHERE 1 = 0\\\";\\n}\\nsql += \\\" LIMIT 1 \\\"\\n \\n  return sql;\\n}\",\"group\":\"dev\"}', '2023-09-27 14:24:58', '1', '管理员');
INSERT INTO `api_log` VALUES (1008, 21, 'insert', '2', '{\"reason\":\"4504\",\"imgs\":\"/profile/upload/2023/09/27/941c8c7bfd9af4244a66dc102db3cb6c_20230927171327A001.jpg\",\"user_id\":\"1\",\"apply_time\":\"2023-09-27 17:14:40.63\",\"asset_id\":\"1945\",\"type\":\"1\",\"status\":\"1\"}', '2023-09-27 17:14:41', '1', '管理员');
INSERT INTO `api_log` VALUES (1009, NULL, 'error', NULL, '脚本未定义 @asset_maintain_my', '2023-09-27 17:37:20', '1', '管理员');
INSERT INTO `api_log` VALUES (1010, 6, 'insert', '22', '{\"code\":\"asset_maintain_my\",\"name\":\"我的维保申请\",\"type\":\"table\",\"is_default\":\"N\",\"content\":\"function exec(params){\\n  var sql = \\\"SELECT t.id, i.num m.name model_name,t.reason, t.apply_time FROM asset_maintain t JOIN asset_info i ON i.id = t.asset_id JOIN asset_model m ON m.id = i.model_id WHERE t.user_id=${userId}\\\";\\n  if(params.keyword){\\n  sql += \\\" AND t.nums like concat(\'%\', \'\\\"+params.keyword+\\\"\',\'%\')\\\" ;\\n} \\nsql += \\\" ORDER BY t.id DESC \\\"\\n  var page = (params && params.page) ? params.page', '2023-09-27 17:38:47', '1', '管理员');
INSERT INTO `api_log` VALUES (1011, 6, 'update', '22', '{\"code\":\"asset_maintain_my\",\"name\":\"我的维保申请\",\"id\":\"22\",\"type\":\"table\",\"is_default\":\"N\",\"content\":\"function exec(params){\\n  var sql = \\\"SELECT t.id, i.num, m.name model_name,t.reason, t.apply_time FROM asset_maintain t JOIN asset_info i ON i.id = t.asset_id JOIN asset_model m ON m.id = i.model_id WHERE t.user_id=${userId}\\\";\\n  if(params.keyword){\\n  sql += \\\" AND t.nums like concat(\'%\', \'\\\"+params.keyword+\\\"\',\'%\')\\\" ;\\n} \\nsql += \\\" ORDER BY t.id DESC \\\"\\n  var page = (params && params.page) ? ', '2023-09-27 17:39:14', '1', '管理员');
INSERT INTO `api_log` VALUES (1012, 6, 'update', '21', '{\"code\":\"dev_asset_num\",\"name\":\"根据编码查资产\",\"id\":\"21\",\"type\":\"table\",\"is_default\":\"N\",\"content\":\"function exec(params){\\n  var sql = \\\"SELECT t.id, t.num, concat(c.name, \' \', m.name) name from asset_info t JOIN asset_model m on m.id = t.model_id JOIN asset_category c ON m.cate_id = c.id\\\";\\nif(params.code){\\nsql += \\\" WHERE t.num = \'\\\" + params.code + \\\"\'\\\";\\n}else if(params.id){\\nsql += \\\" WHERE t.id = \'\\\" + params.id + \\\"\'\\\";\\n}else{\\nsql += \\\" WHERE 1 = 0\\\";\\n}\\nsql += \\\" LIMIT 1 \\\"\\n \\n  retur', '2023-09-28 09:09:14', '1', '管理员');
INSERT INTO `api_log` VALUES (1013, 6, 'update', '21', '{\"code\":\"dev_asset_num\",\"name\":\"根据编码查资产\",\"id\":\"21\",\"type\":\"table\",\"is_default\":\"N\",\"content\":\"function exec(params){\\n  var sql = \\\"SELECT t.id, t.num code, concat(c.name, \' \', m.name) name from asset_info t JOIN asset_model m on m.id = t.model_id JOIN asset_category c ON m.cate_id = c.id\\\";\\nif(params.code){\\nsql += \\\" WHERE t.num = \'\\\" + params.code + \\\"\'\\\";\\n}else if(params.id){\\nsql += \\\" WHERE t.id = \'\\\" + params.id + \\\"\'\\\";\\n}else{\\nsql += \\\" WHERE 1 = 0\\\";\\n}\\nsql += \\\" LIMIT 1 \\\"\\n \\n  ', '2023-09-28 09:44:54', '1', '管理员');
INSERT INTO `api_log` VALUES (1014, 6, 'insert', '23', '{\"code\":\"dev_flow_to_me\",\"name\":\"移动端待我审核\",\"type\":\"table\",\"is_default\":\"N\",\"content\":\"function exec(params){\\n  var sql = \\\"SELECT s.id, t.title, t.num, t.user_name, t.apply_time, t.reason FROM flow_task t JOIN flow_step s ON t.id = s.task_id JOIN asset_model m ON m.id = i.model_id WHERE s.user_id=${userId}\\\";\\n  if(params.keyword){\\n  sql += \\\" AND t.num like concat(\'%\', \'\\\"+params.keyword+\\\"\',\'%\')\\\" ;\\n} \\nsql += \\\" ORDER BY t.apply_time DESC \\\"\\n  var page = (params && params.page) ? params.p', '2023-09-28 11:14:11', '1', '管理员');
INSERT INTO `api_log` VALUES (1015, NULL, 'error', NULL, '脚本未定义 @undefined', '2023-09-28 11:18:50', '1', '管理员');
INSERT INTO `api_log` VALUES (1016, 6, 'update', '23', '{\"code\":\"dev_flow_to_me\",\"name\":\"移动端待我审核\",\"id\":\"23\",\"type\":\"table\",\"is_default\":\"N\",\"content\":\"function exec(params){\\n  var sql = \\\"SELECT s.id, t.title, t.num, t.user_name, t.apply_time, t.apply_reason FROM flow_task t JOIN flow_step s ON t.id = s.task_id JOIN asset_model m ON m.id = i.model_id WHERE s.user_id=${userId}\\\";\\n  if(params.keyword){\\n  sql += \\\" AND t.num like concat(\'%\', \'\\\"+params.keyword+\\\"\',\'%\')\\\" ;\\n} \\nsql += \\\" ORDER BY t.apply_time DESC \\\"\\n  var page = (params && params.', '2023-09-28 11:25:12', '1', '管理员');
INSERT INTO `api_log` VALUES (1017, 6, 'update', '23', '{\"code\":\"dev_flow_to_me\",\"name\":\"移动端待我审核\",\"id\":\"23\",\"type\":\"table\",\"is_default\":\"N\",\"content\":\"function exec(params){\\n  var sql = \\\"SELECT s.id, t.title, t.num, t.user_name, t.apply_time, t.apply_reason FROM flow_task t JOIN flow_step s ON t.id = s.task_id JOIN asset_model m ON m.id = i.model_id WHERE s.check_user_id=${userId}\\\";\\n  if(params.keyword){\\n  sql += \\\" AND t.num like concat(\'%\', \'\\\"+params.keyword+\\\"\',\'%\')\\\" ;\\n} \\nsql += \\\" ORDER BY t.apply_time DESC \\\"\\n  var page = (params && p', '2023-09-28 11:26:01', '1', '管理员');
INSERT INTO `api_log` VALUES (1018, 6, 'update', '23', '{\"code\":\"dev_flow_to_me\",\"name\":\"移动端待我审核\",\"id\":\"23\",\"type\":\"table\",\"is_default\":\"N\",\"content\":\"function exec(params){\\n  var sql = \\\"SELECT s.id, t.title, t.num, t.user_name, t.apply_time, t.apply_reason FROM flow_task t JOIN flow_step s ON t.id = s.task_id WHERE s.check_user_id=${userId}\\\";\\n  if(params.keyword){\\n  sql += \\\" AND t.num like concat(\'%\', \'\\\"+params.keyword+\\\"\',\'%\')\\\" ;\\n} \\nsql += \\\" ORDER BY t.apply_time DESC \\\"\\n  var page = (params && params.page) ? params.page-1 : 0; \\n  var', '2023-09-28 11:26:28', '1', '管理员');
INSERT INTO `api_log` VALUES (1019, 6, 'update', '23', '{\"code\":\"dev_flow_to_me\",\"name\":\"移动端待我审核\",\"id\":\"23\",\"type\":\"table\",\"is_default\":\"N\",\"content\":\"function exec(params){\\n  var sql = \\\"SELECT s.id, t.title, t.num, t.user_name, t.apply_time, t.apply_reason FROM flow_task t JOIN flow_step s ON t.id = s.task_id WHERE t.check_status=\'1\' AND s.check_user_id=${userId}\\\";\\n  if(params.keyword){\\n  sql += \\\" AND t.num like concat(\'%\', \'\\\"+params.keyword+\\\"\',\'%\')\\\" ;\\n} \\nsql += \\\" ORDER BY t.apply_time DESC \\\"\\n  var page = (params && params.page) ? par', '2023-09-28 14:55:58', '1', '管理员');
INSERT INTO `api_log` VALUES (1020, 26, 'update', '30', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"29\",\"pid\":\"0\",\"id\":\"30\",\"check_status\":\"2\",\"check_time\":\"2023-09-28 15:08:23.436\"}', '2023-09-28 15:08:28', '1', '管理员');
INSERT INTO `api_log` VALUES (1021, 26, 'event', '5', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"29\",\"pid\":\"0\",\"id\":\"30\",\"check_status\":\"2\",\"check_time\":\"2023-09-28 15:08:23.436\"}', '2023-09-28 15:08:28', '1', '管理员');
INSERT INTO `api_log` VALUES (1022, 26, 'insert', '69', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":\"29\",\"pid\":\"30\",\"check_status\":\"1\"}', '2023-09-28 15:08:29', '1', '管理员');
INSERT INTO `api_log` VALUES (1023, 26, 'update', '30', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"29\",\"pid\":\"0\",\"id\":\"30\",\"check_status\":\"2\",\"check_time\":\"2023-09-28 15:08:56.761\"}', '2023-09-28 15:08:57', '1', '管理员');
INSERT INTO `api_log` VALUES (1024, 26, 'event', '5', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"29\",\"pid\":\"0\",\"id\":\"30\",\"check_status\":\"2\",\"check_time\":\"2023-09-28 15:08:56.761\"}', '2023-09-28 15:08:57', '1', '管理员');
INSERT INTO `api_log` VALUES (1025, 26, 'insert', '70', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":\"29\",\"pid\":\"30\",\"check_status\":\"1\"}', '2023-09-28 15:08:57', '1', '管理员');
INSERT INTO `api_log` VALUES (1026, 26, 'update', '69', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":\"29\",\"pid\":\"30\",\"id\":\"69\",\"check_status\":\"2\",\"check_time\":\"2023-09-28 15:09:18.682\"}', '2023-09-28 15:09:19', '1', '管理员');
INSERT INTO `api_log` VALUES (1027, 26, 'event', '5', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":\"29\",\"pid\":\"30\",\"id\":\"69\",\"check_status\":\"2\",\"check_time\":\"2023-09-28 15:09:18.682\"}', '2023-09-28 15:09:19', '1', '管理员');
INSERT INTO `api_log` VALUES (1028, 26, 'update', '69', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":\"29\",\"pid\":\"30\",\"id\":\"69\",\"check_status\":\"2\",\"check_time\":\"2023-09-28 15:10:57.583\"}', '2023-09-28 15:10:58', '1', '管理员');
INSERT INTO `api_log` VALUES (1029, 26, 'event', '5', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":\"29\",\"pid\":\"30\",\"id\":\"69\",\"check_status\":\"2\",\"check_time\":\"2023-09-28 15:10:57.583\"}', '2023-09-28 15:10:58', '1', '管理员');
INSERT INTO `api_log` VALUES (1030, 26, 'update', '69', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":\"29\",\"pid\":\"30\",\"id\":\"69\",\"check_status\":\"2\",\"check_time\":\"2023-09-28 15:15:01.061\"}', '2023-09-28 15:15:01', '1', '管理员');
INSERT INTO `api_log` VALUES (1031, 26, 'event', '5', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"task_id\":\"29\",\"pid\":\"30\",\"id\":\"69\",\"check_status\":\"2\",\"check_time\":\"2023-09-28 15:15:01.061\"}', '2023-09-28 15:15:01', '1', '管理员');
INSERT INTO `api_log` VALUES (1032, 25, 'update', '29', '{\"id\":\"29\",\"check_status\":\"2\"}', '2023-09-28 15:15:01', '1', '管理员');
INSERT INTO `api_log` VALUES (1033, 17, 'update', '27', '{\"id\":27,\"check_status\":\"2\"}', '2023-09-28 15:15:01', '1', '管理员');
INSERT INTO `api_log` VALUES (1034, 26, 'update', '30', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"29\",\"pid\":\"0\",\"id\":\"30\",\"check_status\":\"2\",\"check_time\":\"2023-09-28 15:15:10.949\"}', '2023-09-28 15:15:11', '1', '管理员');
INSERT INTO `api_log` VALUES (1035, 26, 'event', '5', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"29\",\"pid\":\"0\",\"id\":\"30\",\"check_status\":\"2\",\"check_time\":\"2023-09-28 15:15:10.949\"}', '2023-09-28 15:15:11', '1', '管理员');
INSERT INTO `api_log` VALUES (1036, 25, 'update', '29', '{\"id\":\"29\",\"check_status\":\"2\"}', '2023-09-28 15:15:11', '1', '管理员');
INSERT INTO `api_log` VALUES (1037, 17, 'update', '27', '{\"id\":27,\"check_status\":\"2\"}', '2023-09-28 15:15:11', '1', '管理员');
INSERT INTO `api_log` VALUES (1038, 26, 'update', '30', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"29\",\"pid\":\"0\",\"id\":\"30\",\"check_status\":\"2\",\"check_time\":\"2023-09-28 15:15:15.485\"}', '2023-09-28 15:15:15', '1', '管理员');
INSERT INTO `api_log` VALUES (1039, 26, 'event', '5', '{\"check_user_name\":\"管理员\",\"check_user_id\":\"1\",\"ignore_flag\":\"0\",\"task_id\":\"29\",\"pid\":\"0\",\"id\":\"30\",\"check_status\":\"2\",\"check_time\":\"2023-09-28 15:15:15.485\"}', '2023-09-28 15:15:15', '1', '管理员');
INSERT INTO `api_log` VALUES (1040, 25, 'update', '29', '{\"id\":\"29\",\"check_status\":\"2\"}', '2023-09-28 15:15:16', '1', '管理员');
INSERT INTO `api_log` VALUES (1041, 17, 'update', '27', '{\"id\":27,\"check_status\":\"2\"}', '2023-09-28 15:15:16', '1', '管理员');
INSERT INTO `api_log` VALUES (1042, NULL, 'error', NULL, '脚本未定义 @undefined', '2023-09-28 15:16:31', '1', '管理员');

-- ----------------------------
-- Table structure for api_model
-- ----------------------------
DROP TABLE IF EXISTS `api_model`;
CREATE TABLE `api_model`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分组',
  `code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '代码',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `pk_field` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键代码',
  `unique_field` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '唯一字段',
  `org_scope` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组织权限',
  `user_scope` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户权限',
  `excels` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '导入导出配置',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '状态',
  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否内置',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注详细',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '数据模型' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of api_model
-- ----------------------------
INSERT INTO `api_model` VALUES (1, 'api', 'api_model', '数据模型', 'id', 'code', NULL, NULL, NULL, '1', 'Y', NULL);
INSERT INTO `api_model` VALUES (2, 'api', 'api_event', '模型事件', 'id', 'model_id,action', NULL, NULL, NULL, '1', 'Y', NULL);
INSERT INTO `api_model` VALUES (3, 'api', 'api_access', '模型权限', 'id', 'model_id,role_id', NULL, NULL, NULL, '1', 'Y', NULL);
INSERT INTO `api_model` VALUES (5, 'api', 'api_log', '接口日志', 'id', NULL, NULL, NULL, '{\n	\"api_log\": {\n		\"user_name\": \"操作人\",\n		\"log_type\": \"操作类型\",\n		\"log_time\": \"记录时间\",\n		\"log_key\": \"数据主键\",\n		\"log_data\": \"数据\"\n	},\n	\"api_model\": {\n		\"model_code\": \"模型代码\",\n		\"model_name\": \"模型名称\"\n	}\n}', '1', 'Y', NULL);
INSERT INTO `api_model` VALUES (6, 'api', 'api_sql', '数据脚本', 'id', 'code', NULL, NULL, NULL, '1', 'Y', NULL);
INSERT INTO `api_model` VALUES (7, NULL, 'sys_role', '角色', 'id', 'role_key', NULL, NULL, NULL, '1', 'Y', NULL);
INSERT INTO `api_model` VALUES (8, NULL, 'sys_field', '模型字段', 'id', 'model_id,code', NULL, NULL, NULL, '1', 'Y', NULL);
INSERT INTO `api_model` VALUES (13, 'asset', 'asset_category', '资产分类', 'id', 'name', NULL, NULL, '{\r\n	\"asset_category\": {\r\n		\"pid\": \"上级分类\",\r\n		\"name\": \"分类名称\",\r\n		\"nums\": \"编码规则\",\r\n		\"list_sort\": \"排序\",\r\n		\"status\": \"状态\"\r\n	},\r\n	\"@ds\": {\r\n		\"pid\": \"select 0 value, \'顶级\' label union SELECT id value, name label FROM asset_category\"\r\n	},\r\n	\"@dict\": {\r\n		\"status\": {\"1\":\"启用\",\"2\":\"禁用\"}\r\n	}\r\n}', '1', 'N', NULL);
INSERT INTO `api_model` VALUES (14, 'asset', 'asset_warehouse', '仓库信息', 'id', 'name', NULL, NULL, '{\n	\"asset_warehouse\": {\n		\"name\": \"仓库名称\",\n		\"address\": \"仓库地址\",\n		\"user_id\": \"负责人\",\n		\"links\": \"联系方式\",\n		\"status\": \"状态\",\n		\"remark\": \"备注\"\n	},\n	\"@ds\": {\n		\"user_id\": \"SELECT user_id value, nick_name label FROM sys_user WHERE del_flag=0\"\n	},\n	\"@dict\": {\n		\"status\": {\"1\":\"启用\"，\"2\":\"禁用\"}\n	}\n}', '1', 'N', NULL);
INSERT INTO `api_model` VALUES (15, 'asset', 'asset_model', '资产型号', 'id', 'name', NULL, NULL, NULL, '1', 'N', NULL);
INSERT INTO `api_model` VALUES (16, 'asset', 'asset_info', '资产信息', 'id', 'num', 'user_id=${userId}', 'dept_id=${orgId}', '{\n	\"asset_info\": {\n		\"cate_id\": \"资产分类\",\n		\"model_id\": \"资产型号\",\n		\"num\": \"编号\",\n		\"house_id\": \"所在仓库\",\n		\"dept_id\": \"所属部门\",\n		\"user_id\": \"负责人\",\n		\"status\": \"状态\",\n		\"price\": \"当前价值\",\n		\"buy_time\": \"购买时间\",\n		\"comment\": \"说明\"\n	},\n	\"@ds\": {\n		\"user_id\": \"SELECT user_id value, nick_name label FROM sys_user\",\n		\"dept_id\": \"SELECT dept_id value, dept_name label FROM sys_dept\",\n		\"house_id\": \"SELECT id value, name label FROM asset_warehouse\",\n		\"cate_id\": \"SELECT id value, name label FROM asset_category\",\n		\"model_id\": \"SELECT id value, name label FROM asset_model\"\n	},\n	\"@dict\": {\n		\"status\": {\"1\":\"闲置\", \"2\":\"在用\", \"3\":\"损坏\",\"4\":\"维修\",\"5\":\"报废\",\"6\":\"已出售\"}\n	}\n}', '1', 'N', NULL);
INSERT INTO `api_model` VALUES (17, 'asset', 'asset_purchase', '资产采购订单', 'id', 'num', NULL, NULL, '{\n  \"asset_purchase\":{\n	\"num\":\"采购编号\",\n	\"reason\":\"采购原由\",\n	\"comment\":\"采购说明\",\n	\"price\":\"总价值\",\n	\"check_status\":\"审核结果\",\n	\"dept_id\":\"申请部门\",\n	\"user_id\":\"申请人\",\n	\"finish_time\":\"完成时间\",\n	\"status\":\"申请状态\"\n  },\n  \"@dict\":{\n    \"status\":{\"1\":\"未提交\",\"2\":\"已提交\",\"3\":\"已完成\",\"4\":\"已撤回\"}\n    \"check_status\":{\"1\":\"待审核\",\"2\":\"审核通过\",\"3\":\"审核不通过\"}\n  },\n  \"@ds\":{\n    \"user_id\": \"@user_select\",\n    \"dept_id\":\"@dept_select\"\n},\n	\"@excel\":{}\n}', '1', 'N', NULL);
INSERT INTO `api_model` VALUES (18, 'asset', 'asset_purchase_detail', '资产采购明细', 'id', NULL, NULL, NULL, '{\n  \"asset_purchase_detail\":{\n	\"id\":\"自增长主键ID\",\n	\"purchase_id\":\"采购单\",\n	\"cate_id\":\"资产分类\",\n	\"model_id\":\"型号\",\n	\"amount\":\"购买数量\",\n	\"nums\":\"入库数量\"\n  },\n	\"@dict\":{},\n	\"@ds\":{},\n	\"@excel\":{}\n}', '1', 'N', NULL);
INSERT INTO `api_model` VALUES (19, 'asset', 'asset_in', '入库记录', 'id', NULL, NULL, NULL, '{\n  \"asset_in\":{\n	\"type\":\"入库类别\",\n	\"cate_id\":\"资产分类\",\n	\"num\":\"资产编号\",\n	\"house_id\":\"仓库\",\n	\"remark\":\"入库说明\",\n	\"user_id\":\"接收人\",\n	\"recv_time\":\"接收时间\"\n  },\n	\"@dict\":{\n\"type\":{\"1\":\"采购\",\"2\":\"归还\"}\n},\n	\"@ds\":{\n\"cate_id\":\"@asset_category_select\",\n\"house_id\":\"@warehouse_select\",\n\"user_id\":\"@user_select\"\n},\n	\"@excel\":{}\n}', '1', 'N', NULL);
INSERT INTO `api_model` VALUES (20, 'asset', 'asset_out', '出库记录', 'id', NULL, NULL, NULL, '{\n  \"asset_out\":{\n	\"id\":\"自增长主键ID\",\n	\"type\":\"出库类别\",\n	\"cate_id\":\"资产分类\",\n	\"asset_id\":\"资产编号\",\n	\"user_id\":\"接收人\",\n	\"remark\":\"出库说明\",\n	\"out_time\":\"出库时间\"\n  },\n\"@dict\":{\n\"type\":{\"1\":\"借用\",\"2\":\"报废\",\"3\":\"出售\"}\n},\n	\"@ds\":{\n\"cate_id\":\"@asset_category_select\",\n\"asset_id\":\"SELECT id value, num label FROM asset_info\",\n\"user_id\":\"@user_select\"\n},\n	\"@excel\":{}\n}', '1', 'N', NULL);
INSERT INTO `api_model` VALUES (21, 'asset', 'asset_maintain', '资产维保', 'id', NULL, NULL, NULL, '{\n  \"asset_maintain\":{\n	\"cate_id\":\"资产分类\",\n	\"asset_id\":\"资产编号\",\n	\"type\":\"维保类型\",\n	\"reason\":\"维保原由\",\n	\"status\":\"维保状态\",\n	\"money\":\"费用\",\n	\"comment\":\"说明\",\n	\"user_id\":\"负责人\",\n	\"apply_time\":\"发起时间\",\n	\"finish_time\":\"完成时间\"\n  },\n\"@dict\":{\n\"type\":{\"1\":\"维修\",\"2\":\"维保\"},\n\"status\":{\"1\":\"未开始\",\"2\":\"进行中\",\"3\":\"已完成\",\"4\":\"无法修复\"}\n},\n	\"@ds\":{\n\"cate_id\":\"@asset_category_select\",\n\"asset_id\":\"SELECT id value, num label FROM asset_info\",\n\"user_id\":\"@user_select\",\n\"dept_id\":\"@dept_select\"\n},\n	\"@excel\":{}\n}', '1', 'N', NULL);
INSERT INTO `api_model` VALUES (22, 'asset', 'asset_use', '资产使用申请', 'id', NULL, NULL, NULL, '{\n  \"asset_use\":{\n	\"id\":\"自增长主键ID\",\n	\"cate_id\":\"分类\",\n	\"model_id\":\"型号\",\n	\"amount\":\"数量\",\n	\"dept_id\":\"申请部门\",\n	\"user_id\":\"申请人\",\n	\"reason\":\"申请原由\",\n	\"remark\":\"申请说明\",\n	\"check_status\":\"审核结果\",\n	\"nums\":\"资产编号\",\n	\"recv_time\":\"接收时间\",\n	\"apply_time\":\"申请时间\",\n	\"apply_status\":\"申请状态\"\n  },\n	\"@dict\":{},\n	\"@ds\":{},\n	\"@excel\":{}\n}', '1', 'N', NULL);
INSERT INTO `api_model` VALUES (23, 'asset', 'asset_stock', '资产盘点', 'id', NULL, NULL, NULL, '{\n  \"asset_stock\":{\n	\"name\":\"代号\",\n	\"asset_count\":\"资产总数\",\n	\"asset_worth\":\"资产总值\",\n	\"scrap_count\":\"报废总数\",\n	\"scrap_worth\":\"报废总值\",\n	\"maintain_count\":\"维保次数\",\n	\"maintain_price\":\"维保费用\",\n	\"purchase_count\":\"采购次数\",\n	\"purchase_worth\":\"采购总值\",\n	\"sale_worth\":\"出售总值\",\n	\"sale_count\":\"出售数量\",\n	\"remark\":\"盘点说明\",\n	\"start_time\":\"开始时间\",\n	\"end_time\":\"结束时间\",\n	\"user_id\":\"负责人\"\n  },\n	\"@dict\":{},\n	\"@ds\":{\n  \"user_id\":\"@user_select\"\n},\n	\"@excel\":{}\n}', '1', 'N', NULL);
INSERT INTO `api_model` VALUES (24, 'asset', 'asset_stock_detail', '资产盘点明细', 'id', 'stock_id,asset_id', NULL, NULL, '{\n  \"asset_stock_detail\":{\n	\"id\":\"编号\",\n	\"cate_name\":\"资产分类\",\n	\"model_name\":\"资产型号\",\n	\"num\":\"资产编码\",\n	\"status\":\"当前状态\",\n	\"user_id\":\"使用人\",\n	\"price1\":\"原价值\",\n	\"price\":\"当前价值\",\n	\"house_id\":\"所在仓库\",\n	\"stock_time\":\"盘点时间\"\n  },\n	\"@dict\":{\"status\": {\"1\":\"闲置\", \"2\":\"在用\", \"3\":\"损坏\",\"4\":\"维修\",\"5\":\"报废\",\"6\":\"已出售\"}},\n	\"@ds\":{\"user_id\":\"@user_select\", \"house_id\":\"@warehouse_select\"},\n	\"@excel\":{}\n}', '1', 'N', NULL);
INSERT INTO `api_model` VALUES (25, 'flow', 'flow_task', '审批任务', 'id', 'num', NULL, NULL, '{\n  \"flow_task\":{\n	\"id\":\"自增长主键ID\",\n	\"type\":\"类型\",\n	\"target_id\":\"业务主键\",\n	\"num\":\"业务编号\",\n	\"title\":\"业务名称\",\n	\"user_id\":\"申请人\",\n	\"user_name\":\"申请人姓名\",\n	\"check_status\":\"审核结果\",\n	\"apply_time\":\"申请时间\",\n	\"apply_reason\":\"申请原因\"\n  },\n	\"@dict\":{},\n	\"@ds\":{},\n	\"@excel\":{}\n}', '1', 'N', NULL);
INSERT INTO `api_model` VALUES (26, 'flow', 'flow_step', '审批过程', 'id', NULL, NULL, NULL, '{\n  \"flow_step\":{\n	\"id\":\"自增长主键ID\",\n	\"task_id\":\"业务主键\",\n	\"check_user_id\":\"审核人\",\n	\"check_user_name\":\"审核人姓名\",\n	\"check_status\":\"审核结果\",\n	\"check_time\":\"审核时间\",\n	\"check_reason\":\"审核不通过原因\",\n	\"pid\":\"上一步骤\",\n	\"ignore_flag\":\"忽略标识\"\n  },\n	\"@dict\":{},\n	\"@ds\":{},\n	\"@excel\":{}\n}', '1', 'N', NULL);
INSERT INTO `api_model` VALUES (27, 'asset', 'asset_sale', '资产出售', 'id', 'asset_id', NULL, NULL, '{\n  \"asset_sale\":{\n	\"cate_id\":\"资产分类\",\n	\"asset_num\":\"资产编号\",\n	\"dept_name\":\"归属部门\",\n	\"user_name\":\"操作人\",\n	\"price\":\"价格\",\n	\"reason\":\"出售原由\",\n	\"sale_time\":\"出售时间\",\n	\"is_out\":\"已出库\"\n  },\n	\"@dict\":{\n\"is_out\":{\"Y\":\"是\",\"N\":\"否\"}},\n	\"@ds\":{cate_id:\"@asset_category_select\"},\n	\"@excel\":{}\n}', '1', 'N', NULL);
INSERT INTO `api_model` VALUES (28, 'sys', 'sys_chart', '报表信息', 'id', '', NULL, NULL, NULL, '1', 'N', NULL);
INSERT INTO `api_model` VALUES (29, 'dev', 'dev_group', '应用分组', 'id', 'code', NULL, NULL, NULL, '1', 'N', NULL);
INSERT INTO `api_model` VALUES (30, 'dev', 'dev_app', '应用信息', 'id', NULL, NULL, NULL, NULL, '1', 'N', NULL);
INSERT INTO `api_model` VALUES (31, 'dev', 'dev_form', '表单字段', 'id', 'app_id,code', NULL, NULL, NULL, '1', 'N', NULL);
INSERT INTO `api_model` VALUES (32, 'dev', 'dev_list', '列表配置', 'id', NULL, NULL, NULL, NULL, '1', 'N', NULL);

-- ----------------------------
-- Table structure for api_sql
-- ----------------------------
DROP TABLE IF EXISTS `api_sql`;
CREATE TABLE `api_sql`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分组',
  `type` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'table' COMMENT '类型:table列表，count计算，chart报表，inner子系统，batch数据批处理',
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '代码',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'SQL脚本',
  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否内置',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '说明',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '内置查询' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of api_sql
-- ----------------------------
INSERT INTO `api_sql` VALUES (1, 'sys', 'table', 'user_select', '用户调用接口', 'SELECT user_id value, nick_name label FROM sys_user WHERE del_flag=0', 'Y', '查询用户信息，生成下拉列表');
INSERT INTO `api_sql` VALUES (2, 'sys', 'table', 'dept_select', '部门列表查询', 'SELECT dept_id value, dept_name label, parent_id FROM sys_dept WHERE del_flag=0', 'Y', '查询部门信息，可生成下拉列表');
INSERT INTO `api_sql` VALUES (3, 'asset', 'table', 'warehouse_select', '查询仓库信息列表', 'select id value, name label from asset_warehouse', 'N', '查询仓库信息,生成下拉列表');
INSERT INTO `api_sql` VALUES (4, 'asset', 'table', 'asset_model_select', '查询资产模型信息列表', 'select id value, name label, cate_id from asset_model', 'N', '查询资产型号信息,生成下拉列表');
INSERT INTO `api_sql` VALUES (5, 'asset', 'table', 'asset_category_select', '查询资产分类信息列表', 'select id value, name label, pid from asset_category', 'N', '查询资产分类信息,生成下拉列表');
INSERT INTO `api_sql` VALUES (6, 'asset', 'count', 'purchase_num', '生成采购单编号', 'select max(num) from(\nselect max(num)+1 num from asset_purchase where num like concat(DATE_FORMAT(now(),\'%Y%m\'), \'%\')\nunion all\nselect concat(DATE_FORMAT(now(),\'%Y%m\'), \'001\') as num\n) t', 'N', '生成采购单编号，年4位月2位+三位序号');
INSERT INTO `api_sql` VALUES (7, 'asset', 'count', 'asset_stock_undone', '统计未盘点资产', 'function exec(data){\n  return \"select count(id) total from asset_stock_detail where stock_id= \" + data.stockId + \" and ( status is null or price is null )\";\n}', 'N', '统计未盘点资产,状态或价值为空');
INSERT INTO `api_sql` VALUES (8, 'asset', 'chart', 'chart_asset_category', '资产分类统计', 'function exec(query){\nvar sql = \"SELECT COUNT(t.id) value, c.name FROM asset_info t JOIN asset_category c ON c.id = t.cate_id\";\nif(query && query.cateId != null){\n  sql += \" WHERE c.pid=\"+query.cateId;\n}\nreturn sql + \" GROUP BY name ORDER BY value\";\n}', 'N', '按小类统计设备数量，可接收参数为上级分类：cateId');
INSERT INTO `api_sql` VALUES (9, 'asset', 'chart', 'asset_status_guage', '资产状态比', 'select count(if(status = \'2\',1, null)) used, count(if(status = \'3\',1, null)) wrong, count(id) total from asset_info where `status` != \'5\' and `status` !=\'6\'', 'N', '资产使用率和完好率');
INSERT INTO `api_sql` VALUES (10, 'asset', 'chart', 'asset_over_view', '资产概况', 'select sum(users) users, sum(assets) assets, sum(purchases) purchases, sum(maintains) maintains from (\nselect count(user_id) users, 0 assets, 0 purchases, 0 maintains from sys_user where del_flag=0\nUNION ALL\nselect 0 users, count(id) assets, 0 purchases, 0 maintains from asset_info where `status` in (\'1\',\'2\',\'3\',\'4\')\nUNION ALL\nselect 0 users, 0 assets, count(id) purchases, 0 maintains from asset_purchase where check_status=\'2\'\nUNION ALL\nselect 0 users, 0 assets, 0 purchases, count(id) maintains from asset_maintain \n)tmp', 'N', '统计用户数、资产数、维保次数和采购次数');
INSERT INTO `api_sql` VALUES (11, 'asset', 'chart', 'asset_dept', '资产分布', 'select count(t.id) as `value`, d.dept_name name from asset_info t \njoin sys_dept d on d.dept_id = t.dept_id\n where t.`status` in (\'1\',\'2\',\'3\',\'4\')\n group by d.dept_name\n \nUNION ALL\n\nselect count(id) as `value`, \'未分配\' name from asset_info \n where (dept_id = 0 || dept_id is null) AND `status` in (\'1\',\'2\',\'3\',\'4\')\n ', 'N', '按部门统计资产数量');
INSERT INTO `api_sql` VALUES (12, 'asset', 'chart', 'asset_stock', '资产盘点统计', 'select * from (select *  from (select asset_count value, DATE_FORMAT(start_time,\'%Y%m\') as name from asset_stock where status =\'4\' order by start_time desc limit 10) t order by name asc) p\nunion all\nselect count(id) value, \'当前\' as  name from asset_info where status in (\'1\',\'2\',\'3\',\'4\')', 'N', '按盘点时间和当前数据统计');
INSERT INTO `api_sql` VALUES (13, 'asset', 'chart', 'asset_status', '资产状态统计', 'select count(t.id) value, d.dict_label name \nfrom sys_dict_data d\nleft join asset_info t  on t.status=d.dict_value\nwhere  d.dict_type=\'assets_status\'\ngroup by name', 'N', '资产状态统计');
INSERT INTO `api_sql` VALUES (14, 'dev', 'table', 'dev_asset_my', '我的资产', 'function exec(params){\n  var sql = \"SELECT t.id, concat(m.name, \' \', t.num) name, c.name cate_name, m.params FROM asset_info t JOIN asset_category c ON c.id = t.cate_id JOIN asset_model m ON m.id = t.model_id WHERE  t.status=\'2\' AND t.user_id=${userId}\";\n  if(params.keyword){\n  sql += \" AND (t.num like concat(\'%\', \'\"+params.keyword+\"\',\'%\') OR m.name like concat(\'%\', \'\"+params.keyword+\"\',\'%\') )\" ;\n} \n  var page = (params && params.page) ? params.page-1 : 0; \n  var limit = (params && params.limit ) ? params.limit : 10; \n if(page == 0){\nsql += \" LIMIT \" + limit;\n }else{\nsql += \" LIMIT \" + (page*limit) + \",\"+limit;\n}\n\n  return sql;\n}', 'N', '移动端个人资产查询');
INSERT INTO `api_sql` VALUES (15, 'dev', 'table', 'dev_model_tree', '移动端模型选择树', 'select distinct c.id + 1000000 value, if(c.pid=0,0, c.pid+ 1000000) pid, c.name text from asset_category c join asset_model m on m.cate_id = c.id\nunion all\nselect id value, cate_id + 1000000 as pid, name text from asset_model', 'N', '移动端模型选择树，包括分类');
INSERT INTO `api_sql` VALUES (16, 'dev', 'table', 'dev_dept_tree', '移动端部门树', 'select dept_id value, parent_id pid, dept_name text from sys_dept', 'N', '移动端部门树');
INSERT INTO `api_sql` VALUES (17, 'dev', 'table', 'dev_user_select', '移动端用户选择', 'select user_id value, nick_name text from sys_user where del_flag=0', 'N', '1');
INSERT INTO `api_sql` VALUES (18, 'dev', 'table', 'dev_warehouse_select', '移动端仓库选择器', 'select id value, name text from asset_warehouse', 'N', '1');
INSERT INTO `api_sql` VALUES (19, 'dev', 'table', 'dev_purchase_my', '我的采购申请', 'function exec(params){\n  var sql = \"SELECT t.id, t.num, t.price price, t.reason, t.comment FROM asset_purchase t WHERE t.user_id=${userId}\";\n  if(params.keyword){\n  sql += \" AND t.num like concat(\'%\', \'\"+params.keyword+\"\',\'%\')\" ;\n} \nsql += \" ORDER BY t.id DESC \"\n  var page = (params && params.page) ? params.page-1 : 0; \n  var limit = (params && params.limit ) ? params.limit : 10; \n if(page == 0){\nsql += \" LIMIT \" + limit;\n }else{\nsql += \" LIMIT \" + (page*limit) + \",\"+limit;\n}\n\n  return sql;\n}', 'N', NULL);
INSERT INTO `api_sql` VALUES (20, 'dev', 'table', 'asset_use_my', '我的使用申请', 'function exec(params){\n  var sql = \"SELECT t.id, m.name model_name, concat(t.amount, \'\') amount, t.reason FROM asset_use t JOIN asset_model m ON m.id = t.model_id WHERE t.user_id=${userId}\";\n  if(params.keyword){\n  sql += \" AND t.nums like concat(\'%\', \'\"+params.keyword+\"\',\'%\')\" ;\n} \nsql += \" ORDER BY t.id DESC \"\n  var page = (params && params.page) ? params.page-1 : 0; \n  var limit = (params && params.limit ) ? params.limit : 10; \n if(page == 0){\nsql += \" LIMIT \" + limit;\n }else{\nsql += \" LIMIT \" + (page*limit) + \",\"+limit;\n}\n\n  return sql;\n}', 'N', NULL);
INSERT INTO `api_sql` VALUES (21, 'dev', 'table', 'dev_asset_num', '根据编码查资产', 'function exec(params){\n  var sql = \"SELECT t.id, t.num code, concat(c.name, \' \', m.name) name from asset_info t JOIN asset_model m on m.id = t.model_id JOIN asset_category c ON m.cate_id = c.id\";\nif(params.code){\nsql += \" WHERE t.num = \'\" + params.code + \"\'\";\n}else if(params.id){\nsql += \" WHERE t.id = \'\" + params.id + \"\'\";\n}else{\nsql += \" WHERE 1 = 0\";\n}\nsql += \" LIMIT 1 \"\n \n  return sql;\n}', 'N', NULL);
INSERT INTO `api_sql` VALUES (22, 'dev', 'table', 'asset_maintain_my', '我的维保申请', 'function exec(params){\n  var sql = \"SELECT t.id, i.num, m.name model_name,t.reason, t.apply_time FROM asset_maintain t JOIN asset_info i ON i.id = t.asset_id JOIN asset_model m ON m.id = i.model_id WHERE t.user_id=${userId}\";\n  if(params.keyword){\n  sql += \" AND t.nums like concat(\'%\', \'\"+params.keyword+\"\',\'%\')\" ;\n} \nsql += \" ORDER BY t.id DESC \"\n  var page = (params && params.page) ? params.page-1 : 0; \n  var limit = (params && params.limit ) ? params.limit : 10; \n if(page == 0){\nsql += \" LIMIT \" + limit;\n }else{\nsql += \" LIMIT \" + (page*limit) + \",\"+limit;\n}\n\n  return sql;\n}', 'N', NULL);
INSERT INTO `api_sql` VALUES (23, 'dev', 'table', 'dev_flow_to_me', '移动端待我审核', 'function exec(params){\n  var sql = \"SELECT s.id, t.title, t.num, t.user_name, t.apply_time, t.apply_reason FROM flow_task t JOIN flow_step s ON t.id = s.task_id WHERE t.check_status=\'1\' AND s.check_user_id=${userId}\";\n  if(params.keyword){\n  sql += \" AND t.num like concat(\'%\', \'\"+params.keyword+\"\',\'%\')\" ;\n} \nsql += \" ORDER BY t.apply_time DESC \"\n  var page = (params && params.page) ? params.page-1 : 0; \n  var limit = (params && params.limit ) ? params.limit : 10; \n if(page == 0){\nsql += \" LIMIT \" + limit;\n }else{\nsql += \" LIMIT \" + (page*limit) + \",\"+limit;\n}\n\n  return sql;\n}', 'N', NULL);

-- ----------------------------
-- Table structure for asset_category
-- ----------------------------
DROP TABLE IF EXISTS `asset_category`;
CREATE TABLE `asset_category`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `pid` int(11) NOT NULL COMMENT '上级',
  `ancestors` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '树形路径',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类名称',
  `nums` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编码规则',
  `list_sort` int(11) NULL DEFAULT 9 COMMENT '排序',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 45 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '资产分类' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of asset_category
-- ----------------------------
INSERT INTO `asset_category` VALUES (35, 0, NULL, 'IT类', NULL, 1, '1');
INSERT INTO `asset_category` VALUES (36, 35, NULL, '服务器', NULL, 1, '1');
INSERT INTO `asset_category` VALUES (37, 0, NULL, '工具类', NULL, 2, '1');
INSERT INTO `asset_category` VALUES (38, 0, NULL, '器械类', NULL, 3, '1');
INSERT INTO `asset_category` VALUES (39, 35, NULL, 'PC', NULL, 2, '1');
INSERT INTO `asset_category` VALUES (40, 35, NULL, '路由器', NULL, 3, '1');
INSERT INTO `asset_category` VALUES (41, 35, NULL, '打印机', NULL, 4, '1');
INSERT INTO `asset_category` VALUES (42, 37, NULL, '饮水机', NULL, 1, '1');
INSERT INTO `asset_category` VALUES (43, 37, NULL, '办公桌', NULL, 2, '1');
INSERT INTO `asset_category` VALUES (44, 38, NULL, '电表', NULL, 1, '1');

-- ----------------------------
-- Table structure for asset_in
-- ----------------------------
DROP TABLE IF EXISTS `asset_in`;
CREATE TABLE `asset_in`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '入库类别',
  `cate_id` int(11) NOT NULL COMMENT '资产分类',
  `num` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资产编号',
  `house_id` int(11) NOT NULL COMMENT '仓库',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '入库说明',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '接收人',
  `recv_time` datetime NULL DEFAULT NULL COMMENT '接收时间',
  `imgs` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收货单',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 68 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '入库记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of asset_in
-- ----------------------------
INSERT INTO `asset_in` VALUES (63, '1', 41, '42314324', 5, '4324324', 1, '2023-09-08 10:14:00', NULL);
INSERT INTO `asset_in` VALUES (64, '1', 41, 'HP453254235', 6, NULL, 1, '2023-09-08 10:16:37', NULL);
INSERT INTO `asset_in` VALUES (67, '2', 36, '2U-2023-9-1', 5, '43214', 1, '2023-09-08 16:10:32', NULL);

-- ----------------------------
-- Table structure for asset_info
-- ----------------------------
DROP TABLE IF EXISTS `asset_info`;
CREATE TABLE `asset_info`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `cate_id` int(11) NOT NULL COMMENT '资产分类',
  `model_id` int(11) NOT NULL COMMENT '型号',
  `num` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资产编号',
  `param1` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '预留字段1',
  `param2` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '预留字段2',
  `param3` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '预留字段3',
  `param4` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '预留字段4',
  `param5` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '预留字段5',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '当前价值',
  `house_id` int(11) NULL DEFAULT NULL COMMENT '仓库',
  `imgs` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图例',
  `is_it` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否IT设备',
  `pid` int(11) NULL DEFAULT NULL COMMENT '上级设备',
  `comment` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资产说明',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '资产状态',
  `dept_id` int(11) NULL DEFAULT NULL COMMENT '所属部门',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '使用人',
  `buy_time` date NULL DEFAULT NULL COMMENT '购买时间',
  `scrap_time` date NULL DEFAULT NULL COMMENT '报废时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1947 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '资产信息' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of asset_info
-- ----------------------------
INSERT INTO `asset_info` VALUES (1924, 42, 1917, '34324', NULL, NULL, NULL, NULL, NULL, 27.20, 5, NULL, 'N', NULL, '', '2', 102, 2, '2023-05-06', NULL);
INSERT INTO `asset_info` VALUES (1925, 36, 1914, '2U-2023-9-1', NULL, NULL, NULL, NULL, NULL, 27200.00, 5, NULL, 'N', NULL, '', '1', 100, NULL, '2023-05-06', NULL);
INSERT INTO `asset_info` VALUES (1926, 41, 1916, '42314324', NULL, NULL, NULL, NULL, NULL, 2600.00, 5, NULL, 'N', NULL, '', '2', NULL, 1, '2023-09-08', NULL);
INSERT INTO `asset_info` VALUES (1927, 41, 1916, 'HP453254235', NULL, NULL, NULL, NULL, NULL, 2600.00, 6, NULL, 'N', NULL, '', '1', NULL, NULL, '2023-09-08', NULL);
INSERT INTO `asset_info` VALUES (1928, 40, 1918, 'TP-100-01', NULL, NULL, NULL, NULL, NULL, 90.00, 6, NULL, 'N', NULL, '', '1', 103, NULL, '2021-08-03', NULL);
INSERT INTO `asset_info` VALUES (1929, 42, 1917, '34324434543', NULL, NULL, NULL, NULL, NULL, 30.00, 5, NULL, 'N', NULL, '', '1', 102, NULL, '2023-05-06', NULL);
INSERT INTO `asset_info` VALUES (1930, 36, 1914, '2U-2023-8-1', NULL, NULL, NULL, NULL, NULL, 27200.00, 5, NULL, 'N', NULL, '', '1', 100, NULL, '2023-05-06', NULL);
INSERT INTO `asset_info` VALUES (1931, 41, 1916, '4231432433', NULL, NULL, NULL, NULL, NULL, 2500.00, 5, NULL, 'N', NULL, '', '1', NULL, NULL, '2023-08-08', NULL);
INSERT INTO `asset_info` VALUES (1932, 41, 1916, 'HP4538774235', NULL, NULL, NULL, NULL, NULL, 2500.00, 6, NULL, 'N', NULL, '', '2', NULL, 1, '2023-09-08', NULL);
INSERT INTO `asset_info` VALUES (1933, 40, 1918, 'TP-100-02', NULL, NULL, NULL, NULL, NULL, 80.00, 6, NULL, 'N', NULL, '', '1', 103, NULL, '2021-08-03', NULL);
INSERT INTO `asset_info` VALUES (1934, 42, 1917, '34324243', NULL, NULL, NULL, NULL, NULL, 27.20, 5, NULL, 'N', NULL, '', '1', 102, NULL, '2023-05-06', NULL);
INSERT INTO `asset_info` VALUES (1935, 41, 1916, 'HP-314324', NULL, NULL, NULL, NULL, NULL, 2800.00, 5, NULL, 'N', NULL, '', '2', NULL, 1, '2023-09-08', NULL);
INSERT INTO `asset_info` VALUES (1936, 41, 1916, 'HP-254235', NULL, NULL, NULL, NULL, NULL, 2800.00, 6, NULL, 'N', NULL, '', '1', NULL, NULL, '2023-09-08', NULL);
INSERT INTO `asset_info` VALUES (1937, 40, 1918, 'TP-100-04', NULL, NULL, NULL, NULL, NULL, 96.00, 6, NULL, 'N', NULL, '', '1', 103, NULL, '2021-08-03', NULL);
INSERT INTO `asset_info` VALUES (1938, 40, 1918, 'TP-100-04', NULL, NULL, NULL, NULL, NULL, 96.00, 6, NULL, 'N', NULL, '', '1', 103, NULL, '2021-08-03', NULL);
INSERT INTO `asset_info` VALUES (1939, 42, 1917, 'W-434543', NULL, NULL, NULL, NULL, NULL, 36.00, 5, NULL, 'N', NULL, '', '1', 102, NULL, '2023-05-06', NULL);
INSERT INTO `asset_info` VALUES (1940, 42, 1917, 'W-434543', NULL, NULL, NULL, NULL, NULL, 36.00, 5, NULL, 'N', NULL, '', '1', 102, NULL, '2023-05-06', NULL);
INSERT INTO `asset_info` VALUES (1941, 41, 1916, 'HP-3332433', NULL, NULL, NULL, NULL, NULL, 2800.00, 5, NULL, 'N', NULL, '', '2', NULL, 1, '2023-08-08', NULL);
INSERT INTO `asset_info` VALUES (1942, 41, 1916, 'HP-3332433', NULL, NULL, NULL, NULL, NULL, 2800.00, 5, NULL, 'N', NULL, '', '2', NULL, 1, '2023-08-08', NULL);
INSERT INTO `asset_info` VALUES (1943, 41, 1916, 'HP-538774', NULL, NULL, NULL, NULL, NULL, 2800.00, 6, NULL, 'N', NULL, '', '1', NULL, NULL, '2023-09-08', NULL);
INSERT INTO `asset_info` VALUES (1944, 41, 1916, 'HP-538774', NULL, NULL, NULL, NULL, NULL, 2800.00, 6, NULL, 'N', NULL, '', '1', NULL, NULL, '2023-09-08', NULL);
INSERT INTO `asset_info` VALUES (1945, 40, 1918, 'TP-100-03', NULL, NULL, NULL, NULL, NULL, 96.00, 6, NULL, 'N', NULL, '', '3', 103, NULL, '2021-08-03', NULL);
INSERT INTO `asset_info` VALUES (1946, 40, 1918, 'TP-100-03', NULL, NULL, NULL, NULL, NULL, 96.00, 6, NULL, 'N', NULL, '', '3', 103, NULL, '2021-08-03', NULL);

-- ----------------------------
-- Table structure for asset_maintain
-- ----------------------------
DROP TABLE IF EXISTS `asset_maintain`;
CREATE TABLE `asset_maintain`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `asset_id` int(11) NOT NULL COMMENT '资产',
  `type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '维保类型',
  `reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '维保原由',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '维保状态',
  `money` decimal(10, 2) NULL DEFAULT NULL COMMENT '费用',
  `comment` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '说明',
  `imgs` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片',
  `user_id` int(11) NOT NULL COMMENT '发起人',
  `apply_time` datetime NULL DEFAULT NULL COMMENT '发起时间',
  `finish_time` datetime NULL DEFAULT NULL COMMENT '完成时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '资产维护' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of asset_maintain
-- ----------------------------
INSERT INTO `asset_maintain` VALUES (1, 1930, '1', '34432', '3', 365.00, '234234', NULL, 1, '2023-09-11 16:10:07', '2023-09-11 16:16:18');
INSERT INTO `asset_maintain` VALUES (2, 1945, '1', '4504', '1', NULL, NULL, '/profile/upload/2023/09/27/941c8c7bfd9af4244a66dc102db3cb6c_20230927171327A001.jpg', 1, '2023-09-27 17:14:41', NULL);

-- ----------------------------
-- Table structure for asset_model
-- ----------------------------
DROP TABLE IF EXISTS `asset_model`;
CREATE TABLE `asset_model`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `cate_id` int(11) NOT NULL COMMENT '资产分类',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '型号名称',
  `factory` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '厂商',
  `param1` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '预留字段1',
  `param2` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '预留字段2',
  `param3` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '预留字段3',
  `param4` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '预留字段4',
  `param5` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '预留字段5',
  `life_time` int(11) NULL DEFAULT NULL COMMENT '使用寿命',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '采购价',
  `params` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数',
  `unit` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '计量单位',
  `price1` decimal(10, 2) NULL DEFAULT NULL COMMENT '市场价',
  `imgs` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图例',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1919 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '资产型号' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of asset_model
-- ----------------------------
INSERT INTO `asset_model` VALUES (1914, 36, '2U-X86', '华为', NULL, NULL, NULL, NULL, NULL, 120, 56400.00, '双核 32G内存', '台', 56400.00, NULL, '1');
INSERT INTO `asset_model` VALUES (1915, 36, '1U ARM', '华为', NULL, NULL, NULL, NULL, NULL, 120, 34000.00, '32G 1T', '台', 36400.00, NULL, '1');
INSERT INTO `asset_model` VALUES (1916, 41, 'HP 1000', 'HP', NULL, NULL, NULL, NULL, NULL, 60, 3500.00, '黑白 双向', '台', 3500.00, NULL, '1');
INSERT INTO `asset_model` VALUES (1917, 42, '立式', '无', NULL, NULL, NULL, NULL, NULL, 24, 50.00, '1米', '台', 50.00, NULL, '1');
INSERT INTO `asset_model` VALUES (1918, 40, 'TPLINK_100', 'TPLINK', NULL, NULL, NULL, NULL, NULL, 100, 190.00, '100M', '台', 210.00, NULL, '1');

-- ----------------------------
-- Table structure for asset_out
-- ----------------------------
DROP TABLE IF EXISTS `asset_out`;
CREATE TABLE `asset_out`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '出库类别',
  `house_id` int(11) NOT NULL COMMENT '仓库',
  `cate_id` int(11) NOT NULL COMMENT '资产分类',
  `asset_id` int(11) NOT NULL COMMENT '资产',
  `num` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资产编号',
  `user_id` int(11) NOT NULL COMMENT '接收人',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '出库说明',
  `imgs` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '确认单',
  `out_time` datetime NULL DEFAULT NULL COMMENT '出库时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 45 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '出库记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of asset_out
-- ----------------------------
INSERT INTO `asset_out` VALUES (41, '1', 6, 36, 1925, NULL, 1, '42134234', NULL, NULL);
INSERT INTO `asset_out` VALUES (42, '1', 6, 36, 1925, NULL, 1, '324324', NULL, '2023-09-08 15:22:57');
INSERT INTO `asset_out` VALUES (43, '1', 5, 41, 1926, NULL, 1, '43523454235', NULL, '2023-09-08 16:21:40');
INSERT INTO `asset_out` VALUES (44, '3', 5, 42, 1929, NULL, 1, '435435', NULL, '2023-09-11 17:07:02');

-- ----------------------------
-- Table structure for asset_purchase
-- ----------------------------
DROP TABLE IF EXISTS `asset_purchase`;
CREATE TABLE `asset_purchase`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `num` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '采购编号',
  `reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '采购原由',
  `comment` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '采购说明',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '总价值',
  `check_status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '审核结果',
  `dept_id` int(11) NOT NULL COMMENT '申请部门',
  `user_id` int(11) NOT NULL COMMENT '申请人',
  `finish_time` datetime NULL DEFAULT NULL COMMENT '完成时间',
  `apply_time` datetime NULL DEFAULT NULL COMMENT '申请时间',
  `apply_status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '申请状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '采购申请' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of asset_purchase
-- ----------------------------
INSERT INTO `asset_purchase` VALUES (33, '2023090617123615', '打印机', '2台3', 7000.00, '2', 103, 1, '2023-09-08 10:52:27', '2023-09-07 17:37:33', '4');
INSERT INTO `asset_purchase` VALUES (34, '2023090811194119', '54325', '543252345', 112800.00, '2', 103, 1, '2023-09-08 11:24:49', '2023-09-08 11:20:51', '3');
INSERT INTO `asset_purchase` VALUES (36, '2023092615272711', 'dad', 'asdasd', 0.00, '1', 103, 1, NULL, '2023-09-27 11:41:18', '2');

-- ----------------------------
-- Table structure for asset_purchase_detail
-- ----------------------------
DROP TABLE IF EXISTS `asset_purchase_detail`;
CREATE TABLE `asset_purchase_detail`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `purchase_id` int(11) NULL DEFAULT NULL COMMENT '采购单',
  `cate_id` int(11) NOT NULL COMMENT '资产分类',
  `model_id` int(11) NOT NULL COMMENT '型号',
  `amount` decimal(10, 3) NOT NULL COMMENT '购买数量',
  `ins` decimal(10, 3) NULL DEFAULT 0.000 COMMENT '入库数量',
  `nums` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '入库编码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 46 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '采购明细' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of asset_purchase_detail
-- ----------------------------
INSERT INTO `asset_purchase_detail` VALUES (43, 33, 41, 1916, 2.000, 2.000, '42314324,HP453254235');
INSERT INTO `asset_purchase_detail` VALUES (44, 34, 36, 1914, 2.000, 0.000, '');
INSERT INTO `asset_purchase_detail` VALUES (45, 36, 0, 1916, 1.000, NULL, NULL);

-- ----------------------------
-- Table structure for asset_sale
-- ----------------------------
DROP TABLE IF EXISTS `asset_sale`;
CREATE TABLE `asset_sale`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `cate_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类',
  `asset_id` int(11) NOT NULL COMMENT '资产',
  `asset_num` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资产编码',
  `dept_id` int(11) NOT NULL COMMENT '归属部门',
  `dept_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门名称',
  `user_id` int(11) NOT NULL COMMENT '操作人',
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '操作人姓名',
  `price` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '价格',
  `reason` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '出售原由',
  `sale_time` datetime NOT NULL COMMENT '出售时间',
  `is_out` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '已出库',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '资产出售' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of asset_sale
-- ----------------------------
INSERT INTO `asset_sale` VALUES (1, '42', 1929, '34324434543', 102, '长沙分公司', 1, '管理员', 45.00, '431241324', '2023-09-04 00:00:00', 'Y');

-- ----------------------------
-- Table structure for asset_stock
-- ----------------------------
DROP TABLE IF EXISTS `asset_stock`;
CREATE TABLE `asset_stock`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '代号',
  `asset_count` int(11) NULL DEFAULT NULL COMMENT '资产总数',
  `asset_worth` decimal(10, 2) NULL DEFAULT NULL COMMENT '资产总值',
  `scrap_count` int(11) NULL DEFAULT NULL COMMENT '报废总数',
  `scrap_worth` int(11) NULL DEFAULT NULL COMMENT '报废总值',
  `sale_worth` decimal(10, 2) NULL DEFAULT NULL COMMENT '出售总值',
  `sale_count` int(11) NULL DEFAULT NULL COMMENT '出售数量',
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '盘点说明',
  `start_time` datetime NOT NULL COMMENT '开始时间',
  `end_time` datetime NOT NULL COMMENT '结束时间',
  `user_id` int(11) NOT NULL COMMENT '负责人',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE COMMENT '不能重复盘点'
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '资产盘点' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of asset_stock
-- ----------------------------
INSERT INTO `asset_stock` VALUES (1, '2023-09-12', 10, 65855.20, 0, NULL, 0.00, NULL, '4324', '2023-09-12 09:48:35', '2023-09-13 10:00:00', 1, '4');
INSERT INTO `asset_stock` VALUES (2, '20230902', 10, 64827.20, 0, NULL, 0.00, NULL, NULL, '2023-08-30 10:00:00', '2023-08-31 10:00:00', 1, '4');

-- ----------------------------
-- Table structure for asset_stock_detail
-- ----------------------------
DROP TABLE IF EXISTS `asset_stock_detail`;
CREATE TABLE `asset_stock_detail`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `stock_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '盘点主键',
  `asset_id` int(11) NOT NULL COMMENT '资产ID',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '当前状态',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '当前价值',
  `cate_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资产分类',
  `model_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资产型号',
  `num` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资产编码',
  `price1` decimal(10, 2) NULL DEFAULT NULL COMMENT '原价值',
  `house_id` int(11) NULL DEFAULT NULL COMMENT '所在仓库',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '使用人',
  `stock_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '盘点时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '资产明细' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of asset_stock_detail
-- ----------------------------
INSERT INTO `asset_stock_detail` VALUES (1, '1', 1924, '1', 27.20, '饮水机', '立式', '34324', 34.00, 5, NULL, '2023-09-12 10:30:00');
INSERT INTO `asset_stock_detail` VALUES (2, '1', 1925, '1', 27200.00, '服务器', '2U-X86', '2U-2023-9-1', 34000.00, 5, NULL, '2023-09-12 10:30:00');
INSERT INTO `asset_stock_detail` VALUES (3, '1', 1926, '2', 2800.00, '打印机', 'HP 1000', '42314324', 3500.00, 5, 1, '2023-09-12 10:30:00');
INSERT INTO `asset_stock_detail` VALUES (4, '1', 1927, '1', 2800.00, '打印机', 'HP 1000', 'HP453254235', 3500.00, 6, NULL, '2023-09-12 10:30:00');
INSERT INTO `asset_stock_detail` VALUES (5, '1', 1928, '1', 96.00, '路由器', 'TPLINK_100', 'TP-100-01', 120.00, 6, NULL, '2023-09-12 10:30:00');
INSERT INTO `asset_stock_detail` VALUES (6, '1', 1929, '1', 36.00, '饮水机', '立式', '34324434543', 45.00, 5, NULL, '2023-09-12 10:30:00');
INSERT INTO `asset_stock_detail` VALUES (7, '1', 1930, '1', 27200.00, '服务器', '2U-X86', '2U-2023-8-1', 34000.00, 5, NULL, '2023-09-12 10:30:00');
INSERT INTO `asset_stock_detail` VALUES (8, '1', 1931, '2', 2800.00, '打印机', 'HP 1000', '4231432433', 3500.00, 5, 1, '2023-09-12 10:30:00');
INSERT INTO `asset_stock_detail` VALUES (9, '1', 1932, '1', 2800.00, '打印机', 'HP 1000', 'HP4538774235', 3500.00, 6, NULL, '2023-09-12 10:30:00');
INSERT INTO `asset_stock_detail` VALUES (10, '1', 1933, '3', 96.00, '路由器', 'TPLINK_100', 'TP-100-02', 120.00, 6, NULL, '2023-09-12 10:30:00');
INSERT INTO `asset_stock_detail` VALUES (16, '2', 1924, '2', 27.20, '饮水机', '立式', '34324', 27.20, 5, 2, '2023-09-14 17:39:36');
INSERT INTO `asset_stock_detail` VALUES (17, '2', 1925, '1', 27200.00, '服务器', '2U-X86', '2U-2023-9-1', 27200.00, 5, NULL, '2023-09-14 17:40:21');
INSERT INTO `asset_stock_detail` VALUES (18, '2', 1926, '2', 2600.00, '打印机', 'HP 1000', '42314324', 2800.00, 5, 1, '2023-09-14 17:40:22');
INSERT INTO `asset_stock_detail` VALUES (19, '2', 1927, '1', 2600.00, '打印机', 'HP 1000', 'HP453254235', 2800.00, 6, NULL, '2023-09-14 17:40:26');
INSERT INTO `asset_stock_detail` VALUES (20, '2', 1928, '1', 90.00, '路由器', 'TPLINK_100', 'TP-100-01', 96.00, 6, NULL, '2023-09-14 17:40:28');
INSERT INTO `asset_stock_detail` VALUES (21, '2', 1929, '1', 30.00, '饮水机', '立式', '34324434543', 36.00, 5, NULL, '2023-09-14 17:40:33');
INSERT INTO `asset_stock_detail` VALUES (22, '2', 1930, '1', 27200.00, '服务器', '2U-X86', '2U-2023-8-1', 27200.00, 5, NULL, '2023-09-14 17:40:34');
INSERT INTO `asset_stock_detail` VALUES (23, '2', 1931, '1', 2500.00, '打印机', 'HP 1000', '4231432433', 2800.00, 5, NULL, '2023-09-14 17:40:37');
INSERT INTO `asset_stock_detail` VALUES (24, '2', 1932, '2', 2500.00, '打印机', 'HP 1000', 'HP4538774235', 2800.00, 6, 1, '2023-09-14 17:40:41');
INSERT INTO `asset_stock_detail` VALUES (25, '2', 1933, '1', 80.00, '路由器', 'TPLINK_100', 'TP-100-02', 96.00, 6, NULL, '2023-09-14 17:40:45');

-- ----------------------------
-- Table structure for asset_use
-- ----------------------------
DROP TABLE IF EXISTS `asset_use`;
CREATE TABLE `asset_use`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `model_id` int(11) NOT NULL COMMENT '型号',
  `amount` tinyint(4) NOT NULL COMMENT '数量',
  `dept_id` int(11) NOT NULL COMMENT '申请部门',
  `user_id` int(11) NOT NULL DEFAULT 0 COMMENT '申请人',
  `reason` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '申请原由',
  `remark` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '申请说明',
  `check_status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '审核结果',
  `nums` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资产编号',
  `recv_time` datetime NULL DEFAULT NULL COMMENT '接收时间',
  `apply_time` datetime NOT NULL COMMENT '申请时间',
  `apply_status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '申请状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '使用申请' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of asset_use
-- ----------------------------
INSERT INTO `asset_use` VALUES (28, 1914, 1, 103, 1, 'rweqrwe', NULL, '2', '2U-2023-9-1', '2023-09-08 15:22:57', '2023-09-08 13:47:32', '4');
INSERT INTO `asset_use` VALUES (29, 1916, 2, 103, 1, '243432434', NULL, '3', NULL, NULL, '2023-09-08 16:19:50', '2');
INSERT INTO `asset_use` VALUES (30, 1916, 2, 103, 1, '3454', NULL, '2', '42314324', '2023-09-08 16:21:40', '2023-09-08 16:21:17', '3');
INSERT INTO `asset_use` VALUES (31, 1914, 4, 103, 1, '24324324', NULL, NULL, NULL, NULL, '2023-09-22 17:45:01', '1');
INSERT INTO `asset_use` VALUES (32, 1917, 43, 103, 1, '234234', NULL, NULL, NULL, NULL, '2023-09-22 17:52:42', '1');
INSERT INTO `asset_use` VALUES (33, 1914, 34, 103, 1, '2341324134', NULL, NULL, NULL, NULL, '2023-09-22 17:54:38', '1');
INSERT INTO `asset_use` VALUES (34, 1000039, 3, 103, 1, '234234324', NULL, NULL, NULL, NULL, '2023-09-26 10:28:51', '1');
INSERT INTO `asset_use` VALUES (35, 1914, 3, 103, 1, '324324', NULL, NULL, NULL, NULL, '2023-09-26 10:29:43', '1');
INSERT INTO `asset_use` VALUES (36, 1916, 1, 103, 1, '2313', NULL, NULL, NULL, NULL, '2023-09-27 11:43:02', '1');

-- ----------------------------
-- Table structure for asset_warehouse
-- ----------------------------
DROP TABLE IF EXISTS `asset_warehouse`;
CREATE TABLE `asset_warehouse`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '仓库名称',
  `address` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '仓库地址',
  `user_id` int(11) NOT NULL COMMENT '负责人',
  `links` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系方式',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '仓库说明',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '仓库' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of asset_warehouse
-- ----------------------------
INSERT INTO `asset_warehouse` VALUES (5, '1号仓库', '28号', 1, '14543565', '1', NULL);
INSERT INTO `asset_warehouse` VALUES (6, '杂物间', '中心二楼', 1, '1', '1', NULL);

-- ----------------------------
-- Table structure for dev_app
-- ----------------------------
DROP TABLE IF EXISTS `dev_app`;
CREATE TABLE `dev_app`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `group_id` int(11) NOT NULL COMMENT '分组',
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '代码',
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类型',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标题',
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `sorts` int(4) NOT NULL DEFAULT 9 COMMENT '排序',
  `permission` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限',
  `api` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接口',
  `links` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '目标连接',
  `show_top` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'Y' COMMENT '桌面展示',
  `title_field` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分组',
  `right_field` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `note_field` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `img_field` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '应用信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dev_app
-- ----------------------------
INSERT INTO `dev_app` VALUES (1, 1, 'asset_my', 'list', '我的资产', 'wallet', 1, NULL, 'dev_asset_my', '/pages/dev/form?app=asset_info&action=view', 'Y', 'name', 'cate_name', 'params', NULL);
INSERT INTO `dev_app` VALUES (2, 4, 'asset_use_add', 'form', '使用申请', 'shop-filled', 2, NULL, 'asset_use', NULL, 'Y', '', '', '', NULL);
INSERT INTO `dev_app` VALUES (3, 1, 'asset_purchase_my', 'page', '我的采购', 'folder-add-filled', 3, NULL, 'dev_purchase_my', '/pages/asset/purchase?action=view', 'Y', 'num', 'price', 'reason', NULL);
INSERT INTO `dev_app` VALUES (4, 4, 'asset_maintain_add', 'form', '维保申请', 'paperclip', 4, NULL, 'asset_maintain', '/pages/dev/page?app=asset_maintrain_my', 'Y', '', '', '', NULL);
INSERT INTO `dev_app` VALUES (5, 1, 'flow_my', 'page', '待我审批', 'auth-filled', 5, NULL, 'dev_flow_to_me', '/pages/dev/flow/check?action=check', 'Y', 'title', 'num', 'apply_reason', NULL);
INSERT INTO `dev_app` VALUES (6, 2, 'purchase_in_add', 'form', '采购入库', 'cart-filled', 1, NULL, 'asset_in', NULL, 'Y', '', '', '', NULL);
INSERT INTO `dev_app` VALUES (7, 2, 'asset_use_revert', 'form', '归还入库', 'redo-filled', 2, NULL, 'asset_in', NULL, 'Y', '', '', '', NULL);
INSERT INTO `dev_app` VALUES (8, 2, 'asset_use_out', 'form', '借用出库', 'undo-filled', 3, NULL, 'asset_out', NULL, 'Y', '', '', '', NULL);
INSERT INTO `dev_app` VALUES (9, 2, 'asset_sale_out', 'form', '出售出库', 'shop', 4, NULL, 'asset_out', NULL, 'Y', '', '', '', NULL);
INSERT INTO `dev_app` VALUES (10, 2, 'asset_scrap_out', 'form', '报废出库', 'trash-filled', 5, NULL, 'asset_out', NULL, 'Y', '', '', '', NULL);
INSERT INTO `dev_app` VALUES (11, 2, 'asset_stock_list', 'list', '资产盘点', 'gift', 5, NULL, 'query_asset_stock_detail', NULL, 'Y', '', '', '', NULL);
INSERT INTO `dev_app` VALUES (12, 3, 'asset_in_query', 'list', '入库查询', 'redo', 1, NULL, 'query_warehoue_in', NULL, 'Y', '', '', '', NULL);
INSERT INTO `dev_app` VALUES (13, 3, 'asset_out_query', 'list', '出库查询', 'undo', 2, NULL, 'query_warehoue_out', NULL, 'Y', '', '', '', NULL);
INSERT INTO `dev_app` VALUES (14, 3, 'asset_purchase_query', 'list', '采购查询', 'cart', 3, NULL, 'query_warehoue_purchase', NULL, 'Y', '', '', '', NULL);
INSERT INTO `dev_app` VALUES (15, 3, 'asset_stock_query', 'list', '盘点查询', 'gift', 5, NULL, 'query_warehouse_stock', '/pages/dev/form?app=asset_info', 'Y', '', '', '', NULL);
INSERT INTO `dev_app` VALUES (16, 3, 'asset_sale_query', 'list', '出售查询', 'shop', 6, NULL, 'query_asset_sale', NULL, 'Y', '', '', '', NULL);
INSERT INTO `dev_app` VALUES (17, 2, 'asset_info', 'form', '资产信息', 'cart-filled', 1, NULL, 'asset_info', NULL, 'N', '', '', '', NULL);
INSERT INTO `dev_app` VALUES (18, 1, 'asset_use', 'page', '我的申请', 'wallet', 1, NULL, 'asset_use_my', '/pages/dev/form?app=use_info&action=view', 'Y', 'model_name', 'amount', 'reason', NULL);
INSERT INTO `dev_app` VALUES (19, 1, 'asset_maintain_my', 'page', '我的维保申请', 'wallet', 1, NULL, 'asset_maintain_my', '/pages/dev/form?app=asset_maintain_add&action=view', 'Y', 'num', 'model_name', 'reason', NULL);

-- ----------------------------
-- Table structure for dev_form
-- ----------------------------
DROP TABLE IF EXISTS `dev_form`;
CREATE TABLE `dev_form`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `app_id` int(11) NOT NULL COMMENT '应用',
  `type` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类型',
  `field` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字段',
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标签',
  `required` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '必填',
  `ds` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据源',
  `mode` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '模式',
  `defaults` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '默认值',
  `sorts` int(4) NOT NULL DEFAULT 9 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '表单字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dev_form
-- ----------------------------
INSERT INTO `dev_form` VALUES (1, 2, 'tree', 'model_id', '资产型号', 'Y', 'dev_model_tree', '1', NULL, 1);
INSERT INTO `dev_form` VALUES (2, 2, 'number', 'amount', '借用数量', 'Y', NULL, '2', NULL, 1);
INSERT INTO `dev_form` VALUES (3, 2, 'textarea', 'reason', '申请原因', 'Y', NULL, '2', NULL, 1);
INSERT INTO `dev_form` VALUES (4, 4, 'scan', 'asset_id', '编号', 'Y', 'dev_asset_num', '1', NULL, 1);
INSERT INTO `dev_form` VALUES (21, 3, 'number', 'amount', '采购数量', 'Y', NULL, '2', NULL, 2);
INSERT INTO `dev_form` VALUES (20, 3, 'tree', 'model_id', '资产型号', 'Y', 'dev_model_tree', '1', NULL, 1);
INSERT INTO `dev_form` VALUES (6, 4, 'images', 'imgs', '图例', 'Y', NULL, '1', NULL, 3);
INSERT INTO `dev_form` VALUES (5, 4, 'textarea', 'reason', '维保原由', 'Y', NULL, '2', NULL, 2);
INSERT INTO `dev_form` VALUES (11, 17, 'text', 'num', '编号', 'Y', NULL, '1', NULL, 2);
INSERT INTO `dev_form` VALUES (12, 17, 'number', 'price', '当前价值 ', 'Y', NULL, '1', NULL, 3);
INSERT INTO `dev_form` VALUES (13, 17, 'select', 'house_id', '所在仓库 ', 'Y', 'dev_warehouse_select', '1', NULL, 4);
INSERT INTO `dev_form` VALUES (14, 17, 'tree', 'dept_id', '归属部门 ', 'Y', 'dev_dept_tree', '1', NULL, 5);
INSERT INTO `dev_form` VALUES (15, 17, 'radio', 'status', '状态', 'Y', 'asset_status', '1', '', 6);
INSERT INTO `dev_form` VALUES (16, 17, 'select', 'user_id', '使用人 ', 'Y', 'dev_user_select', '1', NULL, 7);
INSERT INTO `dev_form` VALUES (17, 17, 'images', 'imgs', '图例', 'Y', NULL, '1', NULL, 8);
INSERT INTO `dev_form` VALUES (10, 17, 'tree', 'model_id', '资产型号', 'Y', 'dev_model_tree', '1', NULL, 1);
INSERT INTO `dev_form` VALUES (22, 3, 'textarea', 'reason', '采购原由', 'Y', NULL, '2', NULL, 3);
INSERT INTO `dev_form` VALUES (23, 3, 'textarea', 'comment', '采购说明', 'N', NULL, '2', NULL, 4);
INSERT INTO `dev_form` VALUES (24, 4, 'radio', 'type', '维保类型', 'Y', 'asset_maintain_type', '1', '1', 2);
INSERT INTO `dev_form` VALUES (25, 4, 'datetime', 'apply_time', '申请时间', 'Y', NULL, '0', NULL, 6);

-- ----------------------------
-- Table structure for dev_group
-- ----------------------------
DROP TABLE IF EXISTS `dev_group`;
CREATE TABLE `dev_group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分组代码',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分组名称',
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分组图标',
  `sorts` int(4) NOT NULL DEFAULT 9 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '应用分组' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dev_group
-- ----------------------------
INSERT INTO `dev_group` VALUES (1, 'person', '个人事务', NULL, 1);
INSERT INTO `dev_group` VALUES (2, 'warehouse', '仓库管理', NULL, 2);
INSERT INTO `dev_group` VALUES (3, 'query', '查询统计', NULL, 3);
INSERT INTO `dev_group` VALUES (4, 'form', '快速入口', NULL, 0);

-- ----------------------------
-- Table structure for flow_step
-- ----------------------------
DROP TABLE IF EXISTS `flow_step`;
CREATE TABLE `flow_step`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `task_id` int(11) NOT NULL COMMENT '业务主键',
  `check_user_id` int(11) NOT NULL COMMENT '审核人',
  `check_user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '审核人姓名',
  `check_status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '审核结果',
  `check_time` datetime NULL DEFAULT NULL COMMENT '审核时间',
  `check_reason` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '审核不通过原因',
  `pid` int(11) NULL DEFAULT 0 COMMENT '上一步骤',
  `ignore_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '忽略标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 71 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '审批步骤' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of flow_step
-- ----------------------------
INSERT INTO `flow_step` VALUES (24, 0, 1, NULL, '1', '2022-03-01 09:58:54', NULL, NULL, '0');
INSERT INTO `flow_step` VALUES (25, 0, 1, NULL, '1', '2022-03-04 14:40:30', NULL, NULL, '0');
INSERT INTO `flow_step` VALUES (27, 0, 3, NULL, '1', '2022-03-07 11:04:31', NULL, NULL, '0');
INSERT INTO `flow_step` VALUES (28, 28, 1, NULL, '1', NULL, NULL, 0, '1');
INSERT INTO `flow_step` VALUES (29, 28, 1, NULL, '2', '2023-09-04 17:51:27', NULL, 0, '0');
INSERT INTO `flow_step` VALUES (30, 29, 1, '管理员', '2', '2023-09-28 15:15:15', NULL, 0, '0');
INSERT INTO `flow_step` VALUES (31, 29, 2, '若依', '1', NULL, NULL, 0, '1');
INSERT INTO `flow_step` VALUES (32, 25, 1, '管理员', '2', '2023-09-05 16:34:53', NULL, 0, '0');
INSERT INTO `flow_step` VALUES (33, 25, 2, '若依', '1', NULL, NULL, 0, '1');
INSERT INTO `flow_step` VALUES (34, 25, 1, '管理员', '3', '2023-09-05 16:35:00', 'fdafdasf', 32, '0');
INSERT INTO `flow_step` VALUES (35, 25, 2, '若依', '1', NULL, NULL, 32, '1');
INSERT INTO `flow_step` VALUES (36, 24, 1, '管理员', '3', '2023-09-05 16:56:12', '5435435', 0, '0');
INSERT INTO `flow_step` VALUES (37, 24, 2, '若依', '1', NULL, NULL, 0, '1');
INSERT INTO `flow_step` VALUES (38, 30, 1, '管理员', '1', NULL, NULL, 0, '1');
INSERT INTO `flow_step` VALUES (39, 30, 3, '一号仓管', '1', NULL, NULL, 0, '1');
INSERT INTO `flow_step` VALUES (40, 30, 1, '管理员', '1', NULL, NULL, 0, '1');
INSERT INTO `flow_step` VALUES (41, 30, 3, '一号仓管', '1', NULL, NULL, 0, '1');
INSERT INTO `flow_step` VALUES (42, 30, 1, '管理员', '3', '2023-09-06 17:27:51', '999', 0, '0');
INSERT INTO `flow_step` VALUES (43, 30, 3, '一号仓管', '1', NULL, NULL, 0, '1');
INSERT INTO `flow_step` VALUES (44, 30, 1, '管理员', '2', '2023-09-07 17:14:12', NULL, 0, '0');
INSERT INTO `flow_step` VALUES (45, 30, 3, '一号仓管', '1', NULL, NULL, 0, '1');
INSERT INTO `flow_step` VALUES (46, 30, 1, '管理员', '3', '2023-09-07 17:34:40', NULL, 0, '0');
INSERT INTO `flow_step` VALUES (47, 30, 3, '一号仓管', '1', NULL, NULL, 0, '1');
INSERT INTO `flow_step` VALUES (48, 30, 1, '管理员', '3', '2023-09-07 17:35:18', NULL, 0, '0');
INSERT INTO `flow_step` VALUES (49, 30, 3, '一号仓管', '1', NULL, NULL, 0, '1');
INSERT INTO `flow_step` VALUES (50, 30, 1, '管理员', '1', NULL, NULL, 0, '1');
INSERT INTO `flow_step` VALUES (51, 30, 3, '一号仓管', '1', NULL, NULL, 0, '1');
INSERT INTO `flow_step` VALUES (52, 30, 1, '管理员', '2', '2023-09-07 17:41:50', '333', 0, '0');
INSERT INTO `flow_step` VALUES (53, 30, 2, '若依', '1', NULL, NULL, 0, '1');
INSERT INTO `flow_step` VALUES (54, 30, 3, '一号仓管', '1', NULL, NULL, 0, '1');
INSERT INTO `flow_step` VALUES (55, 31, 1, '管理员', '1', NULL, NULL, 0, '1');
INSERT INTO `flow_step` VALUES (56, 31, 3, '一号仓管', '1', NULL, NULL, 0, '1');
INSERT INTO `flow_step` VALUES (57, 31, 1, '管理员', '2', '2023-09-08 11:21:13', NULL, 0, '0');
INSERT INTO `flow_step` VALUES (58, 31, 1, '管理员', '2', '2023-09-08 11:21:38', NULL, 57, '0');
INSERT INTO `flow_step` VALUES (59, 32, 1, '管理员', '2', '2023-09-08 13:47:57', NULL, 0, '0');
INSERT INTO `flow_step` VALUES (60, 32, 3, '一号仓管', '1', NULL, NULL, 0, '1');
INSERT INTO `flow_step` VALUES (61, 33, 1, '管理员', '2', '2023-09-08 13:47:44', NULL, 0, '0');
INSERT INTO `flow_step` VALUES (62, 34, 1, '管理员', '3', '2023-09-08 16:20:34', '42134324', 0, '0');
INSERT INTO `flow_step` VALUES (63, 34, 2, '若依', '1', NULL, NULL, 0, '1');
INSERT INTO `flow_step` VALUES (64, 35, 1, '管理员', '2', '2023-09-08 16:21:24', NULL, 0, '0');
INSERT INTO `flow_step` VALUES (65, 36, 1, '管理员', '1', NULL, NULL, 0, NULL);
INSERT INTO `flow_step` VALUES (66, 38, 2, '若依', '1', NULL, NULL, 0, NULL);
INSERT INTO `flow_step` VALUES (67, 39, 2, '若依', '1', NULL, NULL, 0, NULL);
INSERT INTO `flow_step` VALUES (68, 40, 2, '若依', '1', NULL, NULL, 0, NULL);
INSERT INTO `flow_step` VALUES (69, 29, 1, '管理员', '2', '2023-09-28 15:15:01', NULL, 30, NULL);
INSERT INTO `flow_step` VALUES (70, 29, 1, '管理员', '1', NULL, NULL, 30, '1');

-- ----------------------------
-- Table structure for flow_task
-- ----------------------------
DROP TABLE IF EXISTS `flow_task`;
CREATE TABLE `flow_task`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类型',
  `target_id` int(11) NOT NULL COMMENT '业务主键',
  `num` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '业务编号',
  `title` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '业务名称',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '申请人',
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '申请人姓名',
  `check_status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '审核结果',
  `apply_time` datetime NULL DEFAULT NULL COMMENT '申请时间',
  `apply_reason` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '申请原因',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '审批任务' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of flow_task
-- ----------------------------
INSERT INTO `flow_task` VALUES (24, 'asset_purchase', 24, '34124321', '采购订单', 1, '管理员', '3', '2023-09-05 16:56:04', '53245');
INSERT INTO `flow_task` VALUES (25, 'asset_purchase', 25, '12421341324', '采购订单', 1, '管理员', '3', '2023-09-05 16:31:58', '423141234');
INSERT INTO `flow_task` VALUES (27, '', 0, '3214231', '234234', 3, NULL, '1', '2022-03-07 11:04:31', NULL);
INSERT INTO `flow_task` VALUES (28, 'asset_purchase', 31, '20230702', '采购订单', 1, '管理员', '2', '2023-09-04 17:05:18', NULL);
INSERT INTO `flow_task` VALUES (29, 'asset_purchase', 27, '202308011', '采购订单', 1, '管理员', '2', '2023-09-05 15:22:49', '1234324342');
INSERT INTO `flow_task` VALUES (30, 'asset_purchase', 33, '2023090617123615', '采购订单', 1, '管理员', '2', '2023-09-07 17:37:27', '打印机');
INSERT INTO `flow_task` VALUES (31, 'asset_purchase', 34, '2023090811194119', '采购订单', 1, '管理员', '2', '2023-09-08 11:20:51', '54325');
INSERT INTO `flow_task` VALUES (32, 'asset_use', 28, '2023090813463322', '使用申请', 1, '管理员', '2', '2023-09-08 13:46:40', 'rweqrwe');
INSERT INTO `flow_task` VALUES (33, 'asset_use', 28, '2023090813472727', '使用申请', 1, '管理员', '2', '2023-09-08 13:47:32', 'rweqrwe');
INSERT INTO `flow_task` VALUES (34, 'asset_use', 29, '2023090816194523', '使用申请', 1, '管理员', '3', '2023-09-08 16:19:50', '243432434');
INSERT INTO `flow_task` VALUES (40, 'asset_purchase', 36, '2023092615272711', '采购订单', 1, '管理员', '1', '2023-09-27 11:41:18', 'dad');

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (1, 'asset_model', '资产型号', NULL, NULL, 'AssetModel', 'crud', 'com.ruoyi.assets', 'assets', 'model', '资产型号', 'ruoyi', '0', '/', '{\"parentMenuId\":2025}', 'admin', '2023-08-23 11:47:30', '', '2023-08-23 17:17:07', NULL);
INSERT INTO `gen_table` VALUES (2, 'asset_category', '资产分类', '', '', 'AssetCategory', 'tree', 'com.ruoyi.assets', 'assets', 'category', '资产分类', 'ruoyi', '0', '/', '{\"treeCode\":\"id\",\"treeName\":\"name\",\"treeParentCode\":\"pid\",\"parentMenuId\":\"2025\"}', 'admin', '2023-08-23 11:47:59', '', '2023-08-23 13:50:49', NULL);
INSERT INTO `gen_table` VALUES (3, 'asset_info', '资产信息', NULL, NULL, 'AssetInfo', 'crud', 'com.ruoyi.asset', 'asset', 'info', '资产信息', 'ruoyi', '0', '/', '{\"parentMenuId\":\"2025\"}', 'admin', '2023-08-23 11:47:59', '', '2023-08-28 17:33:51', NULL);
INSERT INTO `gen_table` VALUES (4, 'asset_warehouse', '仓库', NULL, NULL, 'AssetWarehouse', 'crud', 'com.ruoyi.asset', 'asset', 'warehouse', '仓库', 'ruoyi', '0', '/', '{\"parentMenuId\":\"2025\"}', 'admin', '2023-08-23 14:57:34', '', '2023-08-23 15:00:42', NULL);
INSERT INTO `gen_table` VALUES (5, 'asset_purchase', '采购申请', 'asset_purchase_detail', 'purchase_id', 'AssetPurchase', 'sub', 'com.ruoyi.asset', 'asset', 'purchase', '采购申请', 'ruoyi', '0', '/', '{\"parentMenuId\":\"2025\"}', 'admin', '2023-08-29 16:10:07', '', '2023-08-29 16:12:32', NULL);
INSERT INTO `gen_table` VALUES (6, 'asset_purchase_detail', '采购明细', NULL, NULL, 'AssetPurchaseDetail', 'crud', 'com.ruoyi.asset', 'asset', 'detail', '采购明细', 'ruoyi', '0', '/', '{\"parentMenuId\":2025}', 'admin', '2023-08-29 16:10:08', '', '2023-08-29 16:11:55', NULL);
INSERT INTO `gen_table` VALUES (7, 'asset_in', '入库记录', NULL, NULL, 'AssetIn', 'crud', 'com.ruoyi.asset', 'asset', 'in', '入库记录', 'ruoyi', '0', '/', '{\"parentMenuId\":2057}', 'admin', '2023-08-31 10:43:41', '', '2023-08-31 10:47:45', NULL);
INSERT INTO `gen_table` VALUES (8, 'asset_maintain', '资产维护', NULL, NULL, 'AssetMaintain', 'crud', 'com.ruoyi.asset', 'asset', 'maintain', '资产维保', 'ruoyi', '0', '/', '{\"parentMenuId\":\"2025\"}', 'admin', '2023-08-31 10:43:41', '', '2023-08-31 17:45:32', NULL);
INSERT INTO `gen_table` VALUES (9, 'asset_out', '出库记录', NULL, NULL, 'AssetOut', 'crud', 'com.ruoyi.asset', 'asset', 'out', '出库记录', 'ruoyi', '0', '/', '{\"parentMenuId\":2057}', 'admin', '2023-08-31 10:43:41', '', '2023-08-31 14:48:52', NULL);
INSERT INTO `gen_table` VALUES (10, 'asset_stock', '资产盘点', 'asset_stock_detail', 'stock_id', 'AssetStock', 'sub', 'com.ruoyi.asset', 'asset', 'stock', '资产盘点', 'ruoyi', '0', '/', '{\"parentMenuId\":\"2057\"}', 'admin', '2023-09-01 10:55:47', '', '2023-09-01 15:58:50', NULL);
INSERT INTO `gen_table` VALUES (11, 'asset_stock_detail', '资产明细', NULL, NULL, 'AssetStockDetail', 'crud', 'com.ruoyi.asset', 'asset', 'detail', '盘点明细', 'ruoyi', '0', '/', '{}', 'admin', '2023-09-01 10:55:47', '', '2023-09-01 11:38:20', NULL);
INSERT INTO `gen_table` VALUES (12, 'asset_use', '使用申请', NULL, NULL, 'AssetUse', 'crud', 'com.ruoyi.asset', 'asset', 'use', '使用申请', 'ruoyi', '0', '/', '{\"parentMenuId\":2025}', 'admin', '2023-09-01 10:56:07', '', '2023-09-01 11:00:31', NULL);
INSERT INTO `gen_table` VALUES (13, 'flow_step', '审批步骤', NULL, NULL, 'FlowStep', 'crud', 'com.ruoyi.flow', 'flow', 'step', '审批步骤', 'ruoyi', '0', '/', '{\"parentMenuId\":2026}', 'admin', '2023-09-04 10:57:30', '', '2023-09-04 11:01:17', NULL);
INSERT INTO `gen_table` VALUES (14, 'flow_task', '审批任务', NULL, NULL, 'FlowTask', 'crud', 'com.ruoyi.flow', 'flow', 'task', '审批任务', 'ruoyi', '0', '/', '{\"parentMenuId\":2026}', 'admin', '2023-09-04 10:57:30', '', '2023-09-04 10:58:40', NULL);
INSERT INTO `gen_table` VALUES (15, 'asset_sale', '资产出售', NULL, NULL, 'AssetSale', 'crud', 'com.ruoyi.asset', 'asset', 'sale', '资产出售', 'ruoyi', '0', '/', '{\"parentMenuId\":2025}', 'admin', '2023-09-08 11:32:55', '', '2023-09-08 11:33:54', NULL);
INSERT INTO `gen_table` VALUES (16, 'sys_chart', '自定义报表', NULL, NULL, 'SysChart', 'crud', 'com.ruoyi.system', 'system', 'chart', '自定义报', 'ruoyi', '0', '/', '{\"parentMenuId\":\"3\"}', 'admin', '2023-09-12 10:35:02', '', '2023-09-12 10:51:59', NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` bigint(20) NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 177 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (1, 1, 'id', '自增长主键ID', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-08-23 11:47:30', '', '2023-08-23 17:17:07');
INSERT INTO `gen_table_column` VALUES (2, 1, 'cate_id', '资产分类', 'int(11)', 'Long', 'cateId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2023-08-23 11:47:30', '', '2023-08-23 17:17:07');
INSERT INTO `gen_table_column` VALUES (3, 1, 'name', '型号名称', 'varchar(50)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', '2023-08-23 11:47:30', '', '2023-08-23 17:17:07');
INSERT INTO `gen_table_column` VALUES (4, 1, 'factory', '厂商', 'varchar(50)', 'String', 'factory', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2023-08-23 11:47:30', '', '2023-08-23 17:17:07');
INSERT INTO `gen_table_column` VALUES (5, 1, 'param1', '预留字段1', 'varchar(200)', 'String', 'param1', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2023-08-23 11:47:30', '', '2023-08-23 17:17:07');
INSERT INTO `gen_table_column` VALUES (6, 1, 'param2', '预留字段2', 'varchar(200)', 'String', 'param2', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2023-08-23 11:47:30', '', '2023-08-23 17:17:07');
INSERT INTO `gen_table_column` VALUES (7, 1, 'param3', '预留字段3', 'varchar(255)', 'String', 'param3', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2023-08-23 11:47:30', '', '2023-08-23 17:17:07');
INSERT INTO `gen_table_column` VALUES (8, 1, 'param4', '预留字段4', 'varchar(255)', 'String', 'param4', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2023-08-23 11:47:30', '', '2023-08-23 17:17:07');
INSERT INTO `gen_table_column` VALUES (9, 1, 'param5', '预留字段5', 'varchar(255)', 'String', 'param5', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2023-08-23 11:47:30', '', '2023-08-23 17:17:07');
INSERT INTO `gen_table_column` VALUES (10, 1, 'life_time', '使用寿命', 'int(11)', 'Long', 'lifeTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2023-08-23 11:47:30', '', '2023-08-23 17:17:07');
INSERT INTO `gen_table_column` VALUES (11, 1, 'price2', '采购价', 'decimal(10,2)', 'BigDecimal', 'price2', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2023-08-23 11:47:30', '', '2023-08-23 17:17:07');
INSERT INTO `gen_table_column` VALUES (12, 1, 'params', '参数', 'varchar(255)', 'String', 'params', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 12, 'admin', '2023-08-23 11:47:30', '', '2023-08-23 17:17:07');
INSERT INTO `gen_table_column` VALUES (13, 1, 'unit', '计量单位', 'varchar(10)', 'String', 'unit', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 13, 'admin', '2023-08-23 11:47:30', '', '2023-08-23 17:17:07');
INSERT INTO `gen_table_column` VALUES (14, 1, 'price1', '市场价', 'decimal(10,2)', 'BigDecimal', 'price1', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 14, 'admin', '2023-08-23 11:47:30', '', '2023-08-23 17:17:07');
INSERT INTO `gen_table_column` VALUES (15, 1, 'imgs', '图例', 'varchar(255)', 'String', 'imgs', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 15, 'admin', '2023-08-23 11:47:30', '', '2023-08-23 17:17:07');
INSERT INTO `gen_table_column` VALUES (16, 1, 'status', '状态', 'char(1)', 'String', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 16, 'admin', '2023-08-23 11:47:30', '', '2023-08-23 17:17:07');
INSERT INTO `gen_table_column` VALUES (17, 2, 'id', '自增长主键ID', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-08-23 11:47:59', '', '2023-08-23 13:50:49');
INSERT INTO `gen_table_column` VALUES (19, 2, 'pid', '上级', 'int(11)', 'Long', 'pid', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-08-23 11:47:59', '', '2023-08-23 13:50:49');
INSERT INTO `gen_table_column` VALUES (20, 2, 'type', '分类类别', 'char(1)', 'String', 'type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'assets_category_type', 4, 'admin', '2023-08-23 11:47:59', '', '2023-08-23 13:50:49');
INSERT INTO `gen_table_column` VALUES (21, 2, 'nums', '编码规则', 'varchar(100)', 'String', 'nums', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2023-08-23 11:47:59', '', '2023-08-23 13:50:49');
INSERT INTO `gen_table_column` VALUES (22, 2, 'list_sort', '排序', 'int(11)', 'Long', 'listSort', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2023-08-23 11:47:59', '', '2023-08-23 13:50:49');
INSERT INTO `gen_table_column` VALUES (23, 2, 'status', '状态', 'char(1)', 'String', 'status', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'radio', 'sys_common_status', 7, 'admin', '2023-08-23 11:47:59', '', '2023-08-23 13:50:49');
INSERT INTO `gen_table_column` VALUES (24, 3, 'id', '自增长主键ID', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-08-23 11:47:59', '', '2023-08-28 17:33:56');
INSERT INTO `gen_table_column` VALUES (25, 3, 'cate_id', '资产分类', 'int(11)', 'Long', 'cateId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2023-08-23 11:47:59', '', '2023-08-28 17:33:56');
INSERT INTO `gen_table_column` VALUES (26, 3, 'model_id', '型号', 'int(11)', 'Long', 'modelId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-08-23 11:47:59', '', '2023-08-28 17:33:56');
INSERT INTO `gen_table_column` VALUES (27, 3, 'num', '资产编号', 'varchar(200)', 'String', 'num', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 4, 'admin', '2023-08-23 11:47:59', '', '2023-08-28 17:33:56');
INSERT INTO `gen_table_column` VALUES (28, 3, 'param1', '预留字段1', 'varchar(200)', 'String', 'param1', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2023-08-23 11:47:59', '', '2023-08-28 17:33:56');
INSERT INTO `gen_table_column` VALUES (29, 3, 'param2', '预留字段2', 'varchar(200)', 'String', 'param2', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2023-08-23 11:47:59', '', '2023-08-28 17:33:56');
INSERT INTO `gen_table_column` VALUES (30, 3, 'param3', '预留字段3', 'varchar(255)', 'String', 'param3', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2023-08-23 11:47:59', '', '2023-08-28 17:33:56');
INSERT INTO `gen_table_column` VALUES (31, 3, 'param4', '预留字段4', 'varchar(255)', 'String', 'param4', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2023-08-23 11:47:59', '', '2023-08-28 17:33:56');
INSERT INTO `gen_table_column` VALUES (32, 3, 'param5', '预留字段5', 'varchar(255)', 'String', 'param5', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2023-08-23 11:47:59', '', '2023-08-28 17:33:56');
INSERT INTO `gen_table_column` VALUES (33, 3, 'price', '当前价值', 'decimal(10,2)', 'BigDecimal', 'price', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2023-08-23 11:47:59', '', '2023-08-28 17:33:56');
INSERT INTO `gen_table_column` VALUES (34, 3, 'house_id', '仓库', 'int(11)', 'Long', 'houseId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2023-08-23 11:47:59', '', '2023-08-28 17:33:56');
INSERT INTO `gen_table_column` VALUES (35, 3, 'imgs', '图例', 'varchar(255)', 'String', 'imgs', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 12, 'admin', '2023-08-23 11:47:59', '', '2023-08-28 17:33:56');
INSERT INTO `gen_table_column` VALUES (36, 3, 'is_it', '是否IT设备', 'char(1)', 'String', 'isIt', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', 'sys_yes_no', 13, 'admin', '2023-08-23 11:47:59', '', '2023-08-28 17:33:56');
INSERT INTO `gen_table_column` VALUES (38, 3, 'pid', '上级设备', 'int(11)', 'Long', 'pid', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 14, 'admin', '2023-08-23 11:47:59', '', '2023-08-28 17:33:56');
INSERT INTO `gen_table_column` VALUES (39, 3, 'comment', '资产说明', 'varchar(200)', 'String', 'comment', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 15, 'admin', '2023-08-23 11:47:59', '', '2023-08-28 17:33:56');
INSERT INTO `gen_table_column` VALUES (40, 3, 'status', '资产状态', 'char(1)', 'String', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'assets_status', 16, 'admin', '2023-08-23 11:47:59', '', '2023-08-28 17:33:56');
INSERT INTO `gen_table_column` VALUES (41, 3, 'dept_id', '所属部门', 'int(11)', 'Long', 'deptId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 17, 'admin', '2023-08-23 11:47:59', '', '2023-08-28 17:33:56');
INSERT INTO `gen_table_column` VALUES (42, 3, 'user_id', '使用人', 'int(11)', 'Long', 'userId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 18, 'admin', '2023-08-23 11:47:59', '', '2023-08-28 17:33:56');
INSERT INTO `gen_table_column` VALUES (43, 3, 'scrap_time', '报废时间', 'date', 'Date', 'scrapTime', '0', '0', NULL, '1', '1', '1', '1', 'BETWEEN', 'datetime', '', 20, 'admin', '2023-08-23 11:47:59', '', '2023-08-28 17:33:56');
INSERT INTO `gen_table_column` VALUES (44, 2, 'name', '分类名称', 'varchar(50)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 2, '', '2023-08-23 13:50:42', '', '2023-08-23 13:50:49');
INSERT INTO `gen_table_column` VALUES (45, 4, 'id', '自增长主键ID', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-08-23 14:57:34', '', '2023-08-23 15:01:12');
INSERT INTO `gen_table_column` VALUES (46, 4, 'name', '仓库名称', 'varchar(50)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2023-08-23 14:57:34', '', '2023-08-23 15:01:12');
INSERT INTO `gen_table_column` VALUES (47, 4, 'address', '仓库地址', 'varchar(200)', 'String', 'address', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-08-23 14:57:34', '', '2023-08-23 15:01:12');
INSERT INTO `gen_table_column` VALUES (48, 4, 'user_id', '负责人', 'int(11)', 'Long', 'userId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2023-08-23 14:57:34', '', '2023-08-23 15:01:12');
INSERT INTO `gen_table_column` VALUES (49, 4, 'links', '联系方式', 'varchar(50)', 'String', 'links', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2023-08-23 14:57:34', '', '2023-08-23 15:01:12');
INSERT INTO `gen_table_column` VALUES (50, 4, 'status', '状态', 'char(1)', 'String', 'status', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'radio', 'ext_status', 6, 'admin', '2023-08-23 14:57:34', '', '2023-08-23 15:01:12');
INSERT INTO `gen_table_column` VALUES (51, 4, 'remark', '仓库说明', 'varchar(255)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 7, 'admin', '2023-08-23 14:57:34', '', '2023-08-23 15:01:12');
INSERT INTO `gen_table_column` VALUES (52, 3, 'buy_time', '购买时间', 'date', 'Date', 'buyTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 19, '', '2023-08-28 17:33:56', '', NULL);
INSERT INTO `gen_table_column` VALUES (53, 5, 'id', '自增长主键ID', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-08-29 16:10:07', '', '2023-08-29 16:12:32');
INSERT INTO `gen_table_column` VALUES (54, 5, 'num', '采购编号', 'varchar(100)', 'String', 'num', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2023-08-29 16:10:07', '', '2023-08-29 16:12:32');
INSERT INTO `gen_table_column` VALUES (55, 5, 'reason', '采购原由', 'varchar(255)', 'String', 'reason', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-08-29 16:10:07', '', '2023-08-29 16:12:32');
INSERT INTO `gen_table_column` VALUES (56, 5, 'comment', '采购说明', 'varchar(255)', 'String', 'comment', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2023-08-29 16:10:08', '', '2023-08-29 16:12:32');
INSERT INTO `gen_table_column` VALUES (57, 5, 'price', '总价值', 'decimal(10,2)', 'BigDecimal', 'price', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2023-08-29 16:10:08', '', '2023-08-29 16:12:32');
INSERT INTO `gen_table_column` VALUES (58, 5, 'check_status', '审核结果', 'char(1)', 'String', 'checkStatus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'flow_check_status', 6, 'admin', '2023-08-29 16:10:08', '', '2023-08-29 16:12:32');
INSERT INTO `gen_table_column` VALUES (59, 5, 'dept_id', '申请部门', 'int(11)', 'Long', 'deptId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2023-08-29 16:10:08', '', '2023-08-29 16:12:32');
INSERT INTO `gen_table_column` VALUES (60, 5, 'user_id', '申请人', 'int(11)', 'Long', 'userId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2023-08-29 16:10:08', '', '2023-08-29 16:12:32');
INSERT INTO `gen_table_column` VALUES (61, 5, 'finish_time', '完成时间', 'datetime', 'Date', 'finishTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 9, 'admin', '2023-08-29 16:10:08', '', '2023-08-29 16:12:32');
INSERT INTO `gen_table_column` VALUES (62, 5, 'status', '采购状态', 'char(1)', 'String', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'flow_apply_status', 10, 'admin', '2023-08-29 16:10:08', '', '2023-08-29 16:12:32');
INSERT INTO `gen_table_column` VALUES (63, 6, 'id', '自增长主键ID', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-08-29 16:10:08', '', '2023-08-29 16:11:55');
INSERT INTO `gen_table_column` VALUES (64, 6, 'purchase_id', '采购单', 'int(11)', 'Long', 'purchaseId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2023-08-29 16:10:08', '', '2023-08-29 16:11:55');
INSERT INTO `gen_table_column` VALUES (65, 6, 'cate_id', '资产分类', 'int(11)', 'Long', 'cateId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-08-29 16:10:08', '', '2023-08-29 16:11:55');
INSERT INTO `gen_table_column` VALUES (66, 6, 'model_id', '型号', 'int(11)', 'Long', 'modelId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2023-08-29 16:10:08', '', '2023-08-29 16:11:55');
INSERT INTO `gen_table_column` VALUES (67, 6, 'amount', '购买数量', 'decimal(10,3)', 'BigDecimal', 'amount', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2023-08-29 16:10:08', '', '2023-08-29 16:11:55');
INSERT INTO `gen_table_column` VALUES (68, 6, 'nums', '入库数量', 'decimal(10,3)', 'BigDecimal', 'nums', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2023-08-29 16:10:08', '', '2023-08-29 16:11:55');
INSERT INTO `gen_table_column` VALUES (69, 7, 'id', '自增长主键ID', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-08-31 10:43:41', '', '2023-08-31 10:47:45');
INSERT INTO `gen_table_column` VALUES (70, 7, 'type', '原由：采购、归还', 'int(11)', 'String', 'type', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', 'asset_in_type', 2, 'admin', '2023-08-31 10:43:41', '', '2023-08-31 10:47:45');
INSERT INTO `gen_table_column` VALUES (71, 7, 'cate_id', '资产分类', 'int(11)', 'Long', 'cateId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-08-31 10:43:41', '', '2023-08-31 10:47:45');
INSERT INTO `gen_table_column` VALUES (72, 7, 'asset_id', '资产ID', 'int(11)', 'Long', 'assetId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2023-08-31 10:43:41', '', '2023-08-31 10:47:45');
INSERT INTO `gen_table_column` VALUES (73, 7, 'house_id', '仓库', 'int(11)', 'Long', 'houseId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2023-08-31 10:43:41', '', '2023-08-31 10:47:45');
INSERT INTO `gen_table_column` VALUES (74, 7, 'remark', '入库说明', 'varchar(255)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 6, 'admin', '2023-08-31 10:43:41', '', '2023-08-31 10:47:45');
INSERT INTO `gen_table_column` VALUES (75, 7, 'user_id', '接收人', 'int(11)', 'Long', 'userId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2023-08-31 10:43:41', '', '2023-08-31 10:47:45');
INSERT INTO `gen_table_column` VALUES (76, 7, 'recv_time', '接收时间', 'datetime', 'Date', 'recvTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 8, 'admin', '2023-08-31 10:43:41', '', '2023-08-31 10:47:45');
INSERT INTO `gen_table_column` VALUES (77, 7, 'imgs', '收货单', 'varchar(255)', 'String', 'imgs', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2023-08-31 10:43:41', '', '2023-08-31 10:47:45');
INSERT INTO `gen_table_column` VALUES (78, 8, 'id', '自增长主键ID', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-08-31 10:43:41', '', '2023-08-31 17:45:32');
INSERT INTO `gen_table_column` VALUES (79, 8, 'cate_id', '资产分类', 'int(11)', 'Long', 'cateId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2023-08-31 10:43:41', '', '2023-08-31 17:45:32');
INSERT INTO `gen_table_column` VALUES (80, 8, 'asset_id', '资产ID', 'int(11)', 'Long', 'assetId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-08-31 10:43:41', '', '2023-08-31 17:45:32');
INSERT INTO `gen_table_column` VALUES (81, 8, 'type', '维保类型', 'char(1)', 'String', 'type', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', 'asset_maintain_type', 4, 'admin', '2023-08-31 10:43:41', '', '2023-08-31 17:45:32');
INSERT INTO `gen_table_column` VALUES (82, 8, 'reason', '维保原由', 'varchar(255)', 'String', 'reason', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2023-08-31 10:43:41', '', '2023-08-31 17:45:32');
INSERT INTO `gen_table_column` VALUES (83, 8, 'status', '维保状态', 'char(1)', 'String', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'asset_maintain_status', 6, 'admin', '2023-08-31 10:43:41', '', '2023-08-31 17:45:32');
INSERT INTO `gen_table_column` VALUES (84, 8, 'money', '费用', 'decimal(10,2)', 'BigDecimal', 'money', '0', '0', NULL, '1', '1', '1', '0', 'EQ', 'input', '', 7, 'admin', '2023-08-31 10:43:41', '', '2023-08-31 17:45:32');
INSERT INTO `gen_table_column` VALUES (85, 8, 'comment', '说明', 'varchar(255)', 'String', 'comment', '0', '0', NULL, '1', '1', '1', '0', 'EQ', 'input', '', 8, 'admin', '2023-08-31 10:43:41', '', '2023-08-31 17:45:32');
INSERT INTO `gen_table_column` VALUES (86, 8, 'imgs', '图片', 'varchar(500)', 'String', 'imgs', '0', '0', NULL, '1', '1', '1', '0', 'EQ', 'imageUpload', '', 9, 'admin', '2023-08-31 10:43:41', '', '2023-08-31 17:45:32');
INSERT INTO `gen_table_column` VALUES (87, 8, 'user_id', '创建人', 'int(11)', 'Long', 'userId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2023-08-31 10:43:41', '', '2023-08-31 17:45:32');
INSERT INTO `gen_table_column` VALUES (88, 8, 'dept_id', '申请部门', 'int(11)', 'Long', 'deptId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2023-08-31 10:43:41', '', '2023-08-31 17:45:32');
INSERT INTO `gen_table_column` VALUES (89, 9, 'id', '自增长主键ID', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-08-31 10:43:41', '', '2023-08-31 14:48:52');
INSERT INTO `gen_table_column` VALUES (90, 9, 'type', '出库类别', 'varchar(50)', 'String', 'type', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', 'asset_out_type', 2, 'admin', '2023-08-31 10:43:41', '', '2023-08-31 14:48:52');
INSERT INTO `gen_table_column` VALUES (91, 9, 'asset_id', '资产编号', 'int(11)', 'Long', 'assetId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2023-08-31 10:43:41', '', '2023-08-31 14:48:52');
INSERT INTO `gen_table_column` VALUES (93, 9, 'remark', '出库说明', 'varchar(255)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 6, 'admin', '2023-08-31 10:43:41', '', '2023-08-31 14:48:52');
INSERT INTO `gen_table_column` VALUES (94, 9, 'imgs', '确认单', 'int(11)', 'String', 'imgs', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'imageUpload', '', 7, 'admin', '2023-08-31 10:43:41', '', '2023-08-31 14:48:52');
INSERT INTO `gen_table_column` VALUES (95, 9, 'cate_id', '资产分类', 'int(11)', 'Long', 'cateId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, '', '2023-08-31 14:45:40', '', '2023-08-31 14:48:52');
INSERT INTO `gen_table_column` VALUES (96, 9, 'user_id', '接收人', 'int(11)', 'Long', 'userId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, '', '2023-08-31 14:45:40', '', '2023-08-31 14:48:52');
INSERT INTO `gen_table_column` VALUES (97, 10, 'id', '自增长主键ID', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-09-01 10:55:47', '', '2023-09-01 15:58:50');
INSERT INTO `gen_table_column` VALUES (98, 10, 'name', '代号', 'varchar(20)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2023-09-01 10:55:47', '', '2023-09-01 15:58:50');
INSERT INTO `gen_table_column` VALUES (99, 10, 'asset_count', '资产总数', 'int(11)', 'Long', 'assetCount', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-09-01 10:55:47', '', '2023-09-01 15:58:50');
INSERT INTO `gen_table_column` VALUES (100, 10, 'asset_worth', '资产总值', 'decimal(10,2)', 'BigDecimal', 'assetWorth', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2023-09-01 10:55:47', '', '2023-09-01 15:58:50');
INSERT INTO `gen_table_column` VALUES (101, 10, 'scrap_count', '报废总数', 'int(11)', 'Long', 'scrapCount', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2023-09-01 10:55:47', '', '2023-09-01 15:58:50');
INSERT INTO `gen_table_column` VALUES (102, 10, 'scrap_worth', '报废总值', 'int(11)', 'Long', 'scrapWorth', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2023-09-01 10:55:47', '', '2023-09-01 15:58:50');
INSERT INTO `gen_table_column` VALUES (103, 10, 'maintain_count', '维保次数', 'int(11)', 'Long', 'maintainCount', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2023-09-01 10:55:47', '', '2023-09-01 15:58:50');
INSERT INTO `gen_table_column` VALUES (104, 10, 'maintain_price', '维保费用', 'decimal(10,2)', 'BigDecimal', 'maintainPrice', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2023-09-01 10:55:47', '', '2023-09-01 15:58:50');
INSERT INTO `gen_table_column` VALUES (105, 10, 'purchase_count', '采购次数', 'int(11)', 'Long', 'purchaseCount', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2023-09-01 10:55:47', '', '2023-09-01 15:58:50');
INSERT INTO `gen_table_column` VALUES (106, 10, 'purchase_worth', '采购总值', 'decimal(10,2)', 'BigDecimal', 'purchaseWorth', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2023-09-01 10:55:47', '', '2023-09-01 15:58:50');
INSERT INTO `gen_table_column` VALUES (107, 10, 'sale_worth', '出售总值', 'decimal(10,2)', 'BigDecimal', 'saleWorth', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2023-09-01 10:55:47', '', '2023-09-01 15:58:50');
INSERT INTO `gen_table_column` VALUES (108, 10, 'sale_count', '出售数量', 'int(11)', 'Long', 'saleCount', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 12, 'admin', '2023-09-01 10:55:47', '', '2023-09-01 15:58:50');
INSERT INTO `gen_table_column` VALUES (109, 10, 'remark', '盘点说明', 'text', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 13, 'admin', '2023-09-01 10:55:47', '', '2023-09-01 15:58:50');
INSERT INTO `gen_table_column` VALUES (110, 10, 'start_time', '开始时间', 'datetime', 'Date', 'startTime', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'datetime', '', 14, 'admin', '2023-09-01 10:55:47', '', '2023-09-01 15:58:50');
INSERT INTO `gen_table_column` VALUES (111, 10, 'end_time', '结束时间', 'datetime', 'Date', 'endTime', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'datetime', '', 15, 'admin', '2023-09-01 10:55:47', '', '2023-09-01 15:58:50');
INSERT INTO `gen_table_column` VALUES (112, 10, 'user_id', '负责人', 'int(11)', 'Long', 'userId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 16, 'admin', '2023-09-01 10:55:47', '', '2023-09-01 15:58:50');
INSERT INTO `gen_table_column` VALUES (113, 11, 'id', '自增长主键ID', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-09-01 10:55:47', '', '2023-09-01 14:40:42');
INSERT INTO `gen_table_column` VALUES (114, 11, 'stock_id', '盘点主键', 'varchar(20)', 'String', 'stockId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2023-09-01 10:55:47', '', '2023-09-01 14:40:42');
INSERT INTO `gen_table_column` VALUES (115, 11, 'asset_id', '资产ID', 'int(11)', 'Long', 'assetId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-09-01 10:55:47', '', '2023-09-01 14:40:42');
INSERT INTO `gen_table_column` VALUES (116, 11, 'status', '当前状态', 'char(1)', 'String', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'assets_status', 4, 'admin', '2023-09-01 10:55:47', '', '2023-09-01 14:40:42');
INSERT INTO `gen_table_column` VALUES (117, 11, 'price', '当前价值', 'decimal(10,2)', 'BigDecimal', 'price', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2023-09-01 10:55:47', '', '2023-09-01 14:40:42');
INSERT INTO `gen_table_column` VALUES (118, 12, 'id', '自增长主键ID', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-09-01 10:56:07', '', '2023-09-01 11:00:31');
INSERT INTO `gen_table_column` VALUES (120, 12, 'dept_id', '申请部门', 'int(11)', 'Long', 'deptId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2023-09-01 10:56:07', '', '2023-09-01 11:00:31');
INSERT INTO `gen_table_column` VALUES (121, 12, 'user_id', '申请人', 'int(11)', 'Long', 'userId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2023-09-01 10:56:07', '', '2023-09-01 11:00:31');
INSERT INTO `gen_table_column` VALUES (122, 12, 'reason', '申请原由', 'varchar(200)', 'String', 'reason', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2023-09-01 10:56:07', '', '2023-09-01 11:00:31');
INSERT INTO `gen_table_column` VALUES (123, 12, 'remark', '申请说明', 'varchar(200)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 8, 'admin', '2023-09-01 10:56:07', '', '2023-09-01 11:00:31');
INSERT INTO `gen_table_column` VALUES (124, 12, 'check_status', '审核结果', 'char(1)', 'String', 'checkStatus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'flow_check_status', 9, 'admin', '2023-09-01 10:56:07', '', '2023-09-01 11:00:31');
INSERT INTO `gen_table_column` VALUES (126, 12, 'imgs', '接收单', 'varchar(255)', 'String', 'imgs', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2023-09-01 10:56:07', '', '2023-09-01 11:00:31');
INSERT INTO `gen_table_column` VALUES (127, 12, 'recv_time', '接收时间', 'datetime', 'Date', 'recvTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 12, 'admin', '2023-09-01 10:56:07', '', '2023-09-01 11:00:31');
INSERT INTO `gen_table_column` VALUES (128, 12, 'apply_time', '申请时间', 'datetime', 'Date', 'applyTime', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'datetime', '', 13, 'admin', '2023-09-01 10:56:07', '', '2023-09-01 11:00:31');
INSERT INTO `gen_table_column` VALUES (129, 12, 'cate_id', '分类', 'varchar(50)', 'String', 'cateId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, '', '2023-09-01 10:59:52', '', '2023-09-01 11:00:31');
INSERT INTO `gen_table_column` VALUES (130, 12, 'model_id', '型号', 'int(11)', 'Long', 'modelId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, '', '2023-09-01 10:59:52', '', '2023-09-01 11:00:31');
INSERT INTO `gen_table_column` VALUES (131, 12, 'amount', '数量', 'tinyint(4)', 'Integer', 'amount', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 4, '', '2023-09-01 10:59:52', '', '2023-09-01 11:00:31');
INSERT INTO `gen_table_column` VALUES (132, 12, 'nums', '资产编号', 'varchar(255)', 'String', 'nums', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, '', '2023-09-01 10:59:52', '', '2023-09-01 11:00:31');
INSERT INTO `gen_table_column` VALUES (133, 11, 'cate_name', '资产分类', 'varchar(50)', 'String', 'cateName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 6, '', '2023-09-01 14:40:42', '', NULL);
INSERT INTO `gen_table_column` VALUES (134, 11, 'model_name', '资产型号', 'varchar(50)', 'String', 'modelName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 7, '', '2023-09-01 14:40:42', '', NULL);
INSERT INTO `gen_table_column` VALUES (135, 11, 'num', '资产编码', 'varchar(50)', 'String', 'num', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 8, '', '2023-09-01 14:40:42', '', NULL);
INSERT INTO `gen_table_column` VALUES (136, 10, 'status', '状态', 'char(1)', 'String', 'status', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'radio', 'asset_stock_status', 17, '', '2023-09-01 15:58:22', '', '2023-09-01 15:58:50');
INSERT INTO `gen_table_column` VALUES (137, 13, 'id', '自增长主键ID', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-09-04 10:57:30', '', '2023-09-04 11:01:17');
INSERT INTO `gen_table_column` VALUES (138, 13, 'task_id', '业务主键', 'int(11)', 'Long', 'taskId', '0', '0', '1', '1', '1', '1', '0', 'EQ', 'input', '', 2, 'admin', '2023-09-04 10:57:30', '', '2023-09-04 11:01:17');
INSERT INTO `gen_table_column` VALUES (139, 13, 'check_user_id', '审核人', 'int(11)', 'Long', 'checkUserId', '0', '0', '1', '1', '1', '1', '0', 'EQ', 'input', '', 3, 'admin', '2023-09-04 10:57:30', '', '2023-09-04 11:01:17');
INSERT INTO `gen_table_column` VALUES (140, 13, 'check_user_name', '审核人姓名', 'varchar(50)', 'String', 'checkUserName', '0', '0', NULL, '1', '1', '1', '0', 'LIKE', 'input', '', 4, 'admin', '2023-09-04 10:57:30', '', '2023-09-04 11:01:17');
INSERT INTO `gen_table_column` VALUES (141, 13, 'check_status', '审核结果', 'char(1)', 'String', 'checkStatus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'flow_check_status', 5, 'admin', '2023-09-04 10:57:30', '', '2023-09-04 11:01:17');
INSERT INTO `gen_table_column` VALUES (142, 13, 'check_time', '审核时间', 'datetime', 'Date', 'checkTime', '0', '0', NULL, '1', '1', '1', '0', 'EQ', 'datetime', '', 6, 'admin', '2023-09-04 10:57:30', '', '2023-09-04 11:01:17');
INSERT INTO `gen_table_column` VALUES (143, 13, 'check_reason', '审核不通过原因', 'varchar(200)', 'String', 'checkReason', '0', '0', NULL, '1', '1', '1', '0', 'EQ', 'input', '', 7, 'admin', '2023-09-04 10:57:30', '', '2023-09-04 11:01:17');
INSERT INTO `gen_table_column` VALUES (144, 13, 'pid', '上一步骤', 'int(11)', 'Long', 'pid', '0', '0', NULL, '1', '1', '1', '0', 'EQ', 'input', '', 8, 'admin', '2023-09-04 10:57:30', '', '2023-09-04 11:01:17');
INSERT INTO `gen_table_column` VALUES (145, 13, 'ignore_flag', '忽略标识', 'char(1)', 'String', 'ignoreFlag', '0', '0', NULL, '1', '1', '1', '0', 'EQ', 'input', '', 9, 'admin', '2023-09-04 10:57:30', '', '2023-09-04 11:01:17');
INSERT INTO `gen_table_column` VALUES (146, 14, 'id', '自增长主键ID', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-09-04 10:57:30', '', '2023-09-04 10:58:40');
INSERT INTO `gen_table_column` VALUES (147, 14, 'type', '类型', 'varchar(20)', 'String', 'type', '0', '0', '1', '1', '1', '1', '0', 'EQ', 'select', '', 2, 'admin', '2023-09-04 10:57:30', '', '2023-09-04 10:58:40');
INSERT INTO `gen_table_column` VALUES (148, 14, 'target_id', '业务主键', 'int(11)', 'Long', 'targetId', '0', '0', '1', '1', '1', '1', '0', 'EQ', 'input', '', 3, 'admin', '2023-09-04 10:57:30', '', '2023-09-04 10:58:40');
INSERT INTO `gen_table_column` VALUES (149, 14, 'num', '业务编号', 'varchar(100)', 'String', 'num', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 4, 'admin', '2023-09-04 10:57:30', '', '2023-09-04 10:58:40');
INSERT INTO `gen_table_column` VALUES (150, 14, 'title', '业务名称', 'varchar(200)', 'String', 'title', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 5, 'admin', '2023-09-04 10:57:30', '', '2023-09-04 10:58:40');
INSERT INTO `gen_table_column` VALUES (151, 14, 'user_id', '申请人', 'int(11)', 'Long', 'userId', '0', '0', NULL, '1', '1', '1', '0', 'EQ', 'input', '', 6, 'admin', '2023-09-04 10:57:30', '', '2023-09-04 10:58:40');
INSERT INTO `gen_table_column` VALUES (152, 14, 'user_name', '申请人姓名', 'varchar(50)', 'String', 'userName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 7, 'admin', '2023-09-04 10:57:30', '', '2023-09-04 10:58:40');
INSERT INTO `gen_table_column` VALUES (153, 14, 'check_status', '审核结果', 'char(1)', 'String', 'checkStatus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'flow_check_status', 8, 'admin', '2023-09-04 10:57:30', '', '2023-09-04 10:58:40');
INSERT INTO `gen_table_column` VALUES (154, 14, 'apply_time', '申请时间', 'datetime', 'Date', 'applyTime', '0', '0', NULL, '1', '1', '1', '1', 'BETWEEN', 'datetime', '', 9, 'admin', '2023-09-04 10:57:30', '', '2023-09-04 10:58:40');
INSERT INTO `gen_table_column` VALUES (155, 14, 'apply_reason', '申请原因', 'varchar(200)', 'String', 'applyReason', '0', '0', NULL, '1', '1', '1', '0', 'EQ', 'input', '', 10, 'admin', '2023-09-04 10:57:30', '', '2023-09-04 10:58:40');
INSERT INTO `gen_table_column` VALUES (156, 15, 'id', '自增长主键ID', 'int(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-09-08 11:32:55', '', '2023-09-08 11:33:54');
INSERT INTO `gen_table_column` VALUES (157, 15, 'cate_id', '分类', 'varchar(50)', 'String', 'cateId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2023-09-08 11:32:55', '', '2023-09-08 11:33:54');
INSERT INTO `gen_table_column` VALUES (158, 15, 'asset_id', '资产', 'int(11)', 'Long', 'assetId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-09-08 11:32:55', '', '2023-09-08 11:33:54');
INSERT INTO `gen_table_column` VALUES (159, 15, 'dept_id', '归属部门', 'int(11)', 'Long', 'deptId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2023-09-08 11:32:55', '', '2023-09-08 11:33:54');
INSERT INTO `gen_table_column` VALUES (160, 15, 'dept_name', '部门名称', 'varchar(500)', 'String', 'deptName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'textarea', '', 5, 'admin', '2023-09-08 11:32:55', '', '2023-09-08 11:33:54');
INSERT INTO `gen_table_column` VALUES (161, 15, 'user_id', '操作人', 'int(11)', 'Long', 'userId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2023-09-08 11:32:55', '', '2023-09-08 11:33:54');
INSERT INTO `gen_table_column` VALUES (162, 15, 'user_name', '操作人姓名', 'varchar(50)', 'String', 'userName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 7, 'admin', '2023-09-08 11:32:55', '', '2023-09-08 11:33:54');
INSERT INTO `gen_table_column` VALUES (163, 15, 'price', '价格', 'decimal(10,2)', 'BigDecimal', 'price', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2023-09-08 11:32:55', '', '2023-09-08 11:33:54');
INSERT INTO `gen_table_column` VALUES (164, 15, 'reason', '出售原由', 'varchar(200)', 'String', 'reason', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2023-09-08 11:32:55', '', '2023-09-08 11:33:54');
INSERT INTO `gen_table_column` VALUES (165, 15, 'sale_time', '出售时间', 'datetime', 'Date', 'saleTime', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'datetime', '', 10, 'admin', '2023-09-08 11:32:55', '', '2023-09-08 11:33:54');
INSERT INTO `gen_table_column` VALUES (166, 15, 'is_out', '已出库', 'char(1)', 'String', 'isOut', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'sys_yes_no', 11, 'admin', '2023-09-08 11:32:55', '', '2023-09-08 11:33:54');
INSERT INTO `gen_table_column` VALUES (167, 16, 'id', '参数主键', 'int(5)', 'Integer', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-09-12 10:35:02', '', '2023-09-12 10:51:59');
INSERT INTO `gen_table_column` VALUES (168, 16, 'type', '类型', 'varchar(50)', 'String', 'type', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', 'chart_type', 2, 'admin', '2023-09-12 10:35:02', '', '2023-09-12 10:51:59');
INSERT INTO `gen_table_column` VALUES (169, 16, 'name', '名称', 'varchar(100)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', '2023-09-12 10:35:02', '', '2023-09-12 10:51:59');
INSERT INTO `gen_table_column` VALUES (170, 16, 'ds_code', '数据源', 'varchar(50)', 'String', 'dsCode', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2023-09-12 10:35:02', '', '2023-09-12 10:51:59');
INSERT INTO `gen_table_column` VALUES (171, 16, 'data_option', '数据解析', 'text', 'String', 'dataOption', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 5, 'admin', '2023-09-12 10:35:02', '', '2023-09-12 10:51:59');
INSERT INTO `gen_table_column` VALUES (172, 16, 'serach_title', '搜索标题', 'varchar(20)', 'String', 'serachTitle', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2023-09-12 10:35:02', '', '2023-09-12 10:51:59');
INSERT INTO `gen_table_column` VALUES (173, 16, 'search_field', '搜索字段', 'varchar(50)', 'String', 'searchField', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2023-09-12 10:35:02', '', '2023-09-12 10:51:59');
INSERT INTO `gen_table_column` VALUES (174, 16, 'search_type', '搜索类型', 'char(10)', 'String', 'searchType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'chart_search_type', 8, 'admin', '2023-09-12 10:35:02', '', '2023-09-12 10:51:59');
INSERT INTO `gen_table_column` VALUES (175, 16, 'search_ds', '搜索来源', 'varchar(50)', 'String', 'searchDs', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2023-09-12 10:35:02', '', '2023-09-12 10:51:59');
INSERT INTO `gen_table_column` VALUES (176, 16, 'status', '状态', 'char(1)', 'String', 'status', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'radio', 'ext_status', 10, 'admin', '2023-09-12 10:35:02', '', '2023-09-12 10:51:59');

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `blob_data` blob NULL COMMENT '存放持久化Trigger对象',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Blob类型的触发器表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '日历名称',
  `calendar` blob NOT NULL COMMENT '存放持久化calendar对象',
  PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '日历信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `cron_expression` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'cron表达式',
  `time_zone_id` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '时区',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Cron类型的触发器表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `entry_id` varchar(95) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度器实例id',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度器实例名',
  `fired_time` bigint(13) NOT NULL COMMENT '触发的时间',
  `sched_time` bigint(13) NOT NULL COMMENT '定时器制定的时间',
  `priority` int(11) NOT NULL COMMENT '优先级',
  `state` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态',
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务组名',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否并发',
  `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否接受恢复执行',
  PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '已触发的触发器表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务组名',
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '相关介绍',
  `job_class_name` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '执行任务类名称',
  `is_durable` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否持久化',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否并发',
  `is_update_data` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否更新数据',
  `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否接受恢复执行',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '任务详细信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `lock_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '悲观锁名称',
  PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '存储的悲观锁信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '暂停的触发器表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '实例名称',
  `last_checkin_time` bigint(13) NOT NULL COMMENT '上次检查时间',
  `checkin_interval` bigint(13) NOT NULL COMMENT '检查间隔时间',
  PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '调度器状态表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `repeat_count` bigint(7) NOT NULL COMMENT '重复的次数统计',
  `repeat_interval` bigint(12) NOT NULL COMMENT '重复的间隔时间',
  `times_triggered` bigint(10) NOT NULL COMMENT '已经触发的次数',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '简单触发器的信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `str_prop_1` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第一个参数',
  `str_prop_2` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第二个参数',
  `str_prop_3` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第三个参数',
  `int_prop_1` int(11) NULL DEFAULT NULL COMMENT 'int类型的trigger的第一个参数',
  `int_prop_2` int(11) NULL DEFAULT NULL COMMENT 'int类型的trigger的第二个参数',
  `long_prop_1` bigint(20) NULL DEFAULT NULL COMMENT 'long类型的trigger的第一个参数',
  `long_prop_2` bigint(20) NULL DEFAULT NULL COMMENT 'long类型的trigger的第二个参数',
  `dec_prop_1` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第一个参数',
  `dec_prop_2` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第二个参数',
  `bool_prop_1` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第一个参数',
  `bool_prop_2` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第二个参数',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '同步机制的行锁表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '触发器的名字',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '触发器所属组的名字',
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_job_details表job_name的外键',
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_job_details表job_group的外键',
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '相关介绍',
  `next_fire_time` bigint(13) NULL DEFAULT NULL COMMENT '上一次触发时间（毫秒）',
  `prev_fire_time` bigint(13) NULL DEFAULT NULL COMMENT '下一次触发时间（默认为-1表示不触发）',
  `priority` int(11) NULL DEFAULT NULL COMMENT '优先级',
  `trigger_state` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '触发器状态',
  `trigger_type` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '触发器的类型',
  `start_time` bigint(13) NOT NULL COMMENT '开始时间',
  `end_time` bigint(13) NULL DEFAULT NULL COMMENT '结束时间',
  `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日程表名称',
  `misfire_instr` smallint(2) NULL DEFAULT NULL COMMENT '补偿执行的策略',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '触发器详细信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for sys_chart
-- ----------------------------
DROP TABLE IF EXISTS `sys_chart`;
CREATE TABLE `sys_chart`  (
  `id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类型',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `ds_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '数据源',
  `data_option` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '数据解析',
  `search_title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '搜索标题',
  `search_field` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '搜索字段',
  `search_type` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '搜索类型',
  `search_ds` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '搜索来源',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '自定义报表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_chart
-- ----------------------------
INSERT INTO `sys_chart` VALUES (1, 'pie', '资产分类统计', 'chart_asset_category', NULL, '资产分类', 'cateId', 'tree', 'asset_category_select', '1');
INSERT INTO `sys_chart` VALUES (2, 'bar', '资产柱状图', 'chart_asset_category', '{\n  \"series\": [\n    {\n      \"type\": \"bar\",\n      \"showBackground\": true,\n      \"backgroundStyle\": {\n        \"color\": \"rgba(180, 180, 180, 0.2)\"\n      }\n    }\n  ]\n}', NULL, 'N', NULL, NULL, '1');
INSERT INTO `sys_chart` VALUES (3, 'line', '资产分类统计线', 'chart_asset_category', NULL, NULL, 'N', NULL, NULL, '1');
INSERT INTO `sys_chart` VALUES (4, 'guage', '资产使用率', 'asset_status_guage', 'function exec(dsData){\n  let data = null;\n if(dsData == null || dsData.length == 0){\n   data = {used:0,wrong:0,total:0};\n}else{\n  data = dsData[0];\n}\n var option = {series: [\n    {\n      type: \'gauge\',\n      min:0,\n      max: data.total,\n      anchor: {\n        show: true,\n        showAbove: true,\n        size: 18,\n        itemStyle: {\n          color: \'#FAC858\'\n        }\n      },      \n      axisLine: {\n        roundCap: true\n      },\n      data: [\n          {\n              value:data.used,\n              name:\"在用\", \n             title: {offsetCenter: [\'-30%\', \'80%\']},\n             detail: {offsetCenter: [\'-30%\', \'95%\']}\n         },\n          {\n             value:data.total-data.wrong,\n             name:\'完好\', \n            title: {offsetCenter: [\'20%\', \'80%\']},\n             detail: {offsetCenter: [\'20%\', \'95%\']}\n         }\n      ],\n      title: {\n        fontSize: 14\n      },\n      detail: {\n        width: 40,\n        height: 14,\n        fontSize: 14,\n        color: \'#fff\',\n        backgroundColor: \'inherit\',\n        borderRadius: 3,\n        formatter: \'{value}\'\n      }\n    }\n  ]\n};\nreturn option;\n}', NULL, 'N', NULL, NULL, '1');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '参数配置表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2023-08-22 10:13:30', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2023-08-22 10:13:30', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2023-08-22 10:13:30', '', NULL, '深色主题theme-dark，浅色主题theme-light');
INSERT INTO `sys_config` VALUES (4, '账号自助-验证码开关', 'sys.account.captchaEnabled', 'true', 'Y', 'admin', '2023-08-22 10:13:30', '', NULL, '是否开启验证码功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES (5, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', '2023-08-22 10:13:30', '', NULL, '是否开启注册用户功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES (6, '用户登录-黑名单列表', 'sys.login.blackIPList', '', 'Y', 'admin', '2023-08-22 10:13:30', '', NULL, '设置登录IP黑名单限制，多个匹配项以;分隔，支持匹配（*通配、网段）');
INSERT INTO `sys_config` VALUES (7, '二维码LOGO', 'asset.qr.logo', 'https://img95.699pic.com/xsj/0q/re/6b.jpg', 'Y', 'admin', '2023-08-31 17:05:32', 'admin', '2023-08-31 17:31:45', NULL);

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 110 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '若依科技', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-08-22 10:13:30', '', NULL);
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-08-22 10:13:30', '', NULL);
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-08-22 10:13:30', '', NULL);
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-08-22 10:13:30', '', NULL);
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-08-22 10:13:30', '', NULL);
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-08-22 10:13:30', '', NULL);
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-08-22 10:13:30', '', NULL);
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-08-22 10:13:30', '', NULL);
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-08-22 10:13:30', '', NULL);
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-08-22 10:13:30', '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 163 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典数据表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '其他操作');
INSERT INTO `sys_dict_data` VALUES (19, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (20, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (21, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (22, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (23, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (24, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (25, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (26, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (27, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (28, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (29, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (100, 1, '列表查询', 'table', 'api_sql_type', NULL, 'info', 'N', '0', 'admin', '2023-08-17 10:30:07', 'admin', '2023-08-17 10:31:59', NULL);
INSERT INTO `sys_dict_data` VALUES (101, 2, '数据计算', 'count', 'api_sql_type', NULL, 'primary', 'N', '0', 'admin', '2023-08-17 10:30:17', 'admin', '2023-08-17 10:31:52', NULL);
INSERT INTO `sys_dict_data` VALUES (102, 3, '报表统计', 'chart', 'api_sql_type', NULL, 'success', 'N', '0', 'admin', '2023-08-17 10:30:30', 'admin', '2023-08-17 10:31:44', NULL);
INSERT INTO `sys_dict_data` VALUES (103, 0, '子查询', 'inner', 'api_sql_type', NULL, 'danger', 'N', '0', 'admin', '2023-08-17 10:30:46', 'admin', '2023-08-17 10:31:25', NULL);
INSERT INTO `sys_dict_data` VALUES (104, 4, '数据处理', 'batch', 'api_sql_type', NULL, 'warning', 'N', '0', 'admin', '2023-08-17 10:31:01', 'admin', '2023-08-17 10:31:19', NULL);
INSERT INTO `sys_dict_data` VALUES (105, 0, '新增', 'insert', 'api_log_type', NULL, 'default', 'N', '0', 'admin', '2023-08-21 15:02:55', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (106, 0, '更新', 'update', 'api_log_type', NULL, 'default', 'N', '0', 'admin', '2023-08-21 15:03:03', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (107, 0, '删除', 'delete', 'api_log_type', NULL, 'default', 'N', '0', 'admin', '2023-08-21 15:03:11', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (108, 0, '导入', 'import', 'api_log_type', NULL, 'default', 'N', '0', 'admin', '2023-08-21 15:03:38', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (109, 0, '事件', 'event', 'api_log_type', NULL, 'default', 'N', '0', 'admin', '2023-08-21 15:03:47', 'admin', '2023-08-21 15:03:58', NULL);
INSERT INTO `sys_dict_data` VALUES (110, 0, '报错', 'error', 'api_log_type', NULL, 'default', 'N', '0', 'admin', '2023-08-21 15:04:10', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (111, 0, '登录', 'login', 'api_log_type', NULL, 'default', 'N', '0', 'admin', '2023-08-21 15:04:28', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (112, 0, '退出', 'logout', 'api_log_type', NULL, 'default', 'N', '0', 'admin', '2023-08-21 15:04:37', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (113, 1, '新增前', 'BI', 'api_event_type', NULL, 'default', 'N', '0', 'admin', '2023-08-22 15:42:10', 'admin', '2023-08-22 15:45:29', NULL);
INSERT INTO `sys_dict_data` VALUES (114, 2, '新增后', 'AI', 'api_event_type', NULL, 'default', 'N', '0', 'admin', '2023-08-22 15:42:10', 'admin', '2023-08-22 15:45:46', NULL);
INSERT INTO `sys_dict_data` VALUES (115, 3, '更新前', 'BU', 'api_event_type', NULL, 'default', 'N', '0', 'admin', '2023-08-22 15:42:10', 'admin', '2023-08-22 15:45:54', NULL);
INSERT INTO `sys_dict_data` VALUES (116, 4, '更新后', 'AU', 'api_event_type', NULL, 'default', 'N', '0', 'admin', '2023-08-22 15:42:10', 'admin', '2023-08-22 15:46:00', NULL);
INSERT INTO `sys_dict_data` VALUES (117, 5, '删除前', 'BD', 'api_event_type', NULL, 'default', 'N', '0', 'admin', '2023-08-22 15:42:10', 'admin', '2023-08-22 15:46:06', NULL);
INSERT INTO `sys_dict_data` VALUES (118, 6, '删除后', 'AD', 'api_event_type', NULL, 'default', 'N', '0', 'admin', '2023-08-22 15:42:10', 'admin', '2023-08-22 15:46:12', NULL);
INSERT INTO `sys_dict_data` VALUES (119, 0, '固定资产', '1', 'assets_category_type', NULL, 'success', 'N', '0', 'admin', '2023-08-23 13:40:19', 'admin', '2023-08-23 13:40:59', NULL);
INSERT INTO `sys_dict_data` VALUES (120, 2, '日常耗材', '2', 'assets_category_type', NULL, 'info', 'N', '0', 'admin', '2023-08-23 13:40:32', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (121, 0, '无形资产', '3', 'assets_category_type', NULL, 'warning', 'N', '0', 'admin', '2023-08-23 13:40:52', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (122, 1, '闲置', '1', 'assets_status', NULL, 'default', 'N', '0', 'admin', '2023-08-23 13:41:34', 'admin', '2023-08-31 14:15:01', NULL);
INSERT INTO `sys_dict_data` VALUES (123, 2, '在用', '2', 'assets_status', NULL, 'default', 'N', '0', 'admin', '2023-08-23 13:41:52', 'admin', '2023-08-23 13:41:59', NULL);
INSERT INTO `sys_dict_data` VALUES (124, 3, '损坏', '3', 'assets_status', NULL, 'default', 'N', '0', 'admin', '2023-08-23 13:42:15', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (125, 4, '维修', '4', 'assets_status', NULL, 'default', 'N', '0', 'admin', '2023-08-23 13:42:26', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (126, 5, '报废', '5', 'assets_status', NULL, 'default', 'N', '0', 'admin', '2023-08-23 13:42:36', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (127, 1, '草稿', '1', 'flow_apply_status', NULL, 'info', 'N', '0', 'admin', '2023-08-23 13:43:32', 'admin', '2023-08-30 15:04:08', NULL);
INSERT INTO `sys_dict_data` VALUES (128, 2, '已提交', '2', 'flow_apply_status', NULL, 'primary', 'N', '0', 'admin', '2023-08-23 13:43:40', 'admin', '2023-08-30 15:04:15', NULL);
INSERT INTO `sys_dict_data` VALUES (129, 3, '进行中', '3', 'flow_apply_status', NULL, 'warning', 'N', '0', 'admin', '2023-08-23 13:44:32', 'admin', '2023-09-08 10:30:16', NULL);
INSERT INTO `sys_dict_data` VALUES (131, 4, '已完成', '4', 'flow_apply_status', NULL, 'success', 'N', '0', 'admin', '2023-08-23 13:44:58', 'admin', '2023-09-08 10:30:11', NULL);
INSERT INTO `sys_dict_data` VALUES (132, 1, '待审核', '1', 'flow_check_status', NULL, 'info', 'N', '0', 'admin', '2023-08-23 13:45:35', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (133, 2, '审核通过', '2', 'flow_check_status', NULL, 'success', 'N', '0', 'admin', '2023-08-23 13:45:49', 'admin', '2023-08-23 13:45:55', NULL);
INSERT INTO `sys_dict_data` VALUES (134, 3, '不通过', '3', 'flow_check_status', NULL, 'warning', 'N', '0', 'admin', '2023-08-23 13:46:08', 'admin', '2023-09-05 16:35:38', NULL);
INSERT INTO `sys_dict_data` VALUES (135, 0, '启用', '1', 'ext_status', NULL, 'success', 'N', '0', 'admin', '2023-08-23 14:59:31', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (136, 0, '禁用', '2', 'ext_status', NULL, 'danger', 'N', '0', 'admin', '2023-08-23 14:59:40', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (137, 1, '采购', '1', 'asset_in_type', NULL, 'default', 'N', '0', 'admin', '2023-08-31 10:46:19', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (138, 2, '归还', '2', 'asset_in_type', NULL, 'default', 'N', '0', 'admin', '2023-08-31 10:46:25', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (139, 1, '借用', '1', 'asset_out_type', NULL, 'default', 'N', '0', 'admin', '2023-08-31 14:46:37', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (140, 2, '报废', '2', 'asset_out_type', NULL, 'default', 'N', '0', 'admin', '2023-08-31 14:46:55', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (141, 3, '出售', '3', 'asset_out_type', NULL, 'default', 'N', '0', 'admin', '2023-08-31 14:47:12', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (142, 6, '已出售', '6', 'assets_status', NULL, 'default', 'N', '0', 'admin', '2023-08-31 15:15:21', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (143, 1, '维修', '1', 'asset_maintain_type', NULL, 'danger', 'N', '0', 'admin', '2023-08-31 17:40:17', 'admin', '2023-08-31 17:40:39', NULL);
INSERT INTO `sys_dict_data` VALUES (144, 2, '保养', '2', 'asset_maintain_type', NULL, 'warning', 'N', '0', 'admin', '2023-08-31 17:40:33', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (145, 1, '未开始', '1', 'asset_maintain_status', NULL, 'default', 'N', '0', 'admin', '2023-08-31 17:41:06', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (146, 2, '进行中', '2', 'asset_maintain_status', NULL, 'default', 'N', '0', 'admin', '2023-08-31 17:41:16', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (147, 3, '已完好', '3', 'asset_maintain_status', NULL, 'default', 'N', '0', 'admin', '2023-08-31 17:41:57', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (148, 4, '无法修复', '4', 'asset_maintain_status', NULL, 'default', 'N', '0', 'admin', '2023-08-31 17:42:14', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (149, 1, '未开始', '1', 'asset_stock_status', NULL, 'info', 'N', '0', 'admin', '2023-09-01 15:57:24', 'admin', '2023-09-01 15:58:09', NULL);
INSERT INTO `sys_dict_data` VALUES (150, 2, '进行中', '2', 'asset_stock_status', NULL, 'warning', 'N', '0', 'admin', '2023-09-01 15:57:35', 'admin', '2023-09-01 15:58:02', NULL);
INSERT INTO `sys_dict_data` VALUES (151, 3, '已完成', '3', 'asset_stock_status', NULL, 'danger', 'N', '0', 'admin', '2023-09-01 15:57:50', 'admin', '2023-09-01 16:02:17', NULL);
INSERT INTO `sys_dict_data` VALUES (152, 4, '已确认', '4', 'asset_stock_status', NULL, 'success', 'N', '0', 'admin', '2023-09-01 16:02:12', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (153, 9, '删除验证', 'DC', 'api_event_type', NULL, 'default', 'N', '0', 'admin', '2023-09-07 09:57:14', 'admin', '2023-09-07 10:41:23', NULL);
INSERT INTO `sys_dict_data` VALUES (154, 1, '柱状图', 'bar', 'chart_type', NULL, 'default', 'N', '0', 'admin', '2023-09-12 10:36:53', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (155, 2, '折线图', 'line', 'chart_type', NULL, 'default', 'N', '0', 'admin', '2023-09-12 10:37:06', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (156, 3, '饼图', 'pie', 'chart_type', NULL, 'default', 'N', '0', 'admin', '2023-09-12 10:37:19', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (157, 4, '雷达图', 'radar', 'chart_type', NULL, 'default', 'N', '0', 'admin', '2023-09-12 10:38:16', 'admin', '2023-09-12 15:57:49', NULL);
INSERT INTO `sys_dict_data` VALUES (158, 5, '仪表盘点', 'guage', 'chart_type', NULL, 'default', 'N', '0', 'admin', '2023-09-12 10:38:45', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (159, 1, '数据源', 'ds', 'chart_search_type', NULL, 'default', 'N', '0', 'admin', '2023-09-12 10:39:44', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (160, 2, '树形数据源', 'tree', 'chart_search_type', NULL, 'default', 'N', '0', 'admin', '2023-09-12 10:39:58', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (161, 3, '字典', 'dict', 'chart_search_type', NULL, 'default', 'N', '0', 'admin', '2023-09-12 10:40:16', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (162, 4, '时间范围', 'dt', 'chart_search_type', NULL, 'default', 'N', '0', 'admin', '2023-09-12 10:40:32', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 115 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典类型表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (100, '接口内置SQL类型', 'api_sql_type', '0', 'admin', '2023-08-17 10:29:49', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (101, '接口日志类型', 'api_log_type', '0', 'admin', '2023-08-21 15:02:35', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (102, '接口事件类型', 'api_event_type', '0', 'admin', '2023-08-22 15:41:53', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (103, '资产类别', 'assets_category_type', '0', 'admin', '2023-08-23 13:39:56', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (104, '资产状态', 'assets_status', '0', 'admin', '2023-08-23 13:41:21', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (105, '申请状态', 'flow_apply_status', '0', 'admin', '2023-08-23 13:43:19', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (106, '审批状态', 'flow_check_status', '0', 'admin', '2023-08-23 13:45:17', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (107, '禁用启动状态', 'ext_status', '0', 'admin', '2023-08-23 14:59:13', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (108, '入库类型', 'asset_in_type', '0', 'admin', '2023-08-31 10:46:08', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (109, '出库类型', 'asset_out_type', '0', 'admin', '2023-08-31 14:46:15', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (110, '维保类型', 'asset_maintain_type', '0', 'admin', '2023-08-31 17:39:44', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (111, '维保状态', 'asset_maintain_status', '0', 'admin', '2023-08-31 17:40:01', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (112, '盘点任务状态', 'asset_stock_status', '0', 'admin', '2023-09-01 15:57:12', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (113, '报表类型', 'chart_type', '0', 'admin', '2023-09-12 10:36:08', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (114, '报表搜索类型', 'chart_search_type', '0', 'admin', '2023-09-12 10:36:33', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2023-08-22 10:13:30', '', NULL, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE,
  INDEX `idx_sys_logininfor_s`(`status`) USING BTREE,
  INDEX `idx_sys_logininfor_lt`(`login_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 198 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统访问记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (100, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '密码输入错误1次', '2023-08-22 11:06:13');
INSERT INTO `sys_logininfor` VALUES (101, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '用户不存在/密码错误', '2023-08-22 11:06:13');
INSERT INTO `sys_logininfor` VALUES (102, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2023-08-22 11:06:18');
INSERT INTO `sys_logininfor` VALUES (103, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2023-08-22 11:06:21');
INSERT INTO `sys_logininfor` VALUES (104, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2023-08-22 15:15:20');
INSERT INTO `sys_logininfor` VALUES (105, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2023-08-23 11:44:33');
INSERT INTO `sys_logininfor` VALUES (106, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2023-08-28 11:03:30');
INSERT INTO `sys_logininfor` VALUES (107, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2023-08-29 14:07:54');
INSERT INTO `sys_logininfor` VALUES (108, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2023-08-30 10:09:37');
INSERT INTO `sys_logininfor` VALUES (109, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2023-08-31 10:43:22');
INSERT INTO `sys_logininfor` VALUES (110, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2023-09-01 08:50:33');
INSERT INTO `sys_logininfor` VALUES (111, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2023-09-04 09:16:23');
INSERT INTO `sys_logininfor` VALUES (112, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2023-09-05 11:03:36');
INSERT INTO `sys_logininfor` VALUES (113, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码已失效', '2023-09-06 09:18:24');
INSERT INTO `sys_logininfor` VALUES (114, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2023-09-06 09:18:28');
INSERT INTO `sys_logininfor` VALUES (115, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2023-09-07 09:40:03');
INSERT INTO `sys_logininfor` VALUES (116, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2023-09-08 09:38:01');
INSERT INTO `sys_logininfor` VALUES (117, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2023-09-11 10:02:57');
INSERT INTO `sys_logininfor` VALUES (118, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2023-09-12 09:16:34');
INSERT INTO `sys_logininfor` VALUES (119, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2023-09-13 14:29:07');
INSERT INTO `sys_logininfor` VALUES (120, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2023-09-14 10:39:00');
INSERT INTO `sys_logininfor` VALUES (121, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2023-09-14 10:39:04');
INSERT INTO `sys_logininfor` VALUES (122, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2023-09-15 15:53:19');
INSERT INTO `sys_logininfor` VALUES (123, 'admin', '127.0.0.1', '内网IP', 'Chrome Mobile', 'Android 6.x', '1', '用户不存在/密码错误', '2023-09-18 14:58:01');
INSERT INTO `sys_logininfor` VALUES (124, 'admin', '127.0.0.1', '内网IP', 'Chrome Mobile', 'Android 6.x', '1', '密码输入错误1次', '2023-09-18 14:58:01');
INSERT INTO `sys_logininfor` VALUES (125, 'admin', '127.0.0.1', '内网IP', 'Chrome Mobile', 'Android 6.x', '0', '登录成功', '2023-09-18 14:58:08');
INSERT INTO `sys_logininfor` VALUES (126, 'admin', '127.0.0.1', '内网IP', 'Chrome Mobile', 'Android 6.x', '0', '登录成功', '2023-09-19 10:20:46');
INSERT INTO `sys_logininfor` VALUES (127, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-19 10:39:27');
INSERT INTO `sys_logininfor` VALUES (128, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-19 10:46:06');
INSERT INTO `sys_logininfor` VALUES (129, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-19 10:50:39');
INSERT INTO `sys_logininfor` VALUES (130, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-19 10:58:24');
INSERT INTO `sys_logininfor` VALUES (131, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-19 11:01:08');
INSERT INTO `sys_logininfor` VALUES (132, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-19 11:39:53');
INSERT INTO `sys_logininfor` VALUES (133, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-19 11:46:49');
INSERT INTO `sys_logininfor` VALUES (134, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2023-09-19 14:08:21');
INSERT INTO `sys_logininfor` VALUES (135, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2023-09-19 14:38:36');
INSERT INTO `sys_logininfor` VALUES (136, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '用户不存在/密码错误', '2023-09-20 17:10:46');
INSERT INTO `sys_logininfor` VALUES (137, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '密码输入错误1次', '2023-09-20 17:10:46');
INSERT INTO `sys_logininfor` VALUES (138, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2023-09-20 17:10:51');
INSERT INTO `sys_logininfor` VALUES (139, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2023-09-20 17:10:53');
INSERT INTO `sys_logininfor` VALUES (140, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2023-09-20 17:45:08');
INSERT INTO `sys_logininfor` VALUES (141, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2023-09-21 16:26:52');
INSERT INTO `sys_logininfor` VALUES (142, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2023-09-21 16:52:33');
INSERT INTO `sys_logininfor` VALUES (143, 'admin', '127.0.0.1', '内网IP', 'Chrome Mobile', 'Android 6.x', '0', '登录成功', '2023-09-21 16:56:33');
INSERT INTO `sys_logininfor` VALUES (144, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '密码输入错误1次', '2023-09-21 17:49:34');
INSERT INTO `sys_logininfor` VALUES (145, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '用户不存在/密码错误', '2023-09-21 17:49:34');
INSERT INTO `sys_logininfor` VALUES (146, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2023-09-21 17:49:39');
INSERT INTO `sys_logininfor` VALUES (147, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2023-09-21 17:49:41');
INSERT INTO `sys_logininfor` VALUES (148, 'admin', '127.0.0.1', '内网IP', 'Chrome Mobile', 'Android 6.x', '0', '登录成功', '2023-09-21 21:55:03');
INSERT INTO `sys_logininfor` VALUES (149, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '用户不存在/密码错误', '2023-09-22 11:35:32');
INSERT INTO `sys_logininfor` VALUES (150, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '密码输入错误1次', '2023-09-22 11:35:32');
INSERT INTO `sys_logininfor` VALUES (151, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2023-09-22 11:35:39');
INSERT INTO `sys_logininfor` VALUES (152, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-09-22 13:49:46');
INSERT INTO `sys_logininfor` VALUES (153, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-22 14:36:05');
INSERT INTO `sys_logininfor` VALUES (154, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-22 15:32:02');
INSERT INTO `sys_logininfor` VALUES (155, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-22 17:57:28');
INSERT INTO `sys_logininfor` VALUES (156, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '密码输入错误1次', '2023-09-22 23:04:06');
INSERT INTO `sys_logininfor` VALUES (157, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '用户不存在/密码错误', '2023-09-22 23:04:06');
INSERT INTO `sys_logininfor` VALUES (158, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2023-09-22 23:04:15');
INSERT INTO `sys_logininfor` VALUES (159, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-25 16:07:58');
INSERT INTO `sys_logininfor` VALUES (160, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-25 16:28:42');
INSERT INTO `sys_logininfor` VALUES (161, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-25 16:54:46');
INSERT INTO `sys_logininfor` VALUES (162, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-25 16:56:12');
INSERT INTO `sys_logininfor` VALUES (163, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-25 17:07:26');
INSERT INTO `sys_logininfor` VALUES (164, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-26 09:13:38');
INSERT INTO `sys_logininfor` VALUES (165, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-26 10:36:58');
INSERT INTO `sys_logininfor` VALUES (166, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-26 13:58:22');
INSERT INTO `sys_logininfor` VALUES (167, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-26 14:02:37');
INSERT INTO `sys_logininfor` VALUES (168, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-26 14:35:45');
INSERT INTO `sys_logininfor` VALUES (169, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-26 15:27:38');
INSERT INTO `sys_logininfor` VALUES (170, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '用户不存在/密码错误', '2023-09-26 15:45:37');
INSERT INTO `sys_logininfor` VALUES (171, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '密码输入错误1次', '2023-09-26 15:45:37');
INSERT INTO `sys_logininfor` VALUES (172, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2023-09-26 15:45:41');
INSERT INTO `sys_logininfor` VALUES (173, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2023-09-26 15:45:43');
INSERT INTO `sys_logininfor` VALUES (174, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-26 15:49:57');
INSERT INTO `sys_logininfor` VALUES (175, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-26 18:09:32');
INSERT INTO `sys_logininfor` VALUES (176, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-26 18:11:19');
INSERT INTO `sys_logininfor` VALUES (177, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-27 09:47:02');
INSERT INTO `sys_logininfor` VALUES (178, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-27 11:42:41');
INSERT INTO `sys_logininfor` VALUES (179, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '用户不存在/密码错误', '2023-09-27 14:08:57');
INSERT INTO `sys_logininfor` VALUES (180, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '密码输入错误1次', '2023-09-27 14:08:57');
INSERT INTO `sys_logininfor` VALUES (181, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2023-09-27 14:09:03');
INSERT INTO `sys_logininfor` VALUES (182, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2023-09-27 14:09:05');
INSERT INTO `sys_logininfor` VALUES (183, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-27 15:46:54');
INSERT INTO `sys_logininfor` VALUES (184, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-27 16:07:36');
INSERT INTO `sys_logininfor` VALUES (185, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-27 16:35:11');
INSERT INTO `sys_logininfor` VALUES (186, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-27 17:37:16');
INSERT INTO `sys_logininfor` VALUES (187, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '密码输入错误1次', '2023-09-28 09:06:46');
INSERT INTO `sys_logininfor` VALUES (188, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '用户不存在/密码错误', '2023-09-28 09:06:46');
INSERT INTO `sys_logininfor` VALUES (189, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2023-09-28 09:06:50');
INSERT INTO `sys_logininfor` VALUES (190, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2023-09-28 09:06:53');
INSERT INTO `sys_logininfor` VALUES (191, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '1', '验证码错误', '2023-09-28 09:10:33');
INSERT INTO `sys_logininfor` VALUES (192, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-28 09:10:37');
INSERT INTO `sys_logininfor` VALUES (193, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-28 10:13:10');
INSERT INTO `sys_logininfor` VALUES (194, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-28 11:06:23');
INSERT INTO `sys_logininfor` VALUES (195, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-28 14:56:07');
INSERT INTO `sys_logininfor` VALUES (196, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-28 15:09:09');
INSERT INTO `sys_logininfor` VALUES (197, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2023-09-28 15:22:43');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路由参数',
  `is_frame` int(1) NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int(1) NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2201 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 5, 'system', NULL, '', 1, 0, 'M', '0', '0', '', 'system', 'admin', '2023-08-22 10:13:30', 'admin', '2023-09-04 10:57:11', '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 4, 'monitor', NULL, '', 1, 0, 'M', '0', '0', '', 'monitor', 'admin', '2023-08-22 10:13:30', 'admin', '2023-08-23 11:46:58', '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 4, 'tool', NULL, '', 1, 0, 'M', '0', '0', '', 'tool', 'admin', '2023-08-22 10:13:30', 'admin', '2023-09-04 10:57:18', '系统工具目录');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', '', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2023-08-22 10:13:30', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', '', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2023-08-22 10:13:30', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', '', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2023-08-22 10:13:30', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', '', 1, 0, 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2023-08-22 10:13:30', '', NULL, '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', '', 1, 0, 'C', '0', '0', 'system:post:list', 'post', 'admin', '2023-08-22 10:13:30', '', NULL, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', '', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2023-08-22 10:13:30', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', '', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2023-08-22 10:13:30', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', '', 1, 0, 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2023-08-22 10:13:30', '', NULL, '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, 'log', '', '', 1, 0, 'M', '0', '0', '', 'log', 'admin', '2023-08-22 10:13:30', '', NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', '', 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2023-08-22 10:13:30', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', '', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2023-08-22 10:13:30', '', NULL, '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, 'druid', 'monitor/druid/index', '', 1, 0, 'C', '0', '0', 'monitor:druid:list', 'druid', 'admin', '2023-08-22 10:13:30', '', NULL, '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 4, 'server', 'monitor/server/index', '', 1, 0, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2023-08-22 10:13:30', '', NULL, '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '缓存监控', 2, 5, 'cache', 'monitor/cache/index', '', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis', 'admin', '2023-08-22 10:13:30', '', NULL, '缓存监控菜单');
INSERT INTO `sys_menu` VALUES (114, '缓存列表', 2, 6, 'cacheList', 'monitor/cache/list', '', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis-list', 'admin', '2023-08-22 10:13:30', '', NULL, '缓存列表菜单');
INSERT INTO `sys_menu` VALUES (115, '表单构建', 3, 1, 'build', 'tool/build/index', '', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2023-08-22 10:13:30', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu` VALUES (116, '代码生成', 3, 2, 'gen', 'tool/gen/index', '', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2023-08-22 10:13:30', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (117, '系统接口', 3, 3, 'swagger', 'tool/swagger/index', '', 1, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2023-08-22 10:13:30', '', NULL, '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, 'operlog', 'monitor/operlog/index', '', 1, 0, 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', '2023-08-22 10:13:30', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, 'logininfor', 'monitor/logininfor/index', '', 1, 0, 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2023-08-22 10:13:30', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1000, '用户查询', 100, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 100, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 100, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 100, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户导出', 100, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导入', 100, 6, '', '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '重置密码', 100, 7, '', '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '角色查询', 101, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色新增', 101, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色修改', 101, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色删除', 101, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色导出', 101, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '菜单查询', 102, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单新增', 102, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单修改', 102, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单删除', 102, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '部门查询', 103, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门新增', 103, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门修改', 103, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门删除', 103, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '岗位查询', 104, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位新增', 104, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位修改', 104, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位删除', 104, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位导出', 104, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '字典查询', 105, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典新增', 105, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典修改', 105, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典删除', 105, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典导出', 105, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '参数查询', 106, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数新增', 106, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数修改', 106, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数删除', 106, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数导出', 106, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '公告查询', 107, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告新增', 107, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告修改', 107, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告删除', 107, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '操作查询', 500, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:query', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '操作删除', 500, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '日志导出', 500, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '登录查询', 501, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:query', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '登录删除', 501, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '日志导出', 501, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '账户解锁', 501, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:unlock', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '在线查询', 109, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '批量强退', 109, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '单条强退', 109, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '任务查询', 110, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '任务新增', 110, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '任务修改', 110, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '任务删除', 110, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '状态修改', 110, 5, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '任务导出', 110, 6, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '生成查询', 116, 1, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '生成修改', 116, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '生成删除', 116, 3, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '导入代码', 116, 4, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '预览代码', 116, 5, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '生成代码', 116, 6, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2000, '接口管理', 0, 5, 'api', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'swagger', 'admin', '2023-08-15 16:58:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2001, '数据模型', 2000, 1, 'model', 'api/model/index', NULL, 1, 0, 'C', '0', '0', 'api:model:list', 'build', 'admin', '2023-08-15 16:58:48', 'admin', '2023-08-22 11:13:17', '数据模型菜单');
INSERT INTO `sys_menu` VALUES (2002, '数据模型查询', 2001, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'api:model:query', '#', 'admin', '2023-08-15 16:58:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2003, '数据模型新增', 2001, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'api:model:add', '#', 'admin', '2023-08-15 16:58:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2004, '数据模型修改', 2001, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'api:model:edit', '#', 'admin', '2023-08-15 16:58:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2005, '数据模型删除', 2001, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'api:model:remove', '#', 'admin', '2023-08-15 16:58:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2006, '数据模型导出', 2001, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'api:model:export', '#', 'admin', '2023-08-15 16:58:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2007, '触发事件', 2000, 1, 'event', 'api/event/index', NULL, 1, 0, 'C', '0', '0', 'api:event:list', 'druid', 'admin', '2023-08-16 10:37:00', 'admin', '2023-08-22 11:14:04', '触发事件菜单');
INSERT INTO `sys_menu` VALUES (2008, '触发事件查询', 2007, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'api:event:query', '#', 'admin', '2023-08-16 10:37:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2009, '触发事件新增', 2007, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'api:event:add', '#', 'admin', '2023-08-16 10:37:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2010, '触发事件修改', 2007, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'api:event:edit', '#', 'admin', '2023-08-16 10:37:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2011, '触发事件删除', 2007, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'api:event:remove', '#', 'admin', '2023-08-16 10:37:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2012, '触发事件导出', 2007, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'api:event:export', '#', 'admin', '2023-08-16 10:37:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2013, '数据脚本', 2000, 1, 'sql', 'api/sql/index', NULL, 1, 0, 'C', '0', '0', 'api:sql:list', 'code', 'admin', '2023-08-16 11:27:46', 'admin', '2023-08-22 11:14:10', '数据脚本菜单');
INSERT INTO `sys_menu` VALUES (2014, '数据脚本查询', 2013, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'api:sql:query', '#', 'admin', '2023-08-16 11:27:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2015, '数据脚本新增', 2013, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'api:sql:add', '#', 'admin', '2023-08-16 11:27:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2016, '数据脚本修改', 2013, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'api:sql:edit', '#', 'admin', '2023-08-16 11:27:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2017, '数据脚本删除', 2013, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'api:sql:remove', '#', 'admin', '2023-08-16 11:27:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2018, '数据脚本导出', 2013, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'api:sql:export', '#', 'admin', '2023-08-16 11:27:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2019, '接口日志', 2000, 1, 'log', 'api/log/index', NULL, 1, 0, 'C', '0', '0', 'api:log:list', 'log', 'admin', '2023-08-17 09:38:15', 'admin', '2023-08-22 11:14:48', '接口日志菜单');
INSERT INTO `sys_menu` VALUES (2020, '接口日志查询', 2019, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'api:log:query', '#', 'admin', '2023-08-17 09:38:15', 'admin', '2023-08-17 10:37:12', '');
INSERT INTO `sys_menu` VALUES (2021, '接口日志新增', 2019, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'api:log:add', '#', 'admin', '2023-08-17 09:38:15', 'admin', '2023-08-17 10:37:21', '');
INSERT INTO `sys_menu` VALUES (2023, '接口日志删除', 2019, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'api:log:remove', '#', 'admin', '2023-08-17 09:38:15', 'admin', '2023-08-17 10:37:37', '');
INSERT INTO `sys_menu` VALUES (2024, '接口日志导出', 2019, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'api:log:export', '#', 'admin', '2023-08-17 09:38:15', 'admin', '2023-08-17 10:37:45', '');
INSERT INTO `sys_menu` VALUES (2025, '资产管理', 0, 2, 'assets', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'component', 'admin', '2023-08-23 11:45:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2026, '个人中心', 0, 1, 'my', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'user', 'admin', '2023-08-23 11:46:34', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2027, '资产分类', 2025, 2, 'category', 'asset/category/index', NULL, 1, 0, 'C', '0', '0', 'asset:category:list', '#', 'admin', '2023-08-23 13:49:29', 'admin', '2023-08-23 15:05:51', '资产分类菜单');
INSERT INTO `sys_menu` VALUES (2028, '资产分类查询', 2027, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:category:query', '#', 'admin', '2023-08-23 13:49:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2029, '资产分类新增', 2027, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:category:add', '#', 'admin', '2023-08-23 13:49:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2030, '资产分类修改', 2027, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:category:edit', '#', 'admin', '2023-08-23 13:49:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2031, '资产分类删除', 2027, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:category:remove', '#', 'admin', '2023-08-23 13:49:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2032, '资产分类导出', 2027, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:category:export', '#', 'admin', '2023-08-23 13:49:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2033, '仓库信息', 2057, 1, 'warehouse', 'asset/warehouse/index', NULL, 1, 0, 'C', '0', '0', 'asset:warehouse:list', '#', 'admin', '2023-08-23 15:01:57', 'admin', '2023-08-31 10:48:14', '仓库菜单');
INSERT INTO `sys_menu` VALUES (2034, '仓库查询', 2033, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:warehouse:query', '#', 'admin', '2023-08-23 15:01:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2035, '仓库新增', 2033, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:warehouse:add', '#', 'admin', '2023-08-23 15:01:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2036, '仓库修改', 2033, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:warehouse:edit', '#', 'admin', '2023-08-23 15:01:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2037, '仓库删除', 2033, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:warehouse:remove', '#', 'admin', '2023-08-23 15:01:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2038, '仓库导出', 2033, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:warehouse:export', '#', 'admin', '2023-08-23 15:01:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2039, '资产型号', 2025, 1, 'model', 'asset/model/index', NULL, 1, 0, 'C', '0', '0', 'asset:model:list', '#', 'admin', '2023-08-23 17:18:42', '', NULL, '资产型号菜单');
INSERT INTO `sys_menu` VALUES (2040, '资产型号查询', 2039, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:model:query', '#', 'admin', '2023-08-23 17:18:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2041, '资产型号新增', 2039, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:model:add', '#', 'admin', '2023-08-23 17:18:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2042, '资产型号修改', 2039, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:model:edit', '#', 'admin', '2023-08-23 17:18:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2043, '资产型号删除', 2039, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:model:remove', '#', 'admin', '2023-08-23 17:18:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2044, '资产型号导出', 2039, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:model:export', '#', 'admin', '2023-08-23 17:18:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2045, '资产信息', 2025, 1, 'info', 'asset/info/index', NULL, 1, 0, 'C', '0', '0', 'asset:info:list', '#', 'admin', '2023-08-28 11:05:52', 'admin', '2023-08-28 11:07:12', '资产信息菜单');
INSERT INTO `sys_menu` VALUES (2046, '资产信息查询', 2045, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:info:query', '#', 'admin', '2023-08-28 11:05:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2047, '资产信息新增', 2045, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:info:add', '#', 'admin', '2023-08-28 11:05:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2048, '资产信息修改', 2045, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:info:edit', '#', 'admin', '2023-08-28 11:05:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2049, '资产信息删除', 2045, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:info:remove', '#', 'admin', '2023-08-28 11:05:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2050, '资产信息导出', 2045, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:info:export', '#', 'admin', '2023-08-28 11:05:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2051, '采购管理', 2025, 1, 'purchase', 'asset/purchase/index', NULL, 1, 0, 'C', '0', '0', 'asset:purchase:list', '#', 'admin', '2023-08-29 16:14:06', 'admin', '2023-09-06 17:31:58', '采购申请菜单');
INSERT INTO `sys_menu` VALUES (2052, '采购申请查询', 2051, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:purchase:query', '#', 'admin', '2023-08-29 16:14:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2053, '采购申请新增', 2051, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:purchase:add', '#', 'admin', '2023-08-29 16:14:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2054, '采购申请修改', 2051, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:purchase:edit', '#', 'admin', '2023-08-29 16:14:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2055, '采购申请删除', 2051, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:purchase:remove', '#', 'admin', '2023-08-29 16:14:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2056, '采购申请导出', 2051, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:purchase:export', '#', 'admin', '2023-08-29 16:14:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2057, '仓库管理', 0, 2, 'store', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'table', 'admin', '2023-08-31 10:45:40', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2058, '入库管理', 2057, 1, 'in', 'asset/in/index', NULL, 1, 0, 'C', '0', '0', 'asset:in:list', '#', 'admin', '2023-08-31 10:49:01', 'admin', '2023-09-08 11:29:02', '入库记录菜单');
INSERT INTO `sys_menu` VALUES (2059, '入库记录查询', 2058, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:in:query', '#', 'admin', '2023-08-31 10:49:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2060, '入库记录新增', 2058, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:in:add', '#', 'admin', '2023-08-31 10:49:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2061, '入库记录修改', 2058, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:in:edit', '#', 'admin', '2023-08-31 10:49:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2062, '入库记录删除', 2058, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:in:remove', '#', 'admin', '2023-08-31 10:49:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2063, '入库记录导出', 2058, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:in:export', '#', 'admin', '2023-08-31 10:49:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2064, '出库管理', 2057, 1, 'out', 'asset/out/index', NULL, 1, 0, 'C', '0', '0', 'asset:out:list', '#', 'admin', '2023-08-31 14:49:06', 'admin', '2023-09-08 11:29:12', '出库记录菜单');
INSERT INTO `sys_menu` VALUES (2065, '出库记录查询', 2064, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:out:query', '#', 'admin', '2023-08-31 14:49:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2066, '出库记录新增', 2064, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:out:add', '#', 'admin', '2023-08-31 14:49:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2067, '出库记录修改', 2064, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:out:edit', '#', 'admin', '2023-08-31 14:49:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2068, '出库记录删除', 2064, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:out:remove', '#', 'admin', '2023-08-31 14:49:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2069, '出库记录导出', 2064, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:out:export', '#', 'admin', '2023-08-31 14:49:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2070, '资产维保', 2025, 1, 'maintain', 'asset/maintain/index', NULL, 1, 0, 'C', '0', '0', 'asset:maintain:list', '#', 'admin', '2023-08-31 17:42:57', '', NULL, '资产维保菜单');
INSERT INTO `sys_menu` VALUES (2071, '资产维保查询', 2070, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:maintain:query', '#', 'admin', '2023-08-31 17:42:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2072, '资产维保新增', 2070, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:maintain:add', '#', 'admin', '2023-08-31 17:42:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2073, '资产维保修改', 2070, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:maintain:edit', '#', 'admin', '2023-08-31 17:42:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2074, '资产维保删除', 2070, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:maintain:remove', '#', 'admin', '2023-08-31 17:42:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2075, '资产维保导出', 2070, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:maintain:export', '#', 'admin', '2023-08-31 17:42:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2076, '使用申请', 2025, 1, 'use', 'asset/use/index', NULL, 1, 0, 'C', '0', '0', 'asset:use:list', '#', 'admin', '2023-09-01 11:00:43', '', NULL, '使用申请菜单');
INSERT INTO `sys_menu` VALUES (2077, '使用申请查询', 2076, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:use:query', '#', 'admin', '2023-09-01 11:00:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2078, '使用申请新增', 2076, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:use:add', '#', 'admin', '2023-09-01 11:00:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2079, '使用申请修改', 2076, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:use:edit', '#', 'admin', '2023-09-01 11:00:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2080, '使用申请删除', 2076, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:use:remove', '#', 'admin', '2023-09-01 11:00:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2081, '使用申请导出', 2076, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:use:export', '#', 'admin', '2023-09-01 11:00:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2082, '资产盘点', 2057, 1, 'stock', 'asset/stock/index', NULL, 1, 0, 'C', '0', '0', 'asset:stock:list', '#', 'admin', '2023-09-01 11:39:39', '', NULL, '资产盘点菜单');
INSERT INTO `sys_menu` VALUES (2083, '资产盘点查询', 2082, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:stock:query', '#', 'admin', '2023-09-01 11:39:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2084, '资产盘点新增', 2082, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:stock:add', '#', 'admin', '2023-09-01 11:39:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2085, '资产盘点修改', 2082, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:stock:edit', '#', 'admin', '2023-09-01 11:39:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2086, '资产盘点删除', 2082, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:stock:remove', '#', 'admin', '2023-09-01 11:39:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2087, '资产盘点导出', 2082, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:stock:export', '#', 'admin', '2023-09-01 11:39:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2089, '审批任务', 2101, 1, 'task', 'flow/task/index', NULL, 1, 0, 'C', '0', '0', 'flow:task:list', '#', 'admin', '2023-09-04 11:00:24', 'admin', '2023-09-05 11:10:44', '审批任务菜单');
INSERT INTO `sys_menu` VALUES (2090, '审批任务查询', 2089, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'flow:task:query', '#', 'admin', '2023-09-04 11:00:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2091, '审批任务新增', 2089, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'flow:task:add', '#', 'admin', '2023-09-04 11:00:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2092, '审批任务修改', 2089, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'flow:task:edit', '#', 'admin', '2023-09-04 11:00:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2093, '审批任务删除', 2089, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'flow:task:remove', '#', 'admin', '2023-09-04 11:00:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2094, '审批任务导出', 2089, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'flow:task:export', '#', 'admin', '2023-09-04 11:00:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2095, '审批步骤', 2101, 1, 'step', 'flow/step/index', NULL, 1, 0, 'C', '0', '0', 'flow:step:list', '#', 'admin', '2023-09-04 11:01:29', 'admin', '2023-09-04 11:03:58', '审批步骤菜单');
INSERT INTO `sys_menu` VALUES (2096, '审批步骤查询', 2095, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'flow:step:query', '#', 'admin', '2023-09-04 11:01:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2097, '审批步骤新增', 2095, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'flow:step:add', '#', 'admin', '2023-09-04 11:01:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2098, '审批步骤修改', 2095, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'flow:step:edit', '#', 'admin', '2023-09-04 11:01:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2099, '审批步骤删除', 2095, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'flow:step:remove', '#', 'admin', '2023-09-04 11:01:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2100, '审批步骤导出', 2095, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'flow:step:export', '#', 'admin', '2023-09-04 11:01:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2101, '审批管理', 0, 2, 'flow', NULL, NULL, 1, 0, 'M', '0', '0', '', 'checkbox', 'admin', '2023-09-04 11:03:37', 'admin', '2023-09-04 14:03:16', '');
INSERT INTO `sys_menu` VALUES (2102, '我的资产', 2026, 1, 'asset', 'asset/info/my', NULL, 1, 0, 'C', '0', '0', 'asset:info:my', '#', 'admin', '2023-09-04 11:05:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2103, '我的维保', 2026, 2, 'maintain', 'asset/maintain/my', NULL, 1, 0, 'C', '0', '0', 'asset:maintain:my', '#', 'admin', '2023-09-04 11:05:34', 'admin', '2023-09-11 16:11:25', '');
INSERT INTO `sys_menu` VALUES (2104, '待我审批', 2026, 1, 'my', 'flow/step/my', NULL, 1, 0, 'C', '0', '0', 'flow:step:my', '#', 'admin', '2023-09-04 14:04:44', 'admin', '2023-09-08 11:28:13', '');
INSERT INTO `sys_menu` VALUES (2105, '采购申请', 2026, 3, 'purchase', 'asset/purchase/my', NULL, 1, 0, 'C', '0', '0', 'asset:purchase:my', '#', 'admin', '2023-09-05 17:41:04', 'admin', '2023-09-05 17:41:35', '');
INSERT INTO `sys_menu` VALUES (2106, '使用申请', 2026, 4, 'use', 'asset/use/my', NULL, 1, 0, 'C', '0', '0', 'asset:use:my', '#', 'admin', '2023-09-05 17:42:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2107, '资产出售', 2025, 1, 'sale', 'asset/sale/index', NULL, 1, 0, 'C', '0', '0', 'asset:sale:list', '#', 'admin', '2023-09-08 11:34:06', '', NULL, '资产出售菜单');
INSERT INTO `sys_menu` VALUES (2108, '资产出售查询', 2107, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:sale:query', '#', 'admin', '2023-09-08 11:34:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2109, '资产出售新增', 2107, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:sale:add', '#', 'admin', '2023-09-08 11:34:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2110, '资产出售修改', 2107, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:sale:edit', '#', 'admin', '2023-09-08 11:34:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2111, '资产出售删除', 2107, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:sale:remove', '#', 'admin', '2023-09-08 11:34:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2112, '资产出售导出', 2107, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'asset:sale:export', '#', 'admin', '2023-09-08 11:34:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2113, '我的盘点', 2026, 6, 'stock', 'asset/stock/my', NULL, 1, 0, 'C', '0', '0', 'asset:stock:my', '#', 'admin', '2023-09-12 09:53:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2114, '报表设计', 3, 1, 'chart', 'system/chart/index', NULL, 1, 0, 'C', '0', '0', 'system:chart:list', '#', 'admin', '2023-09-12 10:52:10', 'admin', '2023-09-12 11:12:29', '自定义报菜单');
INSERT INTO `sys_menu` VALUES (2115, '自定义报查询', 2114, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:chart:query', '#', 'admin', '2023-09-12 10:52:10', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2116, '自定义报新增', 2114, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:chart:add', '#', 'admin', '2023-09-12 10:52:10', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2117, '自定义报修改', 2114, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:chart:edit', '#', 'admin', '2023-09-12 10:52:10', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2118, '自定义报删除', 2114, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:chart:remove', '#', 'admin', '2023-09-12 10:52:10', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2119, '自定义报导出', 2114, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:chart:export', '#', 'admin', '2023-09-12 10:52:10', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2120, '资产分类统计', 2200, 1, 'asset_category', 'chart/index', '{\"id\":1}', 1, 0, 'C', '0', '0', 'chart:view:1', 'chart', 'admin', '2023-09-12 14:14:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2121, '资产柱状图', 2200, 9, 'chart_asset_category', 'chart/index', '{\"id\":2}', 1, 0, 'C', '0', '0', 'chart:view:2', 'chart', 'admin', '2023-09-12 16:24:09', '', NULL, '资产柱状图');
INSERT INTO `sys_menu` VALUES (2200, '查询统计', 0, 3, 'chart', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'chart', 'admin', '2023-09-04 10:57:02', '', NULL, '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '通知公告表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', 0xE696B0E78988E69CACE58685E5AEB9, '0', 'admin', '2023-08-22 10:13:30', '', NULL, '管理员');
INSERT INTO `sys_notice` VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', 0xE7BBB4E68AA4E58685E5AEB9, '0', 'admin', '2023-08-22 10:13:30', '', NULL, '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime NULL DEFAULT NULL COMMENT '操作时间',
  `cost_time` bigint(20) NULL DEFAULT 0 COMMENT '消耗时间',
  PRIMARY KEY (`oper_id`) USING BTREE,
  INDEX `idx_sys_oper_log_bt`(`business_type`) USING BTREE,
  INDEX `idx_sys_oper_log_s`(`status`) USING BTREE,
  INDEX `idx_sys_oper_log_ot`(`oper_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 294 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作日志记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (100, '个人信息', 2, 'com.ruoyi.web.controller.system.SysProfileController.updatePwd()', 'PUT', 1, 'admin', NULL, '/system/user/profile/updatePwd', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-22 11:06:34', 329);
INSERT INTO `sys_oper_log` VALUES (101, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{\"admin\":false,\"createTime\":\"2023-08-22 10:13:30\",\"dataScope\":\"2\",\"delFlag\":\"0\",\"deptCheckStrictly\":true,\"flag\":false,\"menuCheckStrictly\":true,\"menuIds\":[1,100,1000,1001,1002,1003,1004,1005,1006,101,1007,1008,1009,1010,1011,102,1012,1013,1014,1015,103,1016,1017,1018,1019,104,1020,1021,1022,1023,1024,105,1025,1026,1027,1028,1029,106,1030,1031,1032,1033,1034,107,1035,1036,1037,1038,108,500,1039,1040,1041,501,1042,1043,1044,1045,2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,114,3,115,116,1055,1056,1057,1058,1059,1060,117],\"params\":{},\"remark\":\"普通角色\",\"roleId\":2,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"roleSort\":2,\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-22 11:12:51', 35);
INSERT INTO `sys_oper_log` VALUES (102, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/4', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-22 11:12:58', 10);
INSERT INTO `sys_oper_log` VALUES (103, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"api/model/index\",\"createTime\":\"2023-08-15 16:58:48\",\"icon\":\"build\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2001,\"menuName\":\"数据模型\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2000,\"path\":\"model\",\"perms\":\"api:model:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-22 11:13:17', 10);
INSERT INTO `sys_oper_log` VALUES (104, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"api/event/index\",\"createTime\":\"2023-08-16 10:37:00\",\"icon\":\"druid\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2007,\"menuName\":\"触发事件\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2000,\"path\":\"event\",\"perms\":\"api:event:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-22 11:14:04', 6);
INSERT INTO `sys_oper_log` VALUES (105, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"api/sql/index\",\"createTime\":\"2023-08-16 11:27:46\",\"icon\":\"code\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2013,\"menuName\":\"数据脚本\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2000,\"path\":\"sql\",\"perms\":\"api:sql:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-22 11:14:10', 6);
INSERT INTO `sys_oper_log` VALUES (106, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"api/log/index\",\"createTime\":\"2023-08-17 09:38:15\",\"icon\":\"log\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2019,\"menuName\":\"接口日志\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2000,\"path\":\"log\",\"perms\":\"api:log:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-22 11:14:48', 5);
INSERT INTO `sys_oper_log` VALUES (107, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"接口事件类型\",\"dictType\":\"api_event_type\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-22 15:41:53', 22);
INSERT INTO `sys_oper_log` VALUES (108, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"新增前\",\"dictSort\":0,\"dictType\":\"api_event_type\",\"dictValue\":\"BI\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-22 15:42:10', 8);
INSERT INTO `sys_oper_log` VALUES (109, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-22 15:42:10\",\"default\":false,\"dictCode\":113,\"dictLabel\":\"新增前\",\"dictSort\":1,\"dictType\":\"api_event_type\",\"dictValue\":\"BI\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-22 15:45:29', 12);
INSERT INTO `sys_oper_log` VALUES (110, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-22 15:42:10\",\"default\":false,\"dictCode\":115,\"dictLabel\":\"更新前\",\"dictSort\":2,\"dictType\":\"api_event_type\",\"dictValue\":\"BU\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-22 15:45:33', 7);
INSERT INTO `sys_oper_log` VALUES (111, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-22 15:42:10\",\"default\":false,\"dictCode\":114,\"dictLabel\":\"新增后\",\"dictSort\":2,\"dictType\":\"api_event_type\",\"dictValue\":\"AI\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-22 15:45:46', 8);
INSERT INTO `sys_oper_log` VALUES (112, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-22 15:42:10\",\"default\":false,\"dictCode\":115,\"dictLabel\":\"更新前\",\"dictSort\":3,\"dictType\":\"api_event_type\",\"dictValue\":\"BU\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-22 15:45:54', 7);
INSERT INTO `sys_oper_log` VALUES (113, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-22 15:42:10\",\"default\":false,\"dictCode\":116,\"dictLabel\":\"更新后\",\"dictSort\":4,\"dictType\":\"api_event_type\",\"dictValue\":\"AU\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-22 15:46:00', 7);
INSERT INTO `sys_oper_log` VALUES (114, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-22 15:42:10\",\"default\":false,\"dictCode\":117,\"dictLabel\":\"删除前\",\"dictSort\":5,\"dictType\":\"api_event_type\",\"dictValue\":\"BD\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-22 15:46:06', 6);
INSERT INTO `sys_oper_log` VALUES (115, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-22 15:42:10\",\"default\":false,\"dictCode\":118,\"dictLabel\":\"删除后\",\"dictSort\":6,\"dictType\":\"api_event_type\",\"dictValue\":\"AD\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-22 15:46:12', 7);
INSERT INTO `sys_oper_log` VALUES (116, '个人信息', 2, 'com.ruoyi.web.controller.system.SysProfileController.updateProfile()', 'PUT', 1, 'admin', NULL, '/system/user/profile', '127.0.0.1', '内网IP', '{\"admin\":true,\"createBy\":\"admin\",\"createTime\":\"2023-08-22 10:13:30\",\"delFlag\":\"0\",\"dept\":{\"ancestors\":\"0,100,101\",\"children\":[],\"deptId\":103,\"deptName\":\"研发部门\",\"leader\":\"若依\",\"orderNum\":1,\"params\":{},\"parentId\":101,\"status\":\"0\"},\"email\":\"ry@163.com\",\"loginDate\":\"2023-08-22 11:06:21\",\"loginIp\":\"127.0.0.1\",\"nickName\":\"管理员\",\"params\":{},\"phonenumber\":\"15888888888\",\"remark\":\"管理员\",\"roles\":[{\"admin\":true,\"dataScope\":\"1\",\"deptCheckStrictly\":false,\"flag\":false,\"menuCheckStrictly\":false,\"params\":{},\"roleId\":1,\"roleKey\":\"admin\",\"roleName\":\"超级管理员\",\"roleSort\":1,\"status\":\"0\"}],\"sex\":\"1\",\"status\":\"0\",\"userId\":1,\"userName\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-22 16:21:53', 36);
INSERT INTO `sys_oper_log` VALUES (117, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"icon\":\"component\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"资产管理\",\"menuType\":\"M\",\"orderNum\":2,\"params\":{},\"parentId\":0,\"path\":\"assets\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 11:45:29', 18);
INSERT INTO `sys_oper_log` VALUES (118, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"icon\":\"user\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"个人中心\",\"menuType\":\"M\",\"orderNum\":1,\"params\":{},\"parentId\":0,\"path\":\"my\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 11:46:34', 42);
INSERT INTO `sys_oper_log` VALUES (119, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2023-08-22 10:13:30\",\"icon\":\"system\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1,\"menuName\":\"系统管理\",\"menuType\":\"M\",\"orderNum\":3,\"params\":{},\"parentId\":0,\"path\":\"system\",\"perms\":\"\",\"query\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 11:46:49', 7);
INSERT INTO `sys_oper_log` VALUES (120, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2023-08-22 10:13:30\",\"icon\":\"monitor\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2,\"menuName\":\"系统监控\",\"menuType\":\"M\",\"orderNum\":4,\"params\":{},\"parentId\":0,\"path\":\"monitor\",\"perms\":\"\",\"query\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 11:46:58', 6);
INSERT INTO `sys_oper_log` VALUES (121, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":\"asset_model\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 11:47:30', 75);
INSERT INTO `sys_oper_log` VALUES (122, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":\"asset_category,asset_info\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 11:47:59', 72);
INSERT INTO `sys_oper_log` VALUES (123, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"资产类别\",\"dictType\":\"assets_category_type\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:39:56', 8);
INSERT INTO `sys_oper_log` VALUES (124, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"固定资产\",\"dictSort\":0,\"dictType\":\"assets_category_type\",\"dictValue\":\"1\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:40:19', 9);
INSERT INTO `sys_oper_log` VALUES (125, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"日常耗材\",\"dictSort\":2,\"dictType\":\"assets_category_type\",\"dictValue\":\"2\",\"listClass\":\"info\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:40:32', 7);
INSERT INTO `sys_oper_log` VALUES (126, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-23 13:40:19\",\"default\":false,\"dictCode\":119,\"dictLabel\":\"固定资产\",\"dictSort\":0,\"dictType\":\"assets_category_type\",\"dictValue\":\"1\",\"isDefault\":\"N\",\"listClass\":\"primary\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:40:38', 7);
INSERT INTO `sys_oper_log` VALUES (127, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"无形资产\",\"dictSort\":0,\"dictType\":\"assets_category_type\",\"dictValue\":\"3\",\"listClass\":\"warning\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:40:52', 7);
INSERT INTO `sys_oper_log` VALUES (128, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-23 13:40:19\",\"default\":false,\"dictCode\":119,\"dictLabel\":\"固定资产\",\"dictSort\":0,\"dictType\":\"assets_category_type\",\"dictValue\":\"1\",\"isDefault\":\"N\",\"listClass\":\"success\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:40:59', 7);
INSERT INTO `sys_oper_log` VALUES (129, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"资产状态\",\"dictType\":\"assets_status\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:41:21', 7);
INSERT INTO `sys_oper_log` VALUES (130, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"闲置\",\"dictSort\":0,\"dictType\":\"assets_status\",\"dictValue\":\"0\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:41:34', 15);
INSERT INTO `sys_oper_log` VALUES (131, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-23 13:41:34\",\"default\":false,\"dictCode\":122,\"dictLabel\":\"闲置\",\"dictSort\":0,\"dictType\":\"assets_status\",\"dictValue\":\"1\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:41:43', 6);
INSERT INTO `sys_oper_log` VALUES (132, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"在用\",\"dictSort\":1,\"dictType\":\"assets_status\",\"dictValue\":\"1\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:41:52', 8);
INSERT INTO `sys_oper_log` VALUES (133, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-23 13:41:52\",\"default\":false,\"dictCode\":123,\"dictLabel\":\"在用\",\"dictSort\":2,\"dictType\":\"assets_status\",\"dictValue\":\"2\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:41:59', 6);
INSERT INTO `sys_oper_log` VALUES (134, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-23 13:41:34\",\"default\":false,\"dictCode\":122,\"dictLabel\":\"闲置\",\"dictSort\":2,\"dictType\":\"assets_status\",\"dictValue\":\"1\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:42:03', 7);
INSERT INTO `sys_oper_log` VALUES (135, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"损坏\",\"dictSort\":3,\"dictType\":\"assets_status\",\"dictValue\":\"3\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:42:15', 6);
INSERT INTO `sys_oper_log` VALUES (136, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"cssClass\":\"\",\"default\":false,\"dictLabel\":\"维修\",\"dictSort\":4,\"dictType\":\"assets_status\",\"dictValue\":\"4\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:42:26', 6);
INSERT INTO `sys_oper_log` VALUES (137, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"报废\",\"dictSort\":5,\"dictType\":\"assets_status\",\"dictValue\":\"5\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:42:36', 6);
INSERT INTO `sys_oper_log` VALUES (138, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"申请状态\",\"dictType\":\"flow_apply_status\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:43:19', 7);
INSERT INTO `sys_oper_log` VALUES (139, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"草稿\",\"dictSort\":1,\"dictType\":\"flow_apply_status\",\"dictValue\":\"1\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:43:32', 5);
INSERT INTO `sys_oper_log` VALUES (140, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"审核中\",\"dictSort\":2,\"dictType\":\"flow_apply_status\",\"dictValue\":\"2\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:43:40', 7);
INSERT INTO `sys_oper_log` VALUES (141, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"审核通过\",\"dictSort\":3,\"dictType\":\"flow_apply_status\",\"dictValue\":\"3\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:44:32', 7);
INSERT INTO `sys_oper_log` VALUES (142, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"审核未通过\",\"dictSort\":0,\"dictType\":\"flow_apply_status\",\"dictValue\":\"4\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:44:42', 7);
INSERT INTO `sys_oper_log` VALUES (143, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-23 13:44:42\",\"default\":false,\"dictCode\":130,\"dictLabel\":\"审核未通过\",\"dictSort\":4,\"dictType\":\"flow_apply_status\",\"dictValue\":\"4\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:44:48', 5);
INSERT INTO `sys_oper_log` VALUES (144, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"已撤回\",\"dictSort\":5,\"dictType\":\"flow_apply_status\",\"dictValue\":\"5\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:44:58', 6);
INSERT INTO `sys_oper_log` VALUES (145, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"审批状态\",\"dictType\":\"flow_check_status\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:45:17', 6);
INSERT INTO `sys_oper_log` VALUES (146, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"待审核\",\"dictSort\":1,\"dictType\":\"flow_check_status\",\"dictValue\":\"1\",\"listClass\":\"info\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:45:35', 6);
INSERT INTO `sys_oper_log` VALUES (147, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"审核通过\",\"dictSort\":0,\"dictType\":\"flow_check_status\",\"dictValue\":\"2\",\"listClass\":\"success\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:45:49', 8);
INSERT INTO `sys_oper_log` VALUES (148, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-23 13:45:49\",\"default\":false,\"dictCode\":133,\"dictLabel\":\"审核通过\",\"dictSort\":2,\"dictType\":\"flow_check_status\",\"dictValue\":\"2\",\"isDefault\":\"N\",\"listClass\":\"success\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:45:55', 5);
INSERT INTO `sys_oper_log` VALUES (149, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"通过不通过\",\"dictSort\":3,\"dictType\":\"flow_check_status\",\"dictValue\":\"3\",\"listClass\":\"warning\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:46:08', 6);
INSERT INTO `sys_oper_log` VALUES (150, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"category\",\"className\":\"AssetCategory\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":17,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Title\",\"columnComment\":\"分类名称\",\"columnId\":18,\"columnName\":\"title\",\"columnType\":\"varchar(100)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"title\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Pid\",\"columnComment\":\"上级\",\"columnId\":19,\"columnName\":\"pid\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"pid\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Type\",\"columnComment\":\"分类类别\",\"columnId\":20,\"columnName\":\"type\",\"columnType\":\"char(1)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"assets_category_type\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"type\",\"javaType\":\"String\",\"list\":true,\"params', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:48:11', 23);
INSERT INTO `sys_oper_log` VALUES (151, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"category\",\"className\":\"AssetCategory\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":17,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"updateTime\":\"2023-08-23 13:48:11\",\"usableColumn\":false},{\"capJavaField\":\"Title\",\"columnComment\":\"分类名称\",\"columnId\":18,\"columnName\":\"title\",\"columnType\":\"varchar(100)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"title\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"updateTime\":\"2023-08-23 13:48:11\",\"usableColumn\":false},{\"capJavaField\":\"Pid\",\"columnComment\":\"上级\",\"columnId\":19,\"columnName\":\"pid\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"pid\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"updateTime\":\"2023-08-23 13:48:11\",\"usableColumn\":false},{\"capJavaField\":\"Type\",\"columnComment\":\"分类类别\",\"columnId\":20,\"columnName\":\"type\",\"columnType\":\"char(1)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"assets_category_type\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isIns', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:50:38', 15);
INSERT INTO `sys_oper_log` VALUES (152, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/asset_category', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:50:42', 24);
INSERT INTO `sys_oper_log` VALUES (153, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"category\",\"className\":\"AssetCategory\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":17,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"updateTime\":\"2023-08-23 13:50:42\",\"usableColumn\":false},{\"capJavaField\":\"Name\",\"columnComment\":\"分类名称\",\"columnId\":44,\"columnName\":\"name\",\"columnType\":\"varchar(50)\",\"createBy\":\"\",\"createTime\":\"2023-08-23 13:50:42\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Pid\",\"columnComment\":\"上级\",\"columnId\":19,\"columnName\":\"pid\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"pid\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"updateTime\":\"2023-08-23 13:50:42\",\"usableColumn\":false},{\"capJavaField\":\"Type\",\"columnComment\":\"分类类别\",\"columnId\":20,\"columnName\":\"type\",\"columnType\":\"char(1)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"assets_category_type\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 13:50:49', 26);
INSERT INTO `sys_oper_log` VALUES (154, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":\"asset_warehouse\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 14:57:34', 73);
INSERT INTO `sys_oper_log` VALUES (155, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"禁用启动状态\",\"dictType\":\"ext_status\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 14:59:13', 8);
INSERT INTO `sys_oper_log` VALUES (156, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"启用\",\"dictSort\":0,\"dictType\":\"ext_status\",\"dictValue\":\"1\",\"listClass\":\"success\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 14:59:31', 8);
INSERT INTO `sys_oper_log` VALUES (157, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"禁用\",\"dictSort\":0,\"dictType\":\"ext_status\",\"dictValue\":\"2\",\"listClass\":\"danger\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 14:59:40', 7);
INSERT INTO `sys_oper_log` VALUES (158, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"warehouse\",\"className\":\"AssetWarehouse\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":45,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 14:57:34\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":4,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Name\",\"columnComment\":\"仓库名称\",\"columnId\":46,\"columnName\":\"name\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 14:57:34\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":4,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Address\",\"columnComment\":\"地址\",\"columnId\":47,\"columnName\":\"address\",\"columnType\":\"varchar(200)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 14:57:34\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"address\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":4,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"UserId\",\"columnComment\":\"负责人\",\"columnId\":48,\"columnName\":\"user_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 14:57:34\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"userId\",\"javaType\":\"Long\",', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 15:00:21', 32);
INSERT INTO `sys_oper_log` VALUES (159, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"warehouse\",\"className\":\"AssetWarehouse\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":45,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 14:57:34\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":4,\"updateBy\":\"\",\"updateTime\":\"2023-08-23 15:00:21\",\"usableColumn\":false},{\"capJavaField\":\"Name\",\"columnComment\":\"仓库名称\",\"columnId\":46,\"columnName\":\"name\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 14:57:34\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":4,\"updateBy\":\"\",\"updateTime\":\"2023-08-23 15:00:21\",\"usableColumn\":false},{\"capJavaField\":\"Address\",\"columnComment\":\"地址\",\"columnId\":47,\"columnName\":\"address\",\"columnType\":\"varchar(200)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 14:57:34\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"address\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":4,\"updateBy\":\"\",\"updateTime\":\"2023-08-23 15:00:21\",\"usableColumn\":false},{\"capJavaField\":\"UserId\",\"columnComment\":\"负责人\",\"columnId\":48,\"columnName\":\"user_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 14:57:34\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isI', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 15:00:42', 18);
INSERT INTO `sys_oper_log` VALUES (160, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/asset_warehouse', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 15:01:12', 22);
INSERT INTO `sys_oper_log` VALUES (161, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"assets/warehouse/index\",\"createTime\":\"2023-08-23 15:01:57\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2033,\"menuName\":\"仓库\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2025,\"path\":\"warehouse\",\"perms\":\"assets:warehouse:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 15:05:26', 11);
INSERT INTO `sys_oper_log` VALUES (162, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"assets/warehouse/index\",\"createTime\":\"2023-08-23 15:01:57\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2033,\"menuName\":\"仓库管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2025,\"path\":\"warehouse\",\"perms\":\"assets:warehouse:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 15:05:45', 5);
INSERT INTO `sys_oper_log` VALUES (163, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"assets/category/index\",\"createTime\":\"2023-08-23 13:49:29\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2027,\"menuName\":\"资产分类\",\"menuType\":\"C\",\"orderNum\":2,\"params\":{},\"parentId\":2025,\"path\":\"category\",\"perms\":\"assets:category:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 15:05:51', 5);
INSERT INTO `sys_oper_log` VALUES (164, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"model\",\"className\":\"AssetModel\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":1,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:30\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"CateId\",\"columnComment\":\"资产分类\",\"columnId\":2,\"columnName\":\"cate_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:30\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"cateId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Name\",\"columnComment\":\"型号名称\",\"columnId\":3,\"columnName\":\"name\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:30\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Factory\",\"columnComment\":\"厂商\",\"columnId\":4,\"columnName\":\"factory\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:30\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"factory\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-23 17:17:07', 70);
INSERT INTO `sys_oper_log` VALUES (165, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/asset_info', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-28 11:04:25', 96);
INSERT INTO `sys_oper_log` VALUES (166, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"info\",\"className\":\"AssetInfo\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":24,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"updateTime\":\"2023-08-28 11:04:25\",\"usableColumn\":false},{\"capJavaField\":\"CateId\",\"columnComment\":\"资产分类\",\"columnId\":25,\"columnName\":\"cate_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"cateId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"updateTime\":\"2023-08-28 11:04:25\",\"usableColumn\":false},{\"capJavaField\":\"ModelId\",\"columnComment\":\"型号\",\"columnId\":26,\"columnName\":\"model_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"modelId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"updateTime\":\"2023-08-28 11:04:25\",\"usableColumn\":false},{\"capJavaField\":\"Num\",\"columnComment\":\"资产编号\",\"columnId\":27,\"columnName\":\"num\",\"columnType\":\"varchar(200)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-28 11:05:23', 59);
INSERT INTO `sys_oper_log` VALUES (167, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"assets/info/index\",\"createTime\":\"2023-08-28 11:05:52\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2045,\"menuName\":\"资产信息\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2025,\"path\":\"info\",\"perms\":\"asset:info:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-28 11:07:12', 11);
INSERT INTO `sys_oper_log` VALUES (168, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"info\",\"className\":\"AssetInfo\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":24,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"updateTime\":\"2023-08-28 11:05:23\",\"usableColumn\":false},{\"capJavaField\":\"CateId\",\"columnComment\":\"资产分类\",\"columnId\":25,\"columnName\":\"cate_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"cateId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"updateTime\":\"2023-08-28 11:05:23\",\"usableColumn\":false},{\"capJavaField\":\"ModelId\",\"columnComment\":\"型号\",\"columnId\":26,\"columnName\":\"model_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"modelId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"updateTime\":\"2023-08-28 11:05:23\",\"usableColumn\":false},{\"capJavaField\":\"Num\",\"columnComment\":\"资产编号\",\"columnId\":27,\"columnName\":\"num\",\"columnType\":\"varchar(200)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-28 11:09:15', 40);
INSERT INTO `sys_oper_log` VALUES (169, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"info\",\"className\":\"AssetInfo\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":24,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"updateTime\":\"2023-08-28 11:09:15\",\"usableColumn\":false},{\"capJavaField\":\"CateId\",\"columnComment\":\"资产分类\",\"columnId\":25,\"columnName\":\"cate_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"cateId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"updateTime\":\"2023-08-28 11:09:15\",\"usableColumn\":false},{\"capJavaField\":\"ModelId\",\"columnComment\":\"型号\",\"columnId\":26,\"columnName\":\"model_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"modelId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"updateTime\":\"2023-08-28 11:09:15\",\"usableColumn\":false},{\"capJavaField\":\"Num\",\"columnComment\":\"资产编号\",\"columnId\":27,\"columnName\":\"num\",\"columnType\":\"varchar(200)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-28 11:19:18', 40);
INSERT INTO `sys_oper_log` VALUES (170, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"info\",\"className\":\"AssetInfo\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":24,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"updateTime\":\"2023-08-28 11:19:18\",\"usableColumn\":false},{\"capJavaField\":\"CateId\",\"columnComment\":\"资产分类\",\"columnId\":25,\"columnName\":\"cate_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"cateId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"updateTime\":\"2023-08-28 11:19:18\",\"usableColumn\":false},{\"capJavaField\":\"ModelId\",\"columnComment\":\"型号\",\"columnId\":26,\"columnName\":\"model_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"modelId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"updateTime\":\"2023-08-28 11:19:18\",\"usableColumn\":false},{\"capJavaField\":\"Num\",\"columnComment\":\"资产编号\",\"columnId\":27,\"columnName\":\"num\",\"columnType\":\"varchar(200)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-23 11:47:59\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-28 17:33:51', 79);
INSERT INTO `sys_oper_log` VALUES (171, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/asset_info', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-28 17:33:56', 74);
INSERT INTO `sys_oper_log` VALUES (172, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":\"asset_purchase_detail,asset_purchase\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-29 16:10:08', 85);
INSERT INTO `sys_oper_log` VALUES (173, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"purchase\",\"className\":\"AssetPurchase\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":53,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-29 16:10:07\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Num\",\"columnComment\":\"采购编号\",\"columnId\":54,\"columnName\":\"num\",\"columnType\":\"varchar(100)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-29 16:10:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"num\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Reason\",\"columnComment\":\"采购原由\",\"columnId\":55,\"columnName\":\"reason\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-29 16:10:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"reason\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Comment\",\"columnComment\":\"采购说明\",\"columnId\":56,\"columnName\":\"comment\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-29 16:10:08\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"comment\",\"javaType\":\"String\",\"list\":true,\"p', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-29 16:11:14', 31);
INSERT INTO `sys_oper_log` VALUES (174, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"detail\",\"className\":\"AssetPurchaseDetail\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":63,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-29 16:10:08\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":6,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"PurchaseId\",\"columnComment\":\"采购单\",\"columnId\":64,\"columnName\":\"purchase_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-29 16:10:08\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"purchaseId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":6,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"CateId\",\"columnComment\":\"资产分类\",\"columnId\":65,\"columnName\":\"cate_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-29 16:10:08\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"cateId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":6,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"ModelId\",\"columnComment\":\"型号\",\"columnId\":66,\"columnName\":\"model_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-29 16:10:08\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"modelId\",\"javaType\":\"Long\",\"list\":tr', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-29 16:11:55', 27);
INSERT INTO `sys_oper_log` VALUES (175, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"purchase\",\"className\":\"AssetPurchase\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":53,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-29 16:10:07\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"updateTime\":\"2023-08-29 16:11:13\",\"usableColumn\":false},{\"capJavaField\":\"Num\",\"columnComment\":\"采购编号\",\"columnId\":54,\"columnName\":\"num\",\"columnType\":\"varchar(100)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-29 16:10:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"num\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"updateTime\":\"2023-08-29 16:11:13\",\"usableColumn\":false},{\"capJavaField\":\"Reason\",\"columnComment\":\"采购原由\",\"columnId\":55,\"columnName\":\"reason\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-29 16:10:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"reason\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"updateTime\":\"2023-08-29 16:11:13\",\"usableColumn\":false},{\"capJavaField\":\"Comment\",\"columnComment\":\"采购说明\",\"columnId\":56,\"columnName\":\"comment\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-29 16:10:08\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isI', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-29 16:12:32', 21);
INSERT INTO `sys_oper_log` VALUES (176, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-23 13:43:40\",\"default\":false,\"dictCode\":128,\"dictLabel\":\"已提交\",\"dictSort\":2,\"dictType\":\"flow_apply_status\",\"dictValue\":\"2\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-30 15:03:30', 14);
INSERT INTO `sys_oper_log` VALUES (177, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-23 13:44:32\",\"default\":false,\"dictCode\":129,\"dictLabel\":\"已完成\",\"dictSort\":3,\"dictType\":\"flow_apply_status\",\"dictValue\":\"3\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-30 15:03:44', 7);
INSERT INTO `sys_oper_log` VALUES (178, '字典类型', 3, 'com.ruoyi.web.controller.system.SysDictDataController.remove()', 'DELETE', 1, 'admin', NULL, '/system/dict/data/130', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-30 15:03:49', 9);
INSERT INTO `sys_oper_log` VALUES (179, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-23 13:44:58\",\"default\":false,\"dictCode\":131,\"dictLabel\":\"已撤回\",\"dictSort\":4,\"dictType\":\"flow_apply_status\",\"dictValue\":\"4\",\"isDefault\":\"N\",\"listClass\":\"danger\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-30 15:04:02', 7);
INSERT INTO `sys_oper_log` VALUES (180, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-23 13:43:32\",\"default\":false,\"dictCode\":127,\"dictLabel\":\"草稿\",\"dictSort\":1,\"dictType\":\"flow_apply_status\",\"dictValue\":\"1\",\"isDefault\":\"N\",\"listClass\":\"info\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-30 15:04:08', 7);
INSERT INTO `sys_oper_log` VALUES (181, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-23 13:43:40\",\"default\":false,\"dictCode\":128,\"dictLabel\":\"已提交\",\"dictSort\":2,\"dictType\":\"flow_apply_status\",\"dictValue\":\"2\",\"isDefault\":\"N\",\"listClass\":\"primary\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-30 15:04:15', 7);
INSERT INTO `sys_oper_log` VALUES (182, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-23 13:44:32\",\"default\":false,\"dictCode\":129,\"dictLabel\":\"已完成\",\"dictSort\":3,\"dictType\":\"flow_apply_status\",\"dictValue\":\"3\",\"isDefault\":\"N\",\"listClass\":\"success\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-30 15:04:20', 6);
INSERT INTO `sys_oper_log` VALUES (183, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":\"asset_out,asset_in,asset_maintain\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 10:43:41', 124);
INSERT INTO `sys_oper_log` VALUES (184, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"in\",\"className\":\"AssetIn\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":69,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":7,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Type\",\"columnComment\":\"原由：采购、归还\",\"columnId\":70,\"columnName\":\"type\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"type\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":7,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"CateId\",\"columnComment\":\"资产分类\",\"columnId\":71,\"columnName\":\"cate_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"cateId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":7,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"AssetId\",\"columnComment\":\"资产ID\",\"columnId\":72,\"columnName\":\"asset_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"assetId\",\"javaType\":\"Long\",\"list\":true,\"params\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 10:44:13', 30);
INSERT INTO `sys_oper_log` VALUES (185, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"icon\":\"table\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"仓库管理\",\"menuType\":\"M\",\"orderNum\":2,\"params\":{},\"parentId\":0,\"path\":\"store\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 10:45:40', 11);
INSERT INTO `sys_oper_log` VALUES (186, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"入库类型\",\"dictType\":\"asset_in_type\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 10:46:08', 9);
INSERT INTO `sys_oper_log` VALUES (187, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"采购\",\"dictSort\":1,\"dictType\":\"asset_in_type\",\"dictValue\":\"1\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 10:46:19', 7);
INSERT INTO `sys_oper_log` VALUES (188, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"归还\",\"dictSort\":2,\"dictType\":\"asset_in_type\",\"dictValue\":\"2\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 10:46:25', 8);
INSERT INTO `sys_oper_log` VALUES (189, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"in\",\"className\":\"AssetIn\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":69,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":7,\"updateBy\":\"\",\"updateTime\":\"2023-08-31 10:44:13\",\"usableColumn\":false},{\"capJavaField\":\"Type\",\"columnComment\":\"原由\",\"columnId\":70,\"columnName\":\"type\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"asset_in_type\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"type\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":7,\"updateBy\":\"\",\"updateTime\":\"2023-08-31 10:44:13\",\"usableColumn\":false},{\"capJavaField\":\"CateId\",\"columnComment\":\"资产分类\",\"columnId\":71,\"columnName\":\"cate_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"cateId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":7,\"updateBy\":\"\",\"updateTime\":\"2023-08-31 10:44:13\",\"usableColumn\":false},{\"capJavaField\":\"AssetId\",\"columnComment\":\"资产ID\",\"columnId\":72,\"columnName\":\"asset_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"is', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 10:47:24', 23);
INSERT INTO `sys_oper_log` VALUES (190, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/asset_in', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 10:47:29', 27);
INSERT INTO `sys_oper_log` VALUES (191, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"in\",\"className\":\"AssetIn\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":69,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":7,\"updateBy\":\"\",\"updateTime\":\"2023-08-31 10:47:29\",\"usableColumn\":false},{\"capJavaField\":\"Type\",\"columnComment\":\"原由：采购、归还\",\"columnId\":70,\"columnName\":\"type\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"asset_in_type\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"type\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":7,\"updateBy\":\"\",\"updateTime\":\"2023-08-31 10:47:29\",\"usableColumn\":false},{\"capJavaField\":\"CateId\",\"columnComment\":\"资产分类\",\"columnId\":71,\"columnName\":\"cate_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"cateId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":7,\"updateBy\":\"\",\"updateTime\":\"2023-08-31 10:47:29\",\"usableColumn\":false},{\"capJavaField\":\"AssetId\",\"columnComment\":\"资产ID\",\"columnId\":72,\"columnName\":\"asset_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 10:47:45', 21);
INSERT INTO `sys_oper_log` VALUES (192, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"asset/warehouse/index\",\"createTime\":\"2023-08-23 15:01:57\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2033,\"menuName\":\"仓库信息\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2057,\"path\":\"warehouse\",\"perms\":\"asset:warehouse:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 10:48:15', 17);
INSERT INTO `sys_oper_log` VALUES (193, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-23 13:41:34\",\"default\":false,\"dictCode\":122,\"dictLabel\":\"闲置\",\"dictSort\":1,\"dictType\":\"assets_status\",\"dictValue\":\"1\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 14:15:01', 7);
INSERT INTO `sys_oper_log` VALUES (194, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/asset_out', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 14:45:40', 28);
INSERT INTO `sys_oper_log` VALUES (195, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"出库类型\",\"dictType\":\"asset_out_type\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 14:46:15', 5);
INSERT INTO `sys_oper_log` VALUES (196, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"借用\",\"dictSort\":1,\"dictType\":\"asset_out_type\",\"dictValue\":\"1\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 14:46:37', 5);
INSERT INTO `sys_oper_log` VALUES (197, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"报废\",\"dictSort\":2,\"dictType\":\"asset_out_type\",\"dictValue\":\"2\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 14:46:55', 6);
INSERT INTO `sys_oper_log` VALUES (198, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"出售\",\"dictSort\":3,\"dictType\":\"asset_out_type\",\"dictValue\":\"3\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 14:47:12', 6);
INSERT INTO `sys_oper_log` VALUES (199, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"out\",\"className\":\"AssetOut\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":89,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":9,\"updateBy\":\"\",\"updateTime\":\"2023-08-31 14:45:40\",\"usableColumn\":false},{\"capJavaField\":\"Type\",\"columnComment\":\"出库类别\",\"columnId\":90,\"columnName\":\"type\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"asset_out_type\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"type\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":9,\"updateBy\":\"\",\"updateTime\":\"2023-08-31 14:45:40\",\"usableColumn\":false},{\"capJavaField\":\"CateId\",\"columnComment\":\"资产分类\",\"columnId\":95,\"columnName\":\"cate_id\",\"columnType\":\"int(11)\",\"createBy\":\"\",\"createTime\":\"2023-08-31 14:45:40\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"cateId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":9,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"AssetId\",\"columnComment\":\"资产编号\",\"columnId\":91,\"columnName\":\"asset_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 14:48:52', 13);
INSERT INTO `sys_oper_log` VALUES (200, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"已出售\",\"dictSort\":6,\"dictType\":\"assets_status\",\"dictValue\":\"6\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 15:15:21', 6);
INSERT INTO `sys_oper_log` VALUES (201, '参数管理', 1, 'com.ruoyi.web.controller.system.SysConfigController.add()', 'POST', 1, 'admin', NULL, '/system/config', '127.0.0.1', '内网IP', '{\"configKey\":\"asset.qr.logo\",\"configName\":\"二维码LOGO\",\"configType\":\"Y\",\"configValue\":\"https://v2.cn.vuejs.org/images/logo.svg\",\"createBy\":\"admin\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 17:05:32', 7);
INSERT INTO `sys_oper_log` VALUES (202, '参数管理', 2, 'com.ruoyi.web.controller.system.SysConfigController.edit()', 'PUT', 1, 'admin', NULL, '/system/config', '127.0.0.1', '内网IP', '{\"configId\":7,\"configKey\":\"asset.qr.logo\",\"configName\":\"二维码LOGO\",\"configType\":\"Y\",\"configValue\":\"https://img95.699pic.com/xsj/0q/re/6b.jpg\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 17:05:32\",\"params\":{},\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 17:31:45', 8);
INSERT INTO `sys_oper_log` VALUES (203, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/asset_maintain', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 17:38:35', 28);
INSERT INTO `sys_oper_log` VALUES (204, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"maintain\",\"className\":\"AssetMaintain\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":78,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":8,\"updateBy\":\"\",\"updateTime\":\"2023-08-31 17:38:35\",\"usableColumn\":false},{\"capJavaField\":\"CateId\",\"columnComment\":\"资产分类\",\"columnId\":79,\"columnName\":\"cate_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"cateId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":8,\"updateBy\":\"\",\"updateTime\":\"2023-08-31 17:38:35\",\"usableColumn\":false},{\"capJavaField\":\"AssetId\",\"columnComment\":\"资产ID\",\"columnId\":80,\"columnName\":\"asset_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"assetId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":8,\"updateBy\":\"\",\"updateTime\":\"2023-08-31 17:38:35\",\"usableColumn\":false},{\"capJavaField\":\"Type\",\"columnComment\":\"维保类型\",\"columnId\":81,\"columnName\":\"type\",\"columnType\":\"char(1)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 17:39:09', 25);
INSERT INTO `sys_oper_log` VALUES (205, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"维保类型\",\"dictType\":\"asset_maintain_type\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 17:39:44', 5);
INSERT INTO `sys_oper_log` VALUES (206, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"维保状态\",\"dictType\":\"asset_maintain_status\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 17:40:01', 5);
INSERT INTO `sys_oper_log` VALUES (207, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"维修\",\"dictSort\":1,\"dictType\":\"asset_maintain_type\",\"dictValue\":\"1\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 17:40:17', 6);
INSERT INTO `sys_oper_log` VALUES (208, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-31 17:40:17\",\"default\":false,\"dictCode\":143,\"dictLabel\":\"维修\",\"dictSort\":1,\"dictType\":\"asset_maintain_type\",\"dictValue\":\"1\",\"isDefault\":\"N\",\"listClass\":\"warning\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 17:40:23', 14);
INSERT INTO `sys_oper_log` VALUES (209, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"保养\",\"dictSort\":2,\"dictType\":\"asset_maintain_type\",\"dictValue\":\"2\",\"listClass\":\"warning\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 17:40:33', 5);
INSERT INTO `sys_oper_log` VALUES (210, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-31 17:40:17\",\"default\":false,\"dictCode\":143,\"dictLabel\":\"维修\",\"dictSort\":1,\"dictType\":\"asset_maintain_type\",\"dictValue\":\"1\",\"isDefault\":\"N\",\"listClass\":\"danger\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 17:40:39', 5);
INSERT INTO `sys_oper_log` VALUES (211, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"未开始\",\"dictSort\":1,\"dictType\":\"asset_maintain_status\",\"dictValue\":\"1\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 17:41:06', 5);
INSERT INTO `sys_oper_log` VALUES (212, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"进行中\",\"dictSort\":2,\"dictType\":\"asset_maintain_status\",\"dictValue\":\"2\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 17:41:16', 5);
INSERT INTO `sys_oper_log` VALUES (213, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"已完好\",\"dictSort\":3,\"dictType\":\"asset_maintain_status\",\"dictValue\":\"3\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 17:41:57', 7);
INSERT INTO `sys_oper_log` VALUES (214, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"无法修复\",\"dictSort\":4,\"dictType\":\"asset_maintain_status\",\"dictValue\":\"4\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 17:42:14', 6);
INSERT INTO `sys_oper_log` VALUES (215, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"maintain\",\"className\":\"AssetMaintain\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":78,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":8,\"updateBy\":\"\",\"updateTime\":\"2023-08-31 17:39:09\",\"usableColumn\":false},{\"capJavaField\":\"CateId\",\"columnComment\":\"资产分类\",\"columnId\":79,\"columnName\":\"cate_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"cateId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":8,\"updateBy\":\"\",\"updateTime\":\"2023-08-31 17:39:09\",\"usableColumn\":false},{\"capJavaField\":\"AssetId\",\"columnComment\":\"资产ID\",\"columnId\":80,\"columnName\":\"asset_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"assetId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":8,\"updateBy\":\"\",\"updateTime\":\"2023-08-31 17:39:09\",\"usableColumn\":false},{\"capJavaField\":\"Type\",\"columnComment\":\"维保类型\",\"columnId\":81,\"columnName\":\"type\",\"columnType\":\"char(1)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"asset_maintain_type\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 17:42:40', 22);
INSERT INTO `sys_oper_log` VALUES (216, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"maintain\",\"className\":\"AssetMaintain\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":78,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":8,\"updateBy\":\"\",\"updateTime\":\"2023-08-31 17:42:40\",\"usableColumn\":false},{\"capJavaField\":\"CateId\",\"columnComment\":\"资产分类\",\"columnId\":79,\"columnName\":\"cate_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"cateId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":8,\"updateBy\":\"\",\"updateTime\":\"2023-08-31 17:42:40\",\"usableColumn\":false},{\"capJavaField\":\"AssetId\",\"columnComment\":\"资产ID\",\"columnId\":80,\"columnName\":\"asset_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"assetId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":8,\"updateBy\":\"\",\"updateTime\":\"2023-08-31 17:42:40\",\"usableColumn\":false},{\"capJavaField\":\"Type\",\"columnComment\":\"维保类型\",\"columnId\":81,\"columnName\":\"type\",\"columnType\":\"char(1)\",\"createBy\":\"admin\",\"createTime\":\"2023-08-31 10:43:41\",\"dictType\":\"asset_maintain_type\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-08-31 17:45:32', 20);
INSERT INTO `sys_oper_log` VALUES (217, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":\"asset_stock,asset_stock_detail\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-01 10:55:48', 98);
INSERT INTO `sys_oper_log` VALUES (218, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":\"asset_use\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-01 10:56:07', 31);
INSERT INTO `sys_oper_log` VALUES (219, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/asset_use', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-01 10:59:52', 57);
INSERT INTO `sys_oper_log` VALUES (220, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"use\",\"className\":\"AssetUse\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":118,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-01 10:56:07\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":12,\"updateBy\":\"\",\"updateTime\":\"2023-09-01 10:59:52\",\"usableColumn\":false},{\"capJavaField\":\"CateId\",\"columnComment\":\"分类\",\"columnId\":129,\"columnName\":\"cate_id\",\"columnType\":\"varchar(50)\",\"createBy\":\"\",\"createTime\":\"2023-09-01 10:59:52\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"cateId\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":12,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"ModelId\",\"columnComment\":\"型号\",\"columnId\":130,\"columnName\":\"model_id\",\"columnType\":\"int(11)\",\"createBy\":\"\",\"createTime\":\"2023-09-01 10:59:52\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"modelId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":12,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Amount\",\"columnComment\":\"数量\",\"columnId\":131,\"columnName\":\"amount\",\"columnType\":\"tinyint(4)\",\"createBy\":\"\",\"createTime\":\"2023-09-01 10:59:52\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"amount\",\"java', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-01 11:00:31', 30);
INSERT INTO `sys_oper_log` VALUES (221, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"detail\",\"className\":\"AssetStockDetail\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":113,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-01 10:55:47\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":11,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"StockId\",\"columnComment\":\"盘点主键\",\"columnId\":114,\"columnName\":\"stock_id\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-01 10:55:47\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"stockId\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":11,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"AssetId\",\"columnComment\":\"资产编号\",\"columnId\":115,\"columnName\":\"asset_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-01 10:55:47\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"assetId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":11,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Status\",\"columnComment\":\"当前状态\",\"columnId\":116,\"columnName\":\"status\",\"columnType\":\"char(1)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-01 10:55:47\",\"dictType\":\"assets_status\",\"edit\":true,\"htmlType\":\"radio\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"status\",\"javaType\":\"String\",\"list\":t', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-01 11:38:20', 19);
INSERT INTO `sys_oper_log` VALUES (222, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"stock\",\"className\":\"AssetStock\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":97,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-01 10:55:47\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":10,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Name\",\"columnComment\":\"代号\",\"columnId\":98,\"columnName\":\"name\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-01 10:55:47\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":10,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"AssetCount\",\"columnComment\":\"资产总数\",\"columnId\":99,\"columnName\":\"asset_count\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-01 10:55:47\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"0\",\"javaField\":\"assetCount\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":10,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"AssetWorth\",\"columnComment\":\"资产总值\",\"columnId\":100,\"columnName\":\"asset_worth\",\"columnType\":\"decimal(10,2)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-01 10:55:47\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"0\",\"javaField\":\"assetWorth\",\"javaType\":\"BigDecimal\",\"list\":t', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-01 11:39:22', 27);
INSERT INTO `sys_oper_log` VALUES (223, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/asset_stock_detail', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-01 14:40:42', 40);
INSERT INTO `sys_oper_log` VALUES (224, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"盘点任务状态\",\"dictType\":\"asset_stock_status\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-01 15:57:12', 7);
INSERT INTO `sys_oper_log` VALUES (225, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"1\",\"dictSort\":1,\"dictType\":\"asset_stock_status\",\"dictValue\":\"未开始\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-01 15:57:24', 6);
INSERT INTO `sys_oper_log` VALUES (226, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"进行中\",\"dictSort\":2,\"dictType\":\"asset_stock_status\",\"dictValue\":\"2\",\"listClass\":\"info\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-01 15:57:35', 6);
INSERT INTO `sys_oper_log` VALUES (227, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"已完成\",\"dictSort\":3,\"dictType\":\"asset_stock_status\",\"dictValue\":\"3\",\"listClass\":\"success\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-01 15:57:50', 4);
INSERT INTO `sys_oper_log` VALUES (228, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-09-01 15:57:24\",\"default\":false,\"dictCode\":149,\"dictLabel\":\"1\",\"dictSort\":1,\"dictType\":\"asset_stock_status\",\"dictValue\":\"未开始\",\"isDefault\":\"N\",\"listClass\":\"info\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-01 15:57:57', 12);
INSERT INTO `sys_oper_log` VALUES (229, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-09-01 15:57:35\",\"default\":false,\"dictCode\":150,\"dictLabel\":\"进行中\",\"dictSort\":2,\"dictType\":\"asset_stock_status\",\"dictValue\":\"2\",\"isDefault\":\"N\",\"listClass\":\"warning\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-01 15:58:02', 5);
INSERT INTO `sys_oper_log` VALUES (230, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-09-01 15:57:24\",\"default\":false,\"dictCode\":149,\"dictLabel\":\"未开始\",\"dictSort\":1,\"dictType\":\"asset_stock_status\",\"dictValue\":\"1\",\"isDefault\":\"N\",\"listClass\":\"info\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-01 15:58:09', 5);
INSERT INTO `sys_oper_log` VALUES (231, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/asset_stock', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-01 15:58:22', 37);
INSERT INTO `sys_oper_log` VALUES (232, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"stock\",\"className\":\"AssetStock\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":97,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-01 10:55:47\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":10,\"updateBy\":\"\",\"updateTime\":\"2023-09-01 15:58:22\",\"usableColumn\":false},{\"capJavaField\":\"Name\",\"columnComment\":\"代号\",\"columnId\":98,\"columnName\":\"name\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-01 10:55:47\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":10,\"updateBy\":\"\",\"updateTime\":\"2023-09-01 15:58:22\",\"usableColumn\":false},{\"capJavaField\":\"AssetCount\",\"columnComment\":\"资产总数\",\"columnId\":99,\"columnName\":\"asset_count\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-01 10:55:47\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"assetCount\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":10,\"updateBy\":\"\",\"updateTime\":\"2023-09-01 15:58:22\",\"usableColumn\":false},{\"capJavaField\":\"AssetWorth\",\"columnComment\":\"资产总值\",\"columnId\":100,\"columnName\":\"asset_worth\",\"columnType\":\"decimal(10,2)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-01 10:55:47\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isIns', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-01 15:58:50', 42);
INSERT INTO `sys_oper_log` VALUES (233, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"已确认\",\"dictSort\":4,\"dictType\":\"asset_stock_status\",\"dictValue\":\"4\",\"listClass\":\"success\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-01 16:02:12', 6);
INSERT INTO `sys_oper_log` VALUES (234, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-09-01 15:57:50\",\"default\":false,\"dictCode\":151,\"dictLabel\":\"已完成\",\"dictSort\":3,\"dictType\":\"asset_stock_status\",\"dictValue\":\"3\",\"isDefault\":\"N\",\"listClass\":\"danger\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-01 16:02:17', 4);
INSERT INTO `sys_oper_log` VALUES (235, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"icon\":\"chart\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"查询统计\",\"menuType\":\"M\",\"orderNum\":3,\"params\":{},\"parentId\":0,\"path\":\"chart\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-04 10:57:02', 16);
INSERT INTO `sys_oper_log` VALUES (236, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2023-08-22 10:13:30\",\"icon\":\"system\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1,\"menuName\":\"系统管理\",\"menuType\":\"M\",\"orderNum\":5,\"params\":{},\"parentId\":0,\"path\":\"system\",\"perms\":\"\",\"query\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-04 10:57:12', 6);
INSERT INTO `sys_oper_log` VALUES (237, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2023-08-22 10:13:30\",\"icon\":\"tool\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":3,\"menuName\":\"系统工具\",\"menuType\":\"M\",\"orderNum\":4,\"params\":{},\"parentId\":0,\"path\":\"tool\",\"perms\":\"\",\"query\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-04 10:57:18', 5);
INSERT INTO `sys_oper_log` VALUES (238, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":\"flow_step,flow_task\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-04 10:57:31', 113);
INSERT INTO `sys_oper_log` VALUES (239, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"task\",\"className\":\"FlowTask\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":146,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-04 10:57:30\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":14,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Type\",\"columnComment\":\"类型\",\"columnId\":147,\"columnName\":\"type\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-04 10:57:30\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"0\",\"isRequired\":\"1\",\"javaField\":\"type\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":false,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":14,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"TargetId\",\"columnComment\":\"业务主键\",\"columnId\":148,\"columnName\":\"target_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-04 10:57:30\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"0\",\"isRequired\":\"1\",\"javaField\":\"targetId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":false,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":14,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Num\",\"columnComment\":\"业务编号\",\"columnId\":149,\"columnName\":\"num\",\"columnType\":\"varchar(100)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-04 10:57:30\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"num\",\"javaType\":\"String\",\"list\":t', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-04 10:58:40', 54);
INSERT INTO `sys_oper_log` VALUES (240, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"step\",\"className\":\"FlowStep\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":137,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-04 10:57:30\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":13,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"TaskId\",\"columnComment\":\"业务主键\",\"columnId\":138,\"columnName\":\"task_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-04 10:57:30\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"0\",\"isRequired\":\"1\",\"javaField\":\"taskId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":false,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":13,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"CheckUserId\",\"columnComment\":\"审核人\",\"columnId\":139,\"columnName\":\"check_user_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-04 10:57:30\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"0\",\"isRequired\":\"1\",\"javaField\":\"checkUserId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":false,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":13,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"CheckUserName\",\"columnComment\":\"审核人姓名\",\"columnId\":140,\"columnName\":\"check_user_name\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-04 10:57:30\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"0\",\"javaField\":\"checkUserName\",\"j', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-04 11:01:17', 21);
INSERT INTO `sys_oper_log` VALUES (241, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"icon\":\"checkbox\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"审批管理\",\"menuType\":\"M\",\"orderNum\":4,\"params\":{},\"parentId\":0,\"path\":\"flow\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-04 11:03:37', 5);
INSERT INTO `sys_oper_log` VALUES (242, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"flow/task/index\",\"createTime\":\"2023-09-04 11:00:24\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2089,\"menuName\":\"审批任务\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2101,\"path\":\"task\",\"perms\":\"flow:task:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-04 11:03:46', 4);
INSERT INTO `sys_oper_log` VALUES (243, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"flow/step/index\",\"createTime\":\"2023-09-04 11:01:29\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2095,\"menuName\":\"审批步骤\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2101,\"path\":\"step\",\"perms\":\"flow:step:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-04 11:03:59', 5);
INSERT INTO `sys_oper_log` VALUES (244, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"asset/info/my\",\"createBy\":\"admin\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"我的资产\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2026,\"path\":\"asset\",\"perms\":\"asset:info:my\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-04 11:05:01', 6);
INSERT INTO `sys_oper_log` VALUES (245, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"flow/task/my\",\"createBy\":\"admin\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"我的审批\",\"menuType\":\"C\",\"orderNum\":2,\"params\":{},\"parentId\":2026,\"path\":\"task\",\"perms\":\"flow:task:my\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-04 11:05:34', 5);
INSERT INTO `sys_oper_log` VALUES (246, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2023-09-04 11:03:37\",\"icon\":\"checkbox\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2101,\"menuName\":\"审批管理\",\"menuType\":\"M\",\"orderNum\":2,\"params\":{},\"parentId\":0,\"path\":\"flow\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-04 14:03:17', 21);
INSERT INTO `sys_oper_log` VALUES (247, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"flow/step/my\",\"createBy\":\"admin\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"待审批\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2101,\"path\":\"my\",\"perms\":\"flow:step:my\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-04 14:04:44', 8);
INSERT INTO `sys_oper_log` VALUES (248, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"flow/task/index\",\"createTime\":\"2023-09-04 11:00:24\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2089,\"menuName\":\"审批任务\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2101,\"path\":\"task\",\"perms\":\"flow:task:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-05 11:10:44', 14);
INSERT INTO `sys_oper_log` VALUES (249, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-23 13:46:08\",\"default\":false,\"dictCode\":134,\"dictLabel\":\"不通过\",\"dictSort\":3,\"dictType\":\"flow_check_status\",\"dictValue\":\"3\",\"isDefault\":\"N\",\"listClass\":\"warning\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-05 16:35:38', 6);
INSERT INTO `sys_oper_log` VALUES (250, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"asset/purchase/my\",\"createBy\":\"admin\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"我的采购\",\"menuType\":\"C\",\"orderNum\":3,\"params\":{},\"parentId\":2026,\"path\":\"purchase\",\"perms\":\"asset:purchase:my\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-05 17:41:04', 18);
INSERT INTO `sys_oper_log` VALUES (251, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"asset/purchase/my\",\"createTime\":\"2023-09-05 17:41:04\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2105,\"menuName\":\"采购申请\",\"menuType\":\"C\",\"orderNum\":3,\"params\":{},\"parentId\":2026,\"path\":\"purchase\",\"perms\":\"asset:purchase:my\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-05 17:41:35', 7);
INSERT INTO `sys_oper_log` VALUES (252, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"asset/use/my\",\"createBy\":\"admin\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"使用申请\",\"menuType\":\"C\",\"orderNum\":4,\"params\":{},\"parentId\":2026,\"path\":\"use\",\"perms\":\"asset:use:my\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-05 17:42:01', 6);
INSERT INTO `sys_oper_log` VALUES (253, '角色管理', 1, 'com.ruoyi.web.controller.system.SysRoleController.add()', 'POST', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{\"admin\":false,\"createBy\":\"admin\",\"deptCheckStrictly\":true,\"deptIds\":[],\"flag\":false,\"menuCheckStrictly\":true,\"menuIds\":[2057,2058,2059,2060,2061,2062,2063,2064,2065,2066,2067,2068,2069,2082,2083,2084,2085,2086,2087,2033,2034,2035,2036,2037,2038],\"params\":{},\"roleId\":3,\"roleKey\":\"warehouse\",\"roleName\":\"仓库管理员\",\"roleSort\":5,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-06 15:35:17', 38);
INSERT INTO `sys_oper_log` VALUES (254, '角色管理', 1, 'com.ruoyi.web.controller.system.SysRoleController.add()', 'POST', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{\"admin\":false,\"createBy\":\"admin\",\"deptCheckStrictly\":true,\"deptIds\":[],\"flag\":false,\"menuCheckStrictly\":true,\"menuIds\":[2026,2102,2103,2105,2106,2057,2058,2059,2060,2061,2062,2063,2064,2065,2066,2067,2068,2069,2082,2083,2084,2085,2086,2087,2033,2034,2035,2036,2037,2038,2101,2089,2090,2091,2092,2093,2094,2095,2096,2097,2098,2099,2100,2104,2025,2051,2052,2053,2054,2055,2056,2070,2071,2072,2073,2074,2075,2076,2077,2078,2079,2080,2081,2039,2040,2041,2042,2043,2044,2045,2046,2047,2048,2049,2050,2027,2028,2029,2030,2031,2032],\"params\":{},\"roleId\":4,\"roleKey\":\"assets\",\"roleName\":\"资产管理员\",\"roleSort\":10,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-06 15:35:47', 18);
INSERT INTO `sys_oper_log` VALUES (255, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{\"admin\":false,\"createTime\":\"2023-08-22 10:13:30\",\"dataScope\":\"2\",\"delFlag\":\"0\",\"deptCheckStrictly\":true,\"flag\":false,\"menuCheckStrictly\":true,\"menuIds\":[2026,2102,2103,2105,2106],\"params\":{},\"remark\":\"普通角色\",\"roleId\":2,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"roleSort\":2,\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-06 15:35:54', 20);
INSERT INTO `sys_oper_log` VALUES (256, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{\"admin\":false,\"createTime\":\"2023-09-06 15:35:17\",\"dataScope\":\"1\",\"delFlag\":\"0\",\"deptCheckStrictly\":true,\"flag\":false,\"menuCheckStrictly\":true,\"menuIds\":[2057,2058,2059,2060,2061,2062,2063,2064,2065,2066,2067,2068,2069,2082,2083,2084,2085,2086,2087,2033,2034,2035,2036,2037,2038],\"params\":{},\"roleId\":3,\"roleKey\":\"warehouse\",\"roleName\":\"仓库管理员\",\"roleSort\":12,\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-06 15:36:05', 13);
INSERT INTO `sys_oper_log` VALUES (257, '角色管理', 1, 'com.ruoyi.web.controller.system.SysRoleController.add()', 'POST', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{\"admin\":false,\"createBy\":\"admin\",\"deptCheckStrictly\":true,\"deptIds\":[],\"flag\":false,\"menuCheckStrictly\":true,\"menuIds\":[2101,2104],\"params\":{},\"roleId\":5,\"roleKey\":\"dept\",\"roleName\":\"部门负责人\",\"roleSort\":11,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-06 15:36:30', 10);
INSERT INTO `sys_oper_log` VALUES (258, '用户管理', 1, 'com.ruoyi.web.controller.system.SysUserController.add()', 'POST', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{\"admin\":false,\"deptId\":101,\"email\":\"341234@qq.com\",\"nickName\":\"一号仓管\",\"params\":{},\"phonenumber\":\"13412433214\",\"postIds\":[],\"roleIds\":[3],\"sex\":\"0\",\"status\":\"0\",\"userName\":\"admin\"}', '{\"msg\":\"新增用户\'admin\'失败，登录账号已存在\",\"code\":500}', 0, NULL, '2023-09-06 15:42:28', 6);
INSERT INTO `sys_oper_log` VALUES (259, '用户管理', 1, 'com.ruoyi.web.controller.system.SysUserController.add()', 'POST', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{\"admin\":false,\"createBy\":\"admin\",\"deptId\":101,\"email\":\"341234@qq.com\",\"nickName\":\"一号仓管\",\"params\":{},\"phonenumber\":\"13412433214\",\"postIds\":[],\"roleIds\":[3],\"sex\":\"0\",\"status\":\"0\",\"userId\":3,\"userName\":\"assets1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-06 15:42:42', 116);
INSERT INTO `sys_oper_log` VALUES (260, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"asset/purchase/index\",\"createTime\":\"2023-08-29 16:14:06\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2051,\"menuName\":\"采购管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2025,\"path\":\"purchase\",\"perms\":\"asset:purchase:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-06 17:31:58', 9);
INSERT INTO `sys_oper_log` VALUES (261, '角色管理', 1, 'com.ruoyi.web.controller.system.SysRoleController.add()', 'POST', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{\"admin\":false,\"createBy\":\"admin\",\"deptCheckStrictly\":true,\"deptIds\":[],\"flag\":false,\"menuCheckStrictly\":true,\"menuIds\":[2026,2025,2102,2105,2106,2070,2071,2072,2073,2074,2075],\"params\":{},\"roleId\":6,\"roleKey\":\"maintain\",\"roleName\":\"维保人员\",\"roleSort\":15,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-07 09:54:39', 8);
INSERT INTO `sys_oper_log` VALUES (262, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"删除前确认\",\"dictSort\":9,\"dictType\":\"api_event_type\",\"dictValue\":\"DC\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-07 09:57:14', 7);
INSERT INTO `sys_oper_log` VALUES (263, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-09-07 09:57:14\",\"default\":false,\"dictCode\":153,\"dictLabel\":\"删除验证\",\"dictSort\":9,\"dictType\":\"api_event_type\",\"dictValue\":\"DC\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-07 10:41:23', 8);
INSERT INTO `sys_oper_log` VALUES (264, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-23 13:44:32\",\"default\":false,\"dictCode\":129,\"dictLabel\":\"进行中\",\"dictSort\":3,\"dictType\":\"flow_apply_status\",\"dictValue\":\"3\",\"isDefault\":\"N\",\"listClass\":\"info\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-08 10:30:01', 11);
INSERT INTO `sys_oper_log` VALUES (265, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-23 13:44:58\",\"default\":false,\"dictCode\":131,\"dictLabel\":\"已完成\",\"dictSort\":4,\"dictType\":\"flow_apply_status\",\"dictValue\":\"4\",\"isDefault\":\"N\",\"listClass\":\"success\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-08 10:30:11', 5);
INSERT INTO `sys_oper_log` VALUES (266, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-08-23 13:44:32\",\"default\":false,\"dictCode\":129,\"dictLabel\":\"进行中\",\"dictSort\":3,\"dictType\":\"flow_apply_status\",\"dictValue\":\"3\",\"isDefault\":\"N\",\"listClass\":\"warning\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-08 10:30:16', 5);
INSERT INTO `sys_oper_log` VALUES (267, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"flow/step/my\",\"createTime\":\"2023-09-04 14:04:44\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2104,\"menuName\":\"待我审批\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2026,\"path\":\"my\",\"perms\":\"flow:step:my\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-08 11:28:13', 17);
INSERT INTO `sys_oper_log` VALUES (268, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"flow/task/my\",\"createTime\":\"2023-09-04 11:05:34\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2103,\"menuName\":\"我的审批\",\"menuType\":\"C\",\"orderNum\":2,\"params\":{},\"parentId\":2026,\"path\":\"task\",\"perms\":\"flow:task:my\",\"status\":\"1\",\"updateBy\":\"admin\",\"visible\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-08 11:28:50', 6);
INSERT INTO `sys_oper_log` VALUES (269, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"asset/in/index\",\"createTime\":\"2023-08-31 10:49:01\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2058,\"menuName\":\"入库管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2057,\"path\":\"in\",\"perms\":\"asset:in:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-08 11:29:02', 6);
INSERT INTO `sys_oper_log` VALUES (270, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"asset/out/index\",\"createTime\":\"2023-08-31 14:49:06\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2064,\"menuName\":\"出库管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2057,\"path\":\"out\",\"perms\":\"asset:out:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-08 11:29:12', 5);
INSERT INTO `sys_oper_log` VALUES (271, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":\"asset_sale\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-08 11:32:55', 68);
INSERT INTO `sys_oper_log` VALUES (272, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"sale\",\"className\":\"AssetSale\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"自增长主键ID\",\"columnId\":156,\"columnName\":\"id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-08 11:32:55\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":15,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"CateId\",\"columnComment\":\"分类\",\"columnId\":157,\"columnName\":\"cate_id\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-08 11:32:55\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"cateId\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":15,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"AssetId\",\"columnComment\":\"资产\",\"columnId\":158,\"columnName\":\"asset_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-08 11:32:55\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"assetId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":15,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"DeptId\",\"columnComment\":\"归属部门\",\"columnId\":159,\"columnName\":\"dept_id\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-08 11:32:55\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"deptId\",\"javaType\":\"Long\",\"list', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-08 11:33:54', 37);
INSERT INTO `sys_oper_log` VALUES (273, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"asset/maintain/my\",\"createTime\":\"2023-09-04 11:05:34\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2103,\"menuName\":\"我的维保\",\"menuType\":\"C\",\"orderNum\":2,\"params\":{},\"parentId\":2026,\"path\":\"maintain\",\"perms\":\"asset:maintain:my\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-11 16:11:25', 18);
INSERT INTO `sys_oper_log` VALUES (274, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"asset/stock/my\",\"createBy\":\"admin\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"我的盘点\",\"menuType\":\"C\",\"orderNum\":6,\"params\":{},\"parentId\":2026,\"path\":\"stock\",\"perms\":\"asset:stock:my\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-12 09:53:52', 17);
INSERT INTO `sys_oper_log` VALUES (275, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":\"sys_chart\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-12 10:35:02', 47);
INSERT INTO `sys_oper_log` VALUES (276, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"chart\",\"className\":\"SysChart\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"参数主键\",\"columnId\":167,\"columnName\":\"id\",\"columnType\":\"int(5)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-12 10:35:02\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Integer\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":16,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Type\",\"columnComment\":\"类型\",\"columnId\":168,\"columnName\":\"type\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-12 10:35:02\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"type\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":16,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Name\",\"columnComment\":\"名称\",\"columnId\":169,\"columnName\":\"name\",\"columnType\":\"varchar(100)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-12 10:35:02\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":16,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"DsCode\",\"columnComment\":\"数据源\",\"columnId\":170,\"columnName\":\"ds_code\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-12 10:35:02\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"dsCode\",\"javaType\":\"String\",\"list\":t', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-12 10:35:45', 26);
INSERT INTO `sys_oper_log` VALUES (277, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"报表类型\",\"dictType\":\"chart_type\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-12 10:36:08', 6);
INSERT INTO `sys_oper_log` VALUES (278, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"报表搜索类型\",\"dictType\":\"chart_search_type\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-12 10:36:33', 4);
INSERT INTO `sys_oper_log` VALUES (279, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"柱状图\",\"dictSort\":1,\"dictType\":\"chart_type\",\"dictValue\":\"bar\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-12 10:36:53', 6);
INSERT INTO `sys_oper_log` VALUES (280, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"折线图\",\"dictSort\":2,\"dictType\":\"chart_type\",\"dictValue\":\"line\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-12 10:37:06', 21);
INSERT INTO `sys_oper_log` VALUES (281, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"饼图\",\"dictSort\":3,\"dictType\":\"chart_type\",\"dictValue\":\"pie\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-12 10:37:19', 6);
INSERT INTO `sys_oper_log` VALUES (282, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"雷达图\",\"dictSort\":4,\"dictType\":\"chart_type\",\"dictValue\":\"raddar\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-12 10:38:16', 6);
INSERT INTO `sys_oper_log` VALUES (283, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"仪表盘点\",\"dictSort\":5,\"dictType\":\"chart_type\",\"dictValue\":\"guage\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-12 10:38:45', 5);
INSERT INTO `sys_oper_log` VALUES (284, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"数据源\",\"dictSort\":1,\"dictType\":\"chart_search_type\",\"dictValue\":\"ds\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-12 10:39:44', 10);
INSERT INTO `sys_oper_log` VALUES (285, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"树形数据源\",\"dictSort\":2,\"dictType\":\"chart_search_type\",\"dictValue\":\"tree\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-12 10:39:58', 4);
INSERT INTO `sys_oper_log` VALUES (286, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"字典\",\"dictSort\":3,\"dictType\":\"chart_search_type\",\"dictValue\":\"dict\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-12 10:40:16', 5);
INSERT INTO `sys_oper_log` VALUES (287, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"时间范围\",\"dictSort\":4,\"dictType\":\"chart_search_type\",\"dictValue\":\"dt\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-12 10:40:32', 4);
INSERT INTO `sys_oper_log` VALUES (288, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"chart\",\"className\":\"SysChart\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"参数主键\",\"columnId\":167,\"columnName\":\"id\",\"columnType\":\"int(5)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-12 10:35:02\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Integer\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":16,\"updateBy\":\"\",\"updateTime\":\"2023-09-12 10:35:44\",\"usableColumn\":false},{\"capJavaField\":\"Type\",\"columnComment\":\"类型\",\"columnId\":168,\"columnName\":\"type\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-12 10:35:02\",\"dictType\":\"chart_type\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"type\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":16,\"updateBy\":\"\",\"updateTime\":\"2023-09-12 10:35:45\",\"usableColumn\":false},{\"capJavaField\":\"Name\",\"columnComment\":\"名称\",\"columnId\":169,\"columnName\":\"name\",\"columnType\":\"varchar(100)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-12 10:35:02\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":16,\"updateBy\":\"\",\"updateTime\":\"2023-09-12 10:35:45\",\"usableColumn\":false},{\"capJavaField\":\"DsCode\",\"columnComment\":\"数据源\",\"columnId\":170,\"columnName\":\"ds_code\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"createTime\":\"2023-09-12 10:35:02\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isI', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-12 10:51:59', 22);
INSERT INTO `sys_oper_log` VALUES (289, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"system/chart/index\",\"createTime\":\"2023-09-12 10:52:10\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2114,\"menuName\":\"报表设计\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":3,\"path\":\"chart\",\"perms\":\"system:chart:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-12 11:12:29', 6);
INSERT INTO `sys_oper_log` VALUES (290, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"chart/index\",\"createBy\":\"admin\",\"icon\":\"chart\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"资产分类统计\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2088,\"path\":\"asset_category\",\"perms\":\"chart:view:1\",\"query\":\"{\\\"id\\\":1}\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-12 14:14:24', 33);
INSERT INTO `sys_oper_log` VALUES (291, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-09-12 10:38:16\",\"default\":false,\"dictCode\":157,\"dictLabel\":\"雷达图\",\"dictSort\":4,\"dictType\":\"chart_type\",\"dictValue\":\"radar\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-12 15:57:49', 6);
INSERT INTO `sys_oper_log` VALUES (292, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"chart/index\",\"icon\":\"chart\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"资产分类统计\",\"menuType\":\"C\",\"orderNum\":9,\"params\":{},\"parentId\":2200,\"path\":\"chart_asset_category\",\"perms\":\"chart:view:1\",\"query\":\"{\\\"id\\\":1}\",\"remark\":\"资产分类统计\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"新增菜单\'资产分类统计\'失败，菜单名称已存在\",\"code\":500}', 0, NULL, '2023-09-12 16:21:16', 2);
INSERT INTO `sys_oper_log` VALUES (293, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"chart/index\",\"createBy\":\"admin\",\"icon\":\"chart\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"资产柱状图\",\"menuType\":\"C\",\"orderNum\":9,\"params\":{},\"parentId\":2200,\"path\":\"chart_asset_category\",\"perms\":\"chart:view:2\",\"query\":\"{\\\"id\\\":2}\",\"remark\":\"资产柱状图\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-09-12 16:24:09', 5);

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '岗位信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2023-08-22 10:13:30', '', NULL, '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2023-08-22 10:13:30', '', NULL, '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', 1, 1, '0', '0', 'admin', '2023-08-22 10:13:30', '', NULL, '超级管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '2', 1, 1, '0', '0', 'admin', '2023-08-22 10:13:30', 'admin', '2023-09-06 15:35:54', '普通角色');
INSERT INTO `sys_role` VALUES (3, '仓库管理员', 'warehouse', 12, '1', 1, 1, '0', '0', 'admin', '2023-09-06 15:35:17', 'admin', '2023-09-06 15:36:05', NULL);
INSERT INTO `sys_role` VALUES (4, '资产管理员', 'assets', 10, '1', 1, 1, '0', '0', 'admin', '2023-09-06 15:35:47', '', NULL, NULL);
INSERT INTO `sys_role` VALUES (5, '部门负责人', 'dept', 11, '1', 1, 1, '0', '0', 'admin', '2023-09-06 15:36:30', '', NULL, NULL);
INSERT INTO `sys_role` VALUES (6, '维保人员', 'maintain', 15, '1', 1, 1, '0', '0', 'admin', '2023-09-07 09:54:39', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);
INSERT INTO `sys_role_dept` VALUES (2, 105);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 2026);
INSERT INTO `sys_role_menu` VALUES (2, 2102);
INSERT INTO `sys_role_menu` VALUES (2, 2103);
INSERT INTO `sys_role_menu` VALUES (2, 2105);
INSERT INTO `sys_role_menu` VALUES (2, 2106);
INSERT INTO `sys_role_menu` VALUES (3, 2033);
INSERT INTO `sys_role_menu` VALUES (3, 2034);
INSERT INTO `sys_role_menu` VALUES (3, 2035);
INSERT INTO `sys_role_menu` VALUES (3, 2036);
INSERT INTO `sys_role_menu` VALUES (3, 2037);
INSERT INTO `sys_role_menu` VALUES (3, 2038);
INSERT INTO `sys_role_menu` VALUES (3, 2057);
INSERT INTO `sys_role_menu` VALUES (3, 2058);
INSERT INTO `sys_role_menu` VALUES (3, 2059);
INSERT INTO `sys_role_menu` VALUES (3, 2060);
INSERT INTO `sys_role_menu` VALUES (3, 2061);
INSERT INTO `sys_role_menu` VALUES (3, 2062);
INSERT INTO `sys_role_menu` VALUES (3, 2063);
INSERT INTO `sys_role_menu` VALUES (3, 2064);
INSERT INTO `sys_role_menu` VALUES (3, 2065);
INSERT INTO `sys_role_menu` VALUES (3, 2066);
INSERT INTO `sys_role_menu` VALUES (3, 2067);
INSERT INTO `sys_role_menu` VALUES (3, 2068);
INSERT INTO `sys_role_menu` VALUES (3, 2069);
INSERT INTO `sys_role_menu` VALUES (3, 2082);
INSERT INTO `sys_role_menu` VALUES (3, 2083);
INSERT INTO `sys_role_menu` VALUES (3, 2084);
INSERT INTO `sys_role_menu` VALUES (3, 2085);
INSERT INTO `sys_role_menu` VALUES (3, 2086);
INSERT INTO `sys_role_menu` VALUES (3, 2087);
INSERT INTO `sys_role_menu` VALUES (4, 2025);
INSERT INTO `sys_role_menu` VALUES (4, 2026);
INSERT INTO `sys_role_menu` VALUES (4, 2027);
INSERT INTO `sys_role_menu` VALUES (4, 2028);
INSERT INTO `sys_role_menu` VALUES (4, 2029);
INSERT INTO `sys_role_menu` VALUES (4, 2030);
INSERT INTO `sys_role_menu` VALUES (4, 2031);
INSERT INTO `sys_role_menu` VALUES (4, 2032);
INSERT INTO `sys_role_menu` VALUES (4, 2033);
INSERT INTO `sys_role_menu` VALUES (4, 2034);
INSERT INTO `sys_role_menu` VALUES (4, 2035);
INSERT INTO `sys_role_menu` VALUES (4, 2036);
INSERT INTO `sys_role_menu` VALUES (4, 2037);
INSERT INTO `sys_role_menu` VALUES (4, 2038);
INSERT INTO `sys_role_menu` VALUES (4, 2039);
INSERT INTO `sys_role_menu` VALUES (4, 2040);
INSERT INTO `sys_role_menu` VALUES (4, 2041);
INSERT INTO `sys_role_menu` VALUES (4, 2042);
INSERT INTO `sys_role_menu` VALUES (4, 2043);
INSERT INTO `sys_role_menu` VALUES (4, 2044);
INSERT INTO `sys_role_menu` VALUES (4, 2045);
INSERT INTO `sys_role_menu` VALUES (4, 2046);
INSERT INTO `sys_role_menu` VALUES (4, 2047);
INSERT INTO `sys_role_menu` VALUES (4, 2048);
INSERT INTO `sys_role_menu` VALUES (4, 2049);
INSERT INTO `sys_role_menu` VALUES (4, 2050);
INSERT INTO `sys_role_menu` VALUES (4, 2051);
INSERT INTO `sys_role_menu` VALUES (4, 2052);
INSERT INTO `sys_role_menu` VALUES (4, 2053);
INSERT INTO `sys_role_menu` VALUES (4, 2054);
INSERT INTO `sys_role_menu` VALUES (4, 2055);
INSERT INTO `sys_role_menu` VALUES (4, 2056);
INSERT INTO `sys_role_menu` VALUES (4, 2057);
INSERT INTO `sys_role_menu` VALUES (4, 2058);
INSERT INTO `sys_role_menu` VALUES (4, 2059);
INSERT INTO `sys_role_menu` VALUES (4, 2060);
INSERT INTO `sys_role_menu` VALUES (4, 2061);
INSERT INTO `sys_role_menu` VALUES (4, 2062);
INSERT INTO `sys_role_menu` VALUES (4, 2063);
INSERT INTO `sys_role_menu` VALUES (4, 2064);
INSERT INTO `sys_role_menu` VALUES (4, 2065);
INSERT INTO `sys_role_menu` VALUES (4, 2066);
INSERT INTO `sys_role_menu` VALUES (4, 2067);
INSERT INTO `sys_role_menu` VALUES (4, 2068);
INSERT INTO `sys_role_menu` VALUES (4, 2069);
INSERT INTO `sys_role_menu` VALUES (4, 2070);
INSERT INTO `sys_role_menu` VALUES (4, 2071);
INSERT INTO `sys_role_menu` VALUES (4, 2072);
INSERT INTO `sys_role_menu` VALUES (4, 2073);
INSERT INTO `sys_role_menu` VALUES (4, 2074);
INSERT INTO `sys_role_menu` VALUES (4, 2075);
INSERT INTO `sys_role_menu` VALUES (4, 2076);
INSERT INTO `sys_role_menu` VALUES (4, 2077);
INSERT INTO `sys_role_menu` VALUES (4, 2078);
INSERT INTO `sys_role_menu` VALUES (4, 2079);
INSERT INTO `sys_role_menu` VALUES (4, 2080);
INSERT INTO `sys_role_menu` VALUES (4, 2081);
INSERT INTO `sys_role_menu` VALUES (4, 2082);
INSERT INTO `sys_role_menu` VALUES (4, 2083);
INSERT INTO `sys_role_menu` VALUES (4, 2084);
INSERT INTO `sys_role_menu` VALUES (4, 2085);
INSERT INTO `sys_role_menu` VALUES (4, 2086);
INSERT INTO `sys_role_menu` VALUES (4, 2087);
INSERT INTO `sys_role_menu` VALUES (4, 2089);
INSERT INTO `sys_role_menu` VALUES (4, 2090);
INSERT INTO `sys_role_menu` VALUES (4, 2091);
INSERT INTO `sys_role_menu` VALUES (4, 2092);
INSERT INTO `sys_role_menu` VALUES (4, 2093);
INSERT INTO `sys_role_menu` VALUES (4, 2094);
INSERT INTO `sys_role_menu` VALUES (4, 2095);
INSERT INTO `sys_role_menu` VALUES (4, 2096);
INSERT INTO `sys_role_menu` VALUES (4, 2097);
INSERT INTO `sys_role_menu` VALUES (4, 2098);
INSERT INTO `sys_role_menu` VALUES (4, 2099);
INSERT INTO `sys_role_menu` VALUES (4, 2100);
INSERT INTO `sys_role_menu` VALUES (4, 2101);
INSERT INTO `sys_role_menu` VALUES (4, 2102);
INSERT INTO `sys_role_menu` VALUES (4, 2103);
INSERT INTO `sys_role_menu` VALUES (4, 2104);
INSERT INTO `sys_role_menu` VALUES (4, 2105);
INSERT INTO `sys_role_menu` VALUES (4, 2106);
INSERT INTO `sys_role_menu` VALUES (5, 2101);
INSERT INTO `sys_role_menu` VALUES (5, 2104);
INSERT INTO `sys_role_menu` VALUES (6, 2025);
INSERT INTO `sys_role_menu` VALUES (6, 2026);
INSERT INTO `sys_role_menu` VALUES (6, 2070);
INSERT INTO `sys_role_menu` VALUES (6, 2071);
INSERT INTO `sys_role_menu` VALUES (6, 2072);
INSERT INTO `sys_role_menu` VALUES (6, 2073);
INSERT INTO `sys_role_menu` VALUES (6, 2074);
INSERT INTO `sys_role_menu` VALUES (6, 2075);
INSERT INTO `sys_role_menu` VALUES (6, 2102);
INSERT INTO `sys_role_menu` VALUES (6, 2105);
INSERT INTO `sys_role_menu` VALUES (6, 2106);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '管理员', '00', 'ry@163.com', '15888888888', '1', '', '$2a$10$Lkc8ypVPya1QXrSSOWx52uKCeXXWlOwov/BuGwZO3V8hu92pGBOwa', '0', '0', '127.0.0.1', '2023-09-28 15:22:43', 'admin', '2023-08-22 10:13:30', '', '2023-09-28 15:22:43', '管理员');
INSERT INTO `sys_user` VALUES (2, 105, 'ry', '若依', '00', 'ry@qq.com', '15666666666', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2023-08-22 10:13:30', 'admin', '2023-08-22 10:13:30', '', NULL, '测试员');
INSERT INTO `sys_user` VALUES (3, 101, 'assets1', '一号仓管', '00', '341234@qq.com', '13412433214', '0', '', '$2a$10$dkYTKp2UNT0kY1dPDxpP4eLzVPthgXq6QwObUWwPV06QOD68r2kje', '0', '0', '', NULL, 'admin', '2023-09-06 15:42:42', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 2);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);
INSERT INTO `sys_user_role` VALUES (3, 3);

SET FOREIGN_KEY_CHECKS = 1;
