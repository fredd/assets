package org.openapi.common;

import lombok.Data;

/**
 * 用户模型
 */
@Data
public class ApiUser {
    /** 全部数据权限 **/
    public static final int DATA_SCOPE_ALL = 0;
    /** 用户相关的数据权限 **/
    public static final int DATA_SCOPE_USER = 1;
    /** 所管辖组织的数据权限 **/
    public static final int DATA_SCOPE_ORG = 2;

    /** 用户主键 **/
    private String userId;
    /** 用户账号 **/
    private String account;
    /** 用户姓名 **/
    private String username;
    /** 用户角色数组 **/
    private String[] roles;
    /** 用户所在组织 **/
    private String orgId;
    /** 用户所在组织 **/
    private String orgName;

    /** 权限范围，0全部数据、1用户的数据，2管辖组织的数据 **/
    private int dataScope;
    /** 当权限范围为组织时，当前用户所能够管辖的组织主键数组 **/
    private String[] orgIds;
}
