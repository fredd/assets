package org.openapi.domain;

import lombok.Data;

import java.util.Map;

/**
 * 访问控制
 */
@Data
public class ApiAccess {
    public static String READ_MODE = "read";
    public static String WRITE_MODE = "write";
    public static String REMOVE_MODE = "remove";

    /** 主键ID **/
    private Long id;

    /** 角色 **/
    private String roleId;

    /** 模型 **/
    private Long modelId;

    /** 可读 **/
    private Boolean read = true;

    /** 可写 **/
    private Boolean write = false;

    /** 可删除 **/
    private Boolean remove = false;


    /**
     * 类型转换
     * @param data
     * @return
     */
    public static ApiAccess fromMap(Map data){
        if(!data.containsKey("id") || !data.containsKey("role_id")|| !data.containsKey("model_id")){
            return null;
        }
        ApiAccess access = new ApiAccess();
        access.setId(Long.parseLong(data.get("id").toString()));
        access.setRoleId(data.get("role_id").toString());
        access.setModelId(Long.parseLong(data.get("model_id").toString()));

        if(data.get("read") == null){
            access.setRead(true);
        }else{
            access.setRead("Y".equals(data.get("read").toString()));
        }

        if(data.get("write") == null){
            access.setWrite(false);
        }else{
            access.setWrite("Y".equals(data.get("write").toString()));
        }

        if(data.get("remove") == null){
            access.setRemove(false);
        }else{
            access.setRemove("Y".equals(data.get("remove").toString()));
        }

        return access;
    }
}
