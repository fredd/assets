package org.openapi.domain;

import lombok.Data;

import java.util.Map;

/**
 * 内置SQL
 */
@Data
public class ApiExchange {
    /** 主键ID **/
    private Long id;

    /** 接口地址 **/
    private String uri;

    /** 模式 **/
    private String mode;

    /** 名称 **/
    private String name;

    /** JS模拟读脚本 **/
    private String read;

    /** JS模拟写脚本 **/
    private String write;

    /** 说明 **/
    private String remark;

    /** 是否内置模型 **/
    private Boolean isDefault;


    /**
     * 类型转换
     * @param data
     * @return
     */
    public static ApiExchange fromMap(Map data){
        if(!data.containsKey("id") || !data.containsKey("code")){
            return null;
        }
        ApiExchange sql = new ApiExchange();
        sql.setId(Long.parseLong(data.get("id").toString()));
        sql.setUri(data.getOrDefault("uri", "/").toString());
        sql.setMode(data.get("mode").toString());
        sql.setName(data.get("name").toString());
        sql.setRead(data.get("read").toString());
        sql.setWrite(data.get("write").toString());
        sql.setRemark(data.getOrDefault("remark", "").toString());
        sql.setIsDefault("Y".equalsIgnoreCase(data.getOrDefault("is_default", "N").toString()));
        return sql;
    }
}
