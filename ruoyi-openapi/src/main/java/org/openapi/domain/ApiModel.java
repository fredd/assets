package org.openapi.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.openapi.utils.StrUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 模型
 */
@Data
public class ApiModel {
    /** 主键ID **/
    private Long id;

    /** 分组 **/
    private String group;

    /** 模型代码 **/
    private String code;

    /** 模型名称 **/
    private String name;

    /** 主键代码 **/
    private String pkField;

    /** 唯一键代码 **/
    private String[] uniqueField;

    /** 组织权限规则 **/
    private String orgScope;

    /** 用户权限规则 **/
    private String userScope;

    /** 详细说明 **/
    private String remark;

    /** 导入导出配置 **/
    private String excels;

    /** 是否内置模型 **/
    private Boolean isDefault;

    /** 状态 **/
    private String status;

    private List<ApiField> fields =new ArrayList<>();

    private Map<String, ApiAccess> accessMap = new HashMap<>();

    /**
     * 添加一个字段
     * @param field
     */
    public void addField(ApiField field){
        fields.add(field);
    }

    /**
     * 查询一个字段
     * @param code
     */
    public ApiField getField(String code){
        for(ApiField f:fields){
            if(f.getCode().equalsIgnoreCase(code)){
                return f;
            }
        }
        return null;
    }

    /**
     * 添加一个权限
     * @param access
     */
    public void addAccess(ApiAccess access){
        accessMap.put(access.getRoleId(), access);
    }

    /**
     * 类型转换
     * @param data
     * @return
     */
    public static ApiModel fromMap(Map data){
        if(!data.containsKey("id") || !data.containsKey("code")){
            return null;
        }
        ApiModel model = new ApiModel();
        model.setId(Long.parseLong(data.get("id").toString()));
        model.setCode(data.get("code").toString());
        if(data.containsKey("name")){
            model.setName(data.get("name").toString());
        }else{
            model.setName(model.getCode());
        }
        model.setPkField(data.get("pk_field") == null ? "" : data.get("pk_field").toString());
        String ufs = data.get("unique_field") == null ?  "" : data.get("unique_field").toString();
        if(StrUtil.isNotEmpty(ufs.trim())) {
            model.setUniqueField(ufs.split(","));
        }
        model.setGroup(data.get("group") == null ? "default" : data.get("group").toString());
        model.setOrgScope(data.get("org_scope") == null ? "" : data.get("org_scope").toString());
        model.setUserScope(data.get("user_scope") == null ? "" : data.get("user_scope").toString());
        model.setExcels(data.get("excels") == null ? "" : data.get("excels").toString());
        model.setRemark(data.get("remark") == null ? "" : data.get("remark").toString());
        model.setIsDefault(data.get("is_default") == null ? false : "Y".equalsIgnoreCase(data.get("is_default").toString()));
        model.setStatus(data.get("status") == null ? "1" : data.get("status").toString());
        return model;
    }
}
