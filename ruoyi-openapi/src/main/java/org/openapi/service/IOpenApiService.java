package org.openapi.service;

import org.openapi.common.ApiUser;

/**
 * 接口自定义，需要应用来实现
 */
public interface IOpenApiService {

    /**
     * 获取当前登录用户信息
     * @return
     */
    public ApiUser getLoginUser();

    /**
     * 判断当前登录用户是否超级管理员，可以管理模型
     * @return
     */
    public Boolean isAdmin();

    /**
     * 文件上传目录
     * @param excelFolder 业务目录
     * @return
     */
    public String getUploadPath(String excelFolder);

    /**
     * 删除长时间未下载的excel文件
     * @param excelFolder 业务目录
     */
    public void cleanExcel(String excelFolder);

}
