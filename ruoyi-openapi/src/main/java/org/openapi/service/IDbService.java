package org.openapi.service;

import org.openapi.vo.RelQuery;
import org.openapi.vo.TableData;
import org.openapi.vo.TableQuery;

import java.util.List;
import java.util.Map;

/**
 * 数据库操作
 */
public interface IDbService {


    /**
     * 查询
     * @param sql
     * @return
     */
    public List<Map<String,Object>> query(String sql);

    /**
     * 更新
     * @param sql
     * @return
     */
    public int exec(String sql);

    /**
     * 联表分页查询
     * @param query
     * @return
     */
    public List<Map<String,Object>> query(RelQuery query);

    /**
     * 联表单条查询
     * @param query
     * @return
     */
    public Map find(RelQuery query);

    /**
     * 单表分页查询
     * @param table
     * @return
     */
    public List<Map<String,Object>> query(TableQuery table);

    /**
     * 分页查询
     * @param table
     * @return
     */
    public Map page(TableQuery table);

    /**
     * 表单单条查询
     * @param table
     * @return
     */
    public Map find(TableQuery table);

    /**
     * 记录日志
     * @param modelId 模型
     * @param type 类型
     * @param key 主键
     * @param data 参数
     */
    public void log(Long modelId, String type, String key, Object data);


    /**
     * 填充关联值
     * @param retMap
     * @param dataMap
     */
    public void fillData(Map<String, Map> retMap, Map<String, Object> dataMap);

    /**
     * 插入数据
     * @param table
     * @param override 存在是否覆盖
     * @return
     */
    public int insertData(TableData table, boolean override);

    /**
     * 更新数据
     * @param table
     * @return
     */
    public int updateData(TableData table);


    /**
    * 删除数据
     * @param table
     * @return
     */
    public int deleteData(TableQuery table);
}
