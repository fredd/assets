package org.openapi.service;

import org.openapi.domain.ApiEvent;
import org.openapi.domain.ApiExchange;
import org.openapi.domain.ApiModel;
import org.openapi.domain.ApiSql;

/**
 * 自动加载接口相关资源
 */
public interface IApiLoaderService {
    /**
     * 加载模型
     */
    public int loadModel();

    /**
     * 加载模型的字段和权限
     * @param model
     */
    public void loadModel(ApiModel model);

    /**
     * 加载事件
     */
    public int loadEvent();

    /**
     * 加载内置SQL
     */
    public int loadSql();

    /**
     * 加载数据交换接口
     */
    public int loadExchange();

    /**
     * 判断数据库类型
     */
    public void decideDb();

    /**
     * 查询模型
     * @param table
     * @return
     */
    public ApiModel getModel(String table);

    /**
     * 查询模型事件
     * @param modelId
     * @param type
     * @return
     */
    public ApiEvent getEvent(Long modelId, String type);

    /**
     * 查询数据交换接口
     * @param uri
     * @return
     */
    public ApiExchange getExchange(String uri);

    /**
     * 查询内置脚本
     * @param code
     * @return
     */
    public ApiSql getSql(String code);


    /**
     * 验证当前用户对指定模型是否有对应的权限
     * @param model
     * @param mode
     * @return
     */
    public boolean checkAccess(ApiModel model, String mode);

}
