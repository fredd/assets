package org.openapi.service;

import org.openapi.vo.TableExcel;

import java.io.InputStream;

/**
 * 导入导出服务
 */
public interface IOpenExcelService {
    /**
     * 解析导入配置
     * @param importStr
     * @return
     */
    public TableExcel parseImport(String importStr);

    /**
     * 解析导出配置
     * @param exportStr
     * @return
     */
    public TableExcel parseExport(String exportStr);

    /**
     * 导出数据
     * @param paramStr 查询请求
     * @return
     */
    public String exports(String paramStr);

    /**
     * 导入数据
     * @param inputStream 输入流
     * @param modelCode 数据表
     * @param override 覆盖原有数据
     * @return
     */
    public int imports(InputStream inputStream, String modelCode, boolean override);

    /**
     * 导入模板
     * @param modelCode 数据表
     * @return
     */
    public String template(String modelCode);
}
