package org.openapi.service;

import org.openapi.common.ApiResult;
import org.openapi.domain.ApiExchange;

/**
 * 数据交换（读/写）服务
 */
public interface IOpenExchangeService {

    /**
     * GET请求产
     * @param exchange 请求对象
     * @param dataStr 表单
     * @return
     */
    public ApiResult doRead(ApiExchange exchange, String dataStr);

    /**
     * GET请求产
     * @param exchange 请求对象
     * @param dataStr 表单
     * @return
     */
    public ApiResult doWrite(ApiExchange exchange, String dataStr);

}
