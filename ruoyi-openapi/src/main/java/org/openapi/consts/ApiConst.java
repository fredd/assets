package org.openapi.consts;

/**
 * 接口模型常量
 */
public class ApiConst {

    /**
     * Excel导入模板和导出数据临时目录
     */
    public static String EXCEL_FOLDER = "excel";

    /**
     * 自动指令
     */
    public static String AUTO_EXCELS = "AUTO";

    /** HTTP成功状态 **/
    public static int HTTP_OK = 200;
    /** HTTP发生错误状态 **/
    public static int HTTP_ERROR = 500;
    /** HTTP权限不足 **/
    public static int HTTP_NO_AUTH = 401;
    /** HTTP未找到数据或接口 **/
    public static int HTTP_NOT_FOUND = 404;

    /** 启用状态 **/
    public static String STATUS_ENABLED = "1";
    /** 禁用状态 **/
    public static String STATUS_DISABLED = "1";
}
