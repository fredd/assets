package org.openapi.consts;

/**
 * 数据交换接口模式
 */
public class ExchangeMode {

    /**
     * 读，第三方接口从本系统读取数据
     */
    public static String READ = "R";

    /**
     * 写，第三方接口向本系统写入数据
     */
    public static String WRITE = "W";
}
