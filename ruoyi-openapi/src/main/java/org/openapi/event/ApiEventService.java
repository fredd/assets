package org.openapi.event;

import org.openapi.consts.EventType;
import org.openapi.event.EventBus;
import org.openapi.event.process.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * API业务相关事件
 */
@Service
public class ApiEventService {
    @Autowired
    private ModelProcess modelProcess;
    @Autowired
    private FieldProcess fieldProcess;
    @Autowired
    private EventProcess eventProcess;
    @Autowired
    private SqlProcess sqlProcess;
    @Autowired
    private ExchangeProcess exchangeProcess;

    public void initApiEvent(){
        EventBus.on("api_event", EventType.AU, eventProcess);
        EventBus.on("api_event", EventType.AI, eventProcess);
        EventBus.on("api_event", EventType.AD, eventProcess);

        EventBus.on("api_exchange", EventType.AU, exchangeProcess);
        EventBus.on("api_exchange", EventType.AI, exchangeProcess);
        EventBus.on("api_exchange", EventType.AD, exchangeProcess);

        EventBus.on("api_sql", EventType.AU, sqlProcess);
        EventBus.on("api_sql", EventType.AI, sqlProcess);
        EventBus.on("api_sql", EventType.AD, sqlProcess);

        EventBus.on("api_model", EventType.AU, modelProcess);
        EventBus.on("api_model", EventType.AI, modelProcess);

        EventBus.on("api_field", EventType.AU, fieldProcess);
        //EventBus.on("api_field", EventType.AI, fieldProcess);
        //EventBus.on("api_field", EventType.AD, fieldProcess);
    }
}
