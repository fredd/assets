export function subTreeIds(treeOptions, id){
    
    function getNodeValues(treeOptions){
        if(treeOptions == null){
            return '';
        }

        let ids = '';
        for(var i=0;i<treeOptions.length;i++){
            ids += "," + treeOptions[i].value;
            if(treeOptions[i].children){
                ids += getNodeValues(treeOptions[i].children);
            }
        }        
        return ids;
    }

    function getNode(treeOptions, id){
        let node = null;
        for(var i=0;i<treeOptions.length;i++){
            if(treeOptions[i].value == id){
                node = treeOptions[i];
            }else if(treeOptions[i].children){
                node = getNode(treeOptions[i].children, id);
            }else{
                node = null;
            }
            if(node != null){
                break;
            }
        }
        return node;
    }

    let tops = getNode(treeOptions, id);
    if(tops == null){
        return null;
    }

    return id + getNodeValues(tops.children);
}