export default {
  setData: function (dsData) {
    let option = set ? Object.assign(this.options, set) : { ...this.options };
    option.radar.indicator = [];
    option.series[0].data.value = [];
    for (var i = 0; i < dsData.length; i++) {
      option.radar.indicator.push({name:dsData[i]['name'],max:dsData[i]['max']})
      option.series[0].data.value.push({name:dsData[i]['name'],max:dsData[i]['value']})
    }
    return option;
  },

  options: {
    radar: {
      // shape: 'circle',
      indicator: [
        { name: 'Sales', max: 6500 },
        { name: 'Administration', max: 16000 },
        { name: 'Information Technology', max: 30000 },
        { name: 'Customer Support', max: 38000 },
        { name: 'Development', max: 52000 },
        { name: 'Marketing', max: 25000 }
      ]
    },
    series: [
      {
        type: 'radar',
        tooltip: {
          trigger: 'item'
        },
        areaStyle: {},
        data: [
          {
            value: [4200, 3000, 20000, 35000, 50000, 18000]
          }
        ]
      }
    ]
  }
}