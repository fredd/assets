import request from '@/utils/request'


// POST新增数据
export function apiPost(data) {
    return request({
        url: '/openapi/post',
        method: 'post',
        data: data
    })
}

// PUT更新数据
export function apiPut(data) {
    return request({
        url: '/openapi/put',
        method: 'post',
        data: data
    })
}

// PATCH新增或更新数据
export function apiPatch(data) {
    return request({
        url: '/openapi/patch',
        method: 'post',
        data: data
    })
}

// GET查询数据
export function apiGet(data) {
    return request({
        url: '/openapi/get',
        data: data,
        method: 'post'
    })
}

// DELETE删除请求
export function apiDelete(data) {
    return request({
        url: '/openapi/delete',
        data: data,
        method: 'post'
    })
}

// 导出生成excel
export function apiExport(data) {
    return request({
        url: '/openapi/export',
        method: 'post',
        data: data
    })
}

// 下载生成excel
export function apiDownload(fileName) {
    window.open(process.env.VUE_APP_BASE_API+'openapi/down/'+fileName)
}

//加载数据源
export function apiDs(dsCode, query) {
    return request({
        url: '/openapi/ds/'+dsCode,
        method: 'get',
        params: query
    })
}

// 加载核心缓存
export function loadCache(type) {
    return request({
        url: '/openapi/load/'+type,
        method: 'get'
    })
}

//以下是标准Restful请求
export function restPost(uri, data) {
    return request({
        url: uri,
        method: 'post',
        data: data
    })
}
export function restPut(uri,data) {
    return request({
        url: uri,
        method: 'put',
        data: data
    })
}
export function restPatch(uri,data) {
    return request({
        url: uri,
        method: 'patch',
        data: data
    })
}
export function restGet(uri,query) {
    return request({
        url: uri,
        method: 'get',
        params: query
    })
}
export function restDelete(data) {
    return request({
        url: uri,
        method: 'delete',
        params: query
    })
}